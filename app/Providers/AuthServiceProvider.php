<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\User;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        VerifyEmail::toMailUsing(function (User $user, string $verificationUrl) {
            if($user->tipo == 'operador'){
                return (new MailMessage)
                    ->subject('UKI: Confirmar correo electrónico')
                    ->line("{$user->negocio->nombre} acaba de crear una cuenta de operador con tu mail. Por favor pulsa el siguiente botón para confirmar tu correo electrónico.")
                    ->action('Confirmar Correo Electrónico', $verificationUrl)
                    ->line(Lang::get('If you did not create an account, no further action is required.'));
            } else {
                return (new MailMessage)
                    ->subject('UKI: Confirmar correo electrónico')
                    ->line('Por favor pulsa el siguiente botón para confirmar tu correo electrónico y continuar con el proceso de creación de cuenta en UKI. Recuerda que por seguridad debes hacer click en este botón en la misma computadora y navegador en el que registraste tu cuenta.')
                    ->action('Confirmar Correo Electrónico', $verificationUrl)
                    ->line(Lang::get('If you did not create an account, no further action is required.'));
            }
        });
    }
}
