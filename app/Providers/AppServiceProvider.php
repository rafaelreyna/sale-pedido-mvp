<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Melihovv\ShoppingCart\Repositories\ShoppingCartDatabaseRepository;
use Melihovv\ShoppingCart\Repositories\ShoppingCartRedisRepository;
use Melihovv\ShoppingCart\Repositories\ShoppingCartRepositoryInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            ShoppingCartRepositoryInterface::class,
            ShoppingCartDatabaseRepository::class
        );
    }
}
