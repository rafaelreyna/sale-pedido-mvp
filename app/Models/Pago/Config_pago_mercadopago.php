<?php

namespace App\Models\Pago;

use Illuminate\Database\Eloquent\Model;

class Config_pago_mercadopago extends Model
{
    protected $table = "config_pago_mercadopago";

    protected $guarded = ['created_at', 'updated_at'];

    public function negocio()
    {
        return $this->belongsTo('App\Models\Negocio', 'negocio_id');
    }
}
