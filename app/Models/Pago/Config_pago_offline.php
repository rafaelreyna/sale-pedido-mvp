<?php

namespace App\Models\Pago;

use Illuminate\Database\Eloquent\Model;

class Config_pago_offline extends Model
{
    protected $table = "config_pago_offline";

    protected $guarded = ['created_at', 'updated_at'];

    public function negocio()
    {
        return $this->belongsTo('App\Models\Negocio', 'negocio_id');
    }
}
