<?php

namespace App\Models;

use App\Models\Comunicacion\Sms;
use App\Models\Restaurantes\Config_restaurante;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use MercadoPago\Payment;
use MercadoPago\Preference;
use MercadoPago\SDK;

class Pedido extends Model
{
    // Lista de status de pedids
    const PEDIDO_STATUS_CONFIRMADO = "confirmado"; // El usuario completo su pedido en la plataforma, y no eliigio pago online
    const PEDIDO_STATUS_PAGADO = "pagado"; // El usuario completo su pedido en la plataforma con pago online y el pago se proceso correctamente
    const PEDIDO_STATUS_PREPARANDO = "preparando"; // El restaurante dio aceptar al pedido
    const PEDIDO_STATUS_EN_CAMINO = "en_camino"; // El restaruante dio click en enviar / notificar
    const PEDIDO_STATUS_CANCELADO = "cancelado"; // Pedido cancelado
    const PEDIDO_STATUS_POR_CONFIRMAR = "por_confirmar"; // Carrito creado sin completar el proceso
    const PEDIDO_STATUS_LISTO = "listo"; // <---- en desuso
    const PEDIDO_STATUS_TERMINADO = "terminado"; // Pedido completado y cerrado<---
    const PEDIDO_STATUS_ENTREGADO = "entregado"; // <--- en desuso
    const PEDIDO_STATUS_PAGO_RECHAZADO = "pago_rechazado"; // El pago online no se proceso correctamente
    const PEDIDO_STATUS_PROCESANDO_PAGO = "procesando_pago"; // El pago se esta procesando
    const PEDIDO_STATUS_POR_CALIFICAR = "por_calificar"; //el pedido ya se ennvio/se notifico y esa pendiente de calificar

    // Textos de pedidos
    const PEDIDO_STATUS_CONFIRMADO_TEXT = 'Por cobrar';
    const PEDIDO_STATUS_PAGADO_TEXT = 'Pagado';
    const PEDIDO_STATUS_PREPARANDO_TEXT = 'Preparando';
    const PEDIDO_STATUS_EN_CAMINO_TEXT = 'En camino';
    const PEDIDO_STATUS_CANCELADO_TEXT = 'Cancelado';
    const PEDIDO_STATUS_POR_CONFIRMAR_TEXT = 'por confirmar';
    const PEDIDO_STATUS_POR_CALIFICAR_text = "Por Calificar";

    //---- formas de pago
    const PEDIDO_PAGO_EFECTIVO = "efectivo";
    const PEDIDO_PAGO_MP = "mercadopago";
    const PEDIDO_PAGO_TERMINAL = "terminal";

    const PEDIDO_PAGO_EFECTIVO_TEXT = 'Efectivo';
    const PEDIDO_PAGO_MP_TEXT = 'Pago Online';
    const PEDIDO_PAGO_TERMINAL_TEXT_DOMICILIO = 'Envío Terminal';
    const PEDIDO_PAGO_TERMINAL_TEXT_PICKUP = 'Terminal en Tienda';

    //--- metodos de envio
    const PEDIDO_ENVIO_PICKUP = "pickup";
    const PEDIDO_ENVIO_FIJO = "fijo";
    const PEDIDO_ENVIO_DINAMICO = "dinamico";
    const PEDIDO_ENVIO_GRATIS = "gratis";

    const PEDIDO_ENVIO_PICKUP_TEXT = 'Pick up & Go';
    const PEDIDO_ENVIO_FIJO_TEXT = 'A domicilio';
    const PEDIDO_ENVIO_DINAMICO_TEXT = 'A domicilio';
    const PEDIDO_ENVIO_GRATIS_TEXT = 'A domicilio';


    protected $table = "pedidos";

    protected $guarded = ['created_at', 'updated_at'];

    public function negocio()
    {
        return $this->belongsTo('App\Models\Negocio', 'negocio_id');
    }

    public function items()
    {
        switch ($this->negocio->tipo) {
            case 'restaurante':
                return $this->hasMany('App\Models\Restaurantes\Item', 'pedido_id');
                break;
            default:
                return null;
        }
    }

    public function notificar()
    {
        //notificando negocios
        $this->notificarNegocioMail();
        $this->notificarNegocioSms();
        $this->notificarNegocioCall();

        //notificando usuario
        $this->notificarSms();
        $this->notificarMail();

        // notificando super admins solo si esta configurada la bandera en env
        $this->notificarMailSuperAdmin();
        $this->notificarSmsSuperAdmin();
    }

    private function notificarSms()
    {
        $sms = new Sms();
        $sms->to = $this->telefono;
        $sms->body = 'Puedes conocer el estado de tu pedido UKI®; en este enlace '
            . route('food.pedido.seguimiento', base64_encode($this->id));
        $sms->enviar();
    }

    private function notificarMail()
    {
        try {
            $pedido = $this;
            Mail::send('food.emails.nuevo_pedido', ['pedido' => $this], function ($m) use ($pedido) {
                if (env('APP_ENV') == 'staging') {
                    $m->from('pruebas@uki.mx', 'UKI Pruebas');
                    $m->to($pedido->email, "{$pedido->nombre}")->subject("UKI PRUEBAS - Nuevo Pedido");
                } else {
                    $m->from('notificaciones@uki.mx', 'UKI®');
                    $m->to($pedido->email, "{$pedido->nombre}")->subject('UKI® - Nuevo Pedido');
                }
            });
        } catch (\Exception $e) {
            Log::error(" XXX ERROR DE ENVIO MAIL: {$this->email} {$e->getMessage()} - {$e->getFile()} - {$e->getLine()}");
        }

    }

    private function notificarNegocioSms()
    {
        if ($this->negocio->config->notificacion_pedido_sms) {
            foreach ($this->negocio->config->notificacion_pedido_sms as $user_id) {
                //generando notificacion para el negocio
                $notificacion = new Notificacion();
                $notificacion->user_id = $user_id;
                $notificacion->url = route('admin');
                $notificacion->short_message = "Has recibido un nuevo pedido (#{$this->id}), accede a tu panel en: ";
                $notificacion->save();
                $notificacion->sendSms();
            }
        }
    }

    private function notificarNegocioCall()
    {
        if ($this->negocio->config->notificacion_pedido_call) {
            foreach ($this->negocio->config->notificacion_pedido_call as $user_id) {
                //generando notificacion para el negocio
                $notificacion = new Notificacion();
                $notificacion->user_id = $user_id;
                $notificacion->short_message = "Tienes un pedido en uqui. Atiéndelo...";
                $notificacion->save();
                $notificacion->sendCall();
            }
        }
    }

    private function notificarSmsSuperAdmin()
    {
        if (env('NOTIFICAR_PEDIDOS_SUPERADMIN', null) == 'sms') {
            foreach ( User::getSuperUsers() as $user) {
                //generando notificacion para el usuario
                $notificacion = new Notificacion();
                $notificacion->user_id = $user->id;
                $notificacion->url = route('admin');
                $notificacion->short_message = "Se ha generado un nuevo pedido (#{$this->id}) de{$this->negocio->nombre} , accede a tu panel en: ";
                $notificacion->save();
                $notificacion->sendSms();
            }
        }
    }

    private function notificarNegocioMail()
    {
        if ($this->negocio->config->notificacion_pedido_mail) {
            foreach ($this->negocio->config->notificacion_pedido_mail as $user_id) {
                //generando notificacion para el usuario
                $notificacion = new Notificacion();
                $notificacion->user_id = $user_id;
                $notificacion->url = route('admin');
                $notificacion->subject = "Has recibido un nuevo pedido (#{$this->id})";
                $notificacion->message = "<h3>¡Felicidades!</h3>";
                $notificacion->message .= "<p>Has recibido un nuevo pedido con número de pedido #{$this->id}</p>";
                $notificacion->message .= "<p>Atiéndelo ahora mismo</p>";
                $notificacion->call_to_action = "Accede al panel de administración";
                $notificacion->save();
                $notificacion->sendMail();
            }
        }
    }

    /**
     * Notificacion a superadmin
     */
    private function notificarMailSuperAdmin()
    {
        if (env('NOTIFICAR_PEDIDOS_SUPERADMIN', null) == 'mail') {
            foreach ( User::getSuperUsers() as $user) {
                //generando notificacion para el super admin
                $notificacion = new Notificacion();
                $notificacion->user_id = $user->id;
                $notificacion->url = route('admin');
                $notificacion->subject = "Se ha generado un pedido nuevo (#{$this->id})";
                $notificacion->message = "<h3>¡Atención!</h3>";
                $notificacion->message .= "<p>Se ha generado un nuevo pedido #{$this->id} del restaurante {$this->negocio->nombre}</p>";
                $notificacion->message .= "<p>Mantente al pendiente de este pedido para que sea atendido por el restaurante</p>";
                $notificacion->call_to_action = "Accede al panel de administración";
                $notificacion->save();
                $notificacion->sendMail();
            }
        }
    }

    public function notificarCancelacion($razon)
    {
        $sms = new Sms();
        $sms->to = $this->telefono;

        $pedido = $this;
        try {
            Mail::send('food.emails.cancelado', ['pedido' => $this, 'razon' => $razon], function ($m) use ($pedido) {
                if (env('APP_ENV') == 'staging') {
                    $m->from('pruebas@uki.mx', 'UKI Pruebas');
                    $m->to($pedido->email, "{$pedido->nombre}")->subject("UKI PRUEBAS - Pedido Rechazado");
                } else {
                    $m->from('notificaciones@uki.mx', 'UKI®');
                    $m->to($pedido->email, "{$pedido->nombre}")->subject('UKI® - Pedido Rechazado');
                }
            });
        } catch (\Exception $e) {
            Log::error(" XXX ERROR DE ENVIO MAIL: {$this->user->email} {$e->getMessage()} - {$e->getFile()} - {$e->getLine()}");
        }


        if ($this->forma_pago == \App\Models\Pedido::PEDIDO_PAGO_MP) {
            $sms->body = "Lo sentimos, {$this->negocio->nombre} no puede atender tu pedido, tu pago será reembolsado, busca más información en tu email de contacto. Encuentra una alternativa deliciosa en " . route('food.categoria.show',
                    $this->negocio->categorias[0]->id);
        } else {
            $sms->body = "Lo sentimos, {$this->negocio->nombre} no puede atender tu pedido, busca más información en tu correo de contacto. Encuentra una alternativa deliciosa en " . route('food.categoria.show',
                    $this->negocio->categorias[0]->id);
        }
        $sms->enviar();
    }

    public function notificarTiempoEnvio()
    {
        $sms = new Sms();
        $sms->to = $this->telefono;

        if ($this->envio == \App\Models\Pedido::PEDIDO_ENVIO_PICKUP) {
            $sms->body = "¡Tu pedido UKI® está listo! puedes pasar a recogerlo a {$this->negocio->nombre} a partir de este momento";
        } else {
            $hora_entrega_aprox = date('g:i A', strtotime(date('Y-m-d H:i:s') . "+ {$this->tiempo_envio} seconds"));
            $sms->body = '¡Tu pedido UKI® está en camino! estárá en tu puerta aproximadamente a las ' . $hora_entrega_aprox;
        }

        $sms->enviar();
    }

    public function notificarCalificar()
    {
        $hora_calificar = date('H:i', strtotime($this->updated_at . ' + 3 hours'));
        $this->status = Pedido::PEDIDO_STATUS_POR_CALIFICAR;
        $this->save();
        $sms = new Sms();
        $sms->to = $this->telefono;
        $sms->body = "Para UKI® tu experiencia es lo más importante, tu evaluación impacta en el prestigio del restaurante. Tienes hasta las {$hora_calificar} para evaluar: "
            . route('food.pedido.calificar', base64_encode($this->id));
        $sms->enviar();
    }

    public function calificar(Calificacion $calificacion)
    {
        if ($this->status != Pedido::PEDIDO_STATUS_CANCELADO) {
            $this->status = \App\Models\Pedido::PEDIDO_STATUS_TERMINADO;
        }
        $this->save();
        $calificacion->pedido_id = $this->id;
        $calificacion->negocio_id = $this->negocio_id;
        $calificacion->save();
        $this->negocio->calcular_evaluacion();
    }

    public function cancelar_pago()
    {
        switch ($this->forma_pago) {
            case \App\Models\Pedido::PEDIDO_PAGO_MP:
                $this->cancelar_pago_mercadopago();
                break;
        }
    }

    private function cancelar_pago_mercadopago()
    {
        try {
            SDK::setAccessToken($this->negocio->config_mercadopago->access_token);
            $payment = Payment::find_by_id($this->transaccion_id);
            $payment->refund();
            $this->transaccion_payload .= json_encode($payment);
            Log::info('response json: ' . json_encode($payment));
        } catch (\Exception $exception) {
            Log::error('Error MP refund: ' . $exception->getMessage() . ' ' . $exception->getFile() . ' ' . $exception->getLine());
            $notificacion = new Notificacion();
            $notificacion->user_id = $this->negocio->admin()->id;
            $notificacion->url = 'https://www.mercadopago.com.mx/';
            $notificacion->subject = "Falla en cancelación MercadoPago";
            $notificacion->message = "<h3>Falla en cancelación</h3>";
            $notificacion->message .= "<p>El Pedido {$this->id} con email de identificación {$this->email} se ha cancelado correctamente pero ha ocurrido un error con MercadoPago.</p>";
            $notificacion->message .= "<p>Es posible que la devolución automática no haya funcionado, ingresa a MercadoPago y verifica, si es necesario puedes realizar la devolución manual desde tu Panel de MercadoPago.</p>";
            $notificacion->message .= "<p>No es necesario que hagas nada más dentro de la plataforma de UKI.</p>";
            $notificacion->call_to_action = "Revisar en mi cuenta de Mercado Pago";
            $notificacion->save();
            $notificacion->sendMail();
        }

    }


    public function formaPagoText()
    {
        switch ($this->forma_pago) {
            case self::PEDIDO_PAGO_EFECTIVO:
                return self::PEDIDO_PAGO_EFECTIVO_TEXT;
            case self::PEDIDO_PAGO_MP:
                return self::PEDIDO_PAGO_MP_TEXT;
            case self::PEDIDO_PAGO_TERMINAL:
                if ($this->isEnvioDomicilio()) {
                    return self::PEDIDO_PAGO_TERMINAL_TEXT_DOMICILIO;
                } else {
                    return self::PEDIDO_PAGO_TERMINAL_TEXT_PICKUP;
                }
        }
        return $this->forma_pago;
    }

    public function envioText()
    {
        switch ($this->envio) {
            case self::PEDIDO_ENVIO_PICKUP:
                return self::PEDIDO_ENVIO_PICKUP_TEXT;
            case self::PEDIDO_ENVIO_GRATIS:
                return self::PEDIDO_ENVIO_GRATIS_TEXT;
            case self::PEDIDO_ENVIO_FIJO:
                return self::PEDIDO_ENVIO_FIJO_TEXT;
            case self::PEDIDO_ENVIO_DINAMICO:
                return self::PEDIDO_ENVIO_DINAMICO_TEXT;
        }
        return $this->forma_pago;
    }

    public function isEnvioDomicilio()
    {
        if ($this->envio == 'fijo' || $this->envio == 'dinamico') {
            return true;
        } else {
            return false;
        }
    }

    public static function analytics_superadmin()
    {
        $analytics = Pedido::whereNotIn('status',
            [
                \App\Models\Pedido::PEDIDO_STATUS_POR_CONFIRMAR
            ]
        )
            ->orderBy('created_at', 'DESC')
            ->get();
        $analytics->load('negocio');
        return $analytics;
    }
}
