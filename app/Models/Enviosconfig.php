<?php

namespace App\Models;

use App\Helpers\GeopositionHelper;
use Illuminate\Database\Eloquent\Model;

class Enviosconfig extends Model
{
    protected $table = "config_envios";

    protected $guarded = ['created_at', 'updated_at'];

    public function negocio()
    {
        return $this->belongsTo('App\Models\Negocio', 'negocio_id');
    }

    /**
     * @return bool
     */
    public function tieneEnvioDomicilio()
    {
      if ($this->tipo_envio == 'dinamico' || $this->tipo_envio == 'fijo') {
          return true;
      } else {
          return false;
      }
    }

    public function tienePickup()
    {
        if ($this->pickup) {
            return true;
        } else {
            return false;
        }
    }

    public function puedeEnviarEstaDistancia(){
        if (isset($this->distanciaAlcance) && $this->distanciaAlcance <= $this->radio_max) {
            return true;
        } else {
            return false;
        }
    }

    public function getDistanciaAlcance($lat, $lon){
        if ($lat && $lon) {
            $this->distanciaAlcance = GeopositionHelper::distance(
                $lat,
                $lon,
                $this->centro_lat ?? $this->negocio->lat,
                $this->centro_lon ?? $this->negocio->lon
            );
        } else {
            $this->distanciaAlcance = null;
        }
        return $this->distanciaAlcance;
    }
}
