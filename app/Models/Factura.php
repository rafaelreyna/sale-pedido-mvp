<?php

namespace App\Models;

use App\Helpers\DateHelper;
use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    const FACTURA_STATUS_PENDIENTE = "pendiente";
    const FACTURA_STATUS_PAGADA = "pagada";
    const FACTURA_STATUS_VENCIDA = "vencida";

    const FACTURA_STATUS_PENDIENTE_TEXT = "Pendiente";
    const FACTURA_STATUS_PAGADA_TEXT = "Pagado";
    const FACTURA_STATUS_VENCIDA_TEXT = "Vencido";

    protected $table = "facturas";

    protected $guarded = ['created_at', 'updated_at'];

    public function negocio()
    {
        return $this->belongsTo('App\Models\Negocio', 'negocio_id');
    }

    public function enviar_email(){
        //todo generar funcionalidad para enviar una notificacion y enviar la factura via email
    }

    public function transacciones(){
        $transacciones = $this->negocio->pedidos_por_mes($this->mes, $this->ano);
        return $transacciones;
    }

    public function enviar()
    {
        $notificacion = new Notificacion();
        $notificacion->user_id = $this->negocio->admin()->id;
        $notificacion->url = route('admin');
        $notificacion->subject = "UKI® - Notificación Estado de cuenta {$this->mes}/{$this->ano}";
        $notificacion->message = "<h3>Notificación Estado de cuenta</h3>";
        $notificacion->message .= "<p>Te informamos que tu estado de cuenta del mes de " .DateHelper::mes_letra($this->mes). " {$this->ano} ya esta disponible y puedes consultarlo en tu panel de administración.</p>";
        $notificacion->message .= "<p>Te recordamos que tienes hasta el día 5 de cada mes para realizar tu pago y evitar que se suspenda tu servicio.</p>";
        $notificacion->message .= "<p>Gracias por utilizar UKI&reg;</p>";
        $notificacion->call_to_action = "Acceder a panel de adminsitración";
        $notificacion->save();
        $notificacion->sendMail();
    }
}
