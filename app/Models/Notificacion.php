<?php

namespace App\Models;

use App\Models\Comunicacion\Sms;
use App\Models\Comunicacion\VoiceCall;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class Notificacion extends Model
{
    protected $table = "notificaciones";

    protected $guarded = [];

    // Relationships
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function sendMail()
    {
        try {
            $notificacion = $this;

            $email_template = 'admin.emails.notificacion';

            Mail::send($email_template, ['notificacion' => $notificacion], function ($m) use ($notificacion) {
                $m->from(
                    env('NOTIFICACIONES_FROM_EMAIL', 'notificaciones@uki.mx'),
                    env('NOTIFICACIONES_FROM', 'Notificaciones UKI')
                );
                $m->to($notificacion->user->email,
                    "{$notificacion->user->name}")
                    ->subject(
                        env('NOTIFICACIONES_SUBJECT_PREFIX', 'UKI® - ') . $notificacion->subject
                    );
            });
        } catch (\Exception $e) {
            Log::error(" XXX ERROR DE ENVIO MAIL: {$this->user->email} {$e->getMessage()} - {$e->getFile()} - {$e->getLine()}");
        }
    }

    /**
     * Funcion para enviar una notificacion via sms
     * @return array|mixed
     */
    public function sendSms()
    {
        $sms = new Sms();
        $sms->to = $this->user->celular;
        //todo utilizar un url shortener para la url
        $sms->body = env('NOTIFICACIONES_SUBJECT_PREFIX', 'UKI® - ')  . $this->short_message . ' ' . $this->url;
        return $sms->enviar();
    }

    /**
     * Funcion para enviar una notificacion via sms
     * @return array|mixed
     */
    public function sendCall()
    {
        $call = new VoiceCall($this->user->celular);
        $call->message = $this->short_message;
        $call->notificacion_id = $this->id;
        return $call->call();
    }

}
