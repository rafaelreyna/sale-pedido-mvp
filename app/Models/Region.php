<?php

namespace App\Models;

use App\Helpers\GeopositionHelper;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = "regiones";

    protected $guarded = ['created_at', 'updated_at'];

    public function negocios()
    {
        return $this->hasMany(Negocio::class, 'region_id');
    }

    /**
     * @return mixed
     */
    public static function regionesActivas()
    {
        return self::withCount('negocios')
            ->having('negocios_count', '>', 0)
            ->get();
    }

    /**
     * @param $lat
     * @param $lon
     * @return float|void
     */
    public function getDistance($lat, $lon)
    {
        return GeopositionHelper::distance($this->centro_lat, $this->centro_lon, $lat, $lon);
    }
}
