<?php

namespace App\Models;

use App\Models\Comunicacion\Sms;
use App\Models\Restaurantes\Config_restaurante;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Mail;

class Calificacion extends Model
{
    protected $table = "calificaciones";

    protected $guarded = ['created_at', 'updated_at'];

    public function negocio()
    {
        return $this->belongsTo('App\Models\Negocio', 'negocio_id');
    }

    public function pedido()
    {
        return $this->belongsTo('App\Models\Pedido', 'pedido_id');
    }

    public function usaurio()
    {
        return $this->belongsTo('App\User', 'usaurio_id');
    }
}
