<?php

namespace App\Models\Food;

use App\Helpers\GeopositionHelper;
use App\Models\Region;
use Illuminate\Http\Request;

class Perfil
{
    /**
     * @var string
     */
    public $nombre;
    /**
     * @var string
     */
    public $region_id;
    /**
     * @var null
     */
    public $region;
    /**
     * @var string
     */
    public $apellidos;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $telefono;
    /**
     * @var string
     */
    public $direccion;
    /**
     * @var string
     */
    public $lat;
    /**
     * @var string
     */
    public $lon;
    /**
     * @var string
     */
    public $referencias;

    const MAX_KM_REGION = 30;
    const MAX_KM_NEGOCIO = 30;

    public static function getPerfil(Request $request)
    {
        $perfilCookie = $request->cookie('uki_perfil');
        $perfil = new Perfil();
        if(!$perfilCookie){
            $perfil->region = null;
            $perfil->nombre = '';
            $perfil->apellidos = '';
            $perfil->email = '';
            $perfil->telefono = '';
            $perfil->direccion = '';
            $perfil->lat = '';
            $perfil->lon = '';
            $perfil->referencias = '';
        } else {
            $perfilData = json_decode($perfilCookie);
            $perfil->nombre = $perfilData->nombre ?? '';
            $perfil->region_id = $perfilData->region_id ?? '';
            $perfil->region = $perfilData->region ?? null;
            $perfil->apellidos = $perfilData->apellidos ?? '';
            $perfil->email = $perfilData->email ?? '';
            $perfil->telefono = $perfilData->telefono ?? '';
            $perfil->direccion = $perfilData->direccion ?? '';
            $perfil->lat = $perfilData->lat ?? '';
            $perfil->lon = $perfilData->lon ?? '';
            $perfil->referencias = $perfilData->referencias ?? '';
        }

        return $perfil;
    }

    public function selectRegion() {
        $regions = Region::regionesActivas();
        $region = $regions->first();
        $region_distance = $region->getDistance($this->lat, $this->lon);

        foreach($regions as $region_select) {
            $new_distance = $region_select->getDistance($this->lat, $this->lon);
            if ($new_distance < $region_distance) {
                $region = $region_select;
                $region_distance = $new_distance;
            }
        }

        $this->region = $region;
        $this->region_id = $this->region->id;
    }

    public function selectAddress() {
        $params = "latlng={$this->lat},{$this->lon}&key=" . env('GOOGLE_MAPS_KEY', 'AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY');
        $direccionResponse = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?{$params}");
        $direccionResponse = json_decode($direccionResponse);
        $direccion = $direccionResponse->results[0]->formatted_address ?? '';
        $this->direccion = $direccion;
    }

    public function isFarFromRegion()
    {
        $region = Region::find($this->region_id);
        $distance = $region->getDistance($this->lat, $this->lon);
        return $distance >= self::MAX_KM_REGION;
    }

    public function isFarFromNegocio($negocio)
    {
        $distance = GeopositionHelper::distance($this->lat, $this->lon, $negocio->lat, $negocio->lon);
        return $distance >= self::MAX_KM_NEGOCIO;
    }
}
