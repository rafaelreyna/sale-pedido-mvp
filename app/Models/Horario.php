<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    protected $table = "horarios";

    protected $guarded = ['created_at', 'updated_at'];

    public function negocio()
    {
        return $this->belongsTo('App\Models\Negocio', 'negocio_id');
    }
}
