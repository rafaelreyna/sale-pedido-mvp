<?php

namespace App\Models;

use App\Helpers\GeopositionHelper;
use App\Helpers\ObjectHelper;
use App\Models\Restaurantes\Config_restaurante;
use App\Models\Restaurantes\Item;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Negocio extends Model
{
    const DESCUENTOS_TRANSACCIONES = [
        /*
        [
            'min' => 0,
            'max' => 49,
            'descuento' => 0.20,
            'nombre' => 'Menos de 50 transacciones'
        ]
        */
    ];

    use SoftDeletes;

    protected $table = "negocios";

    protected $guarded = ['created_at', 'updated_at', 'deleted_at'];

    public function config()
    {
        switch ($this->tipo) {
            case 'restaurante':
                $config = Config_restaurante::where('negocio_id', '=', $this->id)->first();
                if (!$config) {
                    $config = Config_restaurante::create(['negocio_id' => $this->id]);
                }
                return $this->hasOne('App\Models\Restaurantes\Config_restaurante', 'negocio_id');
                break;
            default:
                return null;
        }
    }

    public function enviosconfig()
    {
        return $this->hasOne('App\Models\Enviosconfig', 'negocio_id');
    }

    public function config_mercadopago()
    {
        return $this->hasOne('App\Models\Pago\Config_pago_mercadopago', 'negocio_id');
    }

    public function config_offline()
    {
        return $this->hasOne('App\Models\Pago\Config_pago_offline', 'negocio_id');
    }

    public function horarios()
    {
        return $this->hasMany('App\Models\Horario', 'negocio_id');
    }

    public function pedidos()
    {
        return $this->hasMany('App\Models\Pedido', 'negocio_id');
    }

    public function pedidos_procesar()
    {
        return $this->hasMany('App\Models\Pedido', 'negocio_id')->whereIn('status', [
            \App\Models\Pedido::PEDIDO_STATUS_CONFIRMADO, \App\Models\Pedido::PEDIDO_STATUS_PAGADO,
            \App\Models\Pedido::PEDIDO_STATUS_PREPARANDO
        ])->orderBy('created_at', 'ASC');
    }

    public function pedidos_atendidos()
    {
        return $this->hasMany('App\Models\Pedido', 'negocio_id')
            ->whereIn('status', [\App\Models\Pedido::PEDIDO_STATUS_EN_CAMINO])
            ->orWhere(function ($query) {
                $query->whereIn('status', [\App\Models\Pedido::PEDIDO_STATUS_CANCELADO]);
                $query->where('updated_at', '>', date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').'- 3 hours')));
            })->orderBy('updated_at', 'DESC');;
    }

    public function pedidos_nuevos()
    {
        return $this->hasMany('App\Models\Pedido', 'negocio_id')->whereIn('status',
            [\App\Models\Pedido::PEDIDO_STATUS_CONFIRMADO, \App\Models\Pedido::PEDIDO_STATUS_PAGADO]);
    }

    public function categorias()
    {
        switch ($this->tipo) {
            case 'restaurante':
                return $this->belongsToMany('App\Models\Restaurantes\Restaurantecategoria',
                    'negocios_restaurantecategorias', 'negocio_id', 'categoria_id');
                break;
            default:
                return null;
        }
    }

    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id');
    }


    /**
     * Relaciones de Restaurantess
     */


    public function platillo_categorias()
    {
        return $this->hasMany('App\Models\Restaurantes\Platillocategoria', 'negocio_id')->orderBy('orden');
    }

    public function platillos()
    {
        return $this->hasMany('App\Models\Restaurantes\Platillo', 'negocio_id')->orderBy('orden');
    }

    public function restaurante_categorias()
    {
        return $this->belongsToMany('App\Models\Restaurantes\Restaurantecategoria', 'negocios_restaurantecategorias',
            'negocio_id', 'categoria_id');
    }

    public function generar_factura($mes, $ano)
    {
        $factura = new Factura();
        $factura->negocio_id = $this->id;
        $factura->mes = $mes;
        $factura->ano = $ano;
        $fecha_actual = date("Y-m-d");
        //la factura vence en 5 dias
        $factura->vencimiento = date("Y-m-d", strtotime($fecha_actual."+ ".env('FACTURA_DIAS_VENCIMIENTO', 5)." days"));
        $factura->cargo_membresia = env('COSTO_MEMBRESIA', 750);
        $factura->descuento_membresia = 750;
        $factura->concepto_descuento_membresia = 'PROMO APERTURA';
        $factura->costo_transaccion = env('COSTO_TRANSACCION', 12.93);
        $factura->numero_transacciones = count($this->pedidos_por_mes($mes, $ano));
        $factura->cargo_transacciones = $factura->numero_transacciones * $factura->costo_transaccion;
        $factura->descuento_transacciones = 0;
        $factura->concepto_descuento_transacciones = null;

        foreach (self::DESCUENTOS_TRANSACCIONES as $descuento) {
            if ($descuento['min'] <= $factura->numero_transacciones && $factura->numero_transacciones <= $descuento['max']) {
                $factura->descuento_transacciones = $factura->cargo_transacciones * $descuento['descuento'];
                $factura->concepto_descuento_transacciones = $descuento['nombre'];
            }
        }

        $factura->subtotal = $factura->cargo_membresia - $factura->descuento_membresia
            + $factura->cargo_transacciones - $factura->descuento_transacciones;

        $factura->iva = $factura->subtotal * env('IVA', 0.16);
        $factura->total = $factura->subtotal + $factura->iva;
        $factura->status = 'pendiente';
        $factura->save();
        return $factura;
    }

    public function pedidos_por_mes($mes, $ano)
    {
        return Pedido::where('negocio_id', '=', $this->id)
            ->whereMonth('created_at', '=', $mes)
            ->whereYear('created_at', '=', $ano)
            ->whereIn('status', [
                \App\Models\Pedido::PEDIDO_STATUS_TERMINADO,
                Pedido::PEDIDO_STATUS_CANCELADO
            ])
            ->get();
    }

    public function facturas()
    {
        return $this->hasMany('App\Models\Factura', 'negocio_id');
    }

    public function admin()
    {
        $admin = User::where('negocio_id', '=', $this->id)
            ->where('tipo', 'admin')
            ->first();

        if (!$admin) {
            $sucursal = Sucursal::where('negocio_id', '=', $this->id)->first();
            if ($sucursal) {
                $admin = $sucursal->user;
            } else {
                Log::info("---- XXXX negocio sin User - Negocio ID{$this->id}");
            }
        }

        return $admin;
    }

    public function operadores()
    {
        return $this->hasMany(User::class, 'negocio_id')->where('tipo', '=','operador');
    }

    public function admins()
    {
        return $this->hasMany(User::class, 'negocio_id')->where('tipo', '=','admin');
    }

    public function usuarios()
    {
        return $this->hasMany(User::class, 'negocio_id');
    }

    public function calificaciones()
    {
        return $this->hasMany('App\Models\Calificacion', 'negocio_id');
    }

    public function calcular_evaluacion()
    {
        $promedio = $this->calificaciones()->average('calificacion');
        $this->calificacion = $promedio;
        $this->save();
    }


    /**
     *
     * Resumenes para dashboard
     */
    public function resumen_ventas()
    {
        $ventas_actual = Pedido::where('negocio_id', $this->id)
            ->whereIn('status', [\App\Models\Pedido::PEDIDO_STATUS_PAGADO, \App\Models\Pedido::PEDIDO_STATUS_TERMINADO])
            ->whereMonth('created_at', '=', date('m'))
            ->whereYear('created_at', '=', date('Y'))
            ->sum('total');

        $ventas_pasado = Pedido::where('negocio_id', $this->id)
            ->whereIn('status', [\App\Models\Pedido::PEDIDO_STATUS_PAGADO, \App\Models\Pedido::PEDIDO_STATUS_TERMINADO])
            //->whereDay('created_at', '<=', date('d'))
            ->whereMonth('created_at', '=', date('m', strtotime("first day of previous month")))
            ->whereYear('created_at', '=', date('Y', strtotime("first day of previous month")))
            ->sum('total');

        if ($ventas_pasado > 0) {
            return [
                'actual' => $ventas_actual,
                'pasado' => $ventas_pasado,
                'porcentaje' => (($ventas_actual - $ventas_pasado) / ($ventas_pasado)) * 100
            ];
        } else {
            return [
                'actual' => $ventas_actual,
                'pasado' => $ventas_pasado
            ];
        }

    }

    public function resumen_pedidos()
    {
        $pedidos_actual = Pedido::where('negocio_id', $this->id)
            ->whereIn('status', [
                \App\Models\Pedido::PEDIDO_STATUS_CONFIRMADO, \App\Models\Pedido::PEDIDO_STATUS_PAGADO,
                \App\Models\Pedido::PEDIDO_STATUS_TERMINADO
            ])
            ->whereMonth('created_at', '=', date('m'))
            ->whereYear('created_at', '=', date('Y'))
            ->count();

        $pedidos_pasado = Pedido::where('negocio_id', $this->id)
            ->whereIn('status', [
                \App\Models\Pedido::PEDIDO_STATUS_CONFIRMADO, \App\Models\Pedido::PEDIDO_STATUS_PAGADO,
                \App\Models\Pedido::PEDIDO_STATUS_TERMINADO
            ])
            //->whereDay('created_at', '<=', date('d'))
            ->whereMonth('created_at', '=', date('m', strtotime("first day of previous month")))
            ->whereYear('created_at', '=', date('Y', strtotime("first day of previous month")))
            ->count();

        if ($pedidos_pasado > 0) {
            return [
                'actual' => $pedidos_actual,
                'pasado' => $pedidos_pasado,
                'porcentaje' => (($pedidos_actual - $pedidos_pasado) / ($pedidos_pasado)) * 100
            ];
        } else {
            return [
                'actual' => $pedidos_actual,
                'pasado' => $pedidos_pasado
            ];
        }
    }

    public function resumen_pedidos_c_cancelados()
    {
        $pedidos_actual = Pedido::where('negocio_id', $this->id)
            ->whereIn('status', [
                \App\Models\Pedido::PEDIDO_STATUS_CONFIRMADO, \App\Models\Pedido::PEDIDO_STATUS_PAGADO,
                \App\Models\Pedido::PEDIDO_STATUS_TERMINADO, Pedido::PEDIDO_STATUS_CANCELADO
            ])
            ->whereMonth('created_at', '=', date('m'))
            ->whereYear('created_at', '=', date('Y'))
            ->count();

        $pedidos_pasado = Pedido::where('negocio_id', $this->id)
            ->whereIn('status', [
                \App\Models\Pedido::PEDIDO_STATUS_CONFIRMADO, \App\Models\Pedido::PEDIDO_STATUS_PAGADO,
                \App\Models\Pedido::PEDIDO_STATUS_TERMINADO, Pedido::PEDIDO_STATUS_CANCELADO
            ])
            //->whereDay('created_at', '<=', date('d'))
            ->whereMonth('created_at', '=', date('m', strtotime("first day of previous month")))
            ->whereYear('created_at', '=', date('Y', strtotime("first day of previous month")))
            ->count();

        if ($pedidos_pasado > 0) {
            return [
                'actual' => $pedidos_actual,
                'pasado' => $pedidos_pasado,
                'porcentaje' => (($pedidos_actual - $pedidos_pasado) / ($pedidos_pasado)) * 100
            ];
        } else {
            return [
                'actual' => $pedidos_actual,
                'pasado' => $pedidos_pasado
            ];
        }
    }

    public function resumen_ticket_promedio()
    {
        $ticket_promedio_actual = Pedido::where('negocio_id', $this->id)
            ->whereIn('status', [
                \App\Models\Pedido::PEDIDO_STATUS_CONFIRMADO, \App\Models\Pedido::PEDIDO_STATUS_PAGADO,
                \App\Models\Pedido::PEDIDO_STATUS_TERMINADO
            ])
            ->whereMonth('created_at', '=', date('m'))
            ->whereYear('created_at', '=', date('Y'))
            ->avg('total');

        $ticket_promedio_pasado = Pedido::where('negocio_id', $this->id)
            ->whereIn('status', [
                \App\Models\Pedido::PEDIDO_STATUS_CONFIRMADO, \App\Models\Pedido::PEDIDO_STATUS_PAGADO,
                \App\Models\Pedido::PEDIDO_STATUS_TERMINADO
            ])
            //->whereDay('created_at', '<=', date('d'))
            ->whereMonth('created_at', '=', date('m', strtotime("first day of previous month")))
            ->whereYear('created_at', '=', date('Y', strtotime("first day of previous month")))
            ->avg('total');

        if ($ticket_promedio_pasado > 0) {
            return [
                'actual' => $ticket_promedio_actual,
                'pasado' => $ticket_promedio_pasado,
                'porcentaje' => (($ticket_promedio_actual - $ticket_promedio_pasado) / ($ticket_promedio_pasado)) * 100
            ];
        } else {
            return [
                'actual' => $ticket_promedio_actual,
                'pasado' => $ticket_promedio_pasado
            ];
        }
    }

    public function resumen_comisiones_uki()
    {
        $comisiones_uki_actual = $this->resumen_pedidos_c_cancelados()['actual'] * (env('COSTO_TRANSACCION',
                    12.93) * 1.16);
        $comisiones_uki_pasado = $this->resumen_pedidos_c_cancelados()['pasado'] * (env('COSTO_TRANSACCION',
                    12.93) * 1.16);

        if ($comisiones_uki_pasado > 0) {
            return [
                'actual' => $comisiones_uki_actual,
                'pasado' => $comisiones_uki_pasado,
                'porcentaje' => (($comisiones_uki_actual - $comisiones_uki_pasado) / ($comisiones_uki_pasado)) * 100
            ];
        } else {
            return [
                'actual' => $comisiones_uki_actual,
                'pasado' => $comisiones_uki_pasado
            ];
        }
    }

    public function resumen_ganancia()
    {
        $ganancia_actual = $this->resumen_ventas()['actual'] - $this->resumen_comisiones_uki()['actual'];
        $ganancia_pasado = $this->resumen_ventas()['pasado'] - $this->resumen_comisiones_uki()['pasado'];

        if ($ganancia_pasado > 0) {
            return [
                'actual' => $ganancia_actual,
                'pasado' => $ganancia_pasado,
                'porcentaje' => (($ganancia_actual - $ganancia_pasado) / ($ganancia_pasado)) * 100
            ];
        } else {
            return [
                'actual' => $ganancia_actual,
                'pasado' => $ganancia_pasado
            ];
        }
    }

    public function resumen_porcentaje_ganancia()
    {
        if ($this->resumen_ventas()['actual'] > 0) {
            $porcentaje_ganancia_actual = (($this->resumen_ganancia()['actual'] * 100) / ($this->resumen_ventas()['actual']));
        } else {
            $porcentaje_ganancia_actual = 0;
        }
        if ($this->resumen_ventas()['pasado'] > 0) {
            $porcentaje_ganancia_pasado = (($this->resumen_ganancia()['pasado'] * 100) / ($this->resumen_ventas()['pasado']));
        } else {
            $porcentaje_ganancia_pasado = 0;
        }


        if ($porcentaje_ganancia_pasado > 0) {
            return [
                'actual' => $porcentaje_ganancia_actual,
                'pasado' => $porcentaje_ganancia_pasado,
                'porcentaje' => (($porcentaje_ganancia_actual - $porcentaje_ganancia_pasado) / ($porcentaje_ganancia_pasado)) * 100
            ];
        } else {
            return [
                'actual' => $porcentaje_ganancia_actual,
                'pasado' => $porcentaje_ganancia_pasado
            ];
        }
    }

    public function resumen_ingresos_envios()
    {
        $ingresos_envios_actual = Pedido::where('negocio_id', $this->id)
            ->whereIn('status', [\App\Models\Pedido::PEDIDO_STATUS_PAGADO, \App\Models\Pedido::PEDIDO_STATUS_TERMINADO])
            ->whereMonth('created_at', '=', date('m'))
            ->whereYear('created_at', '=', date('Y'))
            ->sum('costo_envio');

        $ingresos_envios_pasado = Pedido::where('negocio_id', $this->id)
            ->whereIn('status', [\App\Models\Pedido::PEDIDO_STATUS_PAGADO, \App\Models\Pedido::PEDIDO_STATUS_TERMINADO])
            //->whereDay('created_at', '<=', date('d'))
            ->whereMonth('created_at', '=', date('m', strtotime("first day of previous month")))
            ->whereYear('created_at', '=', date('Y', strtotime("first day of previous month")))
            ->sum('costo_envio');

        if ($ingresos_envios_pasado > 0) {
            return [
                'actual' => $ingresos_envios_actual,
                'pasado' => $ingresos_envios_pasado,
                'porcentaje' => (($ingresos_envios_actual - $ingresos_envios_pasado) / ($ingresos_envios_pasado)) * 100
            ];
        } else {
            return [
                'actual' => $ingresos_envios_actual,
                'pasado' => $ingresos_envios_pasado
            ];
        }
    }

    public function resumen_pedidos_pickup()
    {
        $pedidos_actual = Pedido::where('negocio_id', $this->id)
            ->whereIn('status', [\App\Models\Pedido::PEDIDO_STATUS_PAGADO, \App\Models\Pedido::PEDIDO_STATUS_TERMINADO])
            ->where('envio', \App\Models\Pedido::PEDIDO_ENVIO_PICKUP)
            ->whereMonth('created_at', '=', date('m'))
            ->whereYear('created_at', '=', date('Y'))
            ->count();

        $pedidos_pasado = Pedido::where('negocio_id', $this->id)
            ->whereIn('status', [\App\Models\Pedido::PEDIDO_STATUS_PAGADO, \App\Models\Pedido::PEDIDO_STATUS_TERMINADO])
            ->where('envio', \App\Models\Pedido::PEDIDO_ENVIO_PICKUP)
            //->whereDay('created_at', '<=', date('d'))
            ->whereMonth('created_at', '=', date('m', strtotime("first day of previous month")))
            ->whereYear('created_at', '=', date('Y', strtotime("first day of previous month")))
            ->count();

        if ($pedidos_pasado > 0) {
            return [
                'actual' => $pedidos_actual,
                'pasado' => $pedidos_pasado,
                'porcentaje' => (($pedidos_actual - $pedidos_pasado) / ($pedidos_pasado)) * 100
            ];
        } else {
            return [
                'actual' => $pedidos_actual,
                'pasado' => $pedidos_pasado
            ];
        }
    }

    public function mas_pedidos()
    {
        $mas_pedidos = DB::table('restaurante_pedido_items')
            ->selectRaw('platillo, SUM(cantidad) as count, costo_unitario')
            ->join('pedidos', 'pedidos.id', '=', 'restaurante_pedido_items.pedido_id')
            ->where('negocio_id', $this->id)
            ->whereIn('status', [
                \App\Models\Pedido::PEDIDO_STATUS_CONFIRMADO, \App\Models\Pedido::PEDIDO_STATUS_PAGADO,
                \App\Models\Pedido::PEDIDO_STATUS_TERMINADO
            ])
            ->whereMonth('pedidos.created_at', '=', date('m'))
            ->whereYear('pedidos.created_at', '=', date('Y'))
            ->groupBy(['platillo', 'costo_unitario'])
            ->orderBy('count', 'DESC')
            ->limit(4)
            ->get();

        return $mas_pedidos;
    }

    public function analytics()
    {
        $analytics = DB::table('restaurante_pedido_items')
            ->join('pedidos', 'pedidos.id', '=', 'restaurante_pedido_items.pedido_id')
            ->where('negocio_id', $this->id)
            ->whereIn('status', [
                \App\Models\Pedido::PEDIDO_STATUS_PAGADO, \App\Models\Pedido::PEDIDO_STATUS_TERMINADO,
                \App\Models\Pedido::PEDIDO_STATUS_CANCELADO
            ])
            ->orderBy('pedidos.created_at', 'DESC')
            ->get();

        return $analytics;
    }

    public function analytics_pedidos()
    {
        $analytics = DB::table('pedidos')
            ->where('negocio_id', $this->id)
            ->whereIn('status', [
                \App\Models\Pedido::PEDIDO_STATUS_PAGADO, \App\Models\Pedido::PEDIDO_STATUS_TERMINADO,
                \App\Models\Pedido::PEDIDO_STATUS_CANCELADO
            ])
            ->orderBy('created_at', 'DESC')
            ->get();

        return $analytics;
    }

    public function tiene_metodo_pago()
    {
        if (
        ( // mercadopago
            !$this->config_mercadopago ||
            (
                !$this->config_mercadopago->public_key &&
                !$this->config_mercadopago->access_token
            ) &&
            (
                !$this->config_offline ||
                (
                    !$this->config_offline->efectivo &&
                    !$this->config_offline->efectivo_domicilio &&
                    !$this->config_offline->terminal &&
                    !$this->config_offline->envio_terminal
                )
            )
        )
        ) {
            return false;
        } else {
            return true;
        }
    }

    public function direccion()
    {
        return "{$this->direccion_calle} {$this->direccion_numero_exterior} {$this->direccion_numero_interior}, {$this->direccion_colonia}, {$this->direccion_numero_municipio}, {$this->direccion_estado}";
    }

    /**
     * @return bool
     */
    public function permite_envio_domicilio()
    {
        return $this->enviosconfig && $this->enviosconfig->tipo_envio;
    }

    /**
     * @return bool
     */
    public function permite_pickup()
    {
        return $this->enviosconfig && $this->enviosconfig->pickup;
    }

    /**
     *
     */
    public function esta_habilitado()
    {
        if ($this->verificado && !$this->adeudo) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public static function getNegociosActivos()
    {
        return Negocio::where('verificado', '=', 1)->get();
    }

    /**
     * @return mixed
     */
    public function adeudos()
    {
        return Factura::where('negocio_id', '=', $this->id)
            ->where(function ($query) {
                $query->where('status', Factura::FACTURA_STATUS_PENDIENTE)
                    ->orWhere('status', Factura::FACTURA_STATUS_VENCIDA);
            })
            ->get();
    }


    public function tieneAdeudos()
    {
        return Factura::where('negocio_id', '=', $this->id)
            ->where(function ($query) {
                $query->where('status', Factura::FACTURA_STATUS_PENDIENTE)
                    ->orWhere('status', Factura::FACTURA_STATUS_VENCIDA);
            })
            ->count();
    }

    public static function buscar($termino, $region_id, $ordenado = false, $lat = null, $lon = null)
    {
        $busqueda = Negocio::query();
        $busqueda->select('negocios.*');
        $busqueda->join('negocios_restaurantecategorias', 'negocios_restaurantecategorias.negocio_id', '=',
            'negocios.id');
        $busqueda->join('restaurantecategorias', 'negocios_restaurantecategorias.categoria_id', '=',
            'restaurantecategorias.id');
        $busqueda->join('platillos', 'platillos.negocio_id', '=', 'negocios.id');

        //filtros asicos
        $busqueda = $busqueda->where('verificado', '=', 1)
            ->where('adeudo', '=', 0)
            ->where('region_id', '=', $region_id);

        $busqueda = $busqueda->where(function ($query) use ($termino) {
            //busqueda en nombre
            $query->orWhere('negocios.nombre', "LIKE", "%{$termino}%");
            $query->orWhere('restaurantecategorias.nombre', "LIKE", "%{$termino}%");
            $query->orWhere('platillos.nombre', "LIKE", "%{$termino}%");
            $query->orWhere('platillos.nombre', "LIKE", "%{$termino}%");
        });

        //orderby
        //$busqueda = $busqueda->groupBy('negocios.id');
        $busqueda = $busqueda->orderBy('activo', 'DESC');

        if ($ordenado) {
            return self::ordenar($busqueda->distinct()->get(), $lat, $lon);
        } else {
            return $busqueda->distinct()->get();
        }
    }

    public static function ordenar($negocios, $lat = null, $lon = null)
    {
        $negociosOrdenados = [];
        $negociosEnvio = [];
        $negociosPickup = [];
        $negociosNoDisponibles = [];

        foreach ($negocios as $negocio) {
            // si no hay lat y lon solo pasan los negocios con pickup
            if (!$negocio->enviosconfig) {
                continue;
            }

            if (!($lat && $lon)) {
                if ($negocio->enviosconfig->tienepickup()) {
                    $negociosPickup[] = $negocio;
                }
                continue;
            }

            //Sacando diferencia de distancia
            $negocio->getDistancia($lat, $lon);
            if ($negocio->enviosconfig && $negocio->enviosconfig->tieneEnvioDomicilio()) {
                $negocio->enviosconfig->getDistanciaAlcance($lat, $lon);
            }

            /*
             * Separando negocios en grupos
             */

            //seleccionando los inactivos
            if (!$negocio->activo) {
                $negociosNoDisponibles[] = $negocio;
                continue;
            }

            // Descartando porque no aceptan pickup y quedan fuera del rango
            if (
                !$negocio->enviosconfig->tienepickup() &&
                (
                    $negocio->enviosconfig->tieneEnvioDomicilio() &&
                    !$negocio->enviosconfig->puedeEnviarEstaDistancia()
                )
            ) {
                continue;
            }

            //Pueden enviar a domicilio a esta ubicacion
            if (
                $negocio->enviosconfig->tieneEnvioDomicilio() &&
                $negocio->enviosconfig->distanciaAlcance <= $negocio->enviosconfig->radio_max
            ) {
                $negociosEnvio[] = $negocio;
                continue;
            }

            //Pueden solo recibir pickup and go en esta ubicacion
            if (
                $negocio->enviosconfig && $negocio->enviosconfig->tienepickup() &&
                (
                    !$negocio->enviosconfig->tieneEnvioDomicilio() ||
                    (
                        $negocio->enviosconfig->tieneEnvioDomicilio() &&
                        !$negocio->enviosconfig->puedeEnviarEstaDistancia()
                    )
                )
            ) {
                $negociosPickup[] = $negocio;
            }
        }

        // si no hay coordenadas regresar solo negocios pickup
        if (!($lat && $lon)) {
            return $negociosPickup;
        }

        // ordenando por distancia
        ObjectHelper::orderObjects($negociosEnvio, 'distancia');
        ObjectHelper::orderObjects($negociosPickup, 'distancia');
        ObjectHelper::orderObjects($negociosNoDisponibles, 'distancia');

        //merge de los resultados
        $negociosOrdenados = array_merge($negociosEnvio, $negociosPickup, $negociosNoDisponibles);

        return $negociosOrdenados;
    }

    public function getDistancia($lat, $lon) {
        if ($lat && $lon) {
            $this->distancia = GeopositionHelper::distance($lat, $lon, $this->lat, $this->lon);
        } else {
            $this->distancia = null;
        }
        return $this->distancia;
    }

    public function pendiente_verificacion() {
        $negocio = $this;
        if (
            $negocio->constitucion_identificacion_oficial &&
            count($negocio->categorias) &&
            count($negocio->horarios) &&
            $negocio->enviosconfig &&
            $negocio->config_mercadopago &&
            count($negocio->platillo_categorias) &&
            count($negocio->platillos)
        ) {
            //generando notificacion
            //generando notificacion para el usuario
            $superUsers = User::getSuperUsers();
            foreach($superUsers as $superUser){
                $notificacion = new Notificacion();
                $notificacion->user_id = $superUser->id;
                $notificacion->url = route('admin');
                $notificacion->subject = "{$this->nombre} está pendiente de verificación";
                $notificacion->message = "<h3>{$this->nombre} sigue pendiente de verificación</h3>";
                $notificacion->message .= "<p>este negocio ha cumplido con todos los requisitos de información para UKI, ahora solo hace falta revisar sus documentos para verificarlo.</p>";
                $notificacion->call_to_action = "Acceder a panel de adminsitración";
                $notificacion->save();
                $notificacion->sendMail();
            }
            return true;
        } else {
            return false;
        }
    }

    public static function top($perfil, $number = 5)
    {
        $restaurantes =  Negocio::where('verificado','=', 1)
            ->where('activo','=', 1)
            ->where('adeudo','=', 0);

        if ($perfil->region_id){
            $restaurantes->where('region_id', '=' ,$perfil->region_id);
        }

        if ($perfil->lat && $perfil->lon) {
            $restaurantes = $restaurantes->get();
            $restaurantes =  Negocio::ordenar($restaurantes,  $perfil->lat, $perfil->lon);
            return  array_slice($restaurantes, 0, $number, true);
        } else {
            $restaurantes = $restaurantes->inRandomOrder()->get()->all();
            return  array_slice($restaurantes, 0, $number, true);
        }
    }

    public static function findSlugOrId($slugOrId)
    {
         //revisando slug
         if(!is_numeric($slugOrId)){
            $config = Config_restaurante::where('slug', $slugOrId)->first();
            $restaurante = $config->negocio;
            if (!$restaurante) {
                $restaurante = Negocio::findOrFail($slugOrId);
            }
         } else {
              $restaurante = Negocio::findOrFail($slugOrId);
         }
         return $restaurante;
    }

    public function slugOrId() {
        $slug = $this->config->slug;
        if (!$slug) {
            return $this->id;
        } else {
            return $slug;
        }
    }
}
