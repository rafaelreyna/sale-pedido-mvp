<?php

namespace App\Models\Comunicacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Twilio\Rest\Client;

class VoiceCall extends Model
{

    public $to;
    public $message;

    public function __construct($phone_number = null)
    {
        $this->phone_number = '+52' . $phone_number;
        $this->account_sid = env('ACCOUNT_SID');
        $this->auth_token = env('AUTH_TOKEN');
        $this->from = env('TWILIO_PHONE_NUMBER');
        $this->client = new Client($this->account_sid, $this->auth_token);
        parent::__construct();
    }

    public function call()
    {
        try {
            $phone_number = $this->client->lookups->v1->phoneNumbers($this->phone_number)->fetch();
            if ($phone_number) {
                for ($i = 0; $i <= env('TWILIO_LLAMADA_INTENTOS', 3); $i++) {
                    try {
                        $call = $this->client->account->calls->create(
                            $this->phone_number, // Destination phone number
                            $this->from, // Valid Twilio phone number
                            array(
                                "record" => true,
                                "url" => URL::to("api/notificaciones/{$this->notificacion_id}/call_response"))
                        );

                        if ($call) {
                            Log::info('Realizando llamada a ' . $this->phone_number);
                            return true;
                        } else {
                            Log::error('La llamada falló!');
                        }
                    } catch (\Exception $e) {
                        Log::error(" XXX ERROR DE LLAMADA: {$this->to} {$e->getMessage()} - {$e->getFile()} - {$e->getLine()}");
                    }
                }
            }
            return false;
        } catch (\Exception $e) {
            Log::error(" XXX ERROR PHONE NUMBER: {$this->to} {$e->getMessage()} - {$e->getFile()} - {$e->getLine()}");
        } catch (\RestException $rest) {
            Log::error(" XXX ERROR DE LLAMADA (TWILIO API): {$this->to} {$rest->getMessage()} - {$rest->getFile()} - {$rest->getLine()}");
        } finally {
            return false;
        }
    }
}
