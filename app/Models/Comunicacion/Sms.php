<?php

namespace App\Models\Comunicacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class Sms extends Model
{
    public $to;
    public $body;

    public function enviar()
    {
        try {
            $response = Http::withHeaders([
                'Authorization' => "Bearer " . env('ROUTEE_TOKEN', '4be8ee0d-dbaf-4588-af1a-f40a12c2ba10')
            ])->post("https://connect.routee.net/sms", [
                'body' => $this->body,
                'to' => '+52' . $this->to,
                'from' => env('ROUTEE_FROM', 'UKI®'),
            ]);
            return $response->json();
        } catch (\Exception $e) {
            Log::error(" XXX ERROR DE ENVIO SMS: {$this->to} {$e->getMessage()} - {$e->getFile()} - {$e->getLine()}");
            return null;
        }
    }
}
