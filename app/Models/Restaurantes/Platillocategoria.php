<?php

namespace App\Models\Restaurantes;

use App\Helpers\StringHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

class Platillocategoria extends Model implements Sortable
{
    use SoftDeletes;
    use SortableTrait;

    public $sortable = [
        'order_column_name' => 'orden',
        'sort_when_creating' => true,
    ];

    protected $table = "platillocategorias";

    protected $guarded = ['created_at', 'updated_at', 'deleted_at'];

    public function negocio()
    {
        return $this->belongsTo('App\Models\Negocio', 'negocio_id');
    }

    public function platillos()
    {
        return $this->hasMany('App\Models\Restaurantes\Platillo', 'categoria_id')
            ->orderBy('disponible', 'DESC')
            ->orderBy('orden', 'ASC')
            ->get();
    }

    public function slug(){
        return StringHelper::slug($this->nombre);
    }

    public static function getAllIds($negocio_id)
    {
        return Platillocategoria::where('negocio_id', $negocio_id)->orderBy('orden', 'ASC')->pluck('id')->toArray();
    }

    public function buildSortQuery()
    {
        return static::query()->where('negocio_id', $this->negocio_id);
    }
}
