<?php

namespace App\Models\Restaurantes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Config_restaurante extends Model
{
    protected $table = "config_restaurante";

    protected $guarded = ['created_at', 'updated_at'];

    protected $casts = [
        'notificacion_pedido_sms' => 'array',
        'notificacion_pedido_mail' => 'array',
        'notificacion_pedido_call' => 'array',
    ];

    public function negocio()
    {
        return $this->belongsTo('App\Models\Negocio', 'negocio_id');
    }

    public function platillos()
    {
        return $this->negocio->hasMany('App\Models\Restaurantes\Platillo', 'negocio_id');
    }

    public function minimo_compra()
    {
        $minimo = $this->platillos->filter(function ($item) {
            return !is_null($item->precio);
        });

        return $minimo->min('precio');
    }
}
