<?php

namespace App\Models\Restaurantes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

class Platillo extends Model  implements Sortable
{
    use SoftDeletes;
    use SortableTrait;

    public $sortable = [
        'order_column_name' => 'orden',
        'sort_when_creating' => true,
    ];

    protected $table = "platillos";

    protected $guarded = ['created_at', 'updated_at', 'deleted_at'];

    public function negocio()
    {
        return $this->belongsTo('App\Models\Negocio', 'negocio_id');
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\Restaurantes\Platillocategoria', 'categoria_id');
    }

    public function precio_final(){
        //Generando descuento
        $precio_final = $this->precio;

        if(!is_null($this->precio_promo)) {
            $precio_final = $this->precio_promo;
        }

        return $precio_final;

        //todo: generar otro tipo de descuentos
    }

    public static function getAllIds($negocio_id)
    {
        return Platillo::where('negocio_id', $negocio_id)->orderBy('orden', 'ASC')->pluck('id')->toArray();
    }

    public function buildSortQuery()
    {
        return static::query()->where('negocio_id', $this->negocio_id);
    }
}
