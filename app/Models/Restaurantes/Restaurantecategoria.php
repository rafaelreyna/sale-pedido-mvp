<?php

namespace App\Models\Restaurantes;

use App\Models\Negocio;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Restaurantecategoria extends Model
{
    use SoftDeletes;

    protected $table = "restaurantecategorias";

    protected $guarded = ['created_at', 'updated_at', 'deleted_at'];

    public function restaurantes($region_id = null, $ordenado = false, $lat = null, $lon = null)
    {
        if(!$region_id) {
            return $this->belongsToMany('App\Models\Negocio', 'negocios_restaurantecategorias', 'categoria_id', 'negocio_id')
                ->where('verificado','=', 1)
                ->where('adeudo','=', 0)
                ->inRandomOrder()
                ->get();
;        } else {
            $restaurantes =  $this->belongsToMany('App\Models\Negocio', 'negocios_restaurantecategorias', 'categoria_id', 'negocio_id')
                ->where('verificado','=', 1)
                ->where('adeudo','=', 0)
                ->where('region_id', '=' ,$region_id)
                ->orderBy('activo', 'DESC')
                ->get();
            if ($ordenado) {
                return Negocio::ordenar($restaurantes,  $lat, $lon);
            } else {
                return $restaurantes;
            }
        }
    }

}
