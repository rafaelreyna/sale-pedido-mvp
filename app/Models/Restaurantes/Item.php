<?php

namespace App\Models\Restaurantes;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = "restaurante_pedido_items";

    protected $guarded = ['created_at', 'updated_at'];

    protected $casts = [
        'personalizacion' => 'object'
    ];

    public function pedido()
    {
        return $this->belongsTo('App\Models\Pedido', 'pedido_id');
    }

    public function platillo_data()
    {
        return Platillo::where('nombre',$this->platillo)->where('negocio_id', $this->pedido->negocio->id)->first();
    }

}
