<?php

namespace App\Console;

use App\Console\Commands\GenerarFactura;
use App\Http\Controllers\Admin\NegociosController;
use App\Http\Controllers\Admin\PedidosController;
use App\Http\Controllers\FacturacionController;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        GenerarFactura::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->call(function () {
            $modificados = PedidosController::limpiar_pedidos();
            Log::info("Limpieza de pedidos: {$modificados} pedidos automaticamente terminados.");
        })->everyFifteenMinutes();

        $schedule->call(function () {
            $modificados = PedidosController::cerrar_pedidos();
            Log::info("Cierre de pedidos: {$modificados} pedidos automaticamente cerrados y evaluados con 3 estrellas.");
        })->everyFifteenMinutes();

        $schedule->call(function () {
            $recordatorios = PedidosController::recordar_calificar();
            Log::info("Recordatorio de calificación: {$recordatorios} recordatorios enviados.");
        })->everyFifteenMinutes();

        $schedule->call(function () {
            NegociosController::apertura_cierre();
        })->everyMinute();

        $schedule->call(function () {
            Log::info("--- Corte facturación $$$ ---");
            FacturacionController::corteFacturacion();
        })->monthlyOn(1, '5:00');

        $schedule->call(function () {
            Log::info("--- Corte adeudos $$$ ---");
            FacturacionController::corteAdeudos();
        })->monthlyOn(6, '0:01');

        $schedule->call(function () {
            $verificaciones = NegociosController::revision_verificados();
            Log::info("Enviando recordatorios de verificacion: {$verificaciones} recordatorios enviados.");
        })->dailyAt('07:00');

        /*
         * Notificaciones de pago dias 1 3 y 5
         */
        $schedule->call(function () {
            Log::info("--- Enviando notificacion de pago $$$ ---");
            FacturacionController::notificacionPago();
        })->monthlyOn(1, '9:00');

        $schedule->call(function () {
            Log::info("--- Enviando notificacion de pago $$$ ---");
            FacturacionController::notificacionPago();
        })->monthlyOn(3, '9:00');

        $schedule->call(function () {
            Log::info("--- Enviando notificacion de pago $$$ ---");
            FacturacionController::notificacionPago();
        })->monthlyOn(5, '9:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
