<?php

namespace App\Console\Commands;

use App\Http\Controllers\FacturacionController;
use App\Models\Negocio;
use Illuminate\Console\Command;

class GenerarFactura extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'factura:generar {negocioId} {mes} {ano}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando apra generar una factura individualmente de un mes y ano';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $negocio_id = $this->arguments()['negocioId'];
        $mes = $this->arguments()['mes'];
        $ano = $this->arguments()['ano'];
        $negocio = Negocio::findOrFail($negocio_id);

        FacturacionController::corteFacturacionNegocio($negocio, $mes, $ano);
    }
}
