<?php

namespace App;

use App\Models\Notificacion;
use App\Models\Sucursal;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'negocio_id', 'celular', 'tipo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negocio()
    {
        return $this->belongsTo('App\Models\Negocio', 'negocio_id');
    }

    public function notificaciones()
    {
        return $this->hasMany(Notificacion::class, 'user_id');
    }

    public function sucursales()
    {
        return $this->hasMany(Sucursal::class, 'user_id');
    }

    public static function getSuperUsers(){
        return User::where('tipo', 'superadmin')->get();
    }

    /**
     * Model events
     */
    protected static function boot() {
        parent::boot();
        static::deleting(function($userToDelete) {
            //borrando notificaciones del usuario
            $userToDelete->notificaciones()->delete();
      });
    }

}
