<?php

namespace App\Http\Middleware;

use App\Models\Food\Perfil;
use Closure;

class NeedsLocation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $perfil = Perfil::getPerfil($request);

        //redirect to  get location page
        if(!(isset($perfil->region_id) & ($perfil->lat && $perfil->lon))) {
            return redirect(route('food.perfil.location.edit')."?redirect=".urlencode($request->getRequestUri()));
        }

        return $next($request);
    }
}
