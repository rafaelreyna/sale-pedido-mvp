<?php

namespace App\Http\Controllers;

use App\Models\Factura;
use App\Models\Negocio;
use App\Models\Notificacion;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use MercadoPago\Item;
use MercadoPago\Payer;
use MercadoPago\Preference;
use MercadoPago\SDK;

class FacturacionController extends Controller
{
    public static function corteFacturacion($cadena_fecha = null){
        //obteniendo fecha de facturacion
        if (is_null($cadena_fecha)) {
            $fecha_actual = date("Y-m-d");
            $fecha_facturacion = date("Y-m-d",strtotime($fecha_actual." - 10 days"));
        } else {
            $fecha_facturacion = date("Y-m-d",strtotime($cadena_fecha));
        }
        $mes = date('m', strtotime($fecha_facturacion));
        $ano = date('Y', strtotime($fecha_facturacion));

        //Generando facturas de todos los negocios activos
        $negocios = Negocio::getNegociosActivos();
        foreach($negocios as $negocio) {
            self::corteFacturacionNegocio($negocio, $mes, $ano);
        }
    }

    public static function corteFacturacionNegocio ($negocio, $mes, $ano) {
        $ultima_factura = $negocio->generar_factura($mes, $ano);
        if($ultima_factura->total == 0){
            $ultima_factura->status = 'pagada';
            $ultima_factura->save();
        } else {
            //generando notificacion
            $ultima_factura->enviar();
        }
    }

    public static function corteAdeudos(){
        $superUsers = User::getSuperUsers();
        //Revisando adeudos de negocios activos
        $negocios = Negocio::getNegociosActivos();
        $negocios_adeudos = [];
        foreach($negocios as $negocio) {
            try {
                if ($negocio->tieneAdeudos()){
                    foreach($negocio->adeudos() as $adeudo) {
                        $adeudo->status = Factura::FACTURA_STATUS_VENCIDA;
                        $adeudo->save();
                    }
                    $negocio->adeudo = true;
                    $negocio->save();
                    if ($negocio->adeudo) {
                        $negocios_adeudos[] = $negocio;
                        $notificacion = new Notificacion();
                        $notificacion->user_id = $negocio->admin()->id;
                        $notificacion->url = route('admin.facturas.index');
                        $notificacion->subject = 'URGENTE: Recibos Pendientes de Pago';
                        $notificacion->message = "<h3>URGENTE: Recibos Pendientes de Pago</h3>";
                        $notificacion->message .= "<p>Tienes pendiente uno o más recibos de pago, por lo que tu restaurante se ha desactivado y no podrás recibir pedidos.</p>";
                        $notificacion->message .= "<p>Puedes acceder a tu panel de control UKI&reg; para realizar el pago online y reactivar tu sitio.</p>";
                        $notificacion->short_message = "Tu sitio se ha desactivado por recibos pendientes de pago accede a tu panel en: ";
                        $notificacion->call_to_action = "Acceder a mi estado de cuenta";
                        $notificacion->save();
                        $notificacion->sendMail();
                        $notificacion->sendSms();
                    }
                }
            } catch (\Exception $e) {
                Log::error("Error al procesar adeudo de pedido en {$negocio->id} - {$negocio->nombre}: " . $e->getMessage());
            }
        }

        foreach($superUsers as $superUser){
            $notificacion = new Notificacion();
            $notificacion->user_id = $superUser->id;
            $notificacion->url = route('admin');
            $notificacion->subject = 'Negocios con adeudo este mes';
            $notificacion->message = "<h3>Negocios con adeudo este mes</h3>";
            $notificacion->message .= "<p>Los negocios que presentan adeudo este mes son:</p>";
            $notificacion->message .= "<ul>";
            foreach($negocios_adeudos as $negocio_adeudo) {
                $notificacion->message .= "<li>{$negocio_adeudo->id} - {$negocio_adeudo->nombre} - {$negocio_adeudo->admin()->name} - {$negocio_adeudo->admin()->email} - {$negocio_adeudo->admin()->celular}  </li>";
            }
            $notificacion->message .= "</ul>";
            $notificacion->call_to_action = "Acceder a panel de adminsitración";
            $notificacion->save();
            $notificacion->sendMail();
        }
    }

    public static function notificacionPago(){
        //Revisando adeudos de negocios activos
        $negocios = Negocio::getNegociosActivos();
        foreach($negocios as $negocio) {
            try {
                if ($negocio->tieneAdeudos()){
                    $notificacion = new Notificacion();
                    $notificacion->user_id = $negocio->admin()->id;
                    $notificacion->url = route('admin.facturas.index');
                    $notificacion->subject = 'AVISO: Recuerda pagar tu factura de este mes';
                    $notificacion->message = "<h3>AVISO: Recuerda pagar tu gactura este mes</h3>";
                    $notificacion->message .= "<p>recuerda que tienes hasta el dia 5 de cada mes para hacer tu pago y evitar la suspensión de tu tienda UKI.</p>";
                    $notificacion->message .= "<p>Puedes acceder a tu panel de control UKI&reg; para realizar el pago online.</p>";
                    $notificacion->call_to_action = "Acceder a mi estado de cuenta";
                    $notificacion->save();
                    $notificacion->sendMail();
                }
            } catch (\Exception $e) {
                Log::error("Error al procesar notificacion de pago {$negocio->id} - {$negocio->nombre}: " . $e->getMessage());
            }
        }
    }

    public function pagarFactura($factura_id)
    {
        $factura = Factura::findOrFail($factura_id);

        SDK::setAccessToken(env('UKI_MP_ACCESS_TOKEN', 'TEST-4396439662311675-061816-85ee1dc88b2ca644325c91de7d4e5182-86660669'));

        $preference = new Preference();
        $item = new Item();
        $item->title = "Pago de servicio UKI® para {$factura->negocio->nombre}";
        $item->quantity = 1;
        $item->unit_price = $factura->total;
        $preferenceItems = [$item];

        $preference->items = $preferenceItems;

        $payer = new Payer();
        $payer->name = $factura->negocio->admin()->name;
        $payer->surname = $factura->negocio->admin()->name;
        $payer->email = $factura->negocio->admin()->email;
        $preference->payer = $payer;

        $preference->payment_methods = [
            "installments" => 1
        ];

        $preference->back_urls = [
            "success"=> route('facturas.procesar_pago', $factura->id),
            "failure"=> route('admin'),
            "pending"=> route('admin')
        ];
        $preference->save();

        return redirect($preference->init_point);
    }

    public function procesarPago($factura_id)
    {
        $factura = Factura::findOrFail($factura_id);
        $factura->status = Factura::FACTURA_STATUS_PAGADA;
        $factura->save();

        $negocio = $factura->negocio;
        if(!$negocio->tieneAdeudos()){
            $teniaAdeudos = $negocio->adeudo;

            if($teniaAdeudos) {
                $negocio->adeudo = false;
                $negocio->save();

                $notificacion = new Notificacion();
                $notificacion->user_id = $negocio->admin()->id;
                $notificacion->url = route('admin');
                $notificacion->subject = 'Restaurante reactivado';
                $notificacion->message = "<h3>Restaurante reactivado</h3>";
                $notificacion->message .= "<p>Gracias por tu pago, a partir de este momento tu restaurante se encuentra nuevamente habilitado y puedes empezar a recibir pedidos.</p>";
                $notificacion->short_message = "Restaurante reactivado, gracias por tu pago, accede a tu panel: ";
                $notificacion->call_to_action = "Acceder a mi panel";
                $notificacion->save();
                $notificacion->sendMail();
                $notificacion->sendSms();
            }
        }

        $notificacion = new Notificacion();
        $notificacion->user_id = $negocio->admin()->id;
        $notificacion->url = route('admin');
        $notificacion->subject = 'Gracias por tu pago';
        $notificacion->message = "<h3>Gracias por tu pago</h3>";
        $notificacion->message .= "<p>Tu pago del recibo #{$factura->id} se realizó correctamente, gracias por confiar en UKI.</p>";
        $notificacion->call_to_action = "Acceder a mi panel";
        $notificacion->save();
        $notificacion->sendMail();

        return redirect(route('admin.facturas.index'));
    }

    public function solicitarFactura(Request $request, $factura_id)
    {
        //generando notificacion para el usuario
        $superUsers = User::getSuperUsers();
        $factura = Factura::findOrFail($factura_id);
        foreach($superUsers as $superUser){
            $notificacion = new Notificacion();
            $notificacion->user_id = $superUser->id;
            $notificacion->url = route('admin.facturas.show', $factura_id);
            $notificacion->subject = 'Solicitud de Factura';
            $notificacion->message = "<h3>Solicitud de Factura</h3>";
            $notificacion->message .= "<p>{$factura->negocio->nombre} ha solicitado factura fiscal para su recibo #{$factura_id}</p>";
            $notificacion->short_message = "Solicitud de factura fiscal para el siguiente recibo: ";
            $notificacion->call_to_action = "Ver recibo";
            $notificacion->save();
            $notificacion->sendMail();
        }

        $mensajeFlash['titulo'] = 'Factura solicitada';
        $mensajeFlash['mensaje'] = "Se ha solicitado correctamente la factura fiscal para el recibo #{$factura_id}. El comprobante te será enviado por correo en un plazo no mayor a 72 horas";
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.facturas.show', $factura_id));
    }

}
