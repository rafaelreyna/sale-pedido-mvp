<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImagesController extends Controller
{
    public function public_image(Request $request, $path, $filename){
        $path = base64_decode($path);
        return response()->file(Storage::disk('local')->path("public/{$path}{$filename}"));
    }
}
