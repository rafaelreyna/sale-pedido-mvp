<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FilesController extends Controller
{
    public function negocio_private_file(Request $request, $path, $filename){
        $path = base64_decode($path);
        return response()->file(Storage::disk('local')->path("negocios_data/$path/$filename"));
    }
}
