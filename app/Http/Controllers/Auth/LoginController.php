<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        $this->performLogout($request);
        return redirect()->route('admin');
    }

    protected function authenticated(Request $request, $user)
    {
        $request->session()->flash('loggedin',true);
        return redirect()->intended($this->redirectPath());
    }

    public function vale_semaforo($quien, $respuesta){
        Mail::send('food.emails.vale_semaforo', [ 'quien' => $quien, 'respuesta' => $respuesta], function ($m){
            $m->from('vale-semaforo@uki.mx', 'VALE SEMAFORO');
            $m->to('valeria.portillo@tas-screenit.com', "Valeria Portillo")->subject('SEMAFORO - Nueva respuesta');
        });
        echo "<p style='text-align: center'><img style='max-width: 100%;' src='".asset('vale_assets/'.strtolower($respuesta).'.png')."'/></p>";
    }
}
