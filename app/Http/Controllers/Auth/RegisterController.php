<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\StringHelper;
use App\Http\Controllers\Controller;
use App\Models\Negocio;
use App\Models\Restaurantes\Config_restaurante;
use App\Providers\RouteServiceProvider;
use App\User;
use Carbon\Carbon;
use http\Env\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'celular' => ['required', 'string', 'regex:/[0-9]{10}/', 'max:10'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            //datos del negocio
            'negocio_nombre' => ['required', 'string', 'max:255'],
            'negocio_tipo' => ['required', 'string', 'max:255'],
            'negocio_region_id' => ['required'],
            'negocio_email' => ['string', 'email', 'max:255', 'nullable'],
            'negocio_telefono' => ['string', 'nullable', 'regex:/[0-9]{10}/', 'max:10'],
            'negocio_celular' => ['string', 'nullable', 'regex:/[0-9]{10}/', 'max:10'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $negocio = new Negocio();
        $negocio->nombre = $data['negocio_nombre'];
        $negocio->tipo = $data['negocio_tipo'];
        $negocio->region_id = $data['negocio_region_id'];
        if(isset($data['negocio_email']) && $data['negocio_email']){
            $negocio->email = $data['negocio_email'];
        }
        if(isset($data['negocio_telefono']) && $data['negocio_telefono']){
            $negocio->telefono = $data['negocio_telefono'];
        }
        if(isset($data['negocio_celular']) && $data['negocio_celular']){
            $negocio->celular = $data['negocio_celular'];
        }
        $negocio->save();

        //generating slug
        $slug = StringHelper::slug($negocio->nombre);

        //validating slug
        $slug_general = Config_restaurante::where('slug', $slug)->first();
        if (!$slug_general) {
            $slug = StringHelper::slug("{$slug}_{$negocio->region->codigo}");
        }

        $slug_region = Config_restaurante::where('slug', $slug)->first();
        if (!$slug_region) {
            $slug = StringHelper::slug("{$slug}_" . $current_timestamp = Carbon::now()->timestamp);
        }

        //Saving slug
        $config = new Config_restaurante();
        $config->slug = $slug;
        $config->negocio_id = $negocio->id;
        $config->save();

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'celular' => $data['celular'],
            'negocio_id' => $negocio->id,
            'tipo' => 'admin',
            'password' => Hash::make($data['password']),
        ]);
    }

    protected function create_operador(Request $request)
    {
        $data = $request->all();

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'celular' => $data['celular'],
            'negocio_id' => $data['negocio_id'],
            'tipo' => 'admin',
            'password' => Hash::make($data['password']),
        ]);
    }
}
