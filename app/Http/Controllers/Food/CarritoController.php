<?php

namespace App\Http\Controllers\Food;

use App\Http\Controllers\Controller;
use App\Http\Requests\CartitemtAdd;
use App\Http\Requests\PerfilUpdate;
use App\Http\Requests\ProcessCart;
use App\Models\Calificacion;
use App\Models\Comunicacion\Sms;
use App\Models\Food\Perfil;
use App\Models\Negocio;
use App\Models\Pedido;
use App\Models\Restaurantes\Platillo;
use App\Models\Restaurantes\Restaurantecategoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use App\Services\ShoppingCart\Facades\ShoppingCart;
use MercadoPago\Item;
use MercadoPago\Payer;
use MercadoPago\Preference;
use MercadoPago\SDK;
use phpDocumentor\Reflection\Types\Self_;

class CarritoController extends Controller
{
    public function index(Request $request){

        $perfil = Perfil::getPerfil($request);
        $carrito = null;
        $total = null;
        $preference = null;
        $restaurante = null;
        $instance = $request->session()->get('last-cart-instance');

        if($instance) {
            $restaurante = Negocio::find($instance);
            ShoppingCart::instance($instance)->restore(session()->getId());
            $carrito = ShoppingCart::instance($instance)->content();
            $total = ShoppingCart::instance($instance)->getTotal();
        }

        $viewdata = [
            'instancia' => $instance,
            'perfil' => $perfil,
            'carrito' => $carrito,
            'total' => $total,
            'restaurante' => $restaurante,
            //'mp_preference' => $preference
        ];
        return view('food.pedido', $viewdata);
    }

    public function add(CartitemtAdd $request, $restaurante_id){
        //limpiando el carrito de compra previo

        $instance = $request->session()->get('last-cart-instance');
        if ($request->session()->get('last-cart-instance')){
            $instance = $request->session()->get('last-cart-instance');
            if ($instance != $restaurante_id) {
                self::clear_cart($request);
            }
        }

        ShoppingCart::instance($restaurante_id)->restore(session()->getId());
        $request->session()->put('last-cart-instance', $restaurante_id);

        $data = $request->except('_token, _method');
        if(!isset($data['quantity'])) {
            $data['quantity'] = 1;
        }
        $cartItem = ShoppingCart::add(
            $data['id'],
            $data['name'],
            $data['price'],
            intval($data['quantity']),
            [
                'comentarios_cliente' => isset($data['comentarios_cliente']) ? $data['comentarios_cliente'] : null,
                'opciones' => isset($data['opciones']) ? $data['opciones'] : null,
            ]
        );
        ShoppingCart::instance($restaurante_id)->store(session()->getId());

        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['mensaje'] = "Agregaste {$data['quantity']} {$data['name']}(s) a tu pedido, puedes revisarlo <a href='".
            route('food.checkout.index'). "'>haciendo click aquí</a>";
        $mensajeFlash['accion'] = 'alert-success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        if (isset($data['goToCheckout']) && $data['goToCheckout']) {
            return redirect(route('food.checkout.index'));
        } else {
            return redirect(route('food.restaurante.show', $restaurante_id));
        }
    }

    public function update(CartitemtAdd $request, $restaurante_id, $item_id){


        $shoppingCart = ShoppingCart::instance($restaurante_id)->restore(session()->getId());
        $request->session()->put('last-cart-instance', $restaurante_id);

        $data = $request->except('_token, _method');
        if(!isset($data['quantity'])) {
            $data['quantity'] = 1;
        }

        $cartItem = $shoppingCart->get($item_id);
        $cartItem->quantity = intval($data['quantity']);
        $cartItem->options = [
            'comentarios_cliente' => isset($data['comentarios_cliente']) ? $data['comentarios_cliente'] : null,
            'opciones' => isset($data['opciones']) ? $data['opciones'] : null,
        ];
        $shoppingCart->content->put($cartItem->getUniqueId(), $cartItem);
        ShoppingCart::instance($restaurante_id)->store(session()->getId());

        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['mensaje'] = "Modificaste {$data['name']}(s) en tu pedido, puedes revisarlo <a href='".
            route('food.checkout.index'). "'>haciendo click aquí</a>";
        $mensajeFlash['accion'] = 'alert-success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('food.checkout.index'));
    }

    public function delete(Request $request, $restaurante_id, $item_id){
        ShoppingCart::instance($restaurante_id)->restore(session()->getId());
        $request->session()->put('last-cart-instance', $restaurante_id);
        ShoppingCart::remove($item_id);
        ShoppingCart::instance($restaurante_id)->store(session()->getId());
        return redirect()->back();
    }

    public function clear(Request $request)
    {
        self::clear_cart($request);
        return redirect(route('food.checkout.index'));
    }

    public function procesar(ProcessCart $request){
        $data = $request->all();
        $negocio = Negocio::findOrFail($data['negocio_id']);
        if($data['tipo_envio'] == 'domicilio') {
            if (
                !$negocio->enviosconfig->getDistanciaAlcance($data['lat'], $data['lon']) ||
                !$negocio->enviosconfig->puedeEnviarEstaDistancia()
            ) {
                $mensajeFlash['titulo'] = 'Error';
                $mensajeFlash['mensaje'] = "Tu ubicación cambió y quedó fuera del alcance de envío de este restaurante, cambia tu dirección o realiza un pedido de algún otro restaurante";
                $mensajeFlash['accion'] = 'alert-danger';
                $request->session()->flash('mensajeFlash', $mensajeFlash);
                return redirect(route('food.checkout.index'));
            }
        }


        $viewdata = DB::transaction(function () use($data, $request) {
            //Creando el registro de pedido
            $pedido = new Pedido();
            $pedido->negocio_id = $data['negocio_id'];
            $pedido->nombre = "{$data['nombre']} {$data['apellidos']}";
            $pedido->direccion = $data['direccion'];
            if(isset($data['referencias']) && $data['referencias']) {
                $pedido->direccion .= " ({$data['referencias']})";
            }
            $pedido->email = $data['email'];
            $pedido->lat = $data['lat'];
            $pedido->lon = $data['lon'];
            $pedido->telefono = $data['telefono'];
            $pedido->status = Pedido::PEDIDO_STATUS_POR_CONFIRMAR;
            if(isset($data['comentarios_cliente'])){
                $pedido->comentarios_cliente = $data['comentarios_cliente'];
            }
            $pedido->save();

            //insertando items
            $total = 0;
            foreach($data['item'] as $item){
                $item = json_decode(base64_decode($item));
                $itemObj = new \App\Models\Restaurantes\Item();
                $itemObj->pedido_id = $pedido->id;
                $itemObj->platillo = $item->name;
                $platilloDB = Platillo::find($item->id);
                $itemObj->categoria = $platilloDB->categoria->nombre;
                $itemObj->cantidad = $item->quantity;
                $itemObj->costo_unitario = $item->price;
                $itemObj->costo_total = $item->price * $item->quantity;
                $itemObj->personalizacion = [
                    'comentarios_cliente' => isset($item->options->comentarios_cliente) ? $item->options->comentarios_cliente : null,
                    'opciones' => isset($item->options->opciones) ? $item->options->opciones : null,
                ];
                $itemObj->save();

                $total += $itemObj->costo_total;
            }
            $pedido->total = $total;
            $pedido->save();


            $negocio = Negocio::findOrFail($data['negocio_id']);

            //calculando distancia
            if($data['tipo_envio'] == 'domicilio') {
                $params = "origin={$negocio->lat},{$negocio->lon}&destination={$pedido->lat},{$pedido->lon}";
                $distanciaResponse = file_get_contents("https://maps.googleapis.com/maps/api/directions/json?key=".env('GOOGLE_MAPS_KEY', 'AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY')."&{$params}");
                $distanciaResponse = json_decode($distanciaResponse);
                $distancia = $distanciaResponse->routes[0]->legs[0]->distance->value/1000;
                $pedido->distancia = $distancia;
                $tiempo_envio = $distanciaResponse->routes[0]->legs[0]->duration->value;
                $pedido->tiempo_envio = $tiempo_envio;

                $costo_envio = null;
                //calculando costo de envio
                if($negocio->enviosconfig->envio_gratis) { //revisar si el envio es gratis
                    if(
                        $pedido->total >= $negocio->enviosconfig->envio_gratis_minimo_cantidad &&
                        $pedido->distancia <= $negocio->enviosconfig->envio_gratis_maximo_km
                    ) {
                        $costo_envio = 0;
                        $pedido->envio = \App\Models\Pedido::PEDIDO_ENVIO_GRATIS;
                    }
                }

                if(is_null($costo_envio)){
                    if($negocio->enviosconfig->tipo_envio == \App\Models\Pedido::PEDIDO_ENVIO_FIJO){
                        $costo_envio = $negocio->enviosconfig->costo_envio_fijo;
                        $pedido->envio = \App\Models\Pedido::PEDIDO_ENVIO_FIJO;
                    } else {
                        if ($distancia >= $negocio->enviosconfig->km_base) {
                            $costo_envio = $negocio->enviosconfig->costo_envio_km_base + (($negocio->enviosconfig->costo_envio_km) * ceil($distancia - $negocio->enviosconfig->km_base));
                        } else {
                            $costo_envio = $negocio->enviosconfig->costo_envio_km_base;
                        }
                        $pedido->envio = \App\Models\Pedido::PEDIDO_ENVIO_DINAMICO;
                    }
                }
                $pedido->costo_envio = $costo_envio;

                $pedido->gran_total = $pedido->total + $pedido->costo_envio;
                $pedido->save();
            }

            if($data['tipo_envio'] == 'pickup') {
                $pedido->gran_total = $pedido->total;
                $pedido->envio = Pedido::PEDIDO_ENVIO_PICKUP;
                $pedido->save();
            }

            //Generando formas de pago
            $formas_pago = [];
            //Creando boton de pago si aplica
            if($negocio->config_mercadopago->access_token){
                $formas_pago[\App\Models\Pedido::PEDIDO_PAGO_MP] = [
                    'opcion' => 'Pago en línea (tarjeta débito/crédito)',
                    'value' => \App\Models\Pedido::PEDIDO_PAGO_MP
                ];
            }

            if($negocio->config_offline && $negocio->config_offline->efectivo){
                $formas_pago[\App\Models\Pedido::PEDIDO_PAGO_EFECTIVO] = [
                    'opcion' => 'Pago en efectivo',
                    'value' => \App\Models\Pedido::PEDIDO_PAGO_EFECTIVO,
                ];
            }

            if($data['tipo_envio'] == 'domicilio') {
                if($negocio->config_offline && $negocio->config_offline->envio_terminal){
                    $formas_pago[\App\Models\Pedido::PEDIDO_PAGO_TERMINAL] = [
                        'opcion' => 'Envío de terminal a domicilio',
                        'value' => \App\Models\Pedido::PEDIDO_PAGO_TERMINAL,
                    ];
                }
            }
            if($data['tipo_envio'] == 'pickup') {
                if($negocio->config_offline && $negocio->config_offline->envio_terminal){
                    $formas_pago[\App\Models\Pedido::PEDIDO_PAGO_TERMINAL] = [
                        'opcion' => 'Pago con tarjeta en el negocio',
                        'value' => \App\Models\Pedido::PEDIDO_PAGO_TERMINAL,
                    ];
                }
            }

            if (isset($data['recordar_ubicacion']) && $data['recordar_ubicacion'] == 'true') {
                //Guardando preferencias si aplica
                $perfil = Perfil::getPerfil($request);
                $perfil->nombre = $data['nombre'];
                $perfil->apellidos = $data['apellidos'];
                $perfil->email = $data['email'];
                $perfil->telefono = $data['telefono'];
                $perfil->direccion = $data['direccion'];
                $perfil->lat = $data['lat'];
                $perfil->lon = $data['lon'];
                $perfil->referencias = $data['referencias'];
                Cookie::queue('uki_perfil', json_encode($perfil), 525600);
            }

            $viewdata = [
                'pedido' => $pedido,
                'formas_pago' => $formas_pago,
                'tipo_envio' => $data['tipo_envio'],
            ];

            return $viewdata;
        });

        return view('food.confirmar', $viewdata);
    }

    public function confirmar(Request $request){
        $data = $request->all();

        $pedido = Pedido::find($data['pedido_id']);

        $pedido->forma_pago = $data['forma_pago'];

        $pedido->save();

        //si el negiocio cerro o fue desactivado en el proceso de la compra
        if (!$pedido->negocio->verificado || !$pedido->negocio->activo) {
            //cancelar de último momento la compra
            self::clear_cart($request);
            $mensajeFlash['titulo'] = 'Error';
            $mensajeFlash['mensaje'] = "Lo sentimos, hubo un problema con el restaurante, probablemente cerró antes de que completaras tu pedido o fue desactivado por alguna razón. Te invitamos a buscar más opciones en UKI&reg;";
            $mensajeFlash['accion'] = 'alert-danger';
            $request->session()->flash('mensajeFlash', $mensajeFlash);
            return redirect(route('food.home'));
        }

        if($data['forma_pago'] == \App\Models\Pedido::PEDIDO_PAGO_MP){
            //Creando boton de pago
            SDK::setAccessToken($pedido->negocio->config_mercadopago->access_token);

            $preference = new Preference();

            /* forma de generar ticket en mercadopago con el detalle de los productios
            // Crea un ítem en la preferencia
            $preferenceItems = [];
            foreach($pedido->items as $pedidoItem){
                $item = new Item();
                $item->title = "UKI: {$pedidoItem->nombre} en {$pedido->negocio->nombre}";
                $item->quantity = $pedidoItem->cantidad;
                $item->unit_price = $pedidoItem->costo_unitario;
                $preferenceItems[] = $item;
            }

            //agregando envio
            */
            $item = new Item();
            $item->title = "Pedido Uki en {$pedido->negocio->nombre}, envío {$pedido->envio}";
            $item->quantity = 1;
            $item->unit_price = $pedido->gran_total;
            $preferenceItems = [$item];

            $preference->items = $preferenceItems;

            $payer = new Payer();
            $payer->name = $pedido->nombre;
            $payer->surname = $pedido->nombre;
            $payer->email = $pedido->email;
            $preference->payer = $payer;

            $preference->back_urls = [
                "success"=> route('food.pedido.seguimiento', base64_encode($pedido->id)),
                "failure"=> route('food.pedido.seguimiento', base64_encode($pedido->id)),
            ];

            $preference->notification_url = route('pedido.procesar.mp', $pedido->id);

            //moo binario para recibir respuesta instantanea: aceptado o fallido
            $preference->binary_mode = true;

            $preference->payment_methods = [
                "excluded_payment_types" => [
                    ["id" => "ticket"],
                    ["id" => "prepaid_card"],
                    ["id" => "atm"],
                    ["id" => "digital_currency"],
                    ["id" => "digital_wallet"]
                ],
                "installments" => 1
            ];

            $preference->save();
            $pedido->status = Pedido::PEDIDO_STATUS_PROCESANDO_PAGO;
            $pedido->save();
            return redirect($preference->init_point);
        } else {
            $pedido->status = \App\Models\Pedido::PEDIDO_STATUS_CONFIRMADO;
            $pedido->save();
            $pedido->notificar();
            self::clear_cart($request);
            return redirect(route('food.pedido.seguimiento', base64_encode($pedido->id)));
        }
    }

    public function procesar_pago_mp(Request $request, $pedido_id){
        Log::info('Recibido un procesamiento de mercadopago');
        $pedido = Pedido::findOrFail($pedido_id);
        $data = $request->all();
        Log::info('info recibida: '. json_encode($data));

        if(isset($data['action']) && $data['data_id']){
            if ($data['action'] == 'payment.created') {
                $response = Http::withHeaders([
                    'Authorization' => "Bearer {$pedido->negocio->config_mercadopago->access_token}"
                ])->get("https://api.mercadopago.com/v1/payments/{$data['data_id']}");
                $response_json = $response->json();
                Log::info('response json: '. json_encode($response_json));
                if(isset($response_json['status']) && $response_json['status'] == 'approved'){
                    $pedido->status = \App\Models\Pedido::PEDIDO_STATUS_PAGADO;
                    $pedido->notificar();
                } else {
                    $pedido->status = \App\Models\Pedido::PEDIDO_STATUS_PAGO_RECHAZADO;
                }
                $pedido->transaccion_id = $data['data_id'];
                $pedido->transaccion_payload = $response_json;
                $pedido->save();
            }
        }

        return Response::json([
            'response' => 'pago_procesado'
        ], 201);
    }


    public function seguimiento_pedido(Request $request, $pedido_id){
        $pedido = Pedido::findOrFail(base64_decode($pedido_id));
        if (
            isset($_GET['collection_id'])  && $_GET['collection_id'] == 'null' &&
            isset($_GET['payment_id'])  && $_GET['payment_id'] == 'null' &&
            isset($_GET['status'])  && $_GET['status'] == 'null' &&
            isset($_GET['merchant_order_id'])  && $_GET['merchant_order_id'] == 'null'
        ) {
            if ($pedido->status == Pedido::PEDIDO_STATUS_PROCESANDO_PAGO) {
                $pedido->status = Pedido::PEDIDO_STATUS_POR_CONFIRMAR;
                $pedido->save();
                return redirect(route('food.checkout.index'));
            }
        }

        self::clear_cart($request);
        $viewdata = [
            'pedido' => $pedido
        ];

        return view('food.pedido_success', $viewdata);
    }

    public function calificar_pedido(Request $request, $pedido_id){
        $pedido = Pedido::findOrFail(base64_decode($pedido_id));

        if($pedido->status == Pedido::PEDIDO_STATUS_TERMINADO){
            $mensajeFlash['titulo'] = 'Error';
            $mensajeFlash['mensaje'] = "Este pedido ya fue calificado";
            $mensajeFlash['accion'] = 'alert-danger';
            $request->session()->flash('mensajeFlash', $mensajeFlash);

            return redirect(route('food.home'));
        }

        $viewdata = [
            'pedido' => $pedido
        ];

        return view('food.pedido_calificar', $viewdata);
    }

    public function calificar_store(Request $request, $pedido_id){
        $pedido = Pedido::findOrFail(base64_decode($pedido_id));
        $data = $request->except('_token, _method');
        $calificacion = new Calificacion();
        $calificacion->calificacion = $data['calificacion'];
        $calificacion->comentarios = $data['comentarios'];
        $pedido->calificar($calificacion);

        $mensajeFlash['titulo'] = 'Muchas gracias';
        $mensajeFlash['mensaje'] = "Tu calificación ayuda a los restaurantes a mejorar";
        $mensajeFlash['accion'] = 'alert-success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('food.home'));
    }

    public static function clear_cart($request){
        $instance = $request->session()->get('last-cart-instance');
        ShoppingCart::instance($instance)->restore(session()->getId());
        ShoppingCart::instance($instance)->clear();
        ShoppingCart::instance($instance)->destroy(session()->getId());
    }
}
