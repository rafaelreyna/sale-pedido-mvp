<?php

namespace App\Http\Controllers\Food;

use App\Http\Controllers\Controller;
use App\Models\Food\Perfil;
use App\Models\Negocio;
use App\Models\Region;
use App\Models\Restaurantes\Platillo;
use App\Models\Restaurantes\Restaurantecategoria;
use Melihovv\ShoppingCart\Facades\ShoppingCart;
use Illuminate\Http\Request;

class DirectoryController extends Controller
{
    public function index(Request $request)
    {
        if (env('MANTENIMIENTO', null) == 'true' ) {
            return redirect('https://restaurantes.uki.mx');
        }

        $categorias = Restaurantecategoria::all();
        $perfil = Perfil::getPerfil($request);

        $restaurantes = Negocio::top($perfil);

        $regiones = Region::all();
        $viewdata = [
            'categorias' => $categorias,
            'perfil' => $perfil,
            'regiones' => $regiones,
            'restaurantes' => $restaurantes,
        ];
        return view('food.home', $viewdata);
    }

    public function categoriaListado(Request $request, $categoria_id)
    {
        $categoria = Restaurantecategoria::findOrFail($categoria_id);
        $perfil = Perfil::getPerfil($request);
        $viewdata = [
            'categoria' => $categoria,
            'perfil' => $perfil,
        ];
        return view('food.categoria', $viewdata);
    }

    public function restauranteListado(Request $request, $restaurante_id)
    {
        //Revisando slug
        $restaurante = Negocio::findSlugOrId($restaurante_id);

        if (!$restaurante->esta_habilitado()) {
            $mensajeFlash['titulo'] = 'Error';
            $mensajeFlash['mensaje'] = "Este restaurante no está disponible en este momento por alguna razón.";
            $mensajeFlash['accion'] = 'alert-danger';
            $request->session()->flash('mensajeFlash', $mensajeFlash);
            return redirect(route('food.home'));
        }

        $perfil = Perfil::getPerfil($request);
        $lastrestaurante = null;
        $cartcount = 0;
        if ($request->session()->get('last-cart-instance')){
            ShoppingCart::instance($request->session()->get('last-cart-instance'))->restore(session()->getId());
            $lastrestaurante = Negocio::find($request->session()->get('last-cart-instance'));
            $cartcount = ShoppingCart::instance($request->session()->get('last-cart-instance'))->count();
        }

        //obteniendo distancia
        $restaurante->getDistancia($perfil->lat, $perfil->lon);

        $viewdata = [
            'restaurante' => $restaurante,
            'lastrestaurante' => $lastrestaurante,
            'cartcount' => $cartcount,
            'perfil' => $perfil,
        ];
        return view('food.restaurante', $viewdata);
    }

    public function menuOnline(Request $request, $restaurante_id)
    {
        //Revisando slug
        $restaurante = Negocio::findSlugOrId($restaurante_id);

        if ($restaurante->adeudo) {
            $mensajeFlash['titulo'] = 'Error';
            $mensajeFlash['mensaje'] = "Este restaurante no está disponible en este momento por alguna razón.";
            $mensajeFlash['accion'] = 'alert-danger';
            $request->session()->flash('mensajeFlash', $mensajeFlash);
            return redirect(route('food.home'));
        }

        $viewdata = [
            'restaurante' => $restaurante,
        ];
        return view('food.menu_online', $viewdata);
    }

    public function platilloDetalle(Request $request, $platillo_id)
    {
        $platillo = Platillo::findOrFail($platillo_id);

        $viewdata = [
            'platillo' => $platillo,
        ];
        return view('food.platillo', $viewdata);
    }

    public function platilloEdit(Request $request, $platillo_id, $item_id)
    {
        $platillo = Platillo::findOrFail($platillo_id);
        ShoppingCart::instance($platillo->negocio_id)->restore(session()->getId());
        $cartItem = ShoppingCart::get($item_id);

        $viewdata = [
            'platillo' => $platillo,
            'item' => $cartItem,
        ];
        return view('food.platillo', $viewdata);
    }

    public function buscar(Request $request)
    {
        $termino_busqueda = $request->buscar;

        $perfil = Perfil::getPerfil($request);
        $restaurantes = Negocio::buscar($termino_busqueda, $perfil->region_id, true, $perfil->lat, $perfil->lon);

        $viewdata = [
            'restaurantes' => $restaurantes,
            'perfil' => $perfil
        ];
        return view('food.busqueda', $viewdata);
    }
}
