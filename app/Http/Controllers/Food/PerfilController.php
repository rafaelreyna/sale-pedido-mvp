<?php

namespace App\Http\Controllers\Food;

use App\Http\Controllers\Controller;
use App\Http\Requests\PerfilUpdate;
use App\Models\Food\Perfil;
use App\Models\Negocio;
use App\Models\Region;
use App\Models\Restaurantes\Restaurantecategoria;
use App\Services\ShoppingCart\Facades\ShoppingCart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class PerfilController extends Controller
{
    public function edit(Request $request)
    {
        $perfil = Perfil::getPerfil($request);
        $instance = $request->session()->get('last-cart-instance');

        if($instance) {
            $restaurante = Negocio::find($instance);
            ShoppingCart::instance($instance)->restore(session()->getId());
            $carrito = ShoppingCart::instance($instance)->content();
            $total = ShoppingCart::instance($instance)->getTotal();
        } else {
            $carrito = null;
            $total = null;
            $restaurante = null;
        }

        $viewdata = [
            'instancia' => $instance,
            'perfil' => $perfil,
            'carrito' => $carrito,
            'total' => $total,
            'restaurante' => $restaurante,
            'regiones' => Region::regionesActivas(),
        ];

        return view('food.perfil', $viewdata);
    }

    public function update(PerfilUpdate $request){
        $data = $request->except('_token, _method, _url, region_id');
        $perfil = Perfil::getPerfil($request);
        foreach($data as $campo => $valor) {
            if ( ($campo == 'lat' && $perfil->lat != $valor) || ($campo == 'lon' && $perfil->lon != $valor)) {
                CarritoController::clear_cart($request);
                $request->_url = route('food.home');
            }

            $perfil->$campo = $valor;
        }

        // se asigna la region en base a las coordenadas
        $perfil->selectRegion();

        $cookie = cookie('uki_perfil', json_encode($perfil), 525600); // 1 year

        if($_GET['redirect'] ?? null && $_GET['redirect']) {
            return redirect(URL::to($_GET['redirect']))->withCookie($cookie);
        }

        if ($request->_url) {
            return redirect($request->_url)->withCookie($cookie);
        } else {
            return redirect()->back()->withCookie($cookie);
        }
    }

    public function configurarRegion(Request $request, $region_id){
        $region = Region::findOrFail($region_id);

        $perfil = Perfil::getPerfil($request);
        $perfil->region_id = $region_id;
        $perfil->region = $region;
        $cookie = cookie('uki_perfil', json_encode($perfil), 525600); // 1 year
        return redirect()->back()->withCookie($cookie);
    }

    public function configurarUbicacion(Request $request, $lat, $lon){

        $perfil = Perfil::getPerfil($request);
        $perfil->lat = $lat;
        $perfil->lon = $lon;
        $perfil->selectRegion();
        $perfil->selectAddress();
        $cookie = cookie('uki_perfil', json_encode($perfil), 525600); // 1 year

        $mensajeFlash['titulo'] = 'Ubicación Detectada';
        $mensajeFlash['mensaje'] = "Tu ubicción se ha agregado correctamente a tus preferencias, puedes cambiarla <a href='".
            route('food.perfil.edit'). "'>haciendo click aquí</a>";
        $mensajeFlash['accion'] = 'alert-success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        if($_GET['redirect'] ?? null && $_GET['redirect']) {
            return redirect(URL::to($_GET['redirect']))->withCookie($cookie);
        }

        return redirect(route('food.home'))->withCookie($cookie);
    }

    public function editLocation(Request $request)
    {
        if (env('MANTENIMIENTO', null) == 'true' ) {
            return redirect('https://restaurantes.uki.mx');
        }

        $categorias = Restaurantecategoria::all();
        $perfil = Perfil::getPerfil($request);

        $restaurantes = Negocio::top($perfil);

        $regiones = Region::regionesActivas();
        $viewdata = [
            'categorias' => $categorias,
            'perfil' => $perfil,
            'regiones' => $regiones,
            'restaurantes' => $restaurantes,
        ];
        return view('food.pedir_ubicacion', $viewdata);
    }

    public function searchAdress($lat, $lon) {
        $params = "latlng={$lat},{$lon}&key=" . env('GOOGLE_MAPS_KEY', 'AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY');
        $direccionResponse = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?{$params}");
        $direccionResponse = json_decode($direccionResponse);
        $direccion = $direccionResponse->results[0]->formatted_address ?? '';
        echo $direccion;
    }

}
