<?php

namespace App\Http\Controllers\Food;

use App\Http\Controllers\Controller;
use App\Models\Food\Perfil;
use App\Models\Negocio;
use App\Models\Pedido;
use App\Models\Restaurantes\Platillo;
use App\Services\ShoppingCart\Facades\ShoppingCart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use MercadoPago\Item;
use MercadoPago\Payer;
use MercadoPago\Preference;
use MercadoPago\SDK;

class CheckoutController extends Controller
{
    /**
     * @return void
     */
    public function index(Request $request)
    {
        $perfil = Perfil::getPerfil($request);
        $carrito = null;
        $total = null;
        $preference = null;
        $restaurante = null;
        $instance = $request->session()->get('last-cart-instance');

        if($instance) {
            $restaurante = Negocio::find($instance);
            ShoppingCart::instance($instance)->restore(session()->getId());
            $carrito = ShoppingCart::instance($instance)->content();
            $total = ShoppingCart::instance($instance)->getTotal();
        } else {
            $carrito = null;
            $total = null;
            $restaurante = null;
        }

        $viewdata = [
            'instancia' => $instance,
            'perfil' => $perfil,
            'carrito' => $carrito,
            'total' => $total,
            'restaurante' => $restaurante,
            //'mp_preference' => $preference
        ];
        return view('food.checkout.mi_carrito', $viewdata);
    }

    public function process(Request $request)
    {
       if ($request->step) {
            switch($request->step) {
                case 'tipo_pedido':
                    return $this->tipoPedido($request);
                case 'forma_pago':
                    return $this->formaPago($request);
                case 'confirmar':
                    return $this->confirmar($request);
                case 'end_checkout':
                    return $this->endCheckout($request);
            }
       }
       return redirect(route('food.checkout.index'));
    }

    private function tipoPedido($request)
    {
        $this->validate(
            $request ,
            [
                'item' => 'array|required',
                'nombre' => 'required|string',
                'apellidos' => 'required|string',
                'email' => 'required|email',
                'telefono' => 'required|string|regex:/[0-9]{10}/',
                'comentarios_cliente' => 'nullable|string|max:150',
            ]
        );

        $data = $request->all();
        $perfil = Perfil::getPerfil($request);
        $viewdata = DB::transaction(function () use($data, $request, $perfil) {
            //Actualizando perfil parcial
            $perfil = Perfil::getPerfil($request);
            $perfil->nombre = $data['nombre'];
            $perfil->apellidos = $data['apellidos'];
            $perfil->email = $data['email'];
            $perfil->telefono = $data['telefono'];
            $perfil->referencias = $data['referencias'] ?? null;
            Cookie::queue('uki_perfil', json_encode($perfil), 525600);

            //Creando el registro de pedido
            $pedido = new Pedido();
            $pedido->negocio_id = $data['negocio_id'];
            $pedido->nombre = "{$perfil->nombre} {$perfil->apellidos}";
            $pedido->direccion = $perfil->direccion;
            if($perfil->referencias) {
                $pedido->direccion .= " ({$perfil->referencias})";
            }
            $pedido->email = $perfil->email;
            $pedido->lat = $perfil->lat;
            $pedido->lon = $perfil->lon;
            $pedido->telefono = $perfil->telefono;
            $pedido->status = Pedido::PEDIDO_STATUS_POR_CONFIRMAR;
            if(isset($data['comentarios_cliente'])){
                $pedido->comentarios_cliente = $data['comentarios_cliente'];
            }
            $pedido->save();

            //insertando items
            $total = 0;
            foreach($data['item'] as $item){
                $item = json_decode(base64_decode($item));
                $itemObj = new \App\Models\Restaurantes\Item();
                $itemObj->pedido_id = $pedido->id;
                $itemObj->platillo = $item->name;
                $platilloDB = Platillo::find($item->id);
                $itemObj->categoria = $platilloDB->categoria->nombre;
                $itemObj->cantidad = $item->quantity;
                $itemObj->costo_unitario = $item->price;
                $itemObj->costo_total = $item->price * $item->quantity;
                $itemObj->personalizacion = [
                    'comentarios_cliente' => isset($item->options->comentarios_cliente) ? $item->options->comentarios_cliente : null,
                    'opciones' => isset($item->options->opciones) ? $item->options->opciones : null,
                ];
                $itemObj->save();

                $total += $itemObj->costo_total;
            }
            $pedido->total = $total;
            $pedido->save();

            //generando formas de pago
            $negocio = Negocio::findOrFail($data['negocio_id']);
            $tipos_pedido = [];
            if($negocio->enviosconfig->getDistanciaAlcance($perfil->lat, $perfil->lon)){
                if($negocio->enviosconfig->puedeEnviarEstaDistancia()) {
                    if($negocio->permite_envio_domicilio()) {
                        $tipos_pedido['domicilio'] = [
                            'opcion' => 'Envío a domicilio',
                            'value' => 'domicilio',
                            'icon' => 'motorcycle   ',
                        ];
                    }
                }
            }

            if($negocio->permite_pickup()) {
                $tipos_pedido['pickup'] = [
                    'opcion' => 'Pick Up & Go',
                    'value' => 'pickup',
                    'icon' => 'car',
                ];
            }

            $viewdata = [
                'pedido' => $pedido,
                'tipos_pedido' => $tipos_pedido,
            ];

            return $viewdata;
        });

        return view('food.checkout.tipo_pedido', $viewdata);
    }

    private function formaPago($request)
    {

        $data = $request->all();
        $perfil = Perfil::getPerfil($request);
        $pedido = Pedido::findOrFail($data['pedido_id']);
        $negocio = $pedido->negocio;
        $viewdata = DB::transaction(function () use($data, $request, $pedido, $negocio) {
            //seteando tipo de envio
            //calculando distancia
            if($data['tipo_envio'] == 'domicilio') {
                $params = "origin={$pedido->negocio->lat},{$pedido->negocio->lon}&destination={$pedido->lat},{$pedido->lon}";
                $distanciaResponse = file_get_contents("https://maps.googleapis.com/maps/api/directions/json?key=AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY&{$params}");
                $distanciaResponse = json_decode($distanciaResponse);
                $distancia = $distanciaResponse->routes[0]->legs[0]->distance->value/1000;
                $pedido->distancia = $distancia;
                $tiempo_envio = $distanciaResponse->routes[0]->legs[0]->duration->value;
                $pedido->tiempo_envio = $tiempo_envio;

                $costo_envio = null;
                //calculando costo de envio
                if($negocio->enviosconfig->envio_gratis) { //revisar si el envio es gratis
                    if(
                        $pedido->total >= $negocio->enviosconfig->envio_gratis_minimo_cantidad &&
                        $pedido->distancia <= $negocio->enviosconfig->envio_gratis_maximo_km
                    ) {
                        $costo_envio = 0;
                        $pedido->envio = \App\Models\Pedido::PEDIDO_ENVIO_GRATIS;
                    }
                }

                if(is_null($costo_envio)){
                    if($negocio->enviosconfig->tipo_envio == \App\Models\Pedido::PEDIDO_ENVIO_FIJO){
                        $costo_envio = $negocio->enviosconfig->costo_envio_fijo;
                        $pedido->envio = \App\Models\Pedido::PEDIDO_ENVIO_FIJO;
                    } else {
                        if ($distancia >= $negocio->enviosconfig->km_base) {
                            $costo_envio = $negocio->enviosconfig->costo_envio_km_base + (($negocio->enviosconfig->costo_envio_km) * ceil($distancia - $negocio->enviosconfig->km_base));
                        } else {
                            $costo_envio = $negocio->enviosconfig->costo_envio_km_base;
                        }
                        $pedido->envio = \App\Models\Pedido::PEDIDO_ENVIO_DINAMICO;
                    }
                }
                $pedido->costo_envio = $costo_envio;

                $pedido->gran_total = $pedido->total + $pedido->costo_envio;
                $pedido->save();
            }

            if($data['tipo_envio'] == 'pickup') {
                $pedido->gran_total = $pedido->total;
                $pedido->envio = Pedido::PEDIDO_ENVIO_PICKUP;
                $pedido->save();
            }

            //Generando formas de pago
            $formas_pago = [];
            //Creando boton de pago si aplica
            if($negocio->config_mercadopago->access_token){
                $formas_pago[\App\Models\Pedido::PEDIDO_PAGO_MP] = [
                    'opcion' => 'Pago en línea <br/>(tarjeta débito/crédito)',
                    'value' => \App\Models\Pedido::PEDIDO_PAGO_MP,
                    'icon' => 'desktop'
                ];
            }

            /*
             * Metodos de pago para envio a domicilio
             */
            if($data['tipo_envio'] == 'domicilio') {
                //efectivo a domicilio
                if($negocio->config_offline && $negocio->config_offline->efectivo_domicilio){
                    $formas_pago[\App\Models\Pedido::PEDIDO_PAGO_EFECTIVO] = [
                        'opcion' => 'Pago en efectivo',
                        'value' => \App\Models\Pedido::PEDIDO_PAGO_EFECTIVO,
                        'icon' => 'money'
                    ];
                }

                // terminal a domicilio
                if($negocio->config_offline && $negocio->config_offline->envio_terminal){
                    $formas_pago[\App\Models\Pedido::PEDIDO_PAGO_TERMINAL] = [
                        'opcion' => 'Envío de terminal a domicilio',
                        'value' => \App\Models\Pedido::PEDIDO_PAGO_TERMINAL,
                        'icon' => 'credit-card'
                    ];
                }
            }

            /*
             * Metodos de pago para envio pickup
             */
            if($data['tipo_envio'] == 'pickup') {
                //efectivo a en local
                if($negocio->config_offline && $negocio->config_offline->efectivo){
                    $formas_pago[\App\Models\Pedido::PEDIDO_PAGO_EFECTIVO] = [
                        'opcion' => 'Pago en efectivo',
                        'value' => \App\Models\Pedido::PEDIDO_PAGO_EFECTIVO,
                        'icon' => 'money'
                    ];
                }

                //terminal en local
                if($negocio->config_offline && $negocio->config_offline->terminal){
                    $formas_pago[\App\Models\Pedido::PEDIDO_PAGO_TERMINAL] = [
                        'opcion' => 'Pago con tarjeta en el negocio',
                        'value' => \App\Models\Pedido::PEDIDO_PAGO_TERMINAL,
                        'icon' => 'credit-card'
                    ];
                }
            }

            $viewdata = [
                'pedido' => $pedido,
                'formas_pago' => $formas_pago,
            ];

            return $viewdata;
        });
        return view('food.checkout.forma_pago', $viewdata);
    }

    private function confirmar($request)
    {
        $data = $request->all();
        $pedido = Pedido::find($data['pedido_id']);
        $pedido->forma_pago = $data['forma_pago'];
        $pedido->save();

        $viewdata = [
            'pedido' => $pedido,
        ];

        return view('food.checkout.confirmar', $viewdata);
    }

    private function endCheckout($request)
    {
        $data = $request->all();
        $pedido = Pedido::find($data['pedido_id']);

        //si el negiocio cerro o fue desactivado en el proceso de la compra
        if (!$pedido->negocio->verificado || !$pedido->negocio->activo) {
            //cancelar de último momento la compra
            CarritoController::clear_cart($request);
            $mensajeFlash['titulo'] = 'Error';
            $mensajeFlash['mensaje'] = "Lo sentimos, hubo un problema con el restaurante, probablemente cerró antes de que completaras tu pedido o fue desactivado por alguna razón. Te invitamos a buscar más opciones en UKI&reg;";
            $mensajeFlash['accion'] = 'alert-danger';
            $request->session()->flash('mensajeFlash', $mensajeFlash);
            return redirect(route('food.home'));
        }

        //Procesando pedido
        if($pedido->forma_pago == \App\Models\Pedido::PEDIDO_PAGO_MP){
            //Creando boton de pago
            SDK::setAccessToken($pedido->negocio->config_mercadopago->access_token);

            $preference = new Preference();

            /* forma de generar ticket en mercadopago con el detalle de los productios
            // Crea un ítem en la preferencia
            $preferenceItems = [];
            foreach($pedido->items as $pedidoItem){
                $item = new Item();
                $item->title = "UKI: {$pedidoItem->nombre} en {$pedido->negocio->nombre}";
                $item->quantity = $pedidoItem->cantidad;
                $item->unit_price = $pedidoItem->costo_unitario;
                $preferenceItems[] = $item;
            }

            //agregando envio
            */
            $item = new Item();
            $item->title = "Pedido Uki en {$pedido->negocio->nombre}, envío {$pedido->envio}";
            $item->quantity = 1;
            $item->unit_price = $pedido->gran_total;
            $preferenceItems = [$item];

            $preference->items = $preferenceItems;

            $payer = new Payer();
            $payer->name = $pedido->nombre;
            $payer->surname = $pedido->nombre;
            $payer->email = $pedido->email;
            $preference->payer = $payer;

            $preference->back_urls = [
                "success"=> route('food.pedido.seguimiento', base64_encode($pedido->id)),
                "failure"=> route('food.pedido.seguimiento', base64_encode($pedido->id)),
            ];

            $preference->notification_url = route('pedido.procesar.mp', $pedido->id);

            //moo binario para recibir respuesta instantanea: aceptado o fallido
            $preference->binary_mode = true;

            $preference->payment_methods = [
                "excluded_payment_types" => [
                    ["id" => "ticket"],
                    ["id" => "prepaid_card"],
                    ["id" => "atm"],
                    ["id" => "digital_currency"],
                    ["id" => "digital_wallet"]
                ],
                "installments" => 1
            ];

            $preference->save();
            $pedido->status = Pedido::PEDIDO_STATUS_PROCESANDO_PAGO;
            $pedido->save();
            return redirect($preference->init_point);
        } else {
            $pedido->status = \App\Models\Pedido::PEDIDO_STATUS_CONFIRMADO;
            $pedido->save();
            $pedido->notificar();
            CarritoController::clear_cart($request);
            return redirect(route('food.pedido.seguimiento', base64_encode($pedido->id)));
        }

    }
}
