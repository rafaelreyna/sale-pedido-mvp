<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Negocio;
use Illuminate\Support\Facades\Auth;

class AdminBaseController extends Controller
{
    public $_user;
    public $_negocio;
    public $_config;

    public function __construct()
    {
        setlocale(LC_ALL, 'es_ES');
        $this->middleware(function ($request, $next) {
            $this->_user = Auth::user();
            if($this->_user){
                if($this->_user->tipo != 'superadmin'){
                    $this->_negocio = $this->_user->negocio;
                    $this->_config = $this->_negocio->config;
                } else { //superadmin
                    if($request->negocio_id) {
                        session()->put('su_negocio_id', $request->negocio_id);
                    }
                    if (session()->has('su_negocio_id') && session('su_negocio_id')) {
                        $negocio = Negocio::find(session('su_negocio_id'));
                        $this->_negocio = $negocio;
                        $this->_config = $this->_negocio->config;
                    } else {
                        $this->_negocio = null;
                        $this->_config = null;
                    }
                }
            }
            return $next($request);
        });
    }
}
