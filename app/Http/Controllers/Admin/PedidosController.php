<?php

namespace App\Http\Controllers\Admin;

use App\Models\Calificacion;
use App\Models\Horario;
use App\Models\Negocio;
use App\Models\Pedido;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class PedidosController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pedidos = $this->_negocio->pedidos_procesar;
        $pedidos_atendidos = $this->_negocio->pedidos_atendidos;
        $viewdata = [
            'pedidos' => $pedidos,
            'pedidos_atendidos' => $pedidos_atendidos
        ];
        return view('admin.pedidos.listado', $viewdata);
    }

    public function historial()
    {
        $pedidos = $this->_negocio->analytics_pedidos();
        $viewdata = [
            'pedidos' => $pedidos
        ];
        return view('admin.pedidos.historial', $viewdata);
    }

    public function historial_admin()
    {
        $pedidos = Pedido::analytics_superadmin();
        $viewdata = [
            'pedidos' => $pedidos
        ];
        return view('admin.pedidos.historial_superadmin', $viewdata);
    }

    public function historial_detallado()
    {
        $pedidos = $this->_negocio->analytics();
        $viewdata = [
            'pedidos' => $pedidos
        ];
        return view('admin.pedidos.historial_detallado', $viewdata);
    }

    public function actualizar_status(Request $request, $pedido_id, $status)
    {
        $pedido = Pedido::findOrFail($pedido_id);
        $pedido->status = $status;

        if($status == \App\Models\Pedido::PEDIDO_STATUS_EN_CAMINO){
            $pedido->notificarTiempoEnvio();
        }

        if($status == \App\Models\Pedido::PEDIDO_STATUS_CANCELADO){
            $request->validate(['razon'=>'required|string|min:50']);
            $razon = $request->post('razon');
            $pedido->notificarCancelacion($razon);
            $pedido->cancelar_pago();
            $pedido->gran_total = 0;
            $pedido->costo_envio = 0;
        }

        $pedido->save();


        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['mensaje'] = "Orden #{$pedido->id} actualizado correctamente";
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);
        return redirect(URL::to('/admin/pedidos?cancel_not=true'));
    }

    public function show_public($pedido_id)
    {
        $pedido = Pedido::findOrFail($pedido_id);
        return ['status' => $pedido->status];
    }

    public static function limpiar_pedidos(){
        $hace_2_horas = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' - 3 hours'));

        $modificados_pedidos = Pedido::where('status', \App\Models\Pedido::PEDIDO_STATUS_POR_CALIFICAR)
            ->where('updated_at', '<', $hace_2_horas)->get();

        foreach($modificados_pedidos as $pedido){
            $calificacion_default = new Calificacion();
            $calificacion_default->calificacion = 5;
            $pedido->calificar($calificacion_default);
        }

        return count($modificados_pedidos);
    }

    public static function cerrar_pedidos(){
        $hace_2_hora = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' - 1 hour'));

        $modificados_pedidos = Pedido::where('status', \App\Models\Pedido::PEDIDO_STATUS_PREPARANDO)
            ->where('updated_at', '<', $hace_2_hora)->get();

        foreach($modificados_pedidos as $pedido){
            $calificacion_default = new Calificacion();
            $calificacion_default->calificacion = 3;
            $pedido->calificar($calificacion_default);
        }

        return count($modificados_pedidos);
    }

    public static function recordar_calificar(){
        $hace_2_horas = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' - 2 hours'));

        $modificados_pedidos = Pedido::where('status', \App\Models\Pedido::PEDIDO_STATUS_EN_CAMINO)
            ->where('updated_at', '<', $hace_2_horas)->get();

        foreach($modificados_pedidos as $pedido){
            $pedido->notificarCalificar();
        }

        return count($modificados_pedidos);
    }

    public function pedidos_entrantes($negocio_id)
    {
        $negocio = Negocio::findOrFail($negocio_id);
        $pedidos = $negocio->pedidos_nuevos;
        return $pedidos;
    }
}
