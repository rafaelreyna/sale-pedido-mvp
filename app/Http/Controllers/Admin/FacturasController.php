<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Factura;
use Illuminate\Http\Request;

class FacturasController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facturas = $this->_negocio->facturas()->orderBy('created_at', 'DESC')->get();
        $viewdata = [
            'facturas' => $facturas
        ];
        return view('admin.facturas.listado', $viewdata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function show(Factura $factura)
    {
        $viewdata = [
            'factura' => $factura
        ];
        return view('admin.facturas.detalle', $viewdata);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function edit(Factura $factura)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Factura $factura)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function destroy(Factura $factura)
    {
        //
    }

    public function bajar_desglose($factura_id)
    {
        $factura = Factura::findOrFail($factura_id);
        $fileName = "factura{$factura->id}_desglose.csv";

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('Numero_orden', 'Fecha', 'Status', 'Total', 'Costo_envio', 'Gran_Total');

        $callback = function() use($factura, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($factura->transacciones() as $transaccion) {
                $row['Numero_orden']  = $transaccion->id;
                $row['Fecha']    = date('d-m-Y H:i', strtotime($transaccion->created_at));
                $row['Status']  = $transaccion->status;
                $row['Total']  = $transaccion->total;
                $row['Costo_envio']  = $transaccion->costo_envio;
                $row['Gran_total']  = $transaccion->gran_total;

                fputcsv($file, array($row['Numero_orden'], $row['Fecha'], $row['Status'], $row['Total'], $row['Costo_envio'], $row['Gran_total']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
