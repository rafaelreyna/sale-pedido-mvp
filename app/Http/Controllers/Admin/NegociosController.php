<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\StringHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriasUpdate;
use App\Http\Requests\NegocioUpdate;
use App\Http\Requests\NegocioUpdateEnvios;
use App\Http\Requests\NegocioUpdateSlug;
use App\Http\Requests\NegocioUpdateTiempo;
use App\Http\Requests\NotificacionesUpdate;
use App\Models\Enviosconfig;
use App\Models\Horario;
use App\Models\Negocio;
use App\Models\Notificacion;
use App\Models\Pago\Config_pago_mercadopago;
use App\Models\Pago\Config_pago_offline;
use App\Models\Restaurantes\Platillocategoria;
use App\Models\Restaurantes\Restaurantecategoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class NegociosController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $negocios = Negocio::where('verificado', true)
            ->get();
        $viewdata = [
            'negocios' => $negocios
        ];
        return view('superadmin.negocios.listado', $viewdata);
    }

    public function solicitudes()
    {
        $negocios = Negocio::where('verificado', false)->orWhereNull('verificado')
            ->get();
        $viewdata = [
            'negocios' => $negocios
        ];
        return view('superadmin.negocios.solicitudes', $viewdata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Negocio  $negocio
     * @return \Illuminate\Http\Response
     */
    public function show(Negocio $negocio)
    {
        $viewdata = [
            'negocio' => $negocio
        ];
        return view('admin.negocios.info', $viewdata);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Negocio  $negocio
     * @return \Illuminate\Http\Response
     */
    public function edit(Negocio $negocio)
    {
        if (!$negocio->verificado || \Illuminate\Support\Facades\Auth::user()->tipo == 'superadmin') {
            $titulo = 'Completar información del negocio';
        } else {
            $titulo = 'Datos de contacto de tu negocio';
        }
        $viewdata = [
            'titulo' => $titulo,
            'callToAction' => 'Guardar',
            'negocio' => $negocio,
            'route' => route('admin.negocios.update', $negocio->id),
        ];

        if (Auth::user()->tipo == 'superadmin') {
            $viewdata['route'] .= "?negocio_id=" . $this->_negocio->id;
        }

        return view('admin.negocios.formulario', $viewdata);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Negocio  $negocio
     * @return \Illuminate\Http\Response
     */
    public function update(NegocioUpdate $request, Negocio $negocio)
    {
        $data = $request->except(
            '_token',
            '_method',
            'constitucion_comprobante_domicilio',
            'constitucion_identificacion_oficial',
            'constitucion_identificacion_oficial_reverso',
            'constitucion_acta',
            'logo',
            'return',
            'validate',
            'verificado'
        );

        $negocio->update($data);

        //subiendo logo
        if ($request->hasFile('logo')) {
            $image      = $request->file('logo');
            $nombreArchivo   = "logo{$negocio->id}_". time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->stream();

            if($negocio->logo){
                Storage::delete("public/negocios/logos/{$negocio->logo}");
            }

            Storage::disk('local')->put('public/negocios/logos/'.$nombreArchivo, $img);
            $negocio->logo = $nombreArchivo;
            $negocio->save();
        }

        //subiendo documentos
        self::_subirDocumento($negocio, 'constitucion_comprobante_domicilio', $request);
        self::_subirDocumento($negocio, 'constitucion_identificacion_oficial', $request);
        self::_subirDocumento($negocio, 'constitucion_identificacion_oficial_reverso', $request);
        self::_subirDocumento($negocio, 'constitucion_acta', $request);


        if(Auth::user()->tipo == 'superadmin' || (Auth::user()->negocio && Auth::user()->negocio->verificado)) {
            $mensajeFlash['mensaje'] = "Negocio {$negocio->nombre} actualizado correctamente";
        } else {
            $mensajeFlash['mensaje'] = "Negocio {$negocio->nombre} actualizado correctamente. Accede al dashboard para verificar qué información falta a tu restaruante";
        }
        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        if($request->post('return') == 'back'){
            return back();
        } else {
            return redirect(route('admin.negocios.edit', $negocio->id));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Negocio  $negocio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Negocio $negocio)
    {
        //
    }

    public function envios(Request $request, $negocio_id){
        $negocio = Negocio::findOrFail($negocio_id);
        $config_envios = $negocio->enviosconfig;
        if(!$config_envios){
            Enviosconfig::create(['negocio_id' => $negocio->id]);
            $negocio = Negocio::findOrFail($negocio_id);
            $negocio->enviosconfig->radio_max = $negocio->region->radio_max_negocios;
        }
        if(is_null($negocio->enviosconfig->radio_max)) {
            $negocio->enviosconfig->radio_max = 0;
            $negocio->enviosconfig->centro_lat = $negocio->lat;
            $negocio->enviosconfig->centro_lon = $negocio->lon;
        }
        $viewdata = [
            'titulo' => 'Configurar envíos',
            'callToAction' => 'Guardar',
            'negocio' => $negocio,
            'route' => route('admin.negocios.envios.update', $negocio->id),
        ];

        return view('admin.negocios.envios', $viewdata);
    }

    public function envios_update(NegocioUpdateEnvios $request, $negocio_id)
    {
        $negocio = Negocio::findOrFail($negocio_id);
        $data = $request->except(
            '_token',
            '_method'
        );

        if($data['tipo_envio'] == 'null'){
            $data['tipo_envio'] = null;
        }

        $negocio->enviosconfig->update($data);

        if(Auth::user()->tipo == 'superadmin' || (Auth::user()->negocio && Auth::user()->negocio->verificado)) {
            $mensajeFlash['mensaje'] = "Configuración de envío de {$negocio->nombre} actualizado correctamente";
        } else {
            $mensajeFlash['mensaje'] = "Configuración de envío de {$negocio->nombre} actualizado correctamente. Accede al dashboard para verificar qué información falta a tu restaruante";
        }
        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.negocios.envios.edit', $negocio->id));
    }

    public function tiempo_update(NegocioUpdateTiempo $request, $negocio_id)
    {
        $negocio = Negocio::findOrFail($negocio_id);
        $data = $request->except(
            '_token',
            '_method'
        );

        $negocio->config->update($data);

        $mensajeFlash['mensaje'] = "Configuración de envío de {$negocio->nombre} actualizado correctamente";
        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return back();
    }


    public function slug_update(NegocioUpdateSlug $request, $negocio_id)
    {
        $negocio = Negocio::findOrFail($negocio_id);

        $negocio->config->update(
            [
                'slug' => StringHelper::slug($request->slug)
            ]
        );

        $mensajeFlash['mensaje'] = "Configuración de URL amigable de {$negocio->nombre} actualizada correctamente";
        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return back();
    }


    public function tiempo_delete(Request $request, $negocio_id)
    {
        $negocio = Negocio::findOrFail($negocio_id);
        $data = ['tiempo_estimado' => null];
        $negocio->config->update($data);

        $mensajeFlash['mensaje'] = "Configuración de envío de {$negocio->nombre} actualizado correctamente";
        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return back();
    }

    public function pago_edit(Request $request, $negocio_id){
        $negocio = Negocio::findOrFail($negocio_id);
        $config_mercadopago = $negocio->config_mercadopago;
        if(!$config_mercadopago){
            Config_pago_mercadopago::create(['negocio_id' => $negocio->id]);
        }
        $config_offline = $negocio->config_offline;
        if(!$config_offline){
            Config_pago_offline::create(['negocio_id' => $negocio->id]);
        }
        $negocio->load('config_offline','config_mercadopago');
        $viewdata = [
            'titulo' => 'Configurar opciones de pago',
            'callToAction' => 'Guardar',
            'negocio' => $negocio,
            'route' => route('admin.negocios.pago.update', $negocio->id),
        ];
        return view('admin.negocios.pago.mercadopago', $viewdata);
    }

    public function pago_update(Request $request, $negocio_id)
    {
        $negocio = Negocio::findOrFail($negocio_id);
        $config_relation = $request->input('config_relation');
        $data = $request->except(
            '_token',
            '_method',
            'config_relation'
        );

        $negocio->$config_relation->update($data);

        if(Auth::user()->tipo == 'superadmin' || (Auth::user()->negocio && Auth::user()->negocio->verificado)) {
            $mensajeFlash['mensaje'] = "Configuración de pago de {$negocio->nombre} actualizado correctamente";
        } else {
            $mensajeFlash['mensaje'] = "Configuración de pago de {$negocio->nombre} actualizado correctamente. Accede al dashboard para verificar qué información falta a tu restaruante";
        }

        if ($negocio->tiene_metodo_pago()) {
            $mensajeFlash['titulo'] = 'Todo bien';
            $mensajeFlash['accion'] = 'success';
            $request->session()->flash('mensajeFlash', $mensajeFlash);
        } else {
            $mensajeFlash['titulo'] = 'Error';
            $mensajeFlash['accion'] = 'error';
            $mensajeFlash['mensaje'] = 'Es necesario configurar al menos un método de pago para poder vender';
            $request->session()->flash('mensajeFlash', $mensajeFlash);
        }

        return redirect(route('admin.negocios.pago.edit', $negocio->id));
    }

    public function categorias_edit(Request $request, $negocio_id){
        $categorias = Restaurantecategoria::orderBy('nombre', 'ASC')->get();
        $negocio = Negocio::findOrFail($negocio_id);
        $viewdata = [
            'titulo' => 'Selecciona una o varias categorías para tu Negocio',
            'callToAction' => 'Guardar',
            'negocio' => $negocio,
            'categorias' => $categorias,
            'route' => route('admin.restaurantes.categorias.update', $negocio->id),
        ];
        return view('admin.restaurantes.categorias.formulario', $viewdata);
    }

    public function categorias_update(CategoriasUpdate $request, $negocio_id)
    {
        $negocio = Negocio::findOrFail($negocio_id);
        $data = $request->except(
            '_token',
            '_method'
        );

        $negocio->restaurante_categorias()->sync($data['categorias']);

        if(Auth::user()->tipo == 'superadmin' || (Auth::user()->negocio && Auth::user()->negocio->verificado)) {
            $mensajeFlash['mensaje'] = "Negocio {$negocio->nombre} actualizado correctamente";
        } else {
            $mensajeFlash['mensaje'] = "Negocio {$negocio->nombre} actualizado correctamente. Accede al dashboard para verificar qué información falta a tu restaruante";
        }
        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.restaurantes.categorias.edit', $negocio->id));
    }

    public function notificaciones_edit(Request $request, $negocio_id){
        $negocio = Negocio::findOrFail($negocio_id);
        $viewdata = [
            'titulo' => 'Configuración de notificaciones',
            'callToAction' => 'Guardar',
            'negocio' => $negocio,
            'usuarios' => $negocio->usuarios,
            'route' => route('admin.negocios.notificaciones.update', $negocio->id),
        ];
        return view('admin.negocios.notificaciones', $viewdata);
    }

    public function notificaciones_update(NotificacionesUpdate $request, $negocio_id)
    {
        $negocio = Negocio::findOrFail($negocio_id);
        $data = $request->except(
            '_token',
            '_method'
        );
        $config = $negocio->config;
        if ($data['notificacion_pedido_sms'] == 'null'){
            $config->notificacion_pedido_sms = null;
        } else {
            $config->notificacion_pedido_sms = [$data['notificacion_pedido_sms']];
        }

        if (!isset($data['notificacion_pedido_mail'])){
            $config->notificacion_pedido_mail = null;
        } else {
            $config->notificacion_pedido_mail = $data['notificacion_pedido_mail'];
        }

        if (!isset($data['notificacion_pedido_call'])){
            $config->notificacion_pedido_call = null;
        } else {
            $config->notificacion_pedido_call = [$data['notificacion_pedido_call']];
        }

        $config->save();

        $mensajeFlash['mensaje'] = "Negocio {$negocio->nombre} actualizado correctamente";
        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.negocios.notificaciones.edit', $negocio->id));
    }

    public function verificar(NegocioUpdateEnvios $request, $negocio_id)
    {
        $negocio = Negocio::findOrFail($negocio_id);

        $negocio->verificado = true;
        $negocio->save();

        //generando notificacion para el usuario
        $notificacion = new Notificacion();
        $notificacion->user_id = $negocio->admin()->id;
        $notificacion->url = route('admin');
        $notificacion->subject = 'Tu negocio fue verificado!!!';
        $notificacion->message = "<h3>¡Felicidades!</h3>";
        $notificacion->message .= "<p>Nos complace informarte que tu tienda UKI&reg; ha sido aprobada y se encuentra en línea</p>";
        $notificacion->message .= "<p>Recuerda que es importante avisar a tus clientes que hagan sus pedidos en tu tienda UKI&reg para que ellos ¡paguen menos y tu ganes más!</p>";
        $notificacion->short_message = "Felicidades, tu negocio fue verificado en UKI, accede en: ";
        $notificacion->call_to_action = "Accede al panel de administración";
        $notificacion->save();
        $notificacion->sendMail();
        $notificacion->sendSms();

        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['mensaje'] = "Configuración de envío de  {$negocio->nombre} actualizada correctamente";
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('superadmin.negocios.solicitudes'));
    }



    private static function _subirDocumento($negocio, $documento, $request){
        $documento_simple = str_replace('constitucion_', '', $documento);
        if ($request->hasFile($documento)) {
            $file = $request->file($documento);
            $nombreArchivo   = "negocio{$negocio->id}_{$documento_simple}_". time() . '.' . $file->getClientOriginalExtension();
            //dd($nombreArchivo);
            if(isset($negocio->$documento) && $negocio->$documento){
                //conservando historial
                //Storage::delete("negocios_data/documentos/{$negocio->id}/{$negocio->$documento}");
                Storage::move(
                    "negocios_data/documentos/{$negocio->id}/{$negocio->$documento}",
                    "negocios_data/documentos/{$negocio->id}/_historial/{$negocio->$documento}"
                );
            }
            //dd("negocios_data/documentos/{$negocio->id}/".$nombreArchivo);
            Storage::disk('local')->put("negocios_data/documentos/{$negocio->id}/".$nombreArchivo, file_get_contents($file));
            $negocio->$documento = $nombreArchivo;
            $negocio->save();
        }
    }

    public static function apertura_cierre(){
        $horarios_apertura = Horario::where('dia', '=', intval(date('w')))
            ->where('inicio', '=', date('H:i').':00')
            ->get();

        $horarios_cierre = Horario::where('dia', '=', intval(date('w')))
            ->where('fin', '=', date('H:i').':00')
            ->get();

        foreach($horarios_apertura as $horario){
            try {
                $horario->negocio->activo = 1;
                $horario->negocio->save();
                Log::info("Negocio abre: {$horario->negocio->nombre}");
            } catch (\Exception $e) {
                Log::error("XXX Error de apertura horario {$horario->id}: -- {$e->getMessage()} -- {$e->getFile()} -- {$e->getLine()} XXXX");
            }
        }

        foreach($horarios_cierre as $horario){
            try {
                $horario->negocio->activo = 0;
                $horario->negocio->save();
                Log::info("Negocio cierra: {$horario->negocio->nombre}");
            } catch (\Exception $e) {
                Log::error("XXX Error de cierre de horario {$horario->id}: -- {$e->getMessage()} -- {$e->getFile()} -- {$e->getLine()} XXXX");
            }
        }
    }

    public static function revision_verificados() {
        $verificaciones = 0;
        $negocios = Negocio::where('verificado', false)->orWhereNull('verificado')->get();
        foreach ($negocios as $negocio) {
            if ($negocio->pendiente_verificacion()) {
                $verificaciones ++;
            }
        }
        return $verificaciones;
    }

}
