<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pedido;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        setlocale(LC_ALL, 'es_ES');
        Carbon::setLocale('es');

        if($this->_user->tipo == 'superadmin' && !$request->get('negocio_id')){
            return redirect(route('superadmin.negocios.index'));
        }

        if($this->_negocio->verificado){
            if($this->_user->tipo == 'operador'){
                return redirect(route('admin.pedidos.index'));
            } else {
                $data = [
                    'negocio' => $this->_negocio
                ];
                return view('dashboard', $data);
            }
        } else {
            return view('admin.negocios.verificando', ['negocio' => $this->_negocio]);
        }
    }

}
