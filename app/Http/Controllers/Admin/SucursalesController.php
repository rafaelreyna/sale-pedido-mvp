<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Negocio;
use App\Models\Region;
use App\Models\Sucursal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class SucursalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($negocio_id)
    {
        $negocio = Negocio::findOrFail($negocio_id);
        $user = Auth::user();
        $viewdata = [
            'sucursales' => $user->sucursales,
            'negocio' => $negocio,
        ];
        return view('admin.negocios.sucursales.listado', $viewdata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($negocio_id)
    {
        $negocio = Negocio::findOrFail($negocio_id);
        $user = Auth::user();
        $sucursal = new Sucursal();
        $viewdata = [
            'titulo' => 'Agregar sucursal',
            'callToAction' => 'Agregar',
            'sucursal' => $sucursal,
            'sucursales' => $user->sucursales,
            'regiones' => Region::all(),
            'negocio' => $negocio,
            'user' => $user,
            'route' => route('admin.negocios.sucursales.store', $negocio->id),
        ];
        return view('admin.negocios.sucursales.formulario', $viewdata);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $negocio_id)
    {
        $user = Auth::user();
        $data = $request->except('_token, _method');
        $negocio = Negocio::findOrFail($data['negocio_id']);

        if ($user->sucursales->count() == 0) {
            $sucursal = new Sucursal();
            $sucursal->user_id = $user->id;
            $sucursal->negocio_id = $negocio_id;
            $sucursal->save();
        }

        $sucursal_negocio = new Negocio();
        // Agregando nombre personalizado
        $sucursal_negocio->nombre = $data['nombre'];
        $sucursal_negocio->verificado = false;
        $sucursal_negocio->tipo = $negocio->tipo;
        $sucursal_negocio->logo = $negocio->logo; // TODO copiar logo
        $sucursal_negocio->calificacion = 5;
        $sucursal_negocio->region_id = $data['region_id'];
        $sucursal_negocio->adeudo = false;

        $sucursal_negocio->save();

        $sucursal = new Sucursal();
        $sucursal->user_id = $user->id;
        $sucursal->negocio_id = $sucursal_negocio->id;
        $sucursal->save();



        if (isset($data['productos_categorias']) && $data['productos_categorias']) {
            //copiando todos los productos
            foreach ($negocio->platillo_categorias as $categoria) {
                $nueva_categoria = $categoria->replicate();
                $nueva_categoria->negocio_id = $sucursal_negocio->id;
                $nueva_categoria->save();

                //copiando imagen
                $extension = explode('.',$categoria->foto)[1];
                $nombreArchivo   = "platillo{$nueva_categoria->id}_". time() . '.' . $extension;
                Storage::copy('public/restaurantes/platillocategorias/'.$categoria->foto, 'public/restaurantes/platillocategorias/'.$nombreArchivo);
                $nueva_categoria->foto = $nombreArchivo;

                $nueva_categoria->save();
                foreach ($categoria->platillos() as $platillo) {
                    $nuevo_platillo = $platillo->replicate();
                    $nuevo_platillo->negocio_id = $sucursal_negocio->id;
                    $nuevo_platillo->categoria_id = $nueva_categoria->id;
                    $nuevo_platillo->save();

                    //copiando imagen
                    $extension = explode('.',$platillo->foto)[1];
                    $nombreArchivo   = "platillo{$nuevo_platillo->id}_". time() . '.' . $extension;
                    Storage::copy('public/restaurantes/platillos/'.$platillo->foto, 'public/restaurantes/platillos/'.$nombreArchivo);
                    $nuevo_platillo->foto = $nombreArchivo;
                    $nuevo_platillo->save();
                }
            }
        }

        return redirect(route('admin.negocios.sucursales.index', $negocio_id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function select(Request $request, $negocio_id, $sucursal_id)
    {
        $user = Auth::user();
        $sucursal = Sucursal::findOrFail($sucursal_id);
        $user->negocio_id = $sucursal->negocio->id;
        $user->save();

        return redirect(route('admin'));
    }

    public function select_sucursal(Request $request, $negocio_id)
    {
        $user = Auth::user();
        $data = $request->all();
        $sucursal = Sucursal::findOrFail($data['sucursal_id']);
        $user->negocio_id = $sucursal->negocio->id;
        $user->save();

        return redirect(route('admin'));
    }
}
