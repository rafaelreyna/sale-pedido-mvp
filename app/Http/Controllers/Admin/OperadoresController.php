<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OperadorStore;
use App\Http\Requests\OperadorUpdate;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class OperadoresController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $operadores = User::where('negocio_id', '=', $this->_negocio->id)
            ->where('tipo','=','operador')
            ->get();
        $viewdata = [
            'operadores' => $operadores
        ];
        return view('admin.operadores.listado', $viewdata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $operador = new User();
        $operador->tipo = 'operador';
        $viewdata = [
            'titulo' => 'Agregar operador',
            'callToAction' => 'Agregar',
            'operador' => $operador,
            'route' => route('admin.operadores.store'),
        ];
        return view('admin.operadores.formulario', $viewdata);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OperadorStore $request)
    {
        $data = $request->except('_token, _method');
        $data['negocio_id'] = $this->_negocio->id;
        $operador = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'celular' => $data['celular'],
            'negocio_id' => $data['negocio_id'],
            'tipo' => 'operador',
            'password' => Hash::make($data['password']),
        ]);

        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['mensaje'] = "Operador {$operador->nombre} creado correctamente";
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.operadores.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Restaurantes\Operador  $operador
     * @return \Illuminate\Http\Response
     */
    public function show(Operador $operador)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $operador
     * @return \Illuminate\Http\Response
     */
    public function edit($operador_id)
    {
        $operador = User::findOrFail($operador_id);

        $viewdata = [
            'titulo' => 'Editar operador',
            'callToAction' => 'Guardar',
            'operador' => $operador,
            'route' => route('admin.operadores.update', $operador->id),
        ];
        return view('admin.operadores.formulario', $viewdata);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $operador
     * @return \Illuminate\Http\Response
     */
    public function update(OperadorUpdate $request, $operador_id)
    {
        $operador = User::findOrFail($operador_id);

        $data = $request->except('_token, _method','foto');
        $operador->update($request->only(['name', 'celular']));
        if (isset($data['password'])) {
            $operador->password = Hash::make($data['password']);
            $operador->save();
        }

        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['mensaje'] = "Operador {$operador->nombre} editado correctamente";
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.operadores.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Restaurantes\Operador  $operador
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $operador_id)
    {
        $operador = User::findOrFail($operador_id);

        $operador->delete();

        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['mensaje'] = "Operador {$operador->nombre} eliminado correctamente";
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.operadores.index'));
    }
}
