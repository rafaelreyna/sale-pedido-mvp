<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\HorarioStore;
use App\Models\Horario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HorariosController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $horarios = Horario::where('negocio_id', '=', $this->_negocio->id)->get();
        $viewdata = [
            'horarios' => $horarios
        ];
        return view('admin.horarios.listado', $viewdata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $horario = new Horario();
        $viewdata = [
            'titulo' => 'Agregar horario',
            'callToAction' => 'Agregar',
            'horario' => $horario,
            'route' => route('admin.horarios.store'),
        ];
        return view('admin.horarios.formulario', $viewdata);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HorarioStore $request)
    {
        $data = $request->except('_token, _method','foto');
        foreach($data['dia'] as $dia => $true){
            $data_horario = [
                "inicio" => $data['inicio'],
                "fin" => $data['fin'],
                "dia" => $dia,
                'negocio_id' => $this->_negocio->id
            ];
            Horario::create($data_horario);
        }

        if(Auth::user()->tipo == 'superadmin' || (Auth::user()->negocio && Auth::user()->negocio->verificado)) {
            $mensajeFlash['mensaje'] = "Horario creado correctamente";
        } else {
            $mensajeFlash['mensaje'] = "Horario creado correctamente. Accede al dashboard para verificar qué información falta a tu restaruante";
        }
        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.horarios.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Horario  $horario
     * @return \Illuminate\Http\Response
     */
    public function show(Horario $horario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Horario  $horario
     * @return \Illuminate\Http\Response
     */
    public function edit(Horario $horario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Horario  $horario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Horario $horario)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Horario  $horario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Horario $horario)
    {
        $horario->delete();

        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['mensaje'] = "Horario eliminado correctamente";
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.horarios.index'));
    }
}
