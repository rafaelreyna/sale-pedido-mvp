<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Region;
use Illuminate\Http\Request;

class RegionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regiones = Region::all();
        $viewdata = [
            'regiones' => $regiones
        ];
        return view('admin.regiones.listado', $viewdata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $region = new Region();
        $viewdata = [
            'titulo' => 'Agregar región',
            'callToAction' => 'Agregar',
            'region' => $region,
            'route' => route('superadmin.regiones.store'),
        ];
        return view('admin.regiones.formulario', $viewdata);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token, _method','foto');
        $region = Region::create($data);

        $mensajeFlash['mensaje'] = "Region {$region->nombre} creada correctamente";
        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('superadmin.regiones.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function show(Region $region)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function edit($region_id)
    {
        $region = Region::findOrFail($region_id);
        $viewdata = [
            'titulo' => 'Editar región',
            'callToAction' => 'Guardar',
            'region' => $region,
            'route' => route('superadmin.regiones.update', $region->id),
        ];
        return view('admin.regiones.formulario', $viewdata);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $region_id)
    {
        $region = Region::findOrFail($region_id);
        $data = $request->except('_token, _method','foto');
        $region->update($data);

        $mensajeFlash['mensaje'] = "Region {$region->nombre} actualizada correctamente";
        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('superadmin.regiones.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $region_id)
    {
        $region = Region::findOrFail($region_id);
        $region->delete();

        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['mensaje'] = "Region {$region->nombre} eliminada correctamente";
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('superadmin.regiones.index'));
    }
}
