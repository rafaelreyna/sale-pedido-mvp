<?php

namespace App\Http\Controllers\Admin\Restaurantes;

use App\Http\Controllers\Admin\AdminBaseController;
use App\Http\Requests\Restaurantes\PlatilloStore;
use App\Http\Requests\Restaurantes\PlatilloUpdate;
use App\Models\Restaurantes\Platillo;
use App\Models\Restaurantes\Platillocategoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PlatillosController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        Platillo::setNewOrder(Platillo::getAllIds($this->_negocio->id));
        $platillos = Platillo::ordered()->where('negocio_id', '=', $this->_negocio->id)->get();
        $viewdata = [
            'platillos' => $platillos
        ];
        return view('admin.restaurantes.platillos.listado', $viewdata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $platillo = new Platillo();
        $platillos = Platillocategoria::where('negocio_id', '=', $this->_negocio->id)->get();
        $viewdata = [
            'titulo' => 'Agregar platillo',
            'callToAction' => 'Agregar',
            'platillo' => $platillo,
            'categorias' => $platillos,
            'route' => route('admin.platillos.store'),
        ];
        return view('admin.restaurantes.platillos.formulario', $viewdata);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlatilloStore $request)
    {
        $data = $request->except('_token, _method','foto');
        $data['negocio_id'] = $this->_negocio->id;

        if(isset($data['disponible']) && $data['disponible']) {
            $data['disponible'] = true;
        } else {
            $data['disponible'] = false;
        }

        $platillo = Platillo::create($data);

        if ($request->hasFile('foto')) {
            $image      = $request->file('foto');
            $nombreArchivo   = "platillo{$platillo->id}_". time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream();

            Storage::disk('local')->put('public/restaurantes/platillos/'.$nombreArchivo, $img);

            $platillo->foto = $nombreArchivo;
            $platillo->save();
        }

        if(Auth::user()->tipo == 'superadmin' || (Auth::user()->negocio && Auth::user()->negocio->verificado)) {
            $mensajeFlash['mensaje'] = "Platillo {$platillo->nombre} creado correctamente";
        } else {
            $mensajeFlash['mensaje'] = "Platillo {$platillo->nombre} creado correctamente. Accede al dashboard para verificar qué información falta a tu restaruante";
        }
        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.platillos.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Restaurantes\Platillo  $platillo
     * @return \Illuminate\Http\Response
     */
    public function show(Platillo $platillo)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Restaurantes\Platillo  $platillo
     * @return \Illuminate\Http\Response
     */
    public function edit(Platillo $platillo)
    {
        $platillos = Platillocategoria::where('negocio_id', '=', $this->_negocio->id)->get();
        $viewdata = [
            'titulo' => 'Editar platillo',
            'callToAction' => 'Guardar',
            'platillo' => $platillo,
            'categorias' => $platillos,
            'route' => route('admin.platillos.update', $platillo->id),
        ];
        return view('admin.restaurantes.platillos.formulario', $viewdata);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Restaurantes\Platillo  $platillo
     * @return \Illuminate\Http\Response
     */
    public function update(PlatilloUpdate $request, Platillo $platillo)
    {
        $data = $request->except('_token, _method','foto');

        if(isset($data['disponible']) && $data['disponible']) {
            $data['disponible'] = true;
        } else {
            $data['disponible'] = false;
        }

        $platillo->update($data);

        if ($request->hasFile('foto')) {
            $image      = $request->file('foto');
            $nombreArchivo   = "platillo{$platillo->id}_". time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->stream();

            Storage::delete("public/restaurantes/platillos/{$platillo->foto}");

            Storage::disk('local')->put('public/restaurantes/platillos/'.$nombreArchivo, $img);
            $platillo->foto = $nombreArchivo;
            $platillo->save();
        }

        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['mensaje'] = "Platillo {$platillo->nombre} editado correctamente";
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.platillos.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Restaurantes\Platillo  $platillo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Platillo $platillo)
    {
        $platillo->delete();

        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['mensaje'] = "Platillo {$platillo->nombre} eliminado correctamente";
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.platillos.index'));
    }

    public function order(Request $request, $platillo_id, $action) {
        $platillo = Platillo::findOrFail($platillo_id);

        switch ($action) {
            case 'up': $platillo->moveOrderUp(); break;
            case 'down': $platillo->moveOrderDown(); break;
        }

        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['mensaje'] = "Platillo {$platillo->nombre} actualizado correctamente";
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.platillos.index'));
    }
}
