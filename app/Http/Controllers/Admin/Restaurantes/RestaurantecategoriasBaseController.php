<?php

namespace App\Http\Controllers\Admin\Restaurantes;

use App\Http\Controllers\Admin\AdminBaseController;
use App\Models\Restaurantes\Restaurantecategoria;
use Illuminate\Http\Request;

class RestaurantecategoriasBaseController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Restaurantecategoria::all();
        $viewdata = [
            'categorias' => $categorias
        ];
        return view('Admin.Restaurantes.categorias.index', $viewdata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Restaurantes\Restaurantecategoria  $restaurantecategorias
     * @return \Illuminate\Http\Response
     */
    public function show(Restaurantecategoria $restaurantecategorias)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Restaurantes\Restaurantecategoria  $restaurantecategorias
     * @return \Illuminate\Http\Response
     */
    public function edit(Restaurantecategoria $restaurantecategorias)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Restaurantes\Restaurantecategoria  $restaurantecategorias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Restaurantecategoria $restaurantecategorias)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Restaurantes\Restaurantecategoria  $restaurantecategorias
     * @return \Illuminate\Http\Response
     */
    public function destroy(Restaurantecategoria $restaurantecategorias)
    {
        //
    }
}
