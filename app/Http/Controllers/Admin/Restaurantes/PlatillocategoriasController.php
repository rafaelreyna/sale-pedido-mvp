<?php

namespace App\Http\Controllers\Admin\Restaurantes;

use App\Http\Controllers\Admin\AdminBaseController;
use App\Http\Requests\Restaurantes\PlatillocategoriaStore;
use App\Http\Requests\Restaurantes\PlatillocategoriaUpdate;
use App\Models\Restaurantes\Platillo;
use App\Models\Restaurantes\Platillocategoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PlatillocategoriasController extends AdminBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        Platillocategoria::setNewOrder(Platillocategoria::getAllIds($this->_negocio->id));
        $categorias = Platillocategoria::ordered()->where('negocio_id', '=', $this->_negocio->id)->get();
        $viewdata = [
            'categorias' => $categorias
        ];
        return view('admin.restaurantes.platillocategorias.listado', $viewdata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $categoria = new Platillocategoria();
        $viewdata = [
            'titulo' => 'Agregar categoría',
            'callToAction' => 'Agregar',
            'categoria' => $categoria,
            'route' => route('admin.platillocategorias.store'),
        ];
        return view('admin.restaurantes.platillocategorias.formulario', $viewdata);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlatillocategoriaStore $request)
    {
        $data = $request->except('_token, _method','foto');
        $data['negocio_id'] = $this->_negocio->id;
        $categoria = Platillocategoria::create($data);

        if ($request->hasFile('foto')) {
            $image      = $request->file('foto');
            $nombreArchivo   = "categoria{$categoria->id}_". time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->stream();

            Storage::delete("public/restaurantes/platillocategorias/{$categoria->foto}");

            Storage::disk('local')->put('public/restaurantes/platillocategorias/'.$nombreArchivo, $img);
            $categoria->foto = $nombreArchivo;
            $categoria->save();
        }

        if(Auth::user()->tipo == 'superadmin' || (Auth::user()->negocio && Auth::user()->negocio->verificado)) {
            $mensajeFlash['mensaje'] = "Categoría {$categoria->nombre} creada correctamente";
        } else {
            $mensajeFlash['mensaje'] = "Categoría {$categoria->nombre} creada correctamente. Accede al dashboard para verificar qué información falta a tu restaruante";
        }
        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.platillocategorias.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Restaurantes\Platillocategoria  $platillocategoria
     * @return \Illuminate\Http\Response
     */
    public function show(Platillocategoria $platillocategoria)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Restaurantes\Platillocategoria  $platillocategoria
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($platillocategoria_id)
    {
        $categoria = Platillocategoria::findOrFail($platillocategoria_id);
        $viewdata = [
            'titulo' => 'Editar categoría',
            'callToAction' => 'Guardar',
            'categoria' => $categoria,
            'route' => route('admin.platillocategorias.update', $categoria->id),
        ];
        return view('admin.restaurantes.platillocategorias.formulario', $viewdata);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Restaurantes\Platillocategoria  $platillocategoria
     * @return \Illuminate\Http\Response
     */
    public function update(PlatillocategoriaUpdate $request, $platillocategoria_id)
    {
        $categoria = Platillocategoria::findOrFail($platillocategoria_id);
        $data = $request->except('_token, _method','foto');
        $categoria->update($data);

        if ($request->hasFile('foto')) {
            $image      = $request->file('foto');
            $nombreArchivo   = "categoria{$categoria->id}_". time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->stream();

            Storage::delete("public/restaurantes/platillocategorias/{$categoria->foto}");

            Storage::disk('local')->put('public/restaurantes/platillocategorias/'.$nombreArchivo, $img);
            $categoria->foto = $nombreArchivo;
            $categoria->save();
        }

        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['mensaje'] = "Categoría {$categoria->nombre} actualizada correctamente";
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.platillocategorias.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Restaurantes\Platillocategoria  $platillocategoria
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $platillocategoria_id)
    {
        $categoria = Platillocategoria::findOrFail($platillocategoria_id);
        $categoria->delete();

        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['mensaje'] = "Categoría eliminada correctamente";
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.platillocategorias.index'));
    }

    public function order(Request $request, $categoria_id, $action) {
        $categoria = Platillocategoria::findOrFail($categoria_id);

        switch ($action) {
            case 'up': $categoria->moveOrderUp(); break;
            case 'down': $categoria->moveOrderDown(); break;
        }

        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['mensaje'] = "Categoría {$categoria->nombre} actualizada correctamente";
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.platillocategorias.index'));
    }
}
