<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminperfilUpdate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PerfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $perfil = Auth::user();
        $viewdata = [
            'perfil' => $perfil
        ];
        return view('admin.perfil.formulario', $viewdata);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdminperfilUpdate $request)
    {
        $perfil = Auth::user();
        $data = $request->except('_token, _method','password-confirm');
        $data['password'] = Hash::make($data['password']);

        if ($data['email'] != $perfil->email) {
            $perfil->email_verified_at = null;
            $perfil->save();
            $perfil->sendEmailVerificationNotification();
        }

        $perfil->update($data);

        if ($data['email'] != $perfil->email) {
            $perfil->sendEmailVerificationNotification();
        }

        $mensajeFlash['titulo'] = 'Todo bien';
        $mensajeFlash['mensaje'] = "Perfil de usuario actualizado correctamente";
        $mensajeFlash['accion'] = 'success';
        $request->session()->flash('mensajeFlash', $mensajeFlash);

        return redirect(route('admin.perfil.edit'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
