<?php

namespace App\Http\Controllers;

use App\Models\Notificacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NotificacionesController extends Controller
{
    public function call_response($notificacion_id)
    {
        $notificacion_id = Notificacion::findOrFail($notificacion_id);
        $data = [
            'notificacion' => $notificacion_id
        ];
        return response()->view('call_responses.notificacion', $data)->header('Content-Type', 'application/xml');

    }
}
