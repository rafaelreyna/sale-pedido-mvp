<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HorarioStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'inicio' => 'required|date_format:H:i',
            'fin' => 'required|date_format:H:i',
            'dia' => 'required|array',
        ];
    }

    public function messages()
    {
        return [
            'date_format' => 'El formato del horario es incorrecto'
        ];
    }
}
