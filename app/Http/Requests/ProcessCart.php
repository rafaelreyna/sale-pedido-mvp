<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProcessCart extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item' => 'array|required',
            'nombre' => 'required|string',
            'apellidos' => 'required|string',
            'email' => 'required|email',
            'telefono' => 'required|string|regex:/[0-9]{10}/',
            'direccion' => 'required|string',
            'lat' => 'required|string',
            'lon' => 'required|string',
            'tipo_envio' => 'required|string|in:domicilio,pickup',
            'comentarios_cliente' => 'nullable|string|max:150',
        ];
    }
}
