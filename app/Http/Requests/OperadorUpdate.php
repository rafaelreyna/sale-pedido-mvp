<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OperadorUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['nullable', 'string', 'max:255'],
            'celular' => ['nullable', 'string', 'regex:/[0-9]{10}/'],
            'password' => ['nullable', 'string', 'min:8', 'confirmed'],
            ];
    }
}
