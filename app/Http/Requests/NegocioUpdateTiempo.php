<?php

namespace App\Http\Requests;

use App\Helpers\GeopositionHelper;
use App\Models\Negocio;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class NegocioUpdateTiempo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tiempo_estimado' => 'numeric|required|min:1',
        ];
    }


}
