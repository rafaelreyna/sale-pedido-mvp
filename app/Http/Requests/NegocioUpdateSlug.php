<?php

namespace App\Http\Requests;

use App\Helpers\GeopositionHelper;
use App\Helpers\StringHelper;
use App\Models\Negocio;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class NegocioUpdateSlug extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => "string|required|min:5|unique:config_restaurante,slug",
        ];
    }

    public function messages()
    {
        return [
            'slug.unique' => 'Esta URL amigable ya está en uso, selecciona una diferente',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        if ($this->slug) {
            $this->merge([
                'slug' => StringHelper::slug($this->slug)
            ]);
        }
    }
}
