<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PerfilUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isset($_GET['solo_ubicacion'])) {
            return [
                'region_id' => 'nullable',
                'direccion' => 'required|string',
                'lat' => 'required',
                'lon' => 'required',
            ];
        }

        return [
            'nombre' => 'required|string',
            'apellidos' => 'required|string',
            'email' => 'required|email',
            'telefono' => 'required|string',
            'direccion' => 'required|string',
            ];
    }
}
