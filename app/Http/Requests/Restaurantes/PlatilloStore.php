<?php

namespace App\Http\Requests\Restaurantes;

use Illuminate\Foundation\Http\FormRequest;

class PlatilloStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'categoria_id' => 'required|numeric|exists:platillocategorias,id',
            'descripcion' => 'required|string',
            'nombre' => 'required|string',
            'foto' => 'required|image|max:1024',
            'precio' => 'required|numeric',
            'precio_promo' => 'numeric|nullable',
            ];
    }
}
