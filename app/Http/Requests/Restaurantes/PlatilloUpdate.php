<?php

namespace App\Http\Requests\Restaurantes;

use Illuminate\Foundation\Http\FormRequest;

class PlatilloUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'categoria_id' => 'numeric|exists:platillocategorias,id',
            'descripcion' => 'string',
            'nombre' => 'string',
            'foto' => 'image|max:1024',
            'precio' => 'numeric',
            'precio_promo' => 'numeric|nullable',
            ];
    }
}
