<?php

namespace App\Http\Requests;

use App\Helpers\GeopositionHelper;
use App\Models\Negocio;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class NegocioUpdateEnvios extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        Validator::extend('checkPosition', function ($attribute, $value, $parameters, $validator) {
            $fields = $validator->getData();
            $user = Auth::user();
            if ($user->tipo == 'superadmin') {
                $negocio = Negocio::findOrFail(session('su_negocio_id'));
            } else {
                $negocio = Auth::user()->negocio;
            }
            $distancia = GeopositionHelper::distance($negocio->lat, $negocio->lon, $fields['centro_lat'], $fields['centro_lon']);
            if($distancia > $fields['radio_max']){
                return false;
            } else {
                return true;
            }
        });

        if (Auth::user()->tipo == 'superadmin') {
            return [
                'envio_gratis_minimo_cantidad' => 'required_if:envio_gratis,1',
                'envio_gratis_maximo_km' => 'required_if:envio_gratis,1',
                'costo_envio_fijo' => 'required_if:tipo_envio,fijo',
                'costo_envio_km_base' => 'required_if:tipo_envio,dinamico',
                'km_base' => 'required_if:tipo_envio,dinamico|min:1',
                'costo_envio_km' => 'required_if:tipo_envio,dinamico',
                'radio_max' => 'nullable|numeric',
                'centro_lat' => 'nullable|checkPosition',
                'centro_lon' => 'nullable',
            ];
        } else {
            return [
                'envio_gratis_minimo_cantidad' => 'required_if:envio_gratis,1',
                'envio_gratis_maximo_km' => 'required_if:envio_gratis,1',
                'costo_envio_fijo' => 'required_if:tipo_envio,fijo',
                'costo_envio_km_base' => 'required_if:tipo_envio,dinamico',
                'km_base' => 'required_if:tipo_envio,dinamico|numeric|min:1|max:'. (Auth::user()->negocio->region->radio_max_negocios * 2),
                'costo_envio_km' => 'required_if:tipo_envio,dinamico',
                'radio_max' => 'nullable|numeric|max:'.Auth::user()->negocio->region->radio_max_negocios,
                'centro_lat' => 'nullable|checkPosition',
                'centro_lon' => 'nullable',
            ];
        }
    }

    public function withValidator($validator)
    {
        $validator->sometimes('tipo_envio', 'not_in:null', function($input) {
            return $input->pickup == 0;
        });
    }


    public function messages()
    {
        return [
            'tipo_envio.not_in' => 'Es obligatorio que seleccione por lo menos una de las siguientes opciones: Pick up & GO o un método de envío a domicilio',            'envio_gratis_minimo_cantidad.required_if' => 'El campo Cantidad mínima para envío gratis es obligatorio',
            'envio_gratis_minimo_cantidad.required_if' => 'El campo Cantidad mínima para envío gratis es obligatorio en envíos gratis',
            'envio_gratis_maximo_km.required_if' => 'El campo Km máximos para envío gratis es obligatorio en envíos gratis',
            'costo_envio_fijo.required_if' => 'El campo costo de envío es obligatorio en envíos a domicilio fijos',
            'costo_envio_km_base.required_if' => 'Costo base para el cálculo por Km es obligatorio en envíos a domicilio dinámicos',
            'costo_envio_km.required_if' => 'El campo costo por Km extra es obligatorio en envíos a domicilio dinámicos',
            'centro_lat.check_position' => 'Tu área de envío debe cubrir la posición de tu restaurante',
        ];
    }
}
