<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CartitemtAdd extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|string',
            'name' => 'required|string',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
            'comentarios_cliente' => 'nullable|string|max:150',
            ];
    }
}
