<?php

namespace App\Http\Requests;

use App\Helpers\GeopositionHelper;
use App\Models\Negocio;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class NegocioUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->get('validate') == 'activo') {
            return [
                'activo' => 'numeric|required',
                ];
        }

        if($this->verificado) {
            return [
                'email' => 'required|email',
                'telefono' => 'required|string|regex:/[0-9]{10}/|max:10',
                'sitio_web' => 'nullable|url',
                'facebook' => 'nullable|url',
                'instagram' => 'nullable|url',
                'constitucion_rfc' => 'nullable|string|min:12|max:13',
            ];
        }

        Validator::extend('checkRegion', function ($attribute, $value, $parameters, $validator) {
            $fields = $validator->getData();

            $user = Auth::user();
            if ($user->tipo == 'superadmin') {
                $negocio = Negocio::findOrFail(session('su_negocio_id'));
            } else {
                $negocio = Auth::user()->negocio;
            }
            $region = $negocio->region;
            $distancia = GeopositionHelper::distance($region->centro_lat, $region->centro_lon, $fields['lat'], $fields['lon']);
            if($distancia > $region->radio_max){
                return false;
            } else {
                return true;
            }
        });

        return [
            'logo' => 'nullable|image|max:1024',
            'direccion_cp' => 'required|string|min:5|max:5',
            'direccion_calle' => 'required|string',
            'direccion_numero_exterior' => 'required|string',
            'direccion_numero_interior' => 'nullable|string',
            'direccion_colonia' => 'required|string',
            'direccion_municipio' => 'required|string',
            'direccion_estado' => 'required|string',
            'lat' => 'required|checkRegion',
            'lon' => 'required',
            'email' => 'required|email',
            'telefono' => 'required|string|regex:/[0-9]{10}/|max:10',
            'sitio_web' => 'nullable|url',
            'facebook' => 'nullable|url',
            'instagram' => 'nullable|url',
            'constitucion_razon_social' => 'required|string',
            'constitucion_rfc' => 'nullable|string|min:12|max:13',
            'constitucion_comprobante_domicilio' => 'nullable|file|max:2048|mimes:jpeg,png,pdf',
            'constitucion_identificacion_oficial' => 'nullable|file|max:2048|mimes:jpeg,png,pdf',
            'constitucion_identificacion_oficial_reverso' => 'nullable|file|max:2048|mimes:jpeg,png,pdf',
            'constitucion_acta' => 'nullable|file|max:2048|mimes:jpeg,png,pdf',
            ];
    }

    public function messages()
    {
        return [
            'lat.required' => 'Latitud: Es necesario ubicar un marcador en el mapa, puedes usar el cuadro de búsqueda de dirección, o bien arrastrar el marcador',
            'lon.required' => 'Longitud: Es necesario ubicar un marcador en el mapa, puedes usar el cuadro de búsqueda de dirección, o bien arrastrar el marcador',
            'sitio_web.url' => 'El campo Sitio Web debe ser una dirección web válida',
            'facebook.url' => 'El campo Facebook debe ser una dirección web válida',
            'instagram.url' => 'El campo Instagram Web debe ser una dirección web válida',
            'lat.check_region' => 'La ubicación seleccionada está fuera del rango para esta Ciudad/Región',
        ];
    }
}
