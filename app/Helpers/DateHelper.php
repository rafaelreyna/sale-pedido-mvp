<?php


namespace App\Helpers;


class DateHelper
{
    const MESES = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

    public static function mes_letra($mes_numero) {
        return self::MESES[intval($mes_numero) - 1];
    }
}
