<?php


namespace App\Helpers;


class ObjectHelper
{

    public static function orderObjects(array &$objects, string $attribute, string $order = 'ASC')
    {
        usort($objects, function ($a, $b) use ($order, $attribute) {
            if ($order == 'ASC') {
               return $a->$attribute > $b->$attribute;
            } else {
                return $a->$attribute < $b->$attribute;
            }
        });
    }
}
