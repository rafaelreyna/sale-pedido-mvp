<?php

namespace App\Services\ShoppingCart\Facades;

use App\Services\ShoppingCart\UkiShoppingCart;
use Illuminate\Support\Facades\Facade;

class ShoppingCart extends Facade
{
    protected static function getFacadeAccessor()
    {
        return UkiShoppingCart::class;
    }
}
