(function($) {
  'use strict';

    if (localStorage.getItem('uki_dark_mode') != 'true') {
        $('body').removeClass('ms-dark-theme');
    } else {
        $('body').addClass('ms-dark-theme');
    }

    $('body').toggleClass('ms-aside-left-open');
  $(".ms-settings-toggle").on('click', function(e){
    $('body').toggleClass('ms-settings-open');
  });

  $("#dark-mode").on('click', function(){
      if (localStorage.getItem('uki_dark_mode') == 'true') {
          localStorage.setItem('uki_dark_mode', 'false');
      } else {
          localStorage.setItem('uki_dark_mode', 'true');
      }
    $('body').toggleClass('ms-dark-theme');
  });


  $("#remove-quickbar").on('click', function(){
    $('body').toggleClass('ms-has-quickbar');
  });
  $("#hide-aside-left").on('click', function(){
    $('body').toggleClass('ms-aside-left-open');
  });


})(jQuery);
