<?php

use Illuminate\Database\Seeder;

class RestaurantecategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        \Illuminate\Support\Facades\DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \App\Models\Restaurantes\Restaurantecategoria::truncate();
        \Illuminate\Support\Facades\DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $categorias = [
            [
                "nombre" => "Cafetería",
                "descripcion" => $faker->paragraph(1),
                "foto" => "cafeteria.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Ensaladas",
                "descripcion" => $faker->paragraph(1),
                "foto" => "ensaladas.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Española",
                "descripcion" => $faker->paragraph(1),
                "foto" => "espanola.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Hamburguesas",
                "descripcion" => $faker->paragraph(1),
                "foto" => "hamburguesas.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Mariscos",
                "descripcion" => $faker->paragraph(1),
                "foto" => "mariscos.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Mexicana",
                "descripcion" => $faker->paragraph(1),
                "foto" => "mexicana.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Pasta",
                "descripcion" => $faker->paragraph(1),
                "foto" => "pasta.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Pizza",
                "descripcion" => $faker->paragraph(1),
                "foto" => "pizza.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "PokeBowl",
                "descripcion" => $faker->paragraph(1),
                "foto" => "pokebowl.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Saludable",
                "descripcion" => $faker->paragraph(1),
                "foto" => "saludable.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Sandwich",
                "descripcion" => $faker->paragraph(1),
                "foto" => "sandwich.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Alcohol",
                "descripcion" => $faker->paragraph(1),
                "foto" => "alcohol.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Alitas",
                "descripcion" => $faker->paragraph(1),
                "foto" => "alitas.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Árabe",
                "descripcion" => $faker->paragraph(1),
                "foto" => "arabe.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Argentina",
                "descripcion" => $faker->paragraph(1),
                "foto" => "argentina.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Asiática",
                "descripcion" => $faker->paragraph(1),
                "foto" => "asiatica.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Baguettes",
                "descripcion" => $faker->paragraph(1),
                "foto" => "baguettes.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Conveniencia",
                "descripcion" => $faker->paragraph(1),
                "foto" => "conveniencia.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Corrida",
                "descripcion" => $faker->paragraph(1),
                "foto" => "corrida.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Desayunos",
                "descripcion" => $faker->paragraph(1),
                "foto" => "desayunos.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Gourmet",
                "descripcion" => $faker->paragraph(1),
                "foto" => "gourmet.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Helados",
                "descripcion" => $faker->paragraph(1),
                "foto" => "helados.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Otros",
                "descripcion" => $faker->paragraph(1),
                "foto" => "otros.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Panadería",
                "descripcion" => $faker->paragraph(1),
                "foto" => "panaderia.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Sushi",
                "descripcion" => $faker->paragraph(1),
                "foto" => "sushi.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Pollo",
                "descripcion" => $faker->paragraph(1),
                "foto" => "pollo.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Postres",
                "descripcion" => $faker->paragraph(1),
                "foto" => "postres.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Rápida",
                "descripcion" => $faker->paragraph(1),
                "foto" => "rapida.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Sudamericana",
                "descripcion" => $faker->paragraph(1),
                "foto" => "sudamericana.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Tortas",
                "descripcion" => $faker->paragraph(1),
                "foto" => "tortas.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Vegana",
                "descripcion" => $faker->paragraph(1),
                "foto" => "vegana.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Vegetariana",
                "descripcion" => $faker->paragraph(1),
                "foto" => "vegetariana.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Libanesa",
                "descripcion" => $faker->paragraph(1),
                "foto" => "libanesa.jpg",
                "orden" => 1,
            ],
            [
                "nombre" => "Tacos",
                "descripcion" => $faker->paragraph(1),
                "foto" => "tacos.jpg",
                "orden" => 1,
            ],
        ];

        foreach($categorias as $categoria_data){
            \App\Models\Restaurantes\Restaurantecategoria::create($categoria_data);
        }
    }
}
