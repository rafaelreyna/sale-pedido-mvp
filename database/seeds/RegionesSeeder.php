<?php

use Illuminate\Database\Seeder;

class RegionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \App\Models\Region::truncate();
        \Illuminate\Support\Facades\DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $regiones = [
            [
                "nombre" => 'Querétaro y zona Metropolitana',
                "codigo" => 'QRO',
                "tipo" => 'urbana',
                "centro_lat" => '20.587920317900764',
                "centro_lon" => '-100.38790018388585',
                "radio_max" => 20,
                "radio_max_negocios" => 6,
                "observaciones" => 'Región de lanzamiento de UKI',
            ],
            [
                "nombre" => 'Tequisquiapan',
                "codigo" => 'QRO_TEQUIS',
                "tipo" => 'pueblo_magico',
                "centro_lat" => '20.527127',
                "centro_lon" => '-99.8930297',
                "radio_max" => 10,
                "radio_max_negocios" => 5,
                "observaciones" => 'Región piloto de UKI',
            ],
        ];

        foreach($regiones as $region_data){
            \App\Models\Region::create($region_data);
        }
    }
}
