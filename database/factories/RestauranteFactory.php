<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Negocio;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Negocio::class, function (Faker $faker) {
    return [
        'nombre' => "{$faker->word} {$faker->word}",
        'telefono' => "{$faker->phoneNumber}",
        'tipo' => "restaurante",
        'verificado' => true,
        'activo' => true,
    ];
});
