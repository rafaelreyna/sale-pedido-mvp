<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regiones', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 200)->nullable();
            $table->string('codigo', 10)->nullable();
            $table->string('tipo', 20)->nullable();
            $table->string('centro_lat', 30)->nullable();
            $table->string('centro_lon', 30)->nullable();
            $table->decimal('radio_max', 6,2)->nullable();
            $table->decimal('radio_max_negocios', 6,2)->nullable();
            $table->text('observaciones')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regiones');
    }
}
