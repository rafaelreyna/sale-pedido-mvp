<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('negocio_id')->nullable()->constrained();
            $table->tinyInteger('mes')->nullable();
            $table->integer('ano')->nullable();
            $table->date('vencimiento')->nullable();
            $table->decimal('cargo_membresia', 10, 2)->nullable();
            $table->decimal('descuento_membresia', 10, 2)->nullable();
            $table->string('concepto_descuento_membresia')->nullable();
            $table->bigInteger('numero_transacciones')->nullable();
            $table->decimal('costo_transaccion', 10, 2)->nullable();
            $table->decimal('cargo_transacciones', 10, 2)->nullable();
            $table->decimal('descuento_transacciones', 10, 2)->nullable();
            $table->string('concepto_descuento_transacciones')->nullable();
            $table->decimal('subtotal', 10, 2)->nullable();
            $table->decimal('iva', 10, 2)->nullable();
            $table->decimal('total', 10, 2)->nullable();
            $table->string('status', 20)->nullable();
            $table->string('metodo_pago', 20)->nullable();
            $table->timestamps();

            $table->index(['status','metodo_pago']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
