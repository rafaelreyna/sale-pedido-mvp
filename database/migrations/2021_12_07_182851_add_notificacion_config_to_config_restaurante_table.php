<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNotificacionConfigToConfigRestauranteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('config_restaurante', function (Blueprint $table) {
            $table->text('notificacion_pedido_mail')->nullable();
            $table->text('notificacion_pedido_sms')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('config_restaurante', function (Blueprint $table) {
            $table->dropColumn('notificacion_pedido_mail');
            $table->dropColumn('notificacion_pedido_sms');
        });
    }
}
