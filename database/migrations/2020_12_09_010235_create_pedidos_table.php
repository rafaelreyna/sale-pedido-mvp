<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->id();
            //$table->foreignId('cliente_id')->nullable()->constrained();
            $table->foreignId('negocio_id')->nullable()->constrained();
            $table->string('nombre', '500')->nullable();
            $table->string('email', '500')->nullable();
            $table->text('direccion')->nullable();
            $table->string('lat', 100)->nullable();
            $table->string('lon', 100)->nullable();
            $table->string('telefono', '20')->nullable();
            $table->decimal('distancia', 8, 2)->nullable();
            $table->string('envio', '20')->nullable();
            $table->string('forma_pago', '20')->nullable();
            $table->text('comentarios_cliente')->nullable();
            $table->text('comentarios_negocio')->nullable();
            $table->decimal('total', 10, 2)->nullable();
            $table->decimal('costo_envio', 10, 2)->nullable();
            $table->decimal('gran_total', 10, 2)->nullable();
            $table->string('status', 30)->nullable();
            $table->timestamps();

            $table->index(['status','envio', 'forma_pago']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
