<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantePedidoItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurante_pedido_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pedido_id')->nullable()->constrained();
            $table->string('platillo', '300')->nullable();
            $table->string('categoria', '300')->nullable();
            $table->integer('cantidad');
            $table->decimal('costo_unitario', 10, 2);
            $table->decimal('costo_total', 10, 2);
            $table->text('personalizacion')->nullable();
            $table->timestamps();

            $table->index(['platillo','categoria']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurante_pedido_items');
    }
}
