<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNegocioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('negocios', function (Blueprint $table) {
            $table->id();
            $table->string("nombre")->nullable();
            $table->string("telefono")->nullable();
            $table->string("direccion_cp", 5)->nullable();
            $table->string("direccion_colonia")->nullable();
            $table->string("direccion_calle")->nullable();
            $table->string("direccion_numero_exterior")->nullable();
            $table->string("direccion_numero_interior")->nullable();
            $table->string("direccion_municipio")->nullable();
            $table->string("direccion_estado")->nullable();
            $table->string("sitio_web")->nullable();
            $table->string("email")->nullable();
            $table->string("facebook")->nullable();
            $table->string("instagram")->nullable();
            $table->string("constitucion_tipo")->nullable();
            $table->string("constitucion_rfc")->nullable();
            $table->string("constitucion_razon_social")->nullable();
            $table->string("constitucion_comprobante_domicilio")->nullable();
            $table->string("constitucion_identificacion_oficial")->nullable();
            $table->string("constitucion_identificacion_oficial_reverso")->nullable();
            $table->string("constitucion_acta")->nullable();
            $table->boolean("verificado")->nullable();
            $table->boolean("activo")->nullable();
            $table->string("tipo")->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('tipo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('negocios');
    }
}
