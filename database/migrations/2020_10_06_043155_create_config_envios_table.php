<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigEnviosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_envios', function (Blueprint $table) {
            $table->id();
            $table->foreignId('negocio_id')->nullable()->constrained('negocios');
            $table->boolean('envio_gratis')->nullable();
            $table->decimal('envio_gratis_minimo_cantidad', 10, 2)->nullable();
            $table->decimal('envio_gratis_maximo_km', 4, 2)->nullable();
            $table->boolean(\App\Models\Pedido::PEDIDO_ENVIO_PICKUP)->nullable();
            $table->string('tipo_envio')->nullable();
            $table->decimal('costo_envio_fijo', 10, 2)->nullable();
            $table->decimal('costo_envio_km_base', 10, 2)->nullable();
            $table->decimal('costo_envio_km', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_envios');
    }
}
