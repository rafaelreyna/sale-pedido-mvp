<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCenterFieldToConfigEnviosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('config_envios', function (Blueprint $table) {
            $table->string("centro_lat",30)->nullable();
            $table->string("centro_lon",30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('config_envios', function (Blueprint $table) {
            $table->dropColumn('centro_lat');
            $table->dropColumn('centro_lon');
        });
    }
}
