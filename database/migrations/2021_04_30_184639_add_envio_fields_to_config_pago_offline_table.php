<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEnvioFieldsToConfigPagoOfflineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('config_pago_offline', function (Blueprint $table) {
            $table->boolean('efectivo_domicilio')->nullable();
            $table->boolean('terminal')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('config_pago_offline', function (Blueprint $table) {
            $table->dropColumn('efectivo_domicilio');
            $table->dropColumn('terminal');
        });
    }
}
