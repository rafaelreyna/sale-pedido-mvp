<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('food.home'));
});

Route::get('/admin',  function () {
    return redirect(route('admin.dashboard'));
})->name('admin')->middleware('auth', 'verified');

//favor especial para vale
Route::get('/vale_semaforo/{quien}/{respuesta}', '\App\Http\Controllers\Auth\LoginController@vale_semaforo');


Auth::routes(['verify' => true]);
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/images/public/{path}/{filename}', 'ImagesController@public_image')->name('images.public');
Route::get('/facturas/{id}/pagar', 'FacturacionController@pagarFactura')->name('facturas.pago')->middleware('auth', 'verified');
Route::get('/facturas/{id}/procesar_pago_879827365', 'FacturacionController@procesarPago')->name('facturas.procesar_pago');
Route::middleware('auth', 'verified')->get('/archivos_negocio/{path}/{filename}', 'FilesController@negocio_private_file')->name('files.negocio');

/**
 * ADMIN
 */
Route::middleware('cache.headers:no_cache,private,max_age='.env('CACHE_TIME_BACK', '60').';etag')->middleware('auth', 'verified')->prefix('admin')->name('admin.')->group(function () {
    Route::get('/dashboard', 'Admin\AdminController@index')
        ->name('dashboard');
    Route::resource('horarios', 'Admin\HorariosController');
    Route::resource('negocios', 'Admin\NegociosController');
    Route::get('negocios/{id}/configurar_envios', 'Admin\NegociosController@envios')
        ->name('negocios.envios.edit');
    Route::put('negocios/{id}/configurar_envios', 'Admin\NegociosController@envios_update')
        ->name('negocios.envios.update');
    Route::put('negocios/{id}/slug_update', 'Admin\NegociosController@slug_update')
        ->name('negocios.slug.update');
    Route::put('negocios/{id}/configurar_tiempo', 'Admin\NegociosController@tiempo_update')
        ->name('negocios.tiempo.update');
    Route::get('negocios/{id}/borrar_tiempo', 'Admin\NegociosController@tiempo_delete')
        ->name('negocios.tiempo.delete');
    Route::get('negocios/{id}/configurar_pago', 'Admin\NegociosController@pago_edit')
        ->name('negocios.pago.edit');
    Route::put('negocios/{id}/configurar_pago', 'Admin\NegociosController@pago_update')
        ->name('negocios.pago.update');
    Route::get('negocios/{id}/configurar_categorias', 'Admin\NegociosController@categorias_edit')
        ->name('restaurantes.categorias.edit');
    Route::put('negocios/{id}/configurar_categorias', 'Admin\NegociosController@categorias_update')
        ->name('restaurantes.categorias.update');
    Route::get('negocios/{id}/configurar_notificaciones', 'Admin\NegociosController@notificaciones_edit')
        ->name('negocios.notificaciones.edit');
    Route::put('negocios/{id}/configurar_notificaciones', 'Admin\NegociosController@notificaciones_update')
        ->name('negocios.notificaciones.update');

    //sucursales
    Route::get('negocios/{id}/sucursales', 'Admin\SucursalesController@index')
        ->name('negocios.sucursales.index');
    Route::get('negocios/{id}/sucursales/create', 'Admin\SucursalesController@create')
        ->name('negocios.sucursales.create');
    Route::post('negocios/{id}/sucursales/create', 'Admin\SucursalesController@store')
        ->name('negocios.sucursales.store');
    Route::post('negocios/{id}/sucursales/{sucursal_id}/select', 'Admin\SucursalesController@select')
        ->name('negocios.sucursales.select');
    Route::post('negocios/{id}/sucursales/select', 'Admin\SucursalesController@select_sucursal')
        ->name('negocios.sucursales.selectsucursal');

    Route::get('pedidos', 'Admin\PedidosController@index')
        ->name('pedidos.index');
    Route::get('pedidos/{id}/actualizar_status/{status}', 'Admin\PedidosController@actualizar_status')
        ->name('pedidos.update_status');
    Route::post('pedidos/{id}/actualizar_status/{status}', 'Admin\PedidosController@actualizar_status')
        ->name('pedidos.update_status');
    Route::get('pedidos/historial', 'Admin\PedidosController@historial')->name('pedidos.historial');
    Route::get('pedidos/historial_detallado', 'Admin\PedidosController@historial_detallado')->name('pedidos.historial_detallado');

    //perfil
    Route::get('perfil', 'Admin\PerfilController@edit')->name('perfil.edit');
    Route::put('perfil', 'Admin\PerfilController@update')->name('perfil.update');

    /*
     * Restaurantes
     * */

    Route::resource('categorias-de-platillos', 'Admin\Restaurantes\PlatillocategoriasController')->names([
        'index' => 'platillocategorias.index',
        'create' => 'platillocategorias.create',
        'store' => 'platillocategorias.store',
        'show' => 'platillocategorias.show',
        'edit' => 'platillocategorias.edit',
        'update' => 'platillocategorias.update',
        'destroy' => 'platillocategorias.destroy',
    ]);
    Route::get('categorias-de-platillos/{id}/order/{action}', 'Admin\Restaurantes\PlatillocategoriasController@order')
        ->name('platillocategorias.order');
    Route::resource('platillos', 'Admin\Restaurantes\PlatillosController');
    Route::get('platillos/{id}/order/{action}', 'Admin\Restaurantes\PlatillosController@order')
        ->name('platillos.order');
    Route::resource('operadores', 'Admin\OperadoresController');
    Route::resource('facturas', 'Admin\FacturasController');
    Route::get('facturas/{id}/desglose', 'Admin\FacturasController@bajar_desglose')
        ->name('facturas.desglose');
    Route::resource('tutoriales', 'Admin\TutorialesController');

    /*
     * Facturas
     */
    Route::get('/facturas/{id}/solicitar_factura', 'FacturacionController@solicitarFactura')->name('facturas.solicitar_factura');
});

/**
 * SUPERADMIN
 */

Route::middleware('cache.headers:no_cache,private,max_age='.env('CACHE_TIME_BACK', '60').';etag')->middleware('auth', 'verified')->prefix('superadmin')->name('superadmin.')->group(function () {
    Route::get('/superadmin/negocios', 'Admin\NegociosController@index')->name('negocios.index');
    Route::get('/superadmin/negocios/solicitudes', 'Admin\NegociosController@solicitudes')->name('negocios.solicitudes');
    Route::get('/superadmin/negocios/{id}/verificar', 'Admin\NegociosController@verificar')->name('negocios.verificar');
    Route::resource('regiones', 'Admin\RegionesController');
    Route::get('/pedidos/historial_superadmin', 'Admin\PedidosController@historial_admin')->name('pedidos.historial_superadmin');
});



/**
 *  UKI Food
 */
Route::middleware('cache.headers:no_cache,private,max_age='.env('CACHE_TIME_FRONT', '60').';etag')->prefix('food')->name('food.')->group(function () {
    Route::get('/', '\App\Http\Controllers\Food\DirectoryController@index')->name('home');
    Route::middleware(\App\Http\Middleware\NeedsLocation::class)->get('/categorias/{id}', '\App\Http\Controllers\Food\DirectoryController@categoriaListado')->name('categoria.show');
    Route::middleware(\App\Http\Middleware\NeedsLocation::class)->get('/restaurantes/buscar', '\App\Http\Controllers\Food\DirectoryController@buscar')->name('restaurante.search');
    Route::get('/restaurantes/{id}', '\App\Http\Controllers\Food\DirectoryController@restauranteListado')->name('restaurante.show');
    Route::get('/restaurantes/menu/{id}', '\App\Http\Controllers\Food\DirectoryController@menuOnline')->name('restaurante.menu.show');
    Route::middleware(\App\Http\Middleware\NeedsLocation::class)->get('/platillos/{id}', '\App\Http\Controllers\Food\DirectoryController@platilloDetalle')->name('platillo.show');
    Route::get('/platillos/{id}/{itemId}', '\App\Http\Controllers\Food\DirectoryController@platilloEdit')->name('platillo.edit');
    Route::get('/perfil/region/{region_id}', '\App\Http\Controllers\Food\PerfilController@configurarRegion')->name('perfil.region.update');
    Route::get('/perfil/ubicacion/{lat}/{lon}', '\App\Http\Controllers\Food\PerfilController@configurarUbicacion')->name('perfil.ubicacion.update');
    Route::get('/buscar-direccion/{lat}/{lon}', '\App\Http\Controllers\Food\PerfilController@searchAdress')->name('perfil.ubicacion.buscar');
    Route::get('/perfil', '\App\Http\Controllers\Food\PerfilController@edit')->name('perfil.edit');
    Route::put('/perfil', '\App\Http\Controllers\Food\PerfilController@update')->name('perfil.update');
    Route::get('perfil/comparte_ubicacion', '\App\Http\Controllers\Food\PerfilController@editLocation')->name('perfil.location.edit');

    /**
     * Cart
     */
    Route::post('/carrito/restaurante/{id}/items', '\App\Http\Controllers\Food\CarritoController@add')->name('cart.items.add');
    Route::put('/carrito/restaurante/{id}/items/{item_id}', '\App\Http\Controllers\Food\CarritoController@update')->name('cart.items.update');
    Route::get('/carrito/restaurante/{negocio_id}/items/{item_id}/eliminar', '\App\Http\Controllers\Food\CarritoController@delete')->name('cart.items.delete');
    Route::post('/carrito', '\App\Http\Controllers\Food\CarritoController@procesar')->name('cart.process');
    Route::post('/confirmar', '\App\Http\Controllers\Food\CarritoController@confirmar')->name('cart.confirm');
    Route::get('/carrito', '\App\Http\Controllers\Food\CarritoController@index')->name('cart.show');
    Route::get('/carrito/vaciar', '\App\Http\Controllers\Food\CarritoController@clear')->name('cart.clear');

    Route::get('/pedido/{id}', '\App\Http\Controllers\Food\CarritoController@seguimiento_pedido')->name('pedido.seguimiento');
    //Route::get('pedidos/{id}/entregado', 'Admin\PedidosController@actualizar_status_front')
     //   ->name('pedidos.entregado');
    Route::get('pedidos/{id}/calificar', '\App\Http\Controllers\Food\CarritoController@calificar_pedido')
        ->name('pedido.calificar');
    Route::post('pedidos/{id}/calificar', '\App\Http\Controllers\Food\CarritoController@calificar_store')
        ->name('pedido.calificar_store');

    //checkout
    Route::get('/checkout', '\App\Http\Controllers\Food\CheckoutController@index')->name('checkout.index');
    Route::post('/checkout', '\App\Http\Controllers\Food\CheckoutController@process')->name('checkout.process');
});
