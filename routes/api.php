<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/pedido_public/{id}', 'Admin\PedidosController@show_public');
Route::post('/procesar_pedido/{id}', '\App\Http\Controllers\Food\CarritoController@procesar_pago_mp')->name('pedido.procesar.mp');

Route::get('/negocios/{id}/pedidos/entrantes', 'Admin\PedidosController@pedidos_entrantes');

Route::get('/notificaciones/{id}/call_response', 'NotificacionesController@call_response');
Route::post('/notificaciones/{id}/call_response', 'NotificacionesController@call_response');
