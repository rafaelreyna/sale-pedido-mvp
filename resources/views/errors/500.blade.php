@extends('layouts.error')

@section('content')
    <div class="box__description-title">Algo salió mal</div>
    <div class="box__description-text">Espera unos minutos y vuelve a intentar, si no se resuelve contacta a nuestro equipo de soporte</div>
@endsection
