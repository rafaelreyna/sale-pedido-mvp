@extends('layouts.error')

@section('content')
    <div class="box__description-title">Expiró la página</div>
    <div class="box__description-text">Por cuestion de tiempo, y para tu seguridad esta página expiró, vuelve a iniciar el proceso qaue estabas realizando</div>
@endsection
