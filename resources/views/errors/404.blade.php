@extends('layouts.error')

@section('content')
    <div class="box__description-title">Pagina no encontrada</div>
    <div class="box__description-text">La página que estás buscando no existe, asegurate de escribir la URL correctamente</div>
@endsection
