@extends('layouts.food')
@section('content')
    <hr/>
    <section id="perfil_usuario">
        <div class="container">
            <h3>Mis preferencias</h3>
            @if($instancia && $total != 0 && count($carrito) != 0)
                <p class="alert alert-danger"> Si cambias tu ubicación se vaciará tu carrito actual, para que puedas seleccionar nuevamente un restaurante en base a tu nueva ubicación</p>
            @endif
            <div class="row">
                <!-- REGISTER -->
                <div class="col-md-8">
                    <div class="widget">
                        <div class="widget-body">
                            <!-- Contact form -->
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul style="margin: 0px;">
                                        @foreach ($errors->all() as $error)
                                            <li><i class="flaticon-alert"></i> {{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-horizontal contact-form" role="form">
                                <form autocomplete="disabled" method="POST" action="{{route('food.perfil.update')}}@if(isset($_GET['solo_region']))?solo_region=true @endif
@if(isset($_GET['solo_ubicacion']))?solo_ubicacion=true @endif">
                                    {{ csrf_field() }}
                                    {{method_field('put')}}
                                    <input type="hidden" name="_url" value="{{\Illuminate\Support\Facades\URL::previous()}}">
                                    <fieldset>
                                        <div class="row form-group">
                                            <div class="col-xs-12 col-md-6">
                                                <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre *" @if(!isset($_GET['solo_region']) && !isset($_GET['solo_ubicacion'])) required @endif
                                                value = "{{old('nombre')?old('nombre'):$perfil->nombre}}">
                                            </div>

                                            <div class="col-xs-12 col-md-6">
                                                <input class="form-control" id="apellidos" name="apellidos" type="text" placeholder="Apellidos *" @if(!isset($_GET['solo_region']) && !isset($_GET['solo_ubicacion'])) required @endif
                                                       value = "{{old('apellidos')?old('apellidos'):$perfil->apellidos}}">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-xs-6">
                                                <input class="form-control" id="email" name="email" type="email" placeholder="Email *" @if(!isset($_GET['solo_region']) && !isset($_GET['solo_ubicacion'])) required @endif
                                                       value = "{{old('email')?old('email'):$perfil->email}}">
                                            </div>
                                            <div class="col-xs-6">
                                                <input class="form-control" id="telefono" name="telefono" type="tel" placeholder="Celular *" @if(!isset($_GET['solo_region']) && !isset($_GET['solo_ubicacion'])) required @endif
                                                       value = "{{old('telefono')?old('telefono'):$perfil->telefono}}">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-xs-12 col-md-12">
                                                <textarea autocomplete="disabled" class="form-control" id="pac-input" name="direccion"  placeholder="Direccion" @if(!isset($_GET['solo_region'])) required @endif>{{old('direccion')?old('direccion'):$perfil->direccion}}</textarea>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-xs-12 col-md-12">
                                                <select disabled class="form-control" id="region_id" name="region_id" required>
                                                    <option value="">Selecciona una región *</option>
                                                    @foreach($regiones as $region)
                                                        <option @if($perfil->region_id == $region->id || old('region_id') == $region->id) selected @endif  value="{{$region->id}}">{{$region->nombre}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-xs-12 col-md-12">
                                                <textarea  class="form-control" id="pac-input" name="referencias"  placeholder="Referencias o indicaciones específicas" >{{old('referencias')?old('referencias'):$perfil->referencias}}</textarea>
                                            </div>
                                        </div>



                                        <div id="map" style="height: 300px;" data-zoom="14"></div>
                                        <input type="hidden" class="form-control @error('lon') is-invalid @enderror"
                                               id="lon" name="lon" placeholder=""
                                               value="{{old('lon')?old('lon'):$perfil->lon}}">
                                        <input type="hidden" class="form-control @error('lat') is-invalid @enderror"
                                               id="lat" name="lat" placeholder=""
                                               value="{{old('lat')?old('lat'):$perfil->lat}}">
                                        <br/>


                                        <div class="row form-group">
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-lg theme-btn" type="submit"><i class="fa fa-user"></i> Recordar Preferencias</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                            <!-- End Contact form -->
                        </div>
                    </div>
                    <!-- end: Widget -->
                </div>
                <!-- /REGISTER -->
                <!-- WHY? -->
                <div class="col-md-4">
                    <h4>Pide tu comida fácil y rápido.</h4>
                    <p>Con nuestra plataforma:</p>
                    <ul class="list-check list-unstyled">
                        <li>Pedir comida a domicilio a precios justos.</li>
                        <li>Pedir comida para llevar a precios justos.</li>
                        <li>No necesitas crear una cuenta, puedes comprar como invitado.</li>
                        <li>Puedes pagar en efectivo, o tarjeta si el restaurante lo acepta.</li>
                        <li>Puedes pedir con envío a domicilio o para llevar.</li>
                    </ul>
                    <!--
                    <hr>
                    <h3>Preguntas frecuentes</h3>
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a data-parent="#accordion" data-toggle="collapse" class="panel-toggle collapsed" href="#faq1" aria-expanded="false"><i class="ti-info-alt" aria-hidden="true"></i>¿Cuáles son los medios de pago?</a></h4> </div>
                        <div class="panel-collapse collapse" id="faq1" aria-expanded="false" role="article" style="height: 0px;">
                            <div class="panel-body"> Dependiendo de qué medios acepte el restaurante podrás pagar con efectivo, tarjeta física o pago en línea con tarjeta. </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a data-parent="#accordion" data-toggle="collapse" class="panel-toggle" href="#faq2" aria-expanded="true"><i class="ti-info-alt" aria-hidden="true"></i>¿Cómo recibo mi pedido?</a></h4> </div>
                        <div class="panel-collapse collapse" id="faq2" aria-expanded="true" role="article">
                            <div class="panel-body"> Dependiendo de cada restaurante podrás pedir con envío a domicilio, o bien la modalidad Pick up & Go (Recoger directo en el establecimiento). </div>
                        </div>
                    </div>
                    -->
                </div>
                <!-- /WHY? -->
            </div>
        </div>
    </section>
    <br/>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY', 'AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY')}}&libraries=places"></script>
    <script type="application/javascript">
        $(document).ready(function(){
            $('#pac-input').attr('autocomplete','chrome-off');

            map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: {{$perfil->lat?$perfil->lat:'20.6122138'}},
                    lng: {{$perfil->lon?$perfil->lon:'-100.4452366'}},
                },
                zoom: 15,
                scrollwheel: true,
                heigth: 250
            });

            marker = new google.maps.Marker({
                position: {
                    lat: {{$perfil->lat?$perfil->lat:'20.6122138'}},
                    lng: {{$perfil->lon?$perfil->lon:'-100.4452366'}}
                },
                map: map,
                draggable:true,
                title: '{{$perfil->nombre}}'
            });

            google.maps.event.addListener(marker, 'dragend', function (event) {
                document.getElementById("lat").value = event.latLng.lat();
                document.getElementById("lon").value = event.latLng.lng();
            });

            // Create the search box and link it to the UI element.
            const input = document.getElementById("pac-input");
            const searchBox = new google.maps.places.SearchBox(input);
            //map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(input);
            // Bias the SearchBox results towards current map's viewport.
            map.addListener("bounds_changed", () => {
                searchBox.setBounds(map.getBounds());
            });

            let markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener("places_changed", () => {
                const places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }
                // Clear out the old markers.
                markers.forEach((marker) => {
                    marker.setMap(null);
                });

                const bounds = new google.maps.LatLngBounds();
                places.forEach((place) => {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }

                    marker.setPosition(place.geometry.location);
                    document.getElementById("lat").value = place.geometry.location.lat();
                    document.getElementById("lon").value = place.geometry.location.lng();

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });


        });

    </script>
@endsection
