@extends('layouts.food', ['custom_meta' => true])

@section('custom_meta')
    <meta property="og:title" content="{{$platillo->negocio->nombre}} en Uki&reg;" />
    @if($platillo->negocio->logo)
        <meta property="og:image" content="{{route('images.public', [base64_encode('negocios/logos/'), $platillo->negocio->logo])}}" />
    @else
        <meta property="og:image" content="https://uki.mx/assets/img/logo.png" />
    @endif
    <meta property="og:description" content="Disfruta los platillos de {{$platillo->negocio->nombre}} a través de Uki&reg;: una plataforma que conecta a los restaurantes con los consumidores de manera trasparente y justa." />
    <meta property="og:url" content="{{route('food.platillo.show', $platillo->id)}}" />
    <title>Uki&reg; @if(env('APP_ENV', 'local') == 'beta') - Beta @endif - {{$platillo->negocio->nombre}}</title>
@endsection

@section('content')

    <hr/>
    <section id="perfil_usuario">
        <div class="container">
            <form id="frmItem" method="POST"
                  @if(isset($item))
                    action="{{route('food.cart.items.update', [$platillo->negocio_id, $item->getUniqueId()])}}"
                  @else
                    action="{{route('food.cart.items.add', $platillo->negocio_id)}}"
                  @endif
            >
                {{isset($item)?method_field('put'):method_field('post')}}
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$platillo->id}}">
                <input type="hidden" name="name" value="{{$platillo->nombre}}">
                <input type="hidden" name="price" value="{{$platillo->precio_final()}}">
                <input id="hdnGoToCheckout" type="hidden" name="goToCheckout" />

                <h3>{{$platillo->nombre}}</h3>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin: 0px;">
                            @foreach ($errors->all() as $error)
                                <li><i class="flaticon-alert"></i> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                    <div class="menu-widget m-b-30">
                        <div class="widget-heading">
                            <h3 class="widget-title text-dark">
                                Detalles del platillo
                            </h3>
                            <div class="clearfix"></div>
                        </div>
                        <div class="collapse in" id="1">
                            <div class="food-item white">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img style="width: 350px; max-width:100%; margin-right: 20px;"  src="{{$platillo->foto ? route('images.public', [base64_encode('restaurantes/platillos/'), $platillo->foto]) : URL::to('/assets/img/sin_imagen.jpg')}}" alt="Food logo">
                                        <!-- end:Description -->
                                    </div>
                                    <div class="col-md-8">
                                        <br/>
                                        @component('food.components.platillo.precio')
                                            @slot('platillo', $platillo)
                                            @slot('restaurante', $platillo->negocio)
                                            @slot('botones', false)
                                        @endcomponent
                                        <p>{{$platillo->descripcion}}</p>
                                    </div>
                                </div>
                                <!-- end:row -->
                            </div>
                        </div>
                        <!-- end:Collapse -->
                    </div>
                    <div class="solo_desktop">
                        @if(!isset($item))<button id="btnGoToCheckout" style="margin-bottom: 8px;" type="button" class="btn theme-btn btn-lg"> Agregar e ir a pagar</button><br/>@endif
                        <button style="margin-bottom: 8px;" type="submit" class="btn theme-btn btn-lg">@if(isset($item)) Modificar @else Agregar y seguir comprando @endif</button><br/>
                        <a href="{{url()->previous()}}" class="btn btn-link"><i class="fa fa-arrow-left"></i> Regresar</a>
                    </div>
                </div>
                <div class="col-xs-12 col-md-12 col-lg-4">
                    <div class="sidebar-wrap">
                        <div class="widget widget-cart">
                            <div class="widget-heading">
                                <h3 class="widget-title text-dark">
                                    Personalizar
                                </h3>
                                <div class="clearfix"></div>
                            </div>
                            <div class="order-row bg-white">
                                <div class="widget-body">
                                    <div class="title-row">Cantidad</div>
                                    <div class="form-group row no-gutter">
                                        <input name="quantity" class="form-control" type="number" id="quantity" min="1"
                                               @if(isset($item) && $item->quantity) value="{{$item->quantity}}" @else value="1" @endif
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="order-row bg-white">
                                <div class="widget-body">
                                    <div class="title-row">Indicaciones Extra</div>
                                    <div class="form-group row no-gutter">
                                        <textarea name="comentarios_cliente" maxlength="150" class="form-control txtConteoCaracteres" placeholder="Cualquier comentario de tu platillo que no afecte su precio. Ej: sin Picante">@if(isset($item->options['comentarios_cliente']) && $item->options['comentarios_cliente']){{$item->options['comentarios_cliente']}}@endif</textarea>
                                        <div class="conteoCaracteresContenedor"><span class="conteoCaracteres">0</span> / 150</div>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="price-wrap text-xs-center">
                                    <!--
                                    <p>TOTAL</p>
                                    <h3 class="value"><strong>$ 25,49</strong></h3>
                                    <p>Free Shipping</p>
                                    -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="solo_movil">
                        <button id="btnGoToCheckoutMobile" style="margin-bottom: 8px;" type="button" class="btn theme-btn btn-lg">@if(isset($item)) Modificar e ir a Pagar @else Agregar e ir a pagar @endif</button><br/>
                        <button style="margin-bottom: 8px;" type="submit" class="btn theme-btn btn-lg">@if(isset($item)) Modificar @else Agregar y seguir comprando @endif</button><br/>
                        <a href="{{url()->previous()}}" class="btn btn-link"><i class="fa fa-arrow-left"></i> Regresar</a>
                </div>
            </div>
            </form>
        </div>
    </section>
    <script type="application/javascript">
        $(document).ready(function(){
          $('#btnGoToCheckout').click(function(){
              $('#hdnGoToCheckout').val(true)
              $('#frmItem').submit();
          });
            $('#btnGoToCheckoutMobile').click(function(){
                $('#hdnGoToCheckout').val(true)
                $('#frmItem').submit();
            });
        });
    </script>

@endsection
