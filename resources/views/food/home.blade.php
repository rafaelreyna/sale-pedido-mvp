@extends('layouts.food')

@section('content')

    @component('food.components.hero.hero') @endcomponent

    <hr/>
    <section id="restaurantecategorias">
        <div class="container">
            <div class="row">
                @if(isset($perfil))
                    @if(isset($perfil->region_id))
                        @if($perfil->isFarFromRegion())
                            <p class="alert alert-danger"><i class="fa fa-warning"></i> TU DIRECCIÓN ESTÁ LEJOS DE LA ZONA UKI MÁS CERCANA. SOLO PUEDES REALIZAR PEDIDOS PICK UP & GO. <a href="{{route('food.perfil.edit')}}">REVISA QUE TU DIRECCIÓN SEA LA CORRECTA</a></p>
                        @endif
                    @endif
                @endif
                <br/>
                <h4 style="text-align: center">Explora negocios</h4>
                <br class="solo_desktop"/>
                @foreach($restaurantes as $restaurante)
                    @component('food.components.restaurante')
                        @slot('restaurante', $restaurante)
                        @slot('perfil', $perfil)
                    @endcomponent
                @endforeach
            </div>

            <div class="row">
                <h4 style="text-align: center">Explora negocios por categoría</h4>
                <br class="solo_desktop"/>
                @foreach($categorias as $categoria)
                    @if(isset($perfil->region_id) & ($perfil->lat && $perfil->lon))
                        @if(count($categoria->restaurantes($perfil->region_id??null, true, $perfil->lat, $perfil->lon)) > 0)
                            <a href="{{route('food.categoria.show', $categoria->id)}}">
                                <div class="col col-xs-6 col-md-4 col-lg-3 categoria">
                                    <img class="img-fluid" src="{{asset("food_assets/images/categorias/{$categoria->foto}")}}">
                                </div>
                            </a>
                        @endif
                    @else
                        <a href="{{route('food.perfil.location.edit', $categoria->id)}}">
                            <div class="col col-xs-6 col-md-4 col-lg-3 categoria">
                                <img class="img-fluid" src="{{asset("food_assets/images/categorias/{$categoria->foto}")}}">
                            </div>
                        </a>
                    @endif
                @endforeach
            </div>

        </div>
    </section>
    <br/>
    <script type="text/javascript">
        $(document).ready(function(){

        });
    </script>
@endsection
