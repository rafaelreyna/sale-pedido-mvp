@extends('layouts.food')

@section('content')

    <hr/>
    <section id="perfil_usuario">
        <div class="container">
            <h3>Califica tu pedido</h3>
            <div id="en_camino" class="row result_status" >
                <form method="POST" action="{{route('food.pedido.calificar_store', base64_encode($pedido->id))}}">
                    {{ csrf_field() }}
                    {{method_field('post')}}
                    <input id="calificacion" type="hidden" name="calificacion" value="1">
                    <hr/>
                    <h4>Califica a {{$pedido->negocio->nombre}}</h4>
                    <p>Ayúda al restaurante a mejorar evaluando su servicio y dejando algún comentario</p>

                    <div class="rating-block">
                        <p style="text-align: center">
                            <a id="calificar1" data-calificar="1" data-active="active"><i class="fa fa-star calificar"></i></a>
                            <a id="calificar2" data-calificar="2" data-active="inactive"><i class="fa fa-star-o calificar"></i></a>
                            <a id="calificar3" data-calificar="3" data-active="inactive"><i class="fa fa-star-o calificar"></i></a>
                            <a id="calificar4" data-calificar="4" data-active="inactive"><i class="fa fa-star-o calificar"></i></a>
                            <a id="calificar5" data-calificar="5" data-active="inactive"><i class="fa fa-star-o calificar"></i></a>
                        </p>
                        <br/>
                        <div class="row form-group">
                            <div class="col-xs-8 col-md-12">
                                <textarea autocomplete="disabled" class="form-control" id="pac-input" name="comentarios"  placeholder="Deja tus comentarios"></textarea>
                            </div>
                        </div>
                        <br/>
                        <p style="text-align: center">
                            <button type="submit" class="btn btn-lg theme-btn text-white">Calificar</button>
                        </p>
                    </div>
                </form>
            </div>
            <hr/>
            <h4>Detalles de tu pedido</h4>
            <div class="cart-totals-fields">
                @component('food.components.resumen_pedido')
                    @slot('pedido', $pedido)
                @endcomponent
                <br/>
            </div>
        </div>
    </section>
    <br/>
    <script type="application/javascript">
        $(document).ready(function(){
            $('.calificar').click(function(){
                var star = $(this);
                var parent = $(this).parent()
                $("#calificacion").val(parent.data('calificar'));
                for(var i = 5; i>=1; i= i-1){
                    if(i > parent.data('calificar')) {
                        $('#calificar'+i).find('i').removeClass('fa-star');
                        $('#calificar'+i).find('i').addClass('fa-star-o');
                    } else {
                        $('#calificar'+i).find('i').removeClass('fa-star-o');
                        $('#calificar'+i).find('i').addClass('fa-star');
                    }
                }
            });
        });
    </script>
@endsection
