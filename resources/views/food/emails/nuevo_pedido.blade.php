@extends('layouts.notificacion_email')

@section('content')
    <p>Hemos enviado tu pedido al restaurante. <strong>Puedes conocer el estatus de tu pedido UKI&reg; haciendo click <a href="{{route('food.pedido.seguimiento', base64_encode($pedido->id))}}">aquí</a>.</strong></p>
    <br/>
    <p><strong class="stronguki">Detalles de tu pedido</strong></p>
    @component('food.components.resumen_pedido')
        @slot('pedido', $pedido)
        @slot('is_email', true)
    @endcomponent

    <p>
        <strong>Número de Confirmación:</strong>
        <span>{{$pedido->id}}</span>
    </p>

    <p>
        <strong>Registramos tu pedido el:</strong>
        <span>{{date('d/m/Y',strtotime($pedido->created_at))}} a las {{date('h:i A',strtotime($pedido->created_at))}}.</span>
    </p>

    <p>
        <strong>Forma de Pago:</strong>
        @if($pedido->forma_pago == \App\Models\Pedido::PEDIDO_PAGO_MP)<span>Pago con Tarjeta</span>@endif
        @if($pedido->forma_pago == \App\Models\Pedido::PEDIDO_PAGO_EFECTIVO)<span>Pago en efectivo</span>@endif
        @if($pedido->forma_pago == \App\Models\Pedido::PEDIDO_PAGO_TERMINAL)<span>Envío de terminal</span>@endif
    </p>

    <p>
        <strong>Tipo de Envío:</strong>
        @if($pedido->envio == \App\Models\Pedido::PEDIDO_ENVIO_PICKUP)<span>Pick up & Go (Recoger en Tienda)</span>@endif
        @if($pedido->envio == \App\Models\Pedido::PEDIDO_ENVIO_GRATIS)<span>Envío Gratis</span>@endif
        @if($pedido->envio == \App\Models\Pedido::PEDIDO_ENVIO_FIJO)<span>Envío a domicilio</span>@endif
        @if($pedido->envio == \App\Models\Pedido::PEDIDO_ENVIO_DINAMICO)<span>Envío a domicilio</span>@endif
    </p>

    <br/>
    <p><strong class="stronguki">Información del restaurante</strong></p>
    <p>
        <strong>Restaurante:</strong>:
        <span>{{$pedido->negocio->nombre}}</span>
    </p>
    <p>
        <strong>Dirección:</strong>:
        <span>
            {{$pedido->negocio->direccion_calle}}
            {{$pedido->negocio->direccion_numero_exterior}}
            {{$pedido->negocio->direccion_interior}},
            {{$pedido->negocio->direccion_colonia}},
            {{$pedido->negocio->direccion_municipio}},
            {{$pedido->negocio->direccion_estado}}. <a href="https://www.google.com/maps/search/?api=1&query={{$pedido->negocio->lat}},{{$pedido->negocio->lon}}"><i class="fa fa-map-marker"></i> Ver en mapa</a>
        </span>
    </p>
    <p>
        <strong>Teléfono:</strong>:
        <span><a href="tel:{{$pedido->negocio->telefono}}">{{$pedido->negocio->telefono}}</a></span>
    </p>
    <p>
        <strong>Email:</strong>:
        <span>{{$pedido->negocio->email}}</span>
    </p>



    <br/>
    <p><strong class="stronguki">Información de cliente</strong></p>
    <p>
        <strong>Nombre:</strong>:
        <span>{{$pedido->nombre}}</span>
    </p>
    <p>
        <strong>Dirección:</strong>:
        <span>{{$pedido->direccion}}</span>
    </p>
    <p>
        <strong>Teléfono de contacto:</strong>:
        <span>{{$pedido->telefono}}</span>
    </p>
    <p>
        <strong>Email:</strong>:
        <span>{{$pedido->email}}</span>
    </p>
    <br/>
    <p><small>*En caso de requerir,  consulta si el establecimiento emite comprobante fiscal <a href="mailto:{{$pedido->negocio->email}}">{{$pedido->negocio->email}}</a></small></p>
@endsection
