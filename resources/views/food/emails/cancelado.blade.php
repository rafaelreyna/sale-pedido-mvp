@extends('layouts.notificacion_email')

@section('content')
    <p>Hola, desafortunadamente {{$pedido->negocio->nombre}} no puede atender tu orden en este momento. La razón que ha dado el establecimiento es la siguiente: <strong>{{$razon}}</strong></p>
    <br/>

    <p>Lamentamos mucho lo ocurrido. A causa de este incidente , el establecimiento recibirá una penalización.</p>
    <p>Si tu pago lo realizaste en línea… No te preocupes, te será rembolsado y lo podrás ver reflejado unos días hábiles después.</p>
    <!-- <p>buscar alguna alternativa deliciosa en <a href="{{route('food.categoria.show', $pedido->negocio->categorias[0]->id)}}">UKI&reg;</a></p> -->
    <br/>
    <p><strong class="stronguki">Información del restaurante</strong></p>
    <p>
        <strong>Restaurante:</strong>:
        <span>{{$pedido->negocio->nombre}}</span>
    </p>
    <p>
        <strong>Dirección:</strong>:
        <span>
            {{$pedido->negocio->direccion_calle}}
            {{$pedido->negocio->direccion_numero_exterior}}
            {{$pedido->negocio->direccion_interior}},
            {{$pedido->negocio->direccion_colonia}},
            {{$pedido->negocio->direccion_municipio}},
            {{$pedido->negocio->direccion_estado}}.
        </span>
    </p>
    <p>
        <strong>Teléfono:</strong>:
        <span>{{$pedido->negocio->telefono}}</span>
    </p>
    <p>
        <strong>Email:</strong>:
        <span>{{$pedido->negocio->email}}</span>
    </p>


@endsection
