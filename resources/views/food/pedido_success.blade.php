@extends('layouts.food')

@section('content')

    <hr/>
    <section id="perfil_usuario">
        <div class="container">
            <h3 id="titulo_seccion">¡NO TE VAYAS! Aquí puedes darle seguimiento a tu pedido.</h3>
            <div id="loading" class="row result_status">
                <hr/>
                <p style="text-align: center">
                    <img src="{{URL::to('/')}}/food_assets/images/animaciones/loading-uki.gif"/>
                </p>
            </div>
            <div id="pago_rechazado" class="row result_status" style="display: none">
                <hr/>
                <h4>Error en el pago</h4>
                <p class="alert alert-danger">Hubo un error al intentar procesar el pago con tarjeta de tu pedido, si el problema persiste intenta con otra forma de pago</p>
                <p style="text-align: center">
                    <img style="width:400px;" src="{{URL::to('/')}}/food_assets/images/animaciones/pago_rechazado.gif"/>
                </p>
            </div>
            <div id="recibido" class="row result_status" style="display: none">
                <hr/>
                <h4>Tu pedido se envió al restaurante</h4>
                <p>Hemos enviado tu pedido al Restaurante. Te notificaremos cuando el establecimiento acepte tu pedido.</p>
                <p><snall>Te recordamos que en UKI&reg; el contacto es directo con el restaurante, no somos intermediarios. En el momento que desees puedes ponerte en contacto directo con el establecimiento, sus datos aparecen en la parte inferior.</snall></p>
                @component('food.components.tracker.progress')
                    @slot('steps', ['active','disabled','disabled','disabled'])
                    @slot('pedido', $pedido)
                @endcomponent
                <p style="text-align: center">
                    <img src="{{URL::to('/')}}/food_assets/images/animaciones/nuevo.gif"/>
                </p>
            </div>
            <div id="preparando" class="row result_status" style="display: none">
                <hr/>
                <h4>Tu pedido se está preparando</h4>
                <p>El restaurante ha confirmado tu pedido y ya lo está preparando</p>
                @component('food.components.tracker.progress')
                    @slot('steps', ['complete','active','disabled','disabled'])
                    @slot('pedido', $pedido)
                @endcomponent
                <p style="text-align: center">
                    <img src="{{URL::to('/')}}/food_assets/images/animaciones/cocinando.gif"/>
                </p>
            </div>
            <div id="entregado" class="row result_status" style="display: none">
                <hr/>
                <h4>Pedido cerrado</h4>
                <p>Gracias por tu confianza , vuelve pronto.</p>
                @component('food.components.tracker.progress')
                    @slot('steps', ['complete','complete','complete','complete'])
                    @slot('pedido', $pedido)
                @endcomponent
                <p style="text-align: center">
                    <img src="{{URL::to('/')}}/food_assets/images/animaciones/loading-uki.gif"/><br/>
                    <a href="{{route('food.home')}}">Regresar a Uki Food</a>
                </p>
            </div>
            <div id="cancelado" class="row result_status" style="display: none">
                <hr/>
                <h4>Tu pedido ha sido rechazado </h4>
                <p>Lamentamos mucho lo ocurrido. A causa de este incidente , el establecimiento recibirá una penalización.</p>
                <p>Si tu pago lo realizaste en línea… No te preocupes, te será rembolsado y lo podrás ver reflejado unos días hábiles después.</p>
                <p>Si tienes cualquier duda comunícate directamente al restaurante</p>

                </p>
                <p style="text-align: center">
                    <img src="{{URL::to('/')}}/food_assets/images/animaciones/rechazado.gif"/><br/>
                    <a href="{{route('food.home')}}">Regresar a Uki Food</a>
                </p>
            </div>
            <div id="en_camino" class="row result_status" style="display: none">
                <hr/>
                @if($pedido->envio == \App\Models\Pedido::PEDIDO_ENVIO_PICKUP)
                    <h4>Tu pedido está listo</h4>
                    <p>Lo puedes recoger en la dirección del establecimiento.</p>
                    @component('food.components.tracker.progress')
                        @slot('steps', ['complete','complete','active','disabled'])
                        @slot('pedido', $pedido)
                    @endcomponent
                    <p style="text-align: center">
                        <img src="{{URL::to('/')}}/food_assets/images/animaciones/listo.gif"/>
                    </p>
                @else
                    <h4>Tu pedido está en camino</h4>
                    <p>El restaurante ha enviado tu pedido</p>
                    @component('food.components.tracker.progress')
                        @slot('steps', ['complete','complete','active','disabled'])
                        @slot('pedido', $pedido)
                    @endcomponent
                    <p style="text-align: center">
                        <img src="{{URL::to('/')}}/food_assets/images/animaciones/camino.gif"/>
                    </p>
                @endif
                <div class="row">
                    <div class="col col-lg-6">
                        <p style="text-align: center">
                            @if($pedido->envio == \App\Models\Pedido::PEDIDO_ENVIO_PICKUP)
                                <a href="{{route('food.pedido.calificar', base64_encode($pedido->id))}}" class="btn btn-lg theme-btn text-white">Recogí mi pedido - Calificar al restaurante</a>
                            @else
                                <a href="{{route('food.pedido.calificar', base64_encode($pedido->id))}}" class="btn btn-lg theme-btn text-white">Mi pedido llegó - Calificar al restaurante</a>
                            @endif
                        </p>
                    </div>
                    <div class="col col-lg-6">
                        <p style="text-align: center">
                            <a href="tel:{{$pedido->negocio->telefono}}" class="btn btn-lg theme-btn text-white">Contactar al restaurante</a>
                        </p>
                    </div>
                </div>
            </div>
            <hr/>
            <h4>Detalles de tu pedido</h4>
            <div class="cart-totals-fields">
                @component('food.components.resumen_pedido')
                    @slot('pedido', $pedido)
                @endcomponent
            </div>

            <div>
                @component('food.components.info_pedido_online')
                    @slot('pedido', $pedido)
                @endcomponent
            </div>

        </div>
    </section>
    <br/>
    <script type="application/javascript">
        $(document).ready(function(){
            setInterval(
               function(){
                   $.get( "{{URL::to('api/pedido_public/'.$pedido->id)}}", function( data ) {
                       //$('.result_status').hide(0);
                       switch(data.status){
                           case '{{\App\Models\Pedido::PEDIDO_STATUS_CONFIRMADO}}':
                               $('#loading').hide();
                               $('#recibido').show(1000);
                               $('#cancelado').hide();
                               $('#preparando').hide();
                               $('#en_camino').hide();
                               $('#entregado').hide();
                               $('#pago_rechazado').hide();
                               break;
                           case '{{\App\Models\Pedido::PEDIDO_STATUS_PAGADO}}':
                               $('#loading').hide();
                               $('#recibido').show(1000);
                               $('#cancelado').hide();
                               $('#preparando').hide();
                               $('#en_camino').hide();
                               $('#entregado').hide();
                               $('#pago_rechazado').hide();
                               break;
                           case '{{\App\Models\Pedido::PEDIDO_STATUS_PREPARANDO}}':
                               $('#loading').hide();
                               $('#recibido').hide();
                               $('#cancelado').hide();
                               $('#preparando').show(1000);
                               $('#en_camino').hide();
                               $('#entregado').hide();
                               $('#pago_rechazado').hide();
                               break;
                           case '{{\App\Models\Pedido::PEDIDO_STATUS_EN_CAMINO}}':
                               $('#loading').hide();
                               $('#recibido').hide();
                               $('#preparando').hide();
                               $('#cancelado').hide();
                               $('#en_camino').show(1000);
                               $('#entregado').hide();
                               $('#pago_rechazado').hide();
                               break;
                           case '{{\App\Models\Pedido::PEDIDO_STATUS_TERMINADO}}':
                               $('#loading').hide();
                               $('#recibido').hide();
                               $('#preparando').hide();
                               $('#en_camino').hide();
                               $('#cancelado').hide();
                               $('#entregado').show(1000);
                               $('#pago_rechazado').hide();
                               $('#titulo_seccion').html('Muchas gracias por utilizar Uki');
                               break;
                           case '{{\App\Models\Pedido::PEDIDO_STATUS_ENTREGADO}}':
                               $('#loading').hide();
                               $('#recibido').hide();
                               $('#preparando').hide();
                               $('#en_camino').hide();
                               $('#cancelado').hide();
                               $('#entregado').show(1000);
                               $('#pago_rechazado').hide();
                               break;
                           case '{{\App\Models\Pedido::PEDIDO_STATUS_PAGO_RECHAZADO}}':
                               $('#loading').hide();
                               $('#recibido').hide();
                               $('#preparando').hide();
                               $('#en_camino').hide();
                               $('#entregado').hide();
                               $('#cancelado').hide();
                               $('#pago_rechazado').show(1000);
                               break;
                           case '{{\App\Models\Pedido::PEDIDO_STATUS_CANCELADO}}':
                               $('#loading').hide();
                               $('#recibido').hide();
                               $('#preparando').hide();
                               $('#en_camino').hide();
                               $('#entregado').hide();
                               $('#pago_rechazado').hide();
                               $('#cancelado').show(1000);
                               break;
                           default:
                               $('#loading').show(1000);
                               $('#recibido').hide();
                               $('#preparando').hide();
                               $('#en_camino').hide();
                               $('#entregado').hide();
                               $('#pago_rechazado').hide();
                               break;
                       }
                   });
               } , 5000);
        });
    </script>
@endsection
