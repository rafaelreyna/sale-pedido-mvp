@extends('layouts.food', ['custom_meta' => true])

@section('custom_meta')
    <meta property="og:title" content="{{$categoria->nombre}} en Uki&reg;" />
    <meta property="og:image" content="{{asset("food_assets/images/categorias/{$categoria->foto}")}}" />
    <meta property="og:description" content="Encuentra los mejores restaurantes de {{$categoria->nombre}} a través de Uki&reg;: una plataforma que conecta a los restaurantes con los consumidores de manera trasparente y justa." />
    <meta property="og:url" content="{{route('food.categoria.show', $categoria->id)}}" />
    <title>Uki&reg; @if(env('APP_ENV', 'local') == 'beta') - Beta @endif - {{$categoria->nombre}}</title>
@endsection

@section('content')

@section('content')
    <section class="containerCategoria " style="padding:0px; background-image: url('{{asset("storage/restaurantes/categorias/{$categoria->foto}")}}')">
        <div style="background-color: rgba(0,0,0, .7); padding: 0px; margin: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col col-12">
                        <br/><br/>
                        <h4>{{$categoria->nombre}}</h4>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <hr/>
    <section id="restaurantecategorias">
        <div class="container">
            <div class="row">
                @if(!($perfil->lat && $perfil->lon))
                    <p class="alert alert-warning"><i class="fa fa-warning"></i> Actualmente se muestran solo restaurantes con servicio de Pickup & Go, para mostrar restaurantes con envio a domicilio <a href="{{route('food.perfil.edit')}}?solo_ubicacion=true">configura tu ubicación</a></p>
                @endif
                @foreach($categoria->restaurantes($perfil->region_id??null, true, $perfil->lat, $perfil->lon ) as $restaurante)
                    @component('food.components.restaurante')
                        @slot('restaurante', $restaurante)
                        @slot('perfil', $perfil)
                    @endcomponent
                @endforeach
            </div>
        </div>
    </section>
    <br/>
@endsection
