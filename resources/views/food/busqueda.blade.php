@extends('layouts.food')

@section('content')
    <section class="containerCategoria " style="padding:0px; background-image: ">
        <div style="background-color: rgba(0,0,0, .7); padding: 0px; margin: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col col-12">
                        <br/><br/>
                        <h4>Resultados de la búsqueda</h4>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <hr/>
    <section id="restaurantecategorias">
        <div class="container">
            <div class="row">
                @if(count($restaurantes) > 0)
                    <p class="alert alert-info"><i class="fa fa-info-circle"></i> {{count($restaurantes)}} resultado(s) encontrados, <a href="{{route('food.home')}}">hacer una nueva búsqueda</a></p>
                    @foreach($restaurantes as $restaurante)
                        @component('food.components.restaurante')
                            @slot('restaurante', $restaurante)
                            @slot('perfil', $perfil)
                        @endcomponent
                    @endforeach
                @else
                    <p class="alert alert-waring"><i class="fa fa-warning"></i>Tu búsueda no ha generado ningún resultado, <a href="{{route('food.home')}}">vuelve a intentarlo</a></p>
                @endif
            </div>
        </div>
    </section>
    <br/>
@endsection
