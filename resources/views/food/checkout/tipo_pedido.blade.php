@extends('layouts.food')

@section('content')
    <hr/>
    <section id="carritoCompras">
        <div class="container">
            @component('food.components.checkout.progress')
                @slot('steps', ['complete','active','disabled','disabled'])
            @endcomponent

            <form method="POST" action="{{route('food.checkout.process')}}">
                {{ csrf_field() }}
                {{method_field('post')}}
                <input type="hidden" name="step" value="forma_pago">
                <div class="row" style="margin-top: 80px; margin-bottom: 80px;">
                    @foreach($tipos_pedido as $tipo_pedido)
                        @component('food.components.checkout.option')
                            @slot('options', count($tipos_pedido))
                            @slot('option', $tipo_pedido)
                        @endcomponent
                    @endforeach
                </div>

                <div class="row">
                    <hr/>
                    <div class="col col-md-12" id="btnCheckout">
                        <button onclick="history.back()" id="btnAnterior" style="float: left" type="button" class="text-white btn btn-lg theme-btn"><i
                                class="fa fa-arrow-left"></i> Anterior
                        </button>
                        <button id="btnSiguiente" @if(count($tipos_pedido) > 1) disabled @endif style="float: right" type="submit" class="text-white btn btn-lg theme-btn"><i
                                class="fa fa-arrow-right"></i> Siguiente
                        </button>
                    </div>
                </div>
                <input type="hidden" name="tipo_envio" id="hdnTipoEnvio" value="@if(count($tipos_pedido) == 1){{array_pop($tipos_pedido)['value']}}@endif">
                <input type="hidden" name="pedido_id" id="hdnPedidoId" value="{{$pedido->id}}">
            </form>
        </div>
    </section>
    <br/>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY', 'AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY')}}&libraries=places"></script>
    <script type="application/javascript">
        $(document).ready(function () {
            $('.checkout-option').click(function(){
                $('.checkout-option').removeClass('active');
                $(this).addClass('active');
                $('#btnSiguiente').removeAttr('disabled');
                $('#hdnTipoEnvio').val($(this).attr('title'));
            });
        });

    </script>
@endsection
