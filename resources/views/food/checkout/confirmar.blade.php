@extends('layouts.food')

@section('content')
    <hr/>
    <section id="carritoCompras">
        <div class="container">
            @component('food.components.checkout.progress')
                @slot('steps', ['complete','complete','complete','complete'])
            @endcomponent

            <form method="POST" action="{{route('food.checkout.process')}}">
                {{ csrf_field() }}
                {{method_field('post')}}
                <input type="hidden" name="step" value="end_checkout">

                <div class="row">
                    <div class="col col-md-6">
                        <div class="cart-totals margin-b-20">
                            <div class="cart-totals-title">
                                <h4>Confirma tu pedido</h4>
                                <br/>
                            </div>
                            <div>
                                <h5>Establecimiento: {{$pedido->negocio->nombre}}</h5>
                            </div>
                            <br/>
                            <div class="cart-totals-fields">
                                @if($pedido->isEnvioDomicilio())
                                    @component('food.components.resumen_pedido')
                                        @slot('pedido', $pedido)
                                    @endcomponent
                                @else
                                    @component('food.components.resumen_pedido')
                                        @slot('pedido', $pedido)
                                        @slot('tipo_envio', $pedido->envio)
                                    @endcomponent
                                @endif
                                <br/>
                                <h4 for="envio_gratis">Forma de pago: {{$pedido->formaPagoText()}}</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col col-md-6">
                        @if($pedido->isEnvioDomicilio())
                            <h3>Pedido a Domicilio</h3>
                            <br/>
                            <h4>Datos de envio</h4>
                            <table class="table">
                                <tr>
                                    <th>Nombre</th>
                                    <td>{{$pedido->nombre}}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{$pedido->email}}</td>
                                </tr>
                                <tr>
                                    <th>Teléfono</th>
                                    <td>{{$pedido->telefono}}</td>
                                </tr>
                                <tr>
                                    <th>Dirección</th>
                                    <td>{{$pedido->direccion}}</td>
                                </tr>
                                <tr class="envioDomicilio">
                                    <th>Distancia aprox.</th>
                                    <td>{{$pedido->distancia}} km</td>
                                </tr>
                            <!--
                            <tr class="envioDomicilio">
                                <th>Tipo de envío a domicilio.</th>
                                <td>{{$pedido->envio}}</td>
                            </tr>
                            -->
                                <tr class="envioDomicilio">
                                    <th>Costo de envio a domicilio.</th>
                                    <td>$ {{number_format($pedido->costo_envio, 2, '.', ',')}}</td>
                                </tr>

                            </table>
                        @endif

                        @if($pedido->envio == 'pickup')
                            <h3>Pedido Pick up & Go</h3>
                            <br/>
                            <h4>Datos de cliente</h4>
                            <table class="table">
                                <tr>
                                    <th>Nombre</th>
                                    <td>{{$pedido->nombre}}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{$pedido->email}}</td>
                                </tr>
                                <tr>
                                    <th>Teléfono</th>
                                    <td>{{$pedido->telefono}}</td>
                                </tr>
                            </table>
                        @endif
                        <br/>
                        <h4>Datos de restaurante</h4>
                        <table class="table">
                            <tr>
                                <th>Nombre</th>
                                <td>{{$pedido->negocio->nombre}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$pedido->negocio->email}}</td>
                            </tr>
                            <tr>
                                <th>Teléfono</th>
                                <td>{{$pedido->negocio->telefono}}</td>
                            </tr>
                            <tr>
                                <th>Dirección</th>
                                <td>{{$pedido->negocio->direccion()}}</td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <hr/>
                    <label style="float: right; margin-bottom: 20px;">
                        <input type="checkbox" id="chkAcepto" value="1">
                        He leído y acepto el <a target="_blank" href="#">aviso de privacidad</a> y <a target="_blank" href="#">los términos y condiciones</a>.
                    </label>
                    <div class="col col-md-12" id="btnCheckout">
                        <button onclick="history.back()" id="btnAnterior" style="float: left" type="button" class="text-white btn btn-lg theme-btn"><i
                                class="fa fa-arrow-left"></i> <span class="solo_desktop_inline">Anterior</span>
                        </button>
                        <button id="btnConfirmar" disabled style="float: right" type="submit" class="text-white btn btn-lg theme-btn"><i
                                class="fa fa-check"></i> Confirmar Pedido
                        </button>
                    </div>
                </div>
                <input type="hidden" name="pedido_id" id="hdnPedidoId" value="{{$pedido->id}}">
            </form>
        </div>
    </section>
    <br/>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY', 'AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY')}}&libraries=places"></script>
    <script type="application/javascript">
        $(document).ready(function () {
            $('#chkAcepto').change(function(){
                if($(this).is(':checked')) {
                    $('#btnConfirmar').prop( "disabled", false);
                } else {
                    $('#btnConfirmar').prop( "disabled", true);
                }
            });
        });

    </script>
@endsection
