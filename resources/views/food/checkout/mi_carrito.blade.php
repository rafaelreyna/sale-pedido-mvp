@extends('layouts.food')

@section('content')
    <hr/>
    <section id="carritoCompras">
        <div class="container">
            @component('food.components.checkout.progress')
                @slot('steps', ['active','disabled','disabled','disabled'])
            @endcomponent

            <form method="POST" action="{{route('food.checkout.process')}}">
                {{ csrf_field() }}
                {{method_field('post')}}
                <input type="hidden" name="step" value="tipo_pedido">
                <div class="row">
                    <div class="col col-md-8">
                        <div class="cart-totals margin-b-20">
                            <div class="cart-totals-title">
                                @if($instancia && $total != 0 && count($carrito) != 0)
                                <div class="solo_desktop">
                                <a href="{{route('food.cart.clear')}}" style="float:right; margin-left: 4px;"
                                   class=" btn btn-sm theme-btn text-white"><i class="fa fa-times"></i> Vaciar
                                    Carrito</a>
                                <a href="{{route('food.restaurante.show',$restaurante->id)}}" style="float:right"
                                   class=" btn btn-sm theme-btn text-white"><i class="fa fa-shopping-cart"></i> Agregar
                                    más productos</a>
                                </div>
                                @endif
                                <h4>Tu carrito de compras</h4>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul style="margin: 0px;">
                                            @foreach ($errors->all() as $error)
                                                <li><i class="flaticon-alert"></i> {{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <br/>
                            </div>
                            @if($instancia && $total != 0 && count($carrito) != 0)
                                <div>
                                    <h5>Establecimiento: {{$restaurante->nombre}}</h5>
                                </div>
                                <br/>
                                <div class="cart-totals-fields">
                                    <table class="table">
                                        <thead>
                                        <th>Platillo</th>
                                        <th>Cantidad</th>
                                        <th>Precio</th>
                                        <th></th>
                                        <th></th>
                                        </thead>
                                        <tbody>
                                        @foreach($carrito as $item)
                                            <tr>
                                                <td>
                                                    {{$item->name}}
                                                    <input type="hidden" name="item[{{$item->id}}]"
                                                           value="{{base64_encode(json_encode($item))}}">
                                                    @if(isset($item->options['comentarios_cliente']))
                                                        <pre
                                                            style="margin: 3px!important; padding: 5px!important; font-size: 12px; max-width: 200px; text-overflow: ellipsis; overflow: hidden;">{{$item->options['comentarios_cliente']}}</pre>
                                                        <br/>
                                                    @endif
                                                </td>
                                                <td>{{$item->quantity}}</td>
                                                <td>
                                                    $ {{number_format($item->price * $item->quantity, 2, '.', ',')}}</td>
                                                <td>
                                                    <a href="{{route("food.platillo.edit", [$item->id, $item->getUniqueId()])}}"
                                                       class="small"><i class="fa fa-pencil"></i> editar</a></td>
                                                <td>
                                                    <a href="{{route("food.cart.items.delete", [$restaurante->id, $item->getUniqueId()])}}"
                                                       class="small"><i class="fa fa-times"></i> eliminar</a></td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td class="text-color"><strong>Total</strong></td>
                                            <td></td>
                                            <td class="text-color">
                                                <strong>$ {{number_format($total, 2, '.', ',')}}</strong></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <hr/>
                                    <div class="row form-group">
                                        <div class="col-xs-12 col-md-12">
                                            <textarea class="form-control txtConteoCaracteres" maxlength="150"
                                                      id="comentarios_cliente" name="comentarios_cliente"
                                                      placeholder="Otras indicaciones del pedido que no afecte el precio total del pedido. Ej: Salsas por separado">{{old('comentarios_cliente')?old('comentarios_cliente'):''}}</textarea>
                                            <div class="conteoCaracteresContenedor"><span
                                                    class="conteoCaracteres">0</span> / 150
                                            </div>
                                        </div>
                                    </div>
                                    <div class="solo_movil">
                                        <a href="{{route('food.cart.clear')}}" style="float:left; margin-right: 4px;"
                                           class=" btn btn-sm theme-btn text-white"><i class="fa fa-times"></i> Vaciar</a>
                                        <a href="{{route('food.restaurante.show',$restaurante->id)}}" style="float:left"
                                           class=" btn btn-sm theme-btn text-white"><i class="fa fa-shopping-cart"></i> Agregar más</a>
                                        <div style="float: none; clear: both"></div>
                                        <hr/>
                                    </div>
                                    <small>
                                        <i class="fa fa-info-circle"></i> Todos los montos están en moneda nacional, e
                                        incluyen impuestos.<br/>
                                        <i class="fa fa-info-circle"></i> Las indicaciones de platillos y del pedido
                                        están sujetas a la validación del restaurant
                                    </small>
                                </div>
                            @else
                                <p class="alert alert-warning"><i class="fa fa-shopping-cart"></i>
                                    Tu carrito de compras estrá vacío. <a href="{{route('food.home')}}">Explorar
                                        categorías</a>
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="col col-md-4">


                        <div id="formDatosUsuario" class="form-horizontal contact-form" role="form">
                            <h4>Tu información</h4>
                                <fieldset>
                                    <div class="row form-group">
                                        <div class="col-xs-12 col-md-6">
                                            <input class="form-control" id="nombre" name="nombre" type="text"
                                                   placeholder="Nombre *"
                                                   value="{{old('nombre')?old('nombre'):$perfil->nombre}}">
                                        </div>

                                        <div class="col-xs-12 col-md-6">
                                            <input class="form-control" id="apellidos" name="apellidos" type="text"
                                                   placeholder="Apellidos *"
                                                   value="{{old('apellidos')?old('apellidos'):$perfil->apellidos}}">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-6">
                                            <input class="form-control" id="email" name="email" type="email"
                                                   placeholder="Email *"
                                                   value="{{old('email')?old('email'):$perfil->email}}">
                                        </div>
                                        <div class="col-xs-6">
                                            <input class="form-control" id="telefono" name="telefono" type="tel"
                                                   placeholder="Celular *"
                                                   value="{{old('telefono')?old('telefono'):$perfil->telefono}}">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12 col-md-12">
                                            <textarea autocomplete="disabled" class="form-control" id="pac-input"
                                                      name="direccion" placeholder="Direccion" disabled
                                                      >{{old('direccion')?old('direccion'):$perfil->direccion}}</textarea>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12 col-md-12">
                                            <textarea class="form-control" id="pac-input" name="referencias"
                                                      placeholder="Referencias">{{old('referencias')?old('referencias'):(isset($perfil->referencias)?$perfil->referencias:"")}}</textarea>
                                        </div>
                                    </div>

                                    <div id="map" style="height: 300px;" data-zoom="14"></div>
                                    <input type="hidden" class="form-control @error('lon') is-invalid @enderror"
                                           id="lon" name="lon" placeholder=""
                                           value="{{old('lon')?old('lon'):$perfil->lon}}">
                                    <input type="hidden" class="form-control @error('lat') is-invalid @enderror"
                                           id="lat" name="lat" placeholder=""
                                           value="{{old('lat')?old('lat'):$perfil->lat}}">
                                    <br/>
                                </fieldset>
                        </div>
                        <p style="text-align: right; font-size: 12px;">¿No es esta tu dirección? <a href="{{route('food.perfil.edit')}}">Cambia tus datos</a></p>
                    </div>
                </div>
                @if($instancia && $total != 0 && count($carrito) != 0)
                    <input type="hidden" name="negocio_id" value="{{$restaurante->id}}">
                    <div class="row">
                        <hr/>
                        <div class="col col-md-12" id="btnCheckout">
                            <button style="float: right" type="submit" class="text-white btn btn-lg theme-btn"><i
                                    class="fa fa-arrow-right"></i> Siguiente
                            </button>
                        </div>
                    </div>
                @endif
                <input type="hidden" name="tipo_envio" id="hdnTipoEnvio">
            </form>
        </div>
    </section>
    <br/>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY', 'AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY')}}&libraries=places"></script>
    <script type="application/javascript">
        $(document).ready(function () {
            $('#pac-input').attr('autocomplete', 'chrome-off');

            map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: {{$perfil->lat?$perfil->lat:'20.6122138'}},
                    lng: {{$perfil->lon?$perfil->lon:'-100.4452366'}},
                },
                zoom: 15,
                scrollwheel: true,
                heigth: 250
            });

            marker = new google.maps.Marker({
                position: {
                    lat: {{$perfil->lat?$perfil->lat:'20.6122138'}},
                    lng: {{$perfil->lon?$perfil->lon:'-100.4452366'}}
                },
                map: map,
                draggable: false,
                title: '{{$perfil->nombre}}'
            });

        });
    </script>
@endsection
