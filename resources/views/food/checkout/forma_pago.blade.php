@extends('layouts.food')

@section('content')
    <hr/>
    <section id="carritoCompras">
        <div class="container">
            @component('food.components.checkout.progress')
                @slot('steps', ['complete','complete','active','disabled'])
            @endcomponent

            <form method="POST" action="{{route('food.checkout.process')}}">
                {{ csrf_field() }}
                {{method_field('post')}}
                <input type="hidden" name="step" value="confirmar">
                <div class="row" style="margin-top: 80px; margin-bottom: 80px;">
                    @foreach($formas_pago as $forma_pago)
                        @component('food.components.checkout.option')
                            @slot('options', count($formas_pago))
                            @slot('option', $forma_pago)
                        @endcomponent
                    @endforeach
                </div>

                <div class="row">
                    <hr/>
                    <div class="col col-md-12" id="btnCheckout">
                        <button onclick="history.back()" id="btnAnterior" style="float: left" type="button" class="text-white btn btn-lg theme-btn"><i
                                class="fa fa-arrow-left"></i> Anterior
                        </button>
                        <button id="btnSiguiente" @if(count($formas_pago) > 1) disabled @endif style="float: right" type="submit" class="text-white btn btn-lg theme-btn"><i
                                class="fa fa-arrow-right"></i> Siguiente
                        </button>
                    </div>
                </div>
                <input type="hidden" name="forma_pago" id="hdnFormaPago" value="@if(count($formas_pago) == 1){{array_pop($formas_pago)['value']}}@endif">
                <input type="hidden" name="pedido_id" id="hdnPedidoId" value="{{$pedido->id}}">
            </form>
        </div>
    </section>
    <br/>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY', 'AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY')}}&libraries=places"></script>
    <script type="application/javascript">
        $(document).ready(function () {
            $('.checkout-option').click(function(){
                $('.checkout-option').removeClass('active');
                $(this).addClass('active');
                $('#btnSiguiente').removeAttr('disabled');
                $('#hdnFormaPago').val($(this).attr('title'));
            });
        });

    </script>
@endsection
