@extends('layouts.food', ['custom_meta' => true, 'menu_online' => true])

@section('custom_meta')
    <meta property="og:title" content="{{$restaurante->nombre}} en Uki&reg;" />
    @if($restaurante->logo)
        <meta property="og:image" content="{{route('images.public', [base64_encode('negocios/logos/'), $restaurante->logo])}}" />
    @else
        <meta property="og:image" content="https://uki.mx/assets/img/logo.png" />
    @endif
    <meta property="og:description" content="Disfruta los platillos de {{$restaurante->nombre}} a través de Uki&reg;: una plataforma que conecta a los restaurantes con los consumidores de manera trasparente y justa." />
    <meta property="og:url" content="{{route('food.restaurante.show', $restaurante->id)}}" />
    <title>Uki&reg; @if(env('APP_ENV', 'local') == 'beta') - Beta @endif - {{$restaurante->nombre}}</title>
@endsection

@section('content')
<br/>
    <section>
        <div class="container">
            <div class="row">
                <div class="col col-12">
                    <p style="text-align: center">
                        <a class="img-fluid" href="{{route('food.restaurante.show',$restaurante->id)}}" style="@if(!$restaurante->activo) filter: grayscale(1) @endif">
                            @if($restaurante->logo)
                                <img class="logo-negocio-small" src="{{route('images.public', [base64_encode('negocios/logos/'), $restaurante->logo])}}"/>
                            @else
                                <img class="logo-negocio-small" src="{{URL::to('/assets/img')}}/sin_imagen.jpg"/>
                            @endif
                        </a>
                    </p>
                    <h5 style="text-align: center">{{$restaurante->nombre}}</h5>
                    <div style="text-align: center; font-size: 12px;">

                        <ul class="navbar-nav" id="menu-categorias">
                            @foreach($restaurante->platillo_categorias as $categoria)
                                <li class="nav-item">
                                    <a class="nav-link linkCategoria" href="#{{$categoria->slug()}}">{{$categoria->nombre}} <span class="sr-only">(current)</span></a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <hr/>
    <section id="restaurantecategorias">
        <div class="container">
            @foreach($restaurante->platillo_categorias as $categoria)
                @component('food.components.categoriaRestaurante')
                    @slot('categoria', $categoria)
                    @slot('restaurante', $restaurante)
                    @slot('perfil', null)
                    @slot('menu_online', true)
                @endcomponent
            @endforeach
        </div>
    </section>
    <br/>
<style>
    #menu-categorias {
        z-index: 9;
        background-color: #FFF;
        min-width: 100%;
        left: 0px;
        white-space: nowrap;
        width: 100%;
        overflow: auto;
        padding-right: 30px;
    }
</style>
<script type="text/javascript">
    $(document).ready(function(){

        $("#menu-categorias").sticky({topSpacing:84, zIndex:9999});

        $('.linkCategoria').click(function(){
            var goto = $(this).attr('href');
            $([document.documentElement, document.body]).animate({
                scrollTop: $(goto).offset().top - 140
            }, 600);
            return false;
        });
    });
</script>
@endsection
