@extends('layouts.food')
@section('content')
    <hr/>
    <section id="carritoCompras">
        <div class="container">
            <form method="POST" action="{{route('food.cart.confirm')}}">
                {{ csrf_field() }}
                {{method_field('post')}}
            <div class="row">
                <div class="col col-md-6">
                    <div class="cart-totals margin-b-20">
                        <div class="cart-totals-title">
                            <h4>Confirma tu pedido</h4>
                            <p class="alert alert-info"><i class="fa fa-info-circle"></i> Todos los montos están en moneda nacional, e incluyen impuestos</p>
                            <br/>
                        </div>
                            <div>
                                <h5>Establecimiento: {{$pedido->negocio->nombre}}</h5>
                            </div>
                        <br/>
                        <div class="cart-totals-fields">
                            @component('food.components.resumen_pedido')
                                @slot('pedido', $pedido)
                                @slot('tipo_envio', $tipo_envio)
                            @endcomponent
                            <br/>
                            <div class="form-group">
                                <label for="envio_gratis">Forma de pago</label>
                                <select type="text" class="form-control" id="forma_pago" name="forma_pago">
                                    @foreach($formas_pago as $forma_pago)
                                        <option value="{{$forma_pago['value']}}">{{$forma_pago['opcion']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-md-6">
                    @if($tipo_envio == 'domicilio')
                    <h3>Pedido a Domicilio</h3>
                    <br/>
                    <h4>Datos de envio</h4>
                        <table class="table">
                            <tr>
                                <th>Nombre</th>
                                <td>{{$pedido->nombre}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$pedido->email}}</td>
                            </tr>
                            <tr>
                                <th>Teléfono</th>
                                <td>{{$pedido->telefono}}</td>
                            </tr>
                            <tr>
                                <th>Dirección</th>
                                <td>{{$pedido->direccion}}</td>
                            </tr>
                            <tr class="envioDomicilio">
                                <th>Distancia aprox.</th>
                                <td>{{$pedido->distancia}} km</td>
                            </tr>
                            <!--
                            <tr class="envioDomicilio">
                                <th>Tipo de envío a domicilio.</th>
                                <td>{{$pedido->envio}}</td>
                            </tr>
                            -->
                            <tr class="envioDomicilio">
                                <th>Costo de envio a domicilio.</th>
                                <td>$ {{number_format($pedido->costo_envio, 2, '.', ',')}}</td>
                            </tr>

                        </table>
                    @endif

                    @if($tipo_envio == 'pickup')
                            <h3>Pedido Pick up & Go</h3>
                            <br/>
                            <h4>Datos de cliente</h4>
                            <table class="table">
                                <tr>
                                    <th>Nombre</th>
                                    <td>{{$pedido->nombre}}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{$pedido->email}}</td>
                                </tr>
                                <tr>
                                    <th>Teléfono</th>
                                    <td>{{$pedido->telefono}}</td>
                                </tr>
                            </table>
                    @endif
                        <br/>
                        <h4>Datos de restaurante</h4>
                        <table class="table">
                            <tr>
                                <th>Nombre</th>
                                <td>{{$pedido->negocio->nombre}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$pedido->negocio->email}}</td>
                            </tr>
                            <tr>
                                <th>Teléfono</th>
                                <td>{{$pedido->negocio->telefono}}</td>
                            </tr>
                            <tr>
                                <th>Dirección</th>
                                <td>{{$pedido->negocio->direccion()}}</td>
                            </tr>
                        </table>
                </div>
            </div>
                <input type="hidden" name="negocio_id" value="{{$pedido->negocio->id}}">
                <input type="hidden" name="pedido_id" value="{{$pedido->id}}">
                <input type="hidden" name="envio" value="{{$tipo_envio}}">
                <div class="row">
                    <div class="col col-md-312">
                        <label>
                            <input type="checkbox" id="chkAcepto" value="1">
                            He leído y acepto el <a target="_blank" href="#">aviso de privacidad</a> y <a target="_blank" href="#">los términos y condiciones</a>.
                        </label>
                        <div class="pagoOffline">
                            <a style="margin: 5px;" href="{{route('food.checkout.index')}}" class="text-white btn theme-btn"><i class="fa fa-arrow-left"></i> Modificar Pedido</a>
                            &nbsp;&nbsp;&nbsp;
                            <button style="margin: 5px;" id="btnConfirmar" type="submit" disabled="true" class="text-white btn theme-btn"><i class="fa fa-arrow-right"></i> Confirmar Pedido</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <br/>
    <script type="application/javascript">
        $(document).ready(function(){
            $('#forma_envio').change(function(){
                if($(this).val() == 'domicilio'){
                    $(".envioDomicilio").show();
                } else {
                    $(".envioDomicilio").hide();
                }
            });

            $('#chkAcepto').change(function(){
                if($(this).is(':checked')) {
                    $('#btnConfirmar').prop( "disabled", false);
                } else {
                    $('#btnConfirmar').prop( "disabled", true);
                }
            });
        });
    </script>
@endsection
