@foreach([1,2,3,4,5] as $star)
    @if($calificacion >= $star)<i class="fa fa-star"></i> @endif()
    @if($calificacion < $star)
        @if($star - $calificacion > 0.5)
            <i class="fa fa-star-o"></i>
        @else
            <i class="fa fa-star-half-empty"></i>
        @endif
    @endif
@endforeach
