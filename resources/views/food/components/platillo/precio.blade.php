@if(!is_null($platillo->precio_promo))
    <small>Antes: <span class="old_price">$ {{$platillo->precio}}</span></small><br/>
    <span class="price" style="color:#ee072c; margin: 0px;" >$ {{$platillo->precio_final()}}</span>
@else
    <span class="price">$ {{$platillo->precio_final()}}</span>
@endif
@if($botones)
    @if(isset($perfil) && isset($perfil->region_id) & ($perfil->lat && $perfil->lon))
        @if($platillo->disponible && $restaurante->activo)
            <!-- <input type="submit" class="btn theme-btn-dash pull-right" value="Agregar"/> -->
            <a href="{{route('food.platillo.show', $platillo->id)}}" class="btnAgregarPlatillo btn theme-btn pull-right" style="margin-right: 5px;"><i class="fa fa-plus"></i></a>
        @else
            <h6>No disponible por el momento</h6>
        @endif
    @else
        @if($platillo->disponible && $restaurante->activo)
            <!-- <input type="submit" class="btn theme-btn-dash pull-right" value="Agregar"/> -->
            <a href="{{route('food.perfil.location.edit')."?redirect=".urlencode(request()->getRequestUri())}}" class="btnAgregarPlatillo btn theme-btn pull-right" style="margin-right: 5px;"><i class="fa fa-plus"></i></a>
        @else
            <h6>No disponible por el momento</h6>
        @endif
    @endif
@endif
