<div class="row bs-wizard" style="border-bottom:0; margin-bottom: 50px;margin-top: 30px;">
    <div class="col-xs-3 bs-wizard-step {{$steps[0]}}">
        <div class="text-center bs-wizard-stepnum solo_desktop">Mi Carrito</div>
        <div class="text-center bs-wizard-stepnum solo_movil">Carrito</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"></a>
    </div>

    <div class="col-xs-3 bs-wizard-step {{$steps[1]}}"><!-- complete -->
        <div class="text-center bs-wizard-stepnum solo_desktop">Tipo de pedido</div>
        <div class="text-center bs-wizard-stepnum solo_movil">Pedido</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"></a>
    </div>

    <div class="col-xs-3 bs-wizard-step {{$steps[2]}}"><!-- complete -->
        <div class="text-center bs-wizard-stepnum solo_desktop">Forma de Pago</div>
        <div class="text-center bs-wizard-stepnum solo_movil">Pago</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"></a>
    </div>

    <div class="col-xs-3 bs-wizard-step {{$steps[3]}}"><!-- active -->
        <div class="text-center bs-wizard-stepnum solo_desktop">Confirmar</div>
        <div class="text-center bs-wizard-stepnum solo_movil">Confirmar</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"></a>
    </div>
</div>
