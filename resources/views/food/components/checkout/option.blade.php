<div class="col-md-{{12/$options}}" style="margin-bottom: 20px">
    <div class="checkout-option @if($options == 1) active @endif" title="{{$option['value']}}">
        <p style="text-align: center; font-size: 64px;"><i class="fa fa-{{$option['icon']}}"></i></p>
        <h3 style="text-align: center">{!!  $option['opcion']!!}</h3>
    </div>
</div>
