<section class="hero bg-image" data-image-src="{{URL::to("/food_assets/images/search_bg.jpg")}}" style="background-position: bottom!important; background-size: cover">
    <div class="hero-inner heroInnerNeedsLocation" >
        <div class="container text-center hero-text font-white">
            <h1>Espera, necesitamos tu dirección </h1>
            <h5 class="font-white space-xs">para poder disfrutar la experiencia UKI&reg;</h5>
            <hr/>
            <br/>
            <div class="banner-form">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin: 0px;">
                            @foreach ($errors->all() as $error)
                                <li><i class="flaticon-alert"></i> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form  class="form-inline" autocomplete="disabled" method="POST" action="{{route('food.perfil.update')}}?solo_ubicacion=true&redirect={{$_GET['redirect']??''}}">
                    {{ csrf_field() }}
                    {{method_field('put')}}
                    <input type="hidden" name="_url" value="{{\Illuminate\Support\Facades\URL::previous()}}">
                    <div class="form-group txtDireccion">
                        <label class="sr-only" for="exampleInputAmount">¿Cuál es tu dirección?</label>
                        <div id="pac-input-contqainer" class="form-group" style="width: 100%">
                            <input style="width: 100%" autocomplete="disabled" required id="pac-input" name="direccion" type="text" name="direccion" class="form-control form-control-lg" placeholder="¿Cuál es tu dirección?"> </div>
                    </div><button type="submit" class="btn theme-btn btn-lg solo_desktop_inline">Guardar</button>

                    <div class="heroMap" id="map"  data-zoom="14"></div>
                    <p>*Tip, puedes arrastrar el marcador en el mapa para ajustar tu ubicación</p>
                    <p style="text-align: center"><button style="margin: auto;" type="submit" class="btn theme-btn btn-lg solo_movil">Guardar</button></p>
                    <br />
                    <p><a href="#" class="btnUbicacion btn  btn-lg"><i class="fa fa-marker"></i> Usar mi ubicación</a></p>
                    <input type="hidden" class="form-control @error('lon') is-invalid @enderror"
                           id="lon" name="lon" placeholder=""
                           value="{{old('lon')?old('lon'):$perfil->lon}}">
                    <input type="hidden" class="form-control @error('lat') is-invalid @enderror"
                           id="lat" name="lat" placeholder=""
                           value="{{old('lat')?old('lat'):$perfil->lat}}">
                    <br/>
                </form>
            </div>
            <!-- end:Steps -->
        </div>
    </div>
    <!--end:Hero inner -->

    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY', 'AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY')}}&libraries=places"></script>
    <script type="application/javascript">
        $(document).ready(function(){
            $('#pac-input').attr('autocomplete','chrome-off');

            map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: {{$perfil->lat?$perfil->lat:'20.6122138'}},
                    lng: {{$perfil->lon?$perfil->lon:'-100.4452366'}},
                },
                zoom: 15,
                scrollwheel: true,
                heigth: 250
            });

            marker = new google.maps.Marker({
                position: {
                    lat: {{$perfil->lat?$perfil->lat:'20.6122138'}},
                    lng: {{$perfil->lon?$perfil->lon:'-100.4452366'}}
                },
                map: map,
                draggable:true,
                title: '{{$perfil->nombre}}'
            });

            google.maps.event.addListener(marker, 'dragend', function (event) {
                document.getElementById("lat").value = event.latLng.lat();
                document.getElementById("lon").value = event.latLng.lng();
            });

            // Create the search box and link it to the UI element.
            const input = document.getElementById("pac-input");
            const searchBox = new google.maps.places.SearchBox(input);
            //map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(input);
            // Bias the SearchBox results towards current map's viewport.
            map.addListener("bounds_changed", () => {
                searchBox.setBounds(map.getBounds());
            });

            let markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener("places_changed", () => {
                const places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }
                // Clear out the old markers.
                markers.forEach((marker) => {
                    marker.setMap(null);
                });

                const bounds = new google.maps.LatLngBounds();
                places.forEach((place) => {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }

                    marker.setPosition(place.geometry.location);
                    document.getElementById("lat").value = place.geometry.location.lat();
                    document.getElementById("lon").value = place.geometry.location.lng();

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });

            @component('food.components.hero.ubicacion_funcionalidad')
            @endcomponent
        });

    </script>
</section>
