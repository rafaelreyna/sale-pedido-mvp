<section class="hero bg-image solo_desktop" data-image-src="{{URL::to("/food_assets/images/search_bg.jpg")}}" style="background-position: bottom!important; background-size: cover">
    <div class="hero-inner">
        <div class="container text-center hero-text font-white">
            <h1> Pide directo a tu lugar favorito </h1>
            <!-- <h5 class="font-white space-xs">Compra directo, sin comisiones ni precios inflados</h5> -->
            <div class="banner-form">
                <form class="form-inline" method="GET" action="{{route('food.restaurante.search')}}">
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputAmount">¿Qué se te antoja hoy?...</label>
                        <div class="form-group">
                            <input type="text" name="buscar" class="form-control form-control-lg" id="exampleInputAmount" placeholder="¿Qué se te antoja hoy?..."> </div>
                    </div>
                    <button type="submit" class="btn theme-btn btn-lg">Buscar</button>
                </form>
            </div>
            @component('food.components.hero.steps')@endcomponent
            <!-- end:Steps -->
        </div>
    </div>
    <img id="hero-garantia-uki" src="{{asset('food_assets/images/garantia_uki.png')}}">
    <img id="hero-arrow-down" src="{{asset('food_assets/images/arrow-down-1.gif')}}">
    <!--end:Hero inner -->
</section>
<section class="containerBuscar solo_movil"><br/>
    <div class="container">
        <div class="row">
            <div class="row">
                <div class="col col-12">
                    <h4>¿Qué se te antoja comer hoy?</h4>
                    <form class="form-inline" method="GET" action="{{route('food.restaurante.search')}}">
                        <div class="form-group" style="padding-left: 40px; padding-right: 40px">
                            <input type="text" class="form-control form-control" id="exampleInputAmount" placeholder="Restaurante, tipo de comida o platillo....">
                        </div>
                        <p style="text-align: center">
                            <button type="submit" class="btn theme-btn "><i class="fa fa-search"></i> Buscar</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
</section>
