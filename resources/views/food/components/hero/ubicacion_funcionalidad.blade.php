$('.btnUbicacion').click(function(){
if ("geolocation" in navigator) {
const onUbicacionConcedida = ubicacion => {
console.log("Tengo la ubicación: ", ubicacion);
// document.location = '{{\Illuminate\Support\Facades\URL::to('/food')}}/perfil/ubicacion/' + ubicacion.coords.latitude + '/' +ubicacion.coords.longitude;

marker.setPosition(new google.maps.LatLng(ubicacion.coords.latitude, ubicacion.coords.longitude));
map.setCenter(new google.maps.LatLng(ubicacion.coords.latitude, ubicacion.coords.longitude));
document.getElementById("lat").value = ubicacion.coords.latitude;
document.getElementById("lon").value = ubicacion.coords.longitude;

var url = '{{URL::to('/food/buscar-direccion/')}}/' + ubicacion.coords.latitude + '/' + ubicacion.coords.longitude;
var direccion = httpGet(url)

var html = $("<div />").append($("#pac-input").clone()).html();
$('#pac-input').remove();
$('#pac-input-contqainer').append(html);
$('#pac-input').val(direccion);

searchBox.

Swal.fire({
title: 'Ubicacion encontrada',
text: "Revisa tu direccion para verificar que es la adecuada",
type: 'success',
showCancelButton: false,
confirmButtonColor: '#666',
confirmButtonText: 'cerrar'
}).then(function (result) {

});
}

const onErrorDeUbicacion = err => {
Swal.fire({
title: 'Error intentando determinar tu ubicación',
text: "En este momento no hemos podido determinar tu ubicación, para poder disfrutar la experiencia UKI&reg; configurar tu ubicación",
type: 'info',
showCancelButton: false,
confirmButtonColor: '#666',
confirmButtonText: 'Ingresar mi dirección manualmente'
}).then(function (result) {

});
}

const opcionesDeSolicitud = {
enableHighAccuracy: true, // Alta precisión
maximumAge: 0, // No queremos caché
timeout: 2000 // Esperar solo 5 segundos
};
// Solicitar
navigator.geolocation.getCurrentPosition(onUbicacionConcedida, onErrorDeUbicacion, opcionesDeSolicitud);
}
});

function httpGet(theUrl)
{
var xmlHttp = new XMLHttpRequest();
xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
xmlHttp.send( null );
return xmlHttp.responseText;
}
