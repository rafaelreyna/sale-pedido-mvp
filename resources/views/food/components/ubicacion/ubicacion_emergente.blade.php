@if(!isset($perfil->region_id))
    Swal.fire({
    title: '¿En donde te encuentras?',
    text: "Debes indicarnos tu región para poder mostrarte los restaurantes correctos",
    type: 'warning',
    input: 'select',
    inputOptions: { @foreach(\App\Models\Region::all() as $region) "{{$region->id}}":"{{$region->nombre}}", @endforeach },
    inputPlaceholder: 'Selecciona una region',
    showCancelButton: false,
    confirmButtonColor: '#666',
    confirmButtonText: 'Seleccionar región'
    }).then(function (result) {
    if (result.value) {
    document.location = '{{\Illuminate\Support\Facades\URL::to('/food')}}/perfil/region/' + result.value;
    }
    });
@else
    @if(!($perfil->lat && $perfil->lon))
        if ("geolocation" in navigator) {
        const onUbicacionConcedida = ubicacion => {
        console.log("Tengo la ubicación: ", ubicacion);
        document.location = '{{\Illuminate\Support\Facades\URL::to('/food')}}/perfil/ubicacion/' + ubicacion.coords.latitude + '/' +ubicacion.coords.longitude;
        }

        const onErrorDeUbicacion = err => {
        Swal.fire({
        title: 'Error intentando determinar tu ubicación',
        text: "En este momento no hemos podido determinar tu ubicación, para poder ver restaurantes con envío a domicilio debes configurar tu ubicación",
        type: 'info',
        showCancelButton: true,
        confirmButtonColor: '#666',
        confirmButtonText: 'Configurar mi ubicacion'
        }).then(function (result) {
        if (result.value) {
        document.location = '{{route('food.perfil.edit')}}?solo_ubicacion=true';
        }
        });
        }

        const opcionesDeSolicitud = {
        enableHighAccuracy: true, // Alta precisión
        maximumAge: 0, // No queremos caché
        timeout: 5000 // Esperar solo 5 segundos
        };
        // Solicitar
        navigator.geolocation.getCurrentPosition(onUbicacionConcedida, onErrorDeUbicacion, opcionesDeSolicitud);
        }
    @endif
@endif
