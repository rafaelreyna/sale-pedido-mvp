<div class="bg-gray restaurant-entry">
    <div class="row" @if(!$restaurante->activo) style=" filter: grayscale(1)" @endif>
        <div class="col-sm-12 col-md-12 col-lg-8 text-xs-center text-sm-left">
            <div class="entry-logo">
                <a class="img-fluid" href="{{route('food.restaurante.show',$restaurante->id)}}">
                    @if($restaurante->logo)
                        <img class="logo-negocio" src="{{route('images.public', [base64_encode('negocios/logos/'), $restaurante->logo])}}"/>
                    @else
                        <img class="logo-negocio" src="{{URL::to('/assets/img')}}/sin_imagen.jpg"/>
                    @endif
                </a>
            </div>
            <!-- end:Logo -->
            <div class="entry-dscr">
                <h5>{{$restaurante->nombre}}</h5>
                <span></span>
                @component('food.components.restaurante_datos')
                    @slot('restaurante', $restaurante)
                    @slot('perfil', $perfil)
                @endcomponent
            </div>
            <!-- end:Entry description -->
        </div>
        <div class="col-sm-12 col-md-12 col-lg-4 text-xs-center">
            <div class="right-content bg-white">
                <div class="right-review">
                    <span>{{$restaurante->calificacion}}</span>
                    <div class="rating-block">
                        @component('food.components.calificacion')
                            @slot('calificacion', $restaurante->calificacion)
                        @endcomponent
                    </div>
                    <p><small>{{count($restaurante->calificaciones)}} Calificaciones</small></p>
                    @if($restaurante->activo)
                        <a href="{{route('food.restaurante.show',$restaurante->slugOrId())}}" class="btn theme-btn-dash">Ver Menú</a> </div>
                @else
                    <h6 style="color: #900!important;">No disponible en este momento, fuera de horario</h6>
                @endif
            </div>
            <!-- end:right info -->
        </div>
    </div>
    <!--end:row -->
</div>
