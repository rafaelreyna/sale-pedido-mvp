<ul class="list-inline">
    @if($restaurante->enviosconfig && $restaurante->enviosconfig->getDistanciaAlcance($perfil->lat, $perfil->lon))
        @if($restaurante->enviosconfig->puedeEnviarEstaDistancia())
            @if($restaurante->enviosconfig &&$restaurante->enviosconfig->envio_gratis)
            <li class="list-inline-item"><i class="fa fa-star"></i> Envío Gratis*</li>
            @endif
            @if($restaurante->enviosconfig && $restaurante->enviosconfig->tipo_envio == "dinamico")
            <li class="list-inline-item"><i class="fa fa-motorcycle"></i> Envío a Domicilio</li>
            @endif
            @if($restaurante->enviosconfig && $restaurante->enviosconfig->tipo_envio == "fijo")
            <li class="list-inline-item"><i class="fa fa-motorcycle"></i> Envío a Domicilio</li>
            @endif
        @endif
    @endif

    @if($restaurante->enviosconfig && $restaurante->enviosconfig->pickup)
    <li class="list-inline-item">
        <i class="fa fa-car"></i> Pick up & Go
        <a target="_blank" href="https://www.google.com/maps/search/?api=1&query={{$restaurante->lat}},{{$restaurante->lon}}">
            ( <i class="fa fa-map-marker"></i> Ver ubicación )
        </a>
    </li>
    @endif
</ul>
@if($restaurante->enviosconfig && $restaurante->enviosconfig->getDistanciaAlcance($perfil->lat, $perfil->lon))
    @if($restaurante->enviosconfig->puedeEnviarEstaDistancia())
        @if($restaurante->enviosconfig && $restaurante->enviosconfig->envio_gratis)
        <p style="font-size: 11px;">Envío gratis a partir de ${{$restaurante->enviosconfig->envio_gratis_minimo_cantidad}} y con distancia máxima de {{$restaurante->enviosconfig->envio_gratis_maximo_km}}km </p>
        @endif
    @endif
@endif
<ul class="list-inline">
    @if($restaurante->config->tiempo_estimado)
    <li class="list-inline-item"><i class="fa fa-clock-o"></i> {{$restaurante->config->tiempo_estimado}} min. Tiempo estimado de preparación.</li>
    @endif
    @if($restaurante->distancia)
    <li class="list-inline-item"><i class="fa fa-map"></i> {{$restaurante->distancia}} km aprox.</li>
    @endif
</ul>
