<div class="row bs-wizard" style="border-bottom:0; margin-bottom: 50px;margin-top: 30px;">
    <div class="col-xs-3 bs-wizard-step {{$steps[0]}}">
        <div class="text-center bs-wizard-stepnum solo_desktop">Nuevo Pedido</div>
        <div class="text-center bs-wizard-stepnum solo_movil">Nuevo</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"></a>
    </div>

    <div class="col-xs-3 bs-wizard-step {{$steps[1]}}"><!-- complete -->
        <div class="text-center bs-wizard-stepnum solo_desktop">Cocinando</div>
        <div class="text-center bs-wizard-stepnum solo_movil">Cocinando</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"></a>
    </div>


    <div class="col-xs-3 bs-wizard-step {{$steps[2]}}"><!-- complete -->
        @if($pedido->envio == \App\Models\Pedido::PEDIDO_ENVIO_PICKUP)
            <div class="text-center bs-wizard-stepnum solo_desktop">Pedido listo</div>
            <div class="text-center bs-wizard-stepnum solo_movil">Listo</div>
        @else
            <div class="text-center bs-wizard-stepnum solo_desktop">En Camino</div>
            <div class="text-center bs-wizard-stepnum solo_movil">En camino</div>
        @endif
        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"></a>
    </div>

    <div class="col-xs-3 bs-wizard-step {{$steps[3]}}"><!-- active -->
        <div class="text-center bs-wizard-stepnum solo_desktop">Entregado</div>
        <div class="text-center bs-wizard-stepnum solo_movil">Entregado</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a href="#" class="bs-wizard-dot"></a>
    </div>
</div>
