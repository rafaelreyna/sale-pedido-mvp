<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 food-item" style="@if((!$platillo->disponible || !$restaurante->activo) && empty($menu_online)) filter: grayscale(1) @endif">
    <div class="food-item-wrap">
        <div class="figure-wrap bg-image" data-image-src="{{$platillo->foto ? route('images.public', [base64_encode('restaurantes/platillos/'), $platillo->foto]) : URL::to('/assets/img/sin_imagen.jpg')}}"
             style="background: url('{{$platillo->foto ? route('images.public', [base64_encode('restaurantes/platillos/'), $platillo->foto]) : URL::to('/assets/img/sin_imagen.jpg')}}') center center / cover no-repeat;">
            @if(!is_null($platillo->precio_promo))
                <div class="distance"><i class="fa fa-pin"></i>Promoción</div>
            @endif
        </div>
        <div class="content">
            @if((!$platillo->disponible || !$restaurante->activo || !empty($menu_online)))
                <h5>{{$platillo->nombre}}</h5>
            @else
                <h5><a href="{{route('food.platillo.show', $platillo->id)}}">{{$platillo->nombre}}</a></h5>
            @endif
            <div class="product-name" style="white-space: pre-wrap">{{$platillo->descripcion}}</div>
            <form method="POST" action="{{route('food.cart.items.add', $platillo->negocio_id)}}">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$platillo->id}}">
                <input type="hidden" name="name" value="{{$platillo->nombre}}">
                <input type="hidden" name="price" value="{{$platillo->precio_final()}}">
                <input type="hidden" name="quantity" value="1">
                <div class="price-btn-block">
                    @component('food.components.platillo.precio')
                        @slot('platillo', $platillo)
                        @slot('restaurante', $restaurante)
                        @if(!empty($menu_online))
                            @slot('botones', false)
                        @else
                            @slot('botones', true)
                        @endif
                        @slot('perfil', $perfil)
                    @endcomponent
                </div>
            </form>
        </div>
    </div>
</div>
