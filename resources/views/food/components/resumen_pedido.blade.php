<table class="table" @if($is_email ?? null) style="width: 400px; border-width: 1px; border-color: #333; border-style: solid" @endif>
    <thead>
    <th>Platillo</th>
    <th>Cantidad</th>
    <th>Precio</th>
    </thead>
    <tbody>
    @foreach($pedido->items as $item)
    <tr>
        <td>
            {{$item->platillo}}
            @if ($item->personalizacion && $item->personalizacion->comentarios_cliente)
                <pre style="margin: 3px!important; padding: 5px!important; font-size: 12px; max-width: 200px; overflow: hidden;">{{$item->personalizacion->comentarios_cliente}}</pre>
            @endif
        </td>
        <td>{{$item->cantidad}} x $ {{number_format($item->costo_unitario, 2, '.', ',')}}</td>
        <td>$ {{number_format($item->costo_total, 2, '.', ',')}}</td>
    </tr>
    @endforeach
    <tr>
        <td class="text-color"><strong>Subtotal</strong></td>
        <td></td>
        <td class="text-color"><strong>$ {{number_format($pedido->total, 2, '.', ',')}}</strong></td>
    </tr>

    @if((isset($tipo_envio) && $tipo_envio == 'domicilio') || !isset($tipo_envio))
    <tr class="envioDomicilio">
        <td class="text-color"><strong>Envio</strong></td>
        <td></td>
        <td class="text-color"><strong>$ {{number_format($pedido->costo_envio, 2, '.', ',')}}</strong></td>
    </tr>
    @endif

    <tr class="envioDomicilio">
        <td class="text-color"><strong>Total</strong></td>
        <td></td>
        <td class="text-color"><strong>$ {{number_format($pedido->gran_total, 2, '.', ',')}}</strong></td>
    </tr>
    </tbody>
</table>
@if($pedido->comentarios_cliente)
<p>
    Comentarios generales del pedido:<br/>
<pre style="margin: 3px!important; padding: 5px!important; font-size: 12px; max-width: 200px; overflow: hidden;">{{$pedido->comentarios_cliente}}</pre>
</p>
@endif
<p><small>* Todos los montos están en moneda nacional en incluyen impuestos</small></p>
