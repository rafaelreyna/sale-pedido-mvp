<div class="row" id="{{$categoria->slug()}}">
    @if($categoria->foto)
        <div class="titulo-categoria" style="background-image: url('{{route('images.public', [base64_encode('restaurantes/platillocategorias/'), $categoria->foto])}}')">
            <div class="overlay" >
                <h4>{{$categoria->nombre}}</h4>
            </div>
        </div>
    @else
        <h4 style="text-align: center">{{$categoria->nombre}}</h4>
    @endif
    @foreach($categoria->platillos() as $platillo)
            @component('food.components.platillo')
                @slot('platillo', $platillo)
                @slot('restaurante', $restaurante)
                @slot('perfil', $perfil)
                @if(!empty($menu_online))
                    @slot('menu_online', $menu_online)
                @endif
            @endcomponent
    @endforeach
</div>
<hr/>
