@extends('layouts.food')

@section('content')
    <hr/>
    <section id="carritoCompras">
        <div class="container">
            <form method="POST" action="{{route('food.cart.process')}}">
                {{ csrf_field() }}
                {{method_field('post')}}
            <div class="row">
                <div class="col col-md-8">
                    <div class="cart-totals margin-b-20">
                        <div class="cart-totals-title">
                            <a href="{{route('food.cart.clear')}}" style="float:right" class=" btn btn-sm btn-danger text-white"><i class="fa fa-times"></i> Vaciar Carrito</a>
                            <h4>Tu carrito de compras</h4>
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul style="margin: 0px;">
                                        @foreach ($errors->all() as $error)
                                            <li><i class="flaticon-alert"></i> {{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <p class="alert alert-info">
                                <i class="fa fa-info-circle"></i> Todos los montos están en moneda nacional, e incluyen impuestos.<br/>
                                <i class="fa fa-info-circle"></i> Las indicaciones de platillos y del pedido están sujetas a la validación del restaurante
                            </p>
                            <br/>
                        </div>
                        @if($instancia && $total != 0 && count($carrito) != 0)
                            <div>
                                <h5>Establecimiento: {{$restaurante->nombre}}</h5>
                            </div>
                        <br/>
                        <div class="cart-totals-fields" style="overflow: auto">
                            <table class="table">
                                <thead>
                                    <th>Platillo</th>
                                    <th>Cantidad</th>
                                    <th>Precio</th>
                                    <th></th>
                                    <th></th>
                                </thead>
                                <tbody>
                                @foreach($carrito as $item)
                                    <tr>
                                        <td>
                                            {{$item->name}}
                                            <input type="hidden" name="item[{{$item->id}}]" value="{{base64_encode(json_encode($item))}}">
                                            @if(isset($item->options['comentarios_cliente']))
                                                <pre style="margin: 3px!important; padding: 5px!important; font-size: 12px; max-width: 200px; text-overflow: ellipsis; overflow: hidden;">{{$item->options['comentarios_cliente']}}</pre>
                                                <br/>
                                            @endif
                                        </td>
                                        <td>{{$item->quantity}}</td>
                                        <td>$ {{number_format($item->price * $item->quantity, 2, '.', ',')}}</td>
                                        <td><a href="{{route("food.platillo.edit", [$item->id, $item->getUniqueId()])}}" class="small"><i class="fa fa-user"></i> personalizar</a></td>
                                        <td><a href="{{route("food.cart.items.delete", [$restaurante->id, $item->getUniqueId()])}}" class="small"><i class="fa fa-times"></i> eliminar</a></td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td class="text-color"><strong>Total</strong></td>
                                    <td></td>
                                    <td class="text-color"><strong>$ {{number_format($total, 2, '.', ',')}}</strong></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                            <hr/>
                            <div class="row form-group">
                                <div class="col-xs-12 col-md-12">
                                    <textarea class="form-control txtConteoCaracteres" maxlength="150" id="comentarios_cliente" name="comentarios_cliente"  placeholder="Otras indicaciones del pedido que no afecte el precio total del pedido. Ej: Salsas por separado">{{old('comentarios_cliente')?old('comentarios_cliente'):''}}</textarea>
                                    <div class="conteoCaracteresContenedor"><span class="conteoCaracteres">0</span> / 150</div>
                                </div>
                            </div>

                        </div>
                        @else
                            <p class="alert alert-warning"><i class="fa fa-shopping-cart"></i>
                                Tu carrito de compras estrá vacío. <a href="{{route('food.home')}}">Explorar categorías</a>
                            </p>
                        @endif
                    </div>
                </div>
                <div class="col col-md-4">
                    @if($instancia && $total != 0 && count($carrito) != 0)
                    <div id="divBotonescheckout">
                        @if($restaurante->enviosconfig->getDistanciaAlcance($perfil->lat, $perfil->lon))
                            @if($restaurante->enviosconfig->puedeEnviarEstaDistancia())
                                @if($restaurante->permite_envio_domicilio())
                                <div class="divBtnDomicilio">
                                    <div class="overlay"></div>
                                    <br/><br/><br/><br/>
                                    <p style="text-align: center">
                                        <a id="btnCheckoutDomicilio" href="#" class="text-white btn btn-lg theme-btn"><i class="fa fa-motorcycle"></i> Pedir a Domicilio</a>
                                    </p>
                                    <br/><br/><br/>
                                </div>
                                @endif
                            @endif
                        @endif

                        @if($restaurante->permite_pickup())
                        <div class="divBtnPickup">
                            <div class="overlay"></div>
                            <br/><br/><br/><br/>
                            <p style="text-align: center">
                                <a id="btnCheckoutPickup" href="#" class="text-white btn btn-lg theme-btn"><i class="fa fa-car"></i> Pickup & Go</a>
                            </p>
                            <br/><br/><br/>
                        </div>
                        @endif
                    </div>
                    <br/>
                    <p style="text-align: center;">
                        <a href="{{route('food.restaurante.show',$restaurante->id)}}" class="btn btn-light"><i class="fa fa-shopping-cart"></i> Agregar más productos</a>
                    </p>
                    @endif


            <div id="formDatosUsuario" class="form-horizontal contact-form" role="form" style="display: none">
                            <fieldset>
                                <div class="row form-group">
                                    <div class="col-xs-12 col-md-6">
                                        <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Nombre *" required=""
                                               value = "{{old('nombre')?old('nombre'):$perfil->nombre}}">
                                    </div>

                                    <div class="col-xs-12 col-md-6">
                                        <input class="form-control" id="apellidos" name="apellidos" type="text" placeholder="Apellidos *" required=""
                                               value = "{{old('apellidos')?old('apellidos'):$perfil->apellidos}}">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-6">
                                        <input class="form-control" id="email" name="email" type="email" placeholder="Email *" required=""
                                               value = "{{old('email')?old('email'):$perfil->email}}">
                                    </div>
                                    <div class="col-xs-6">
                                        <input class="form-control" id="telefono" name="telefono" type="tel" placeholder="Celular *"
                                               value = "{{old('telefono')?old('telefono'):$perfil->telefono}}">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12 col-md-12">
                                        <textarea autocomplete="disabled" class="form-control" id="pac-input" name="direccion"  placeholder="Direccion" required="">{{old('direccion')?old('direccion'):$perfil->direccion}}</textarea>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-12 col-md-12">
                                        <textarea class="form-control" id="pac-input" name="referencias"  placeholder="Referencias">{{old('referencias')?old('referencias'):(isset($perfil->referencias)?$perfil->referencias:"")}}</textarea>
                                    </div>
                                </div>

                                <div id="map" style="height: 300px;" data-zoom="14"></div>
                                <input type="hidden" class="form-control @error('lon') is-invalid @enderror"
                                       id="lon" name="lon" placeholder=""
                                       value="{{old('lon')?old('lon'):$perfil->lon}}">
                                <input type="hidden" class="form-control @error('lat') is-invalid @enderror"
                                       id="lat" name="lat" placeholder=""
                                       value="{{old('lat')?old('lat'):$perfil->lat}}">
                                <br/>

                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <label>
                                            <input type="checkbox" name="recordar_ubicacion" value="true">
                                            <i class="fa fa-user"></i> Recordar Preferencias
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            <p style="text-align: right"><a href="#" id="btnSeleccionar">Seleccionar otra forma de compra</a></p>
                    </div>
                </div>
            </div>
            @if($instancia && $total != 0 && count($carrito) != 0)
                <input type="hidden" name="negocio_id" value="{{$restaurante->id}}">
                <div class="row">
                    <div class="col col-md-12" id="btnCheckout" style="display: none">
                        <button type="submit" class="text-white btn btn-lg theme-btn"><i class="fa fa-arrow-right"></i> Seleccionar forma de pago</button>
                    </div>
                </div>
            @endif
                <input type="hidden" name="tipo_envio" id="hdnTipoEnvio">
            </form>
        </div>
    </section>
    <br/>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY', 'AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY')}}&libraries=places"></script>
    <script type="application/javascript">
        $(document).ready(function(){
            $('#pac-input').attr('autocomplete','chrome-off');

            map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: {{$perfil->lat?$perfil->lat:'20.6122138'}},
                    lng: {{$perfil->lon?$perfil->lon:'-100.4452366'}},
                },
                zoom: 15,
                scrollwheel: true,
                heigth: 250
            });

            marker = new google.maps.Marker({
                position: {
                    lat: {{$perfil->lat?$perfil->lat:'20.6122138'}},
                    lng: {{$perfil->lon?$perfil->lon:'-100.4452366'}}
                },
                map: map,
                draggable:true,
                title: '{{$perfil->nombre}}'
            });

            google.maps.event.addListener(marker, 'dragend', function (event) {
                document.getElementById("lat").value = event.latLng.lat();
                document.getElementById("lon").value = event.latLng.lng();
            });

            // Create the search box and link it to the UI element.
            const input = document.getElementById("pac-input");
            const searchBox = new google.maps.places.SearchBox(input);
            //map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(input);
            // Bias the SearchBox results towards current map's viewport.
            map.addListener("bounds_changed", () => {
                searchBox.setBounds(map.getBounds());
            });

            let markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener("places_changed", () => {
                const places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }
                // Clear out the old markers.
                markers.forEach((marker) => {
                    marker.setMap(null);
                });

                const bounds = new google.maps.LatLngBounds();
                places.forEach((place) => {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }

                    marker.setPosition(place.geometry.location);
                    document.getElementById("lat").value = place.geometry.location.lat();
                    document.getElementById("lon").value = place.geometry.location.lng();

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });

            $("#btnCheckoutDomicilio").click(function(){
                $("#divBotonescheckout").hide();
                $("#formDatosUsuario").slideDown(500);
                $("#btnCheckout").slideDown(300);
                $("#hdnTipoEnvio").val('domicilio');
                return false;
            });

            $("#btnCheckoutPickup").click(function(){
                $("#divBotonescheckout").hide();
                $("#formDatosUsuario").slideDown(500);
                $("#btnCheckout").slideDown(300);
                $("#hdnTipoEnvio").val('pickup');
                return false;
            });

            $("#btnSeleccionar").click(function(){
                $("#divBotonescheckout").slideDown(500);
                $("#formDatosUsuario").hide(0);
                $("#btnCheckout").slideUp(300);
                $("#hdnTipoEnvio").val('');
                return false;
            });

        });

    </script>
@endsection
