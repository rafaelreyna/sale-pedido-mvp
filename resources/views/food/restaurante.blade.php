@extends('layouts.food', ['custom_meta' => true])

@section('custom_meta')
    <meta property="og:title" content="{{$restaurante->nombre}} en Uki&reg;" />
    @if($restaurante->logo)
        <meta property="og:image" content="{{route('images.public', [base64_encode('negocios/logos/'), $restaurante->logo])}}" />
    @else
        <meta property="og:image" content="https://uki.mx/assets/img/logo.png" />
    @endif
    <meta property="og:description" content="Disfruta los platillos de {{$restaurante->nombre}} a través de Uki&reg;: una plataforma que conecta a los restaurantes con los consumidores de manera trasparente y justa." />
    <meta property="og:url" content="{{route('food.restaurante.show', $restaurante->id)}}" />
    <title>Uki&reg; @if(env('APP_ENV', 'local') == 'beta') - Beta @endif - {{$restaurante->nombre}}</title>
@endsection

@section('content')
<br/>
    <section>
        <div class="container">
            <div class="row">
                <div class="col col-12">
                    <p style="text-align: center">
                        <a class="img-fluid" href="{{route('food.restaurante.show',$restaurante->id)}}" style="@if(!$restaurante->activo) filter: grayscale(1) @endif">
                            @if($restaurante->logo)
                                <img class="logo-negocio-small" src="{{route('images.public', [base64_encode('negocios/logos/'), $restaurante->logo])}}"/>
                            @else
                                <img class="logo-negocio-small" src="{{URL::to('/assets/img')}}/sin_imagen.jpg"/>
                            @endif
                        </a>
                    </p>
                    <h5 style="text-align: center">{{$restaurante->nombre}}</h5>
                    <div style="text-align: center; font-size: 12px;">
                        @component('food.components.restaurante_datos')
                            @slot('restaurante', $restaurante)
                            @slot('perfil', $perfil)
                        @endcomponent
                        <span>{{$restaurante->calificacion}}</span>
                        <div class="rating-block">
                            @component('food.components.calificacion')
                                @slot('calificacion', $restaurante->calificacion)
                            @endcomponent
                        </div>
                        <p><small>{{count($restaurante->calificaciones)}} Calificaciones</small></p>

                        <ul class="navbar-nav" id="menu-categorias">
                            @foreach($restaurante->platillo_categorias as $categoria)
                                <li class="nav-item">
                                    <a class="nav-link linkCategoria" href="#{{$categoria->slug()}}">{{$categoria->nombre}} <span class="sr-only">(current)</span></a>
                                </li>
                            @endforeach
                        </ul>

                        @if($lastrestaurante && $lastrestaurante->id != $restaurante->id && $cartcount > 0)
                            <div style="clear: both; float:none">
                                <p class="alert alert-danger">
                                    <big><i class="fa fa-warning"></i> Tu pedido en <strong>{{$lastrestaurante->nombre }}</strong> se perderá</big><br/>
                                    Sólo puedes realizar pedidos de un restaurante a la vez, por lo que si ordenas algún producto de este restaurante, tu carrito anterior se perderá
                                </p>
                            </div>
                        @endif
                            @if(isset($perfil))
                                @if(isset($perfil->region_id))
                                    @if($perfil->isFarFromNegocio($restaurante))
                                        <p class="alert alert-danger"><i class="fa fa-warning"></i> TE ENCUENTRAS A MÁS DE {{\App\Models\Food\Perfil::MAX_KM_REGION}} KM DE ESTE RESTAURANTE, SI DESEAS CONTINUAR, SOLO PODRÁS REALIZAR PEDIDOS PICK UP & GO. <a href="{{route('food.perfil.edit')}}">REVISA QUE TU DIRECCIÓN SEA LA CORRECTA</a></p>
                                    @endif
                                @endif
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <hr/>
    <section id="restaurantecategorias">
        <div class="container">
            @foreach($restaurante->platillo_categorias as $categoria)
                @component('food.components.categoriaRestaurante')
                    @slot('categoria', $categoria)
                    @slot('restaurante', $restaurante)
                    @slot('perfil', $perfil)
                @endcomponent
            @endforeach
        </div>
    </section>
    <br/>
<style>
    #menu-categorias {
        z-index: 9;
        background-color: #FFF;
        min-width: 100%;
        left: 0px;
        white-space: nowrap;
        width: 100%;
        overflow: auto;
        padding-right: 30px;
    }
</style>
<script type="text/javascript">
    $(document).ready(function(){

        $("#menu-categorias").sticky({topSpacing:84, zIndex:9999});

        $('.linkCategoria').click(function(){
            var goto = $(this).attr('href');
            $([document.documentElement, document.body]).animate({
                scrollTop: $(goto).offset().top - 140
            }, 600);
            return false;
        });
    });
</script>
@endsection
