@extends('layouts.access')

@section('content')
    <link rel="stylesheet" media="screen" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"/>
    <div class="card">
        <div class="card-header">{{ __('Register') }}</div>
        <div class="card-body">
            <form id="formid" method="POST" action="{{ route('register') }}">
                @csrf
                <h4>Datos del usuario</h4>
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}*</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                               name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}*</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="celular" class="col-md-4 col-form-label text-md-right">Celular*</label>

                    <div class="col-md-6">
                        <input id="celular" type="celular" class="form-control @error('celular') is-invalid @enderror"
                               name="celular" value="{{ old('celular') }}" required placeholder="10 dígitos">

                        @error('celular')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}*</label>

                    <div class="col-md-6">
                        <input id="password" type="password"
                               class="checkCaps form-control @error('password') is-invalid @enderror" name="password" required
                               autocomplete="new-password"
                        >
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                    <div class="capslockdiv" style="display:none">Mayus Activado.</div>
                </div>

                <div class="form-group row">
                    <label for="password-confirm"
                           class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}*</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="checkCaps form-control" name="password_confirmation"
                               required autocomplete="new-password"
                        >
                    </div>

                    <div class="capslockdiv" style="display:none">Mayus Activado.</div>
                </div>
                <hr/>
                <br/>
                <h4>Los datos de tu negocio</h4>

                <div class="form-group row">
                    <label for="negocio_tipo" class="col-md-4 col-form-label text-md-right">Ciudad / Región</label>
                    <div class="col-md-6">
                        <select class="form-control @error('negocio_region_id') is-invalid @enderror"
                                id="negocio_region_id" name="negocio_region_id">
                            <option value="">SELECCIONA UNA OPCIÓN</option>
                            @foreach(\App\Models\Region::all() as $region)
                                <option @if(old('negocio_region_id') == $region->id) selected="selected" @endif  value="{{$region->id}}">
                                    {{$region->nombre}}
                                </option>
                            @endforeach
                        </select>
                        @error('negocio_region_id')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col-md-6 offset-md-4">
                        <p style="color: #0d4982!important;">
                            <small>
                            * Si no aparece tu zona, 
                            <a style="color: #ee072c; cursor:pointer!important;" href="mailto:hola@uki.mx">escríbenos a hola@uki.mx</a> 
                            para habilitarla
                            </small>
                        </p>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="negocio_nombre" class="col-md-4 col-form-label text-md-right">Nombre Comercial*</label>

                    <div class="col-md-6">
                        <input id="negocio_nombre" type="text"
                               class="form-control @error('negocio_nombre') is-invalid @enderror" name="negocio_nombre"
                               value="{{ old('negocio_nombre') }}" required>

                        @error('negocio_nombre')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="negocio_tipo" class="col-md-4 col-form-label text-md-right">Tipo de negocio</label>

                    <div class="col-md-6">
                        <select class="form-control @error('negocio_tipo') is-invalid @enderror"
                                id="negocio_tipo" name="negocio_tipo">
                            <option value="">SELECCIONA UNA OPCIÓN</option>
                            <option @if(old('negocio_tipo') == 'restaurante') selected="selected" @endif  value="restaurante">Restautrante / Venta de comida</option>
                        </select>
                        @error('negocio_tipo')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="negocio_email" class="col-md-4 col-form-label text-md-right">Correo electrónico</label>

                    <div class="col-md-6">
                        <input id="negocio_email" type="email"
                               class="form-control @error('negocio_email') is-invalid @enderror" name="negocio_email"
                               value="{{ old('negocio_email') }}">

                        @error('negocio_email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="negocio_telefono" class="col-md-4 col-form-label text-md-right">Teléfono</label>

                    <div class="col-md-6">
                        <input id="negocio_telefono" type="text"
                               class="form-control @error('negocio_telefono') is-invalid @enderror"
                               name="negocio_telefono" value="{{ old('negocio_telefono') }}"
                                placeholder="10 dígitos">

                        @error('negocio_telefono')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="negocio_celular" class="col-md-4 col-form-label text-md-right">Celular</label>

                    <div class="col-md-6">
                        <input id="negocio_celular" type="text"
                               class="form-control @error('negocio_celular') is-invalid @enderror"
                               name="negocio_celular" value="{{ old('negocio_celular') }}" placeholder="10 dígitos">

                        @error('negocio_celular')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>


                <hr/>
                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Register') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <p style="text-align: center">¿Ya tienes una cuenta? <a href="{{URL::to('/login')}}">Inicia sesión aquí</a></p>
    
    <!-- GetButton.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            whatsapp: "+524424540553", // WhatsApp number
            call_to_action: "Envíanos un mensaje", // Call to action
            position: "right", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /GetButton.io widget -->
    
    <script type="text/javascript">
        $(document).ready(function () {
            var options = {
                bootstrap3: true,
                scores: [17, 26, 40, 50],
                verdicts: ["Débil", "Normal", "Media", "Fuerte", "Muy Fuerte"],
                showVerdicts: true,
                showVerdictsInitially: true,
                raisePower: 1.4,
            };
            $('#password').pwstrength(options);

            check_capslock_form($('#formid'));


            document.onkeydown = function (e) { //check if capslock key was pressed in the whole window
                e = e || event;
                if (typeof (window.lastpress) === 'undefined') { window.lastpress = e.timeStamp; }
                if (typeof (window.capsLockEnabled) !== 'undefined') {
                    if (e.keyCode == 20 && e.timeStamp > window.lastpress + 50) {
                        window.capsLockEnabled = !window.capsLockEnabled;
                        $('.capslockdiv').toggle();
                    }
                    window.lastpress = e.timeStamp;
                    //sometimes this function is called twice when pressing capslock once, so I use the timeStamp to fix the problem
                }

            };

            function check_capslock(e) { //check what key was pressed in the form
                var s = String.fromCharCode(e.keyCode);
                if (s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey) {
                    window.capsLockEnabled = true;
                    $('.capslockdiv').show();
                }
                else {
                    window.capsLockEnabled = false;
                    $('.capslockdiv').hide();
                }
            }

            function check_capslock_form(where) {
                if (!where) { where = $(document); }
                where.find('input,select').each(function () {
                    if (this.type != "hidden") {
                        $(this).keypress(check_capslock);
                    }
                });
            }
        });
    </script>
    <style type="text/css">
        .progress{margin: 3px;}
        .invalid-feedback{position: inherit; bottom: 0px;}
        .capslockdiv {color:#900}
    </style>
    <style type="text/css">
        .btn:hover{
            color:#ee072c!important;
        }
        .btn-primary:hover{
            color:#FFF!important;
        }
    </style>

@endsection
