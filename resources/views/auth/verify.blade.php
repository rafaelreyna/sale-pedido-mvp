@extends('layouts.access')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                        <strong>Recuerda revisar tu bandeja de SPAM o Correo no deseado. </strong><br/><br/>
                        <p>Tienes 3 horas a aprtir de este momento para verificar el correo electrónico, si este tiempo transcurre,
                            puedes solicitar un nuevo correo de confirmación en esta misma página</p>
                        <br/><br/>
                    {{ __('If you did not receive the email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                    </form>
                    <hr/>
                    <p style="text-align: right">
                        <a class="btn btn-sm btn-link" href="{{URL::to('/logout')}}"> <span>
                                <i class="flaticon-shut-down mr-2"></i> Cerrar sesión</span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .btn:hover{
        color:#ee072c!important;
    }
    .btn-primary:hover{
        color:#FFF!important;
    }
</style>
@endsection
