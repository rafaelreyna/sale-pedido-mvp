@extends('layouts.app')

@section('content')
    <div _ngcontent-vbt-c44="" class="row">
        <div _ngcontent-vbt-c44="" class="col-md-12">
            <nav _ngcontent-vbt-c44="" aria-label="breadcrumb">
                <ol _ngcontent-vbt-c44="" class="breadcrumb pl-0">
                    <li _ngcontent-vbt-c44="" class="breadcrumb-item"><a _ngcontent-vbt-c44="" href="{{route('admin')}}"><i
                                _ngcontent-vbt-c44="" class="material-icons">home</i> Inicio</a></li>
                    <li _ngcontent-vbt-c44="" aria-current="page" class="breadcrumb-item active">Negocios</li>
                </ol>
            </nav>
        </div>
        <div _ngcontent-vbt-c51="" class="ms-panel col-md-12">
            <div _ngcontent-vbt-c51="" class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 _ngcontent-vbt-c51="">Negocios</h6>
                    </div>
                </div>
            </div>
            <div _ngcontent-vbt-c51="" class="ms-panel-body">
            @if($negocios->count() > 0)
                <div class="table-responsive">
                    <table id="example" class="table table-sm table-datatable table-striped thead-primary w-100 dataTable no-footer" style="width:100%">
                    <thead >
                        <tr>
                            <th style="width: 220px;">Logo</th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Responsable</th>
                            <th>Email Admin</th>
                            <th>Cel Admin</th>
                            <th>Email Restaurante</th>
                            <th>Tel Restaurante</th>
                            <th style="width: 160px;">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($negocios as $negocio)
                            <tr>
                                <td>
                                    @if($negocio->logo)
                                        <img class="table-img-lg" src="{{route('images.public', [base64_encode('negocios/logos/'), $negocio->logo])}}"/>
                                    @else
                                        <img class="table-img-lg" src="{{URL::to('/assets/img')}}/sin_imagen.jpg"/>
                                    @endif
                                </td>
                                <td>{{$negocio->nombre}}</td>
                                <td><span class="badge badge-dark">{{$negocio->tipo}}</span></td>
                                <td>@if($negocio->admin()){{$negocio->admin()->name}}@endif</td>
                                <td>@if($negocio->admin()){{$negocio->admin()->email}}@endif</td>
                                <td>@if($negocio->admin()){{$negocio->admin()->celular}}@endif</td>
                                <td>{{$negocio->email}}</td>
                                <td>{{$negocio->telefono}}</td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Revisar
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{URL::to("admin/dashboard?negocio_id={$negocio->id}")}}"><i class="flaticon-search"></i> Dashboard</a>
                                            <a class="dropdown-item" href="{{URL::to("admin/categorias-de-platillos?negocio_id={$negocio->id}")}}"><i class="flaticon-search"></i> Categorías de Platillos</a>
                                            <a class="dropdown-item" href="{{URL::to("admin/platillos?negocio_id={$negocio->id}")}}"><i class="flaticon-search"></i> Platillos</a>
                                            <a class="dropdown-item" href="{{route('admin.negocios.edit', $negocio->id)}}?negocio_id={{$negocio->id}}"><i class="flaticon-search"></i> Perfil</a>
                                            <a class="dropdown-item" href="{{route('admin.restaurantes.categorias.edit', $negocio->id)}}?negocio_id={{$negocio->id}}"><i class="flaticon-search"></i> Categorias de Restaurante</a>
                                            <a class="dropdown-item" href="{{URL::to("admin/horarios?negocio_id={$negocio->id}")}}"><i class="flaticon-search"></i> Horarios</a>
                                            <a class="dropdown-item" href="{{route('admin.negocios.pago.edit', $negocio->id)}}?negocio_id={{$negocio->id}}"><i class="flaticon-search"></i> Formas de pago</a>
                                            <a class="dropdown-item" href="{{route('admin.negocios.envios.edit', $negocio->id)}}?negocio_id={{$negocio->id}}"><i class="flaticon-search"></i> Envios</a>
                                            <a class="dropdown-item" href="{{URL::to("admin/facturas?negocio_id={$negocio->id}")}}"><i class="flaticon-search"></i> Estado de cuenta</a>
                                            <a class="dropdown-item" href="{{URL::to("admin/pedidos/historial?negocio_id={$negocio->id}")}}"><i class="flaticon-search"></i> Historial de Pedidos</a>
                                            <a class="dropdown-item" href="{{URL::to("admin/pedidos/historial_detallado?negocio_id={$negocio->id}")}}"><i class="flaticon-search"></i> Historial Detallado</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            @endif
                @if($negocios->count() <= 0)
                    <p class="alert alert-info"><i class="flaticon-information"></i> Aún no se han dado de alta ningún negocio</p>
                @endif
            </div>
        </div>
    </div>
    <div class="delete_form">
        <form id="deleteForm" method="POST" action="" >
            {{ csrf_field() }}
            {{method_field('delete')}}
        </form>
    </div>
    <script type="application/javascript">
        $(document).ready(function() {
            $('.table-datatable').DataTable({"ordering": false, "pageLength": 25, "bLengthChange": false,
                "language": {
                    "url": "{{URL::to('/assets/lang/dataTable/Spanish.json')}}"
                }});
        } );
    </script>
@endsection
