@extends('layouts.app')

@section('content')
    <div  class="row">
        <div  class="col-md-12">
            <nav  aria-label="breadcrumb">
                <ol  class="breadcrumb pl-0">
                    <li  class="breadcrumb-item">
                        <a  href="{{route('admin')}}"><i class="material-icons">home</i> Inicio</a>
                    </li>
                    <li  class="breadcrumb-item"><a  href="#">Menú</a></li>
                    <li  class="breadcrumb-item">
                        <a  href="{{route('admin.operadores.index')}}">Operadores</a>
                    </li>
                    @if($operador->id)
                        <li  aria-current="page" class="breadcrumb-item active">Editar</li>
                    @else
                        <li  aria-current="page" class="breadcrumb-item active">Agregar</li>
                    @endif
                </ol>
            </nav>
        </div>
        <div  class="ms-panel col-md-8">
            <div  class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 >{{$titulo}}</h6>
                    </div>
                    <a href="{{route('admin.operadores.index')}}"><i class="material-icons">arrow_back</i> Regresar</a>
                </div>
            </div>
            <div  class="ms-panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin: 0px;">
                            @foreach ($errors->all() as $error)
                                <li><i class="flaticon-alert"></i> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{$route}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{$operador->id?method_field('put'):method_field('post')}}
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror"
                               id="name" name="name" placeholder="Nombre del operador"
                               value="{{old('name')?old('name'):$operador->name}}">
                    </div>

                    @if(!$operador->id)
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                               id="email" name="email" placeholder="Email del operador"
                               value="{{old('email')?old('email'):$operador->email}}">
                    </div>
                    @endif

                    <div class="form-group">
                        <label for="celular">Celular</label>
                        <input type="text" class="form-control @error('nombre') is-invalid @enderror"
                               id="celular" name="celular" placeholder="Celular a 10 digitos"
                               value="{{old('celular')?old('celular'):$operador->celular}}">
                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input id="password" type="password"
                               class="checkCaps form-control @error('password') is-invalid @enderror" name="password"
                               autocomplete="new-password"
                        >

                        <div class="capslockdiv" style="display:none">Mayus Activado.</div>
                    </div>

                    <div class="form-group">
                        <label for="password">Confirmar Password</label>
                        <input id="password-confirm" type="password" class="checkCaps form-control" name="password_confirmation"
                                autocomplete="new-password"
                        >

                        <div class="capslockdiv" style="display:none">Mayus Activado.</div>
                    </div>

                    <input type="submit" class="btn btn-primary" value="{{$callToAction}}">
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var options = {
                bootstrap3: true,
                scores: [17, 26, 40, 50],
                verdicts: ["Débil", "Normal", "Media", "Fuerte", "Muy Fuerte"],
                showVerdicts: true,
                showVerdictsInitially: true,
                raisePower: 1.4,
            };
            $('#password').pwstrength(options);

            check_capslock_form($('#formid'));


            document.onkeydown = function (e) { //check if capslock key was pressed in the whole window
                e = e || event;
                if (typeof (window.lastpress) === 'undefined') { window.lastpress = e.timeStamp; }
                if (typeof (window.capsLockEnabled) !== 'undefined') {
                    if (e.keyCode == 20 && e.timeStamp > window.lastpress + 50) {
                        window.capsLockEnabled = !window.capsLockEnabled;
                        $('.capslockdiv').toggle();
                    }
                    window.lastpress = e.timeStamp;
                    //sometimes this function is called twice when pressing capslock once, so I use the timeStamp to fix the problem
                }

            };

            function check_capslock(e) { //check what key was pressed in the form
                var s = String.fromCharCode(e.keyCode);
                if (s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey) {
                    window.capsLockEnabled = true;
                    $('.capslockdiv').show();
                }
                else {
                    window.capsLockEnabled = false;
                    $('.capslockdiv').hide();
                }
            }

            function check_capslock_form(where) {
                if (!where) { where = $(document); }
                where.find('input,select').each(function () {
                    if (this.type != "hidden") {
                        $(this).keypress(check_capslock);
                    }
                });
            }
        });
    </script>
    <style type="text/css">
        .progress{margin: 3px;}
        .invalid-feedback{position: inherit; bottom: 0px;}
        .capslockdiv {color:#900}
    </style>
@endsection
