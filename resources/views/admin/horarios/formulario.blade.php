@extends('layouts.app')

@section('content')
    <div  class="row">
        <div  class="col-md-12">
            <nav  aria-label="breadcrumb">
                <ol  class="breadcrumb pl-0">
                    <li  class="breadcrumb-item">
                        <a  href="{{route('admin')}}"><i class="material-icons">home</i> Inicio</a>
                    </li>
                    <li  class="breadcrumb-item"><a  href="#">Configuración</a></li>
                    <li  class="breadcrumb-item">
                        <a  href="{{route('admin.horarios.index')}}">Horarios</a>
                    </li>
                    <li  aria-current="page" class="breadcrumb-item active">Crear</li>
                </ol>
            </nav>
        </div>
        <div  class="ms-panel col-md-8">
            <div  class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 >{{$titulo}}</h6>
                    </div>
                    <a href="{{route('admin.horarios.index')}}"><i class="material-icons">arrow_back</i> Regresar</a>
                </div>
            </div>
            <div  class="ms-panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin: 0px;">
                            @foreach ($errors->all() as $error)
                                <li><i class="flaticon-alert"></i> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{$route}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{$horario->id?method_field('put'):method_field('post')}}

                    <ul class="selectorDias">
                        <li class="alert alert-secondary" >
                            <label class="ms-checkbox-wrap ms-checkbox-primary">
                                <input name="dia[1]" type="checkbox" value="true" horario> <i class="ms-checkbox-check"></i>
                            </label> <span> L </span>
                        </li>

                        <li class="alert alert-secondary" >
                            <label class="ms-checkbox-wrap ms-checkbox-primary">
                                <input name="dia[2]" type="checkbox" value="true" horario> <i class="ms-checkbox-check"></i>
                            </label> <span> M </span>
                        </li>

                        <li class="alert alert-secondary" >
                            <label class="ms-checkbox-wrap ms-checkbox-primary">
                                <input name="dia[3]" type="checkbox" value="true" horario> <i class="ms-checkbox-check"></i>
                            </label> <span> M </span>
                        </li>

                        <li class="alert alert-secondary" >
                            <label class="ms-checkbox-wrap ms-checkbox-primary">
                                <input name="dia[4]" type="checkbox" value="true" horario> <i class="ms-checkbox-check"></i>
                            </label> <span> J </span>
                        </li>

                        <li class="alert alert-secondary" >
                            <label class="ms-checkbox-wrap ms-checkbox-primary">
                                <input name="dia[5]" type="checkbox" value="true" horario> <i class="ms-checkbox-check"></i>
                            </label> <span> V </span>
                        </li>

                        <li class="alert alert-secondary" >
                            <label class="ms-checkbox-wrap ms-checkbox-primary">
                                <input name="dia[6]" type="checkbox" value="true" horario> <i class="ms-checkbox-check"></i>
                            </label> <span> S </span>
                        </li>

                        <li class="alert alert-secondary" >
                            <label class="ms-checkbox-wrap ms-checkbox-primary">
                                <input name="dia[0]" type="checkbox" value="true" horario> <i class="ms-checkbox-check"></i>
                            </label> <span> D </span>
                        </li>
                    </ul>

                    <div class="form-group">
                        <label for="inicio">Hora de apertura </label>
                        <input style="width: 150px;" type="time" class="form-control @error('inicio') is-invalid @enderror" id="inicio"
                               name="inicio" placeholder="Hora de inicio" value="{{old('inicio')?old('inicio'):$horario->inicio}}">
                    </div>

                    <div class="form-group">
                        <label for="fin">Hora de cierre </label>
                        <input style="width: 160px;" type="time" class="form-control @error('fin') is-invalid @enderror" id="inicio"
                               name="fin" placeholder="Hora de inicio" value="{{old('fin')?old('fin'):$horario->fin}}">
                    </div>

                    <input type="submit" class="btn btn-primary" value="{{$callToAction}}">
                </form>
            </div>
        </div>
    </div>
    <style type="text/css">
        .selectorDias {
            margin-bottom: 50px;
            margin-top: 30px;
        }

        .selectorDias li {
            margin-right: 10px;
            padding:15px;
            background-color: #FFF!important;
            line-height: 56px;
            display: inline;
        }

        .selectorDias li label input {
            background-color: #FFF;
        }

        .selectorDias li label {
            top: 15px;
        }

    </style>
@endsection
