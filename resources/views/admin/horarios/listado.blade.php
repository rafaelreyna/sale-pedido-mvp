@extends('layouts.app')

@section('content')
    <div _ngcontent-vbt-c44="" class="row">
        <div _ngcontent-vbt-c44="" class="col-md-12">
            <nav _ngcontent-vbt-c44="" aria-label="breadcrumb">
                <ol _ngcontent-vbt-c44="" class="breadcrumb pl-0">
                    <li _ngcontent-vbt-c44="" class="breadcrumb-item"><a _ngcontent-vbt-c44="" href="{{route('admin')}}"><i
                                _ngcontent-vbt-c44="" class="material-icons">home</i> Inicio</a></li>
                    <li _ngcontent-vbt-c44="" class="breadcrumb-item"><a _ngcontent-vbt-c44="" href="#">Configuración</a></li>
                    <li _ngcontent-vbt-c44="" aria-current="page" class="breadcrumb-item active">Horarios</li>
                </ol>
            </nav>
        </div>
        <div  class="ms-panel col-md-12">
            <div  class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 >Configuración de Horarios de Restaurante</h6>
                    </div>
                    <a href="{{route('admin.horarios.create')}}" class="btn text-white btn-info" ><i class="material-icons">add</i> Nuevo horario</a>
                </div>
            </div>
            <div  class="ms-panel-body" id="panel-body-div">
                @if($horarios->count() <= 0)
                    <p class="alert alert-info"><i class="flaticon-information"></i> Aún no se han dado de alta Horarios para este Negocio</p>
                @endif
                <div id="calendar-div" style="width: 1800px; max-width: 100%">
                    <div id='calendar' style="height: 400px"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="delete_form">
        <form id="deleteForm" method="POST" action="" >
            {{ csrf_field() }}
            {{method_field('delete')}}
        </form>
    </div>
    <script type="application/javascript">
        $(document).ready(function() {
            $('.table-datatable').DataTable({"ordering": false, "pageLength": 25, "bLengthChange": false,
                "language": {
                    "url": "{{URL::to('/assets/lang/dataTable/Spanish.json')}}"
                }});

            $('.btnEliminar').click(function(){
                const borrarId = $(this).data('deleteId');
                Swal.fire({
                    title: '¿Estás seguro que deseas borrar este elemento?',
                    text: "No podrás revertir esta acción",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#666',
                    confirmButtonText: 'Sí, quiero eliminarlo'
                }).then(function (result) {
                    if (result.value) {
                        $('#deleteForm').attr('action', '{{route('admin.platillocategorias.index')}}' + '/' + borrarId);
                        $('#deleteForm').submit();
                    }
                });
            });

            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
                initialView: 'timeGridWeek',
                weekNumberCalculation: "ISO",
                locale: 'es',
                editable: false,
                firstDay: 1,
                weekends: true,

                allDaySlot: false,
                headerToolbar: {
                    left: '',
                    center: '',
                    right: ''
                },
                events: [
                        @foreach($horarios as $horario)
                    {
                        title:"Abierto",
                        description:'de {{$horario->inicio}} a {{$horario->fin}} HRS.',
                        startTime: '{{$horario->inicio}}',
                        endTime: '{{$horario->fin}}',
                        daysOfWeek: [ {{$horario->dia}} ],
                        horario_id: '{{$horario->id}}',
                    },
                    @endforeach
                ],
                eventClick: function(info) {
                    const borrarId = info.event.extendedProps.horario_id;
                    Swal.fire({
                        title: '¿Estás seguro que deseas borrar este elemento?',
                        text: "No podrás revertir esta acción",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#666',
                        confirmButtonText: 'Sí, quiero eliminarlo'
                    }).then(function (result) {
                        if (result.value) {
                            $('#deleteForm').attr('action', '{{route('admin.horarios.index')}}' + '/' + borrarId);
                            $('#deleteForm').submit();
                        }
                    });
                }
            });

            $('.ms-aside-toggler').click();
            calendar.render();
        } );
    </script>
@endsection
