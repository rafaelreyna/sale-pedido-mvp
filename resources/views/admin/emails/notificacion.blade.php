@extends('layouts.notificacion_email')

@section('content')
    <h2>Notificación - {{$notificacion->subject}}</h2>
    <hr/>
    <div>
        {!! $notificacion->message !!}
    </div>
    <br/>
    <p><a href="{{$notificacion->url}}">{{$notificacion->call_to_action}}</a></p>
@endsection
