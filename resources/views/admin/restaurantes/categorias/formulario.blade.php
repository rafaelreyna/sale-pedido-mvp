@extends('layouts.app')

@section('content')
    <div  class="row">
        <div  class="col-md-12">
            <nav  aria-label="breadcrumb">
                <ol  class="breadcrumb pl-0">
                    <li  class="breadcrumb-item">
                        <a  href="{{route('admin')}}"><i class="material-icons">home</i> Inicio</a>
                    </li>
                    <li  class="breadcrumb-item"><a  href="#">Configuracion</a></li>
                    <li  aria-current="page" class="breadcrumb-item active">Categorías de restaurante</li>
                </ol>
            </nav>
        </div>
        <div  class="ms-panel col-md-12">
            <div  class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 >{{$titulo}}</h6>
                        <p class="alert alert-info"><i class="fa fa-info-circle"></i>Recuerda que puedes seleccionar un máximo de 2 categorías</p>
                    </div>
                </div>
            </div>
            <div  class="ms-panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin: 0px;">
                            @foreach ($errors->all() as $error)
                                <li><i class="flaticon-alert"></i> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{$route}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{$negocio->id?method_field('put'):method_field('post')}}
                    <h5>Categorías</h5>
                    <div class="row">
                    @foreach($categorias as $categoria)

                            <div class="col-md-4">
                                <div class="alert alert-secondary" style="background-color: #EEE">
                                    <label class="ms-checkbox-wrap ms-checkbox-primary">
                                        <input  {{ $negocio->restaurante_categorias->contains($categoria) ? 'checked' : '' }} name="categorias[{{$categoria->id}}]" type="checkbox" value="{{$categoria->id}}" style="background-color: #FFF"> <i class="ms-checkbox-check"></i>
                                    </label><span> {{$categoria->nombre}} </span>
                                    <!--
                                    <hr/>
                                    <p>{{$categoria->descripcion}}</p>
                                    -->
                                </div>
                            </div>

                    @endforeach
                        </div>
                    <input type="submit" class="btn btn-primary" value="{{$callToAction}}">
                </form>
            </div>
        </div>
    </div>
@endsection
