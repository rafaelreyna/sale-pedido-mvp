@extends('layouts.app')

@section('content')
    <div  class="row">
        <div  class="col-md-12">
            <nav  aria-label="breadcrumb">
                <ol  class="breadcrumb pl-0">
                    <li  class="breadcrumb-item">
                        <a  href="{{route('admin')}}"><i class="material-icons">home</i> Inicio</a>
                    </li>
                    <li  class="breadcrumb-item"><a  href="#">Menú</a></li>
                    <li  class="breadcrumb-item">
                        <a  href="{{route('admin.platillocategorias.index')}}">Categorías de Platillos</a>
                    </li>
                    <li  aria-current="page" class="breadcrumb-item active">{{$categoria->id?"Editar":"Agregar"}}</li>
                </ol>
            </nav>
        </div>
        <div  class="ms-panel col-md-8">
            <div  class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 >{{$titulo}}</h6>
                    </div>
                    <a href="{{route('admin.platillocategorias.index')}}"><i class="material-icons">arrow_back</i> Regresar</a>
                </div>
            </div>
            <div  class="ms-panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin: 0px;">
                            @foreach ($errors->all() as $error)
                                <li><i class="flaticon-alert"></i> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{$route}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{$categoria->id?method_field('put'):method_field('post')}}
                    <p style="text-align: center">
                        @if($categoria->foto)
                            <img class="img-fluid img-detail" src="{{route('images.public', [base64_encode('restaurantes/platillocategorias/'), $categoria->foto])}}"/>
                        @else
                            <img class="img-fluid img-detail" src="{{URL::to('/assets/img')}}/sin_imagen.jpg"/>
                        @endif
                    </p>
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control @error('nombre') is-invalid @enderror" id="nombre" name="nombre" placeholder="Nombre de la categoría" value="{{old('nombre')?old('nombre'):$categoria->nombre}}">
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripción</label>
                        <textarea class="form-control text-counter @error('descripcion') is-invalid @enderror" id="descripcion"
                                  name="descripcion" rows="3" placeholder="Descripción de esta categoría"
                                  maxlength="250">{{old('descripcion')?old('descripcion'):$categoria->descripcion}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="foto">Cambiar imagen <small> (PNG, JPG | max. 1 MB | recomendado 1024 x 768)</small></label>
                        <input type="file" class="form-control @error('foto') is-invalid @enderror" id="foto" name="foto" />
                    </div>
                    @if(auth::user()->tipo != 'operador')
                        <input type="submit" class="btn btn-primary" value="{{$callToAction}}">
                    @endif
                </form>
            </div>
        </div>
    </div>
@endsection
