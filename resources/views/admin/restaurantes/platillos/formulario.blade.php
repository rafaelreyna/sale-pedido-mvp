@extends('layouts.app')

@section('content')
    <div  class="row">
        <div  class="col-md-12">
            <nav  aria-label="breadcrumb">
                <ol  class="breadcrumb pl-0">
                    <li  class="breadcrumb-item">
                        <a  href="{{route('admin')}}"><i class="material-icons">home</i> Inicio</a>
                    </li>
                    <li  class="breadcrumb-item"><a  href="#">Menú</a></li>
                    <li  class="breadcrumb-item">
                        <a  href="{{route('admin.platillos.index')}}">Platillos</a>
                    </li>
                    <li  aria-current="page" class="breadcrumb-item active">{{$platillo->id?"Editar":"Agregar"}}</li>
                </ol>
            </nav>
        </div>
        <div  class="ms-panel col-md-8">
            <div  class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 >{{$titulo}}</h6>
                    </div>
                    <a href="{{route('admin.platillos.index')}}"><i class="material-icons">arrow_back</i> Regresar</a>
                </div>
            </div>
            <div  class="ms-panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin: 0px;">
                            @foreach ($errors->all() as $error)
                                <li><i class="flaticon-alert"></i> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{$route}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{$platillo->id?method_field('put'):method_field('post')}}
                    <p style="text-align: center">
                        @if($platillo->foto)
                            <img class="img-fluid img-detail" src="{{route('images.public', [base64_encode('restaurantes/platillos/'), $platillo->foto])}}"/>
                        @else
                            <img class="img-fluid img-detail" src="{{URL::to('/assets/img')}}/sin_imagen.jpg"/>
                        @endif
                    </p>
                    <div class="form-group" style="text-align: right">
                        <small>Disponible&nbsp;&nbsp;</small><br/>
                        <label class="ms-switch">
                            <input type="checkbox" @if(!$platillo->id || ($platillo->disponible || old('disponible'))) checked="" @endif name="disponible" id="disponible"> <span class="ms-switch-slider ms-switch-primary round"></span>
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="categoria_id">Categoría</label>
                        <select class="form-control @error('categoria_id') is-invalid @enderror"
                               id="categoria_id" name="categoria_id">
                            <option value="">SELECCIONA UNA OPCIÓN</option>
                            @foreach($categorias as $categoria)
                                <option @if($platillo->categoria_id == $categoria->id || old('categoria_id') == $categoria->id) selected="selected" @endif
                                    value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                            @endforeach()
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control @error('nombre') is-invalid @enderror"
                               id="nombre" name="nombre" placeholder="Nombre del platillo"
                               value="{{old('nombre')?old('nombre'):$platillo->nombre}}">
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripción</label>
                        <textarea class="form-control text-counter @error('descripcion') is-invalid @enderror"
                            id="descripcion" name="descripcion" rows="3" placeholder="Descripción de este platillo" maxlength="250"
                            >{{old('descripcion')?old('descripcion'):$platillo->descripcion}}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="foto">Cambiar imagen<small> (PNG, JPG | max. 1 MB | recomendado 1024 x 768)</small></label>
                        <input type="file" class="form-control @error('foto') is-invalid @enderror" id="foto" name="foto" />
                    </div>

                    <label for="precio">Precio <small>Recuerda que los precios tienen que ser igual o menores que el precio en tu negocio</small></label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">$</div>
                        </div>
                        <input type="number" min="0" step="0.01" class="form-control @error('precio') is-invalid @enderror"
                               id="precio" name="precio" placeholder="Precio del platillo"
                               value="{{old('precio')?old('precio'):$platillo->precio}}">
                    </div>

                    <label for="precio_promo">Precio en promoción <small>Recuerda que el precio que pongas es el precio que le aparecerá al cliente. Dejar en blanco para deshabilitar la promoción</small></label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">$</div>
                        </div>

                        <input type="number" min="0" step="0.01" class="form-control @error('precio_promo') is-invalid @enderror"
                               id="precio_promo" name="precio_promo" placeholder="Precio del platillo en promoción"
                               value="{{old('precio_promo')?old('precio_promo'):$platillo->precio_promo}}">

                    </div>

                    @if(auth::user()->tipo != 'operador')
                        <input type="submit" class="btn btn-primary" value="{{$callToAction}}">
                    @endif
                </form>
            </div>
        </div>
    </div>
@endsection
