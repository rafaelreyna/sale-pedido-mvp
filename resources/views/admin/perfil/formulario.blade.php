@extends('layouts.app')

@section('content')
    <div  class="row">
        <div  class="col-md-12">
            <nav  aria-label="breadcrumb">
                <ol  class="breadcrumb pl-0">
                    <li  class="breadcrumb-item">
                        <a  href="{{route('admin')}}"><i class="material-icons">home</i> Inicio</a>
                    </li>
                    <li  class="breadcrumb-item"><a  href="#">Configuracion</a></li>
                    <li  aria-current="page" class="breadcrumb-item active">Configuración de Perfil</li>
                </ol>
            </nav>
        </div>
        <div  class="ms-panel col-md-12">
            <div  class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 >Configurar Perfil de Usuario (Acceso al sistema)</h6>
                        <p class="alert alert-info"><i class="fa fa-info-circle"></i>Recuerda que si decides cambiar tu correo electrónico, no podrás acceder a la plataforma hasta que lo verifiques por medio del enlace que será enviado a la nueva dirección de correo.</p>
                    </div>
                </div>
            </div>
            <div  class="ms-panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin: 0px;">
                            @foreach ($errors->all() as $error)
                                <li><i class="flaticon-alert"></i> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{route('admin.perfil.update')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{method_field('put')}}
                    <div class="form-group">
                        <label for="nombre">Nombre Completo</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror"
                               id="nombre" name="name" placeholder="Nombre de la categoría"
                               value="{{old('name')?old('name'):$perfil->name}}">
                    </div>

                    <div class="form-group">
                        <label for="nombre">Email</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                               id="nombre" name="email" placeholder="Nombre de la categoría"
                               value="{{old('email')?old('email'):$perfil->email}}">
                    </div>

                    <div class="form-group">
                        <label for="nombre">Celular</label>
                        <input type="text" class="form-control @error('celular') is-invalid @enderror"
                               id="nombre" name="celular" placeholder="Nombre de la categoría"
                               value="{{old('celular')?old('celular'):$perfil->celular}}">
                    </div>

                    <div class="form-group">
                        <label for="nombre">Cambiar contraseña</label>
                        <input id="password" type="password"
                               class="checkCaps form-control @error('password') is-invalid @enderror" name="password"
                               autocomplete="new-password"
                        >
                        <div class="capslockdiv" style="display:none">Mayus Activado.</div>
                    </div>

                    <div class="form-group">
                        <label for="nombre">confirmar nueva contraseña</label>
                        <input id="password-confirm" type="password"
                               class="checkCaps form-control @error('password-confirm') is-invalid @enderror" name="password_confirmation"
                               autocomplete="new-password"
                        >
                        <div class="capslockdiv" style="display:none">Mayus Activado.</div>
                    </div>

                    <input type="submit" class="btn btn-primary" value="Guardar Perfil">
                </form>
            </div>
        </div>
    </div>
    <script src="{{URL::to('/')}}/assets/js/pwstrength.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var options = {
                bootstrap3: true,
                scores: [17, 26, 40, 50],
                verdicts: ["Débil", "Normal", "Media", "Fuerte", "Muy Fuerte"],
                showVerdicts: true,
                showVerdictsInitially: true,
                raisePower: 1.4,
            };
            $('#password').pwstrength(options);

            check_capslock_form($('#formid'));


            document.onkeydown = function (e) { //check if capslock key was pressed in the whole window
                e = e || event;
                if (typeof (window.lastpress) === 'undefined') { window.lastpress = e.timeStamp; }
                if (typeof (window.capsLockEnabled) !== 'undefined') {
                    if (e.keyCode == 20 && e.timeStamp > window.lastpress + 50) {
                        window.capsLockEnabled = !window.capsLockEnabled;
                        $('.capslockdiv').toggle();
                    }
                    window.lastpress = e.timeStamp;
                    //sometimes this function is called twice when pressing capslock once, so I use the timeStamp to fix the problem
                }

            };

            function check_capslock(e) { //check what key was pressed in the form
                var s = String.fromCharCode(e.keyCode);
                if (s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey) {
                    window.capsLockEnabled = true;
                    $('.capslockdiv').show();
                }
                else {
                    window.capsLockEnabled = false;
                    $('.capslockdiv').hide();
                }
            }

            function check_capslock_form(where) {
                if (!where) { where = $(document); }
                where.find('input,select').each(function () {
                    if (this.type != "hidden") {
                        $(this).keypress(check_capslock);
                    }
                });
            }
        });
    </script>
    <style type="text/css">
        .progress{margin: 3px;}
        .invalid-feedback{position: inherit; bottom: 0px;}
        .capslockdiv {color:#900}
    </style>
    <style type="text/css">
        .btn:hover{
            color:#ee072c!important;
        }
        .btn-primary:hover{
            color:#FFF!important;
        }
    </style>
@endsection
