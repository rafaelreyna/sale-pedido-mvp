@extends('layouts.app')

@section('content')
    <div _ngcontent-vbt-c44="" class="row">
        <div _ngcontent-vbt-c44="" class="col-md-12">
            <nav _ngcontent-vbt-c44="" aria-label="breadcrumb">
                <ol _ngcontent-vbt-c44="" class="breadcrumb pl-0">
                    <li _ngcontent-vbt-c44="" class="breadcrumb-item"><a _ngcontent-vbt-c44="" href="{{route('admin')}}"><i
                                _ngcontent-vbt-c44="" class="material-icons">home</i> Inicio</a></li>
                    <li _ngcontent-vbt-c44="" class="breadcrumb-item"><a _ngcontent-vbt-c44="" href="#">Menú</a></li>
                    <li _ngcontent-vbt-c44="" aria-current="page" class="breadcrumb-item active">Estado de Cuenta</li>
                </ol>
            </nav>
        </div>
        <div _ngcontent-vbt-c51="" class="ms-panel col-md-12">
            <div _ngcontent-vbt-c51="" class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 _ngcontent-vbt-c51="">Estado de Cuenta UKI&reg;</h6>
                        <p>* Te recordamos que tienes hasta el día 5 de cada mes para realizar tu pago y evitar que se suspenda tu servicio.</p>
                    </div>
                </div>
            </div>
            <div _ngcontent-vbt-c51="" class="ms-panel-body">
            @if($facturas->count() > 0)
                <div class="table-responsive">
                    <table id="example" class="table table-sm table-datatable table-striped thead-primary w-100 dataTable no-footer" style="width:100%">
                    <thead >
                        <tr>
                            <th>Status</th>
                            <th>Mes de facturación</th>
                            <th>No. Transacciones</th>
                            <th>Subtotal</th>
                            <th>IVA</th>
                            <th>Total</th>
                            <th style="width: 160px;">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($facturas as $factura)
                            <tr>
                                <td>
                                    @if($factura->status == \App\Models\Factura::FACTURA_STATUS_PENDIENTE)
                                        <span class="badge badge-warning">{{\App\Models\Factura::FACTURA_STATUS_PENDIENTE_TEXT}}</span><br>
                                        <small>Vence el {{date('d-m-Y', strtotime($factura->vencimiento))}}</small>
                                    @endif
                                    @if($factura->status == \App\Models\Factura::FACTURA_STATUS_PAGADA)
                                            <span class="badge badge-success">{{\App\Models\Factura::FACTURA_STATUS_PAGADA_TEXT}}</span>
                                    @endif
                                    @if($factura->status == \App\Models\Factura::FACTURA_STATUS_VENCIDA)
                                            <span class="badge badge-danger">{{\App\Models\Factura::FACTURA_STATUS_VENCIDA_TEXT}}</span>
                                    @endif
                                </td>
                                <td>
                                    {{\App\Helpers\DateHelper::mes_letra($factura->mes)}}
                                    {{date('Y', strtotime("{$factura->ano}/{$factura->mes}/01"))}}
                                </td>
                                <td>{{$factura->numero_transacciones}} Transacciones</td>
                                <td>$ {{number_format($factura->subtotal, 2, '.', ',')}}</td>
                                <td>$ {{number_format($factura->iva, 2, '.', ',')}}</td>
                                <td>$ {{number_format($factura->total, 2, '.', ',')}}</td>
                                <td>
                                    @if($factura->status == \App\Models\Factura::FACTURA_STATUS_PENDIENTE)
                                        <a href="{{route('facturas.pago', $factura->id)}}" class="btn  btn-success text-white"> Pagar</a>
                                        &nbsp;
                                    @endif
                                    @if($factura->status == \App\Models\Factura::FACTURA_STATUS_VENCIDA)
                                        <a href="{{route('facturas.pago', $factura->id)}}" class="btn  btn-danger text-white"> Pagar</a>
                                        &nbsp;
                                    @endif
                                    <a href="{{route('admin.facturas.show', $factura->id)}}" class="btn  text-white btn-info"> Detalle</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            @endif
                @if($facturas->count() <= 0)
                    <p class="alert alert-info"><i class="flaticon-information"></i> Aún no tienes facturas pendientes</p>
                @endif
            </div>
        </div>
    </div>
    <script type="application/javascript">
        $(document).ready(function() {
            $('.table-datatable').DataTable({"ordering": false, "pageLength": 25, "bLengthChange": false,
                "language": {
                    "url": "{{URL::to('/assets/lang/dataTable/Spanish.json')}}"
                }});
        } );
    </script>
@endsection
