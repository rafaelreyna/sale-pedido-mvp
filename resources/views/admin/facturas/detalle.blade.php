@extends('layouts.app')

@section('content')
    <div _ngcontent-vbt-c44="" class="row">
        <div _ngcontent-vbt-c44="" class="col-md-12">
            <nav _ngcontent-vbt-c44="" aria-label="breadcrumb">
                <ol _ngcontent-vbt-c44="" class="breadcrumb pl-0">
                    <li _ngcontent-vbt-c44="" class="breadcrumb-item"><a _ngcontent-vbt-c44="" href="{{route('admin')}}"><i
                                _ngcontent-vbt-c44="" class="material-icons">home</i> Inicio</a></li>
                    <li _ngcontent-vbt-c44="" class="breadcrumb-item"><a _ngcontent-vbt-c44="" href="#">Menú</a></li>
                    <li _ngcontent-vbt-c44="" aria-current="page" class="breadcrumb-item active">Estado de Cuenta</li>
                    <li _ngcontent-vbt-c44="" aria-current="page" class="breadcrumb-item active">Recibo</li>
                </ol>
            </nav>
        </div>
        <div _ngcontent-vbt-c51="" class="ms-panel col-md-12">
            <div _ngcontent-vbt-c51="" class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 _ngcontent-vbt-c51="">Detalle de recibo #{{date('Ymd', strtotime($factura->created_at)) . $factura->id}}</h6>
                    </div>
                </div>
            </div>
        </div>

        <div _ngcontent-vbt-c44="" class="col-md-12">
            @if( auth::user()->tipo == 'superadmin')
                <p style="text-align: right"><a href="{{URL::to("admin/facturas?negocio_id={$factura->negocio->id}")}}"><i class="fa fa-arrow-left"></i> Regresar</a></p>
            @else
                <p style="text-align: right"><a href="{{route('admin.facturas.index')}}"><i class="fa fa-arrow-left"></i> Regresar</a></p>
            @endif
        </div>

        <div class="ms-panel col-md-10">
            <div class="ms-panel-header header-mini">
                <div class="d-flex justify-content-between">
                    <h6>Recibo
                        @if($factura->status == 'pendiente')
                            <span class="badge badge-warning">Pendiente</span>
                        @endif
                        @if($factura->status == 'pagada') <span class="badge badge-success">Pagado</span>@endif
                    </h6>
                    <h6>#{{date('Ymd', strtotime($factura->created_at)) . $factura->id}}</h6>
                </div>
            </div>
            <div class="ms-panel-body">
                <!-- Invoice To -->
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="invoice-address">
                            <h3>Receptor: </h3>
                            <h5>{{$factura->negocio->nombre}}</h5>
                            <p>{{$factura->negocio->admin()->name}}</p>
                            <p>{{$factura->negocio->email}}</p>
                            <p>{{$factura->negocio->constitucion_rfc}}</p>
                        </div>
                    </div>
                    <div class="col-md-6 text-md-right">
                        <ul class="invoice-date">
                            <li>Fecha de recibo : {{date('d / m / Y', strtotime($factura->created_at))}}</li>
                            <li>Periodo:
                                {{\App\Helpers\DateHelper::mes_letra(date('m', strtotime("{$factura->ano}/{$factura->mes}/01")))}}
                                {{date('Y', strtotime("{$factura->ano}/{$factura->mes}/01"))}}
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Invoice Table -->
                <div class="ms-invoice-table table-responsive mt-5">
                    <table class="table table-hover text-right thead-light">
                        <thead>
                        <tr class="text-capitalize">
                            <th class="text-center w-5">#</th>
                            <th class="text-left">Concepto</th>
                            <th>Costo unitario</th>
                            <th>Cantidad</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-center">1</td>
                            <td class="text-left">Membresía Mensual UKI&reg;</td>
                            <td>$ {{number_format($factura->cargo_membresia, 2, '.', ',')}}</td>
                            <td>1</td>
                            <td>$ {{number_format($factura->cargo_membresia, 2, '.', ',')}}</td>
                        </tr>
                        <tr>
                            <td class="text-center">1</td>
                            <td class="text-left">Descuento en membresía: {{$factura->concepto_descuento_membresia}}</td>
                            <td>- $ {{number_format($factura->descuento_membresia, 2, '.', ',')}}</td>
                            <td>1</td>
                            <td>- $ {{number_format($factura->descuento_membresia, 2, '.', ',')}}</td>
                        </tr>
                        <tr>
                            <td class="text-center">1</td>
                            <td class="text-left">Comisión por Transacciones</td>
                            <td>$ {{number_format($factura->costo_transaccion, 2, '.', ',')}}</td>
                            <td>{{$factura->numero_transacciones}}</td>
                            <td>$ {{number_format($factura->cargo_transacciones, 2, '.', ',')}}</td>
                        </tr>
                        @if($factura->descuento_transacciones != 0)
                            <tr>
                                <td class="text-center">1</td>
                                <td class="text-left">Descuento en comisión de transacciones: {{$factura->concepto_descuento_transacciones}}</td>
                                <td>- $ {{number_format($factura->descuento_transacciones, 2, '.', ',')}}</td>
                                <td>1</td>
                                <td>- $ {{number_format($factura->descuento_transacciones, 2, '.', ',')}}</td>
                            </tr>
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="4">Subtotal:</td>
                            <td>$ {{number_format($factura->subtotal, 2, '.', ',')}} MXN</td>
                        </tr>
                        <tr>
                            <td colspan="4">IVA:</td>
                            <td>$ {{number_format($factura->iva, 2, '.', ',')}} MXN</td>
                        </tr>
                        <tr>
                            <td colspan="4">TOTAL:</td>
                            <td>$ {{number_format($factura->total, 2, '.', ',')}} MXN</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="invoice-buttons text-right">
                    <a href="{{route('admin.facturas.desglose', $factura->id)}}" class="btn btn-primary mr-2">Descargar detalle de transacciones</a>
                    @if($factura->status == 'pendiente')
                        <a href="{{route('facturas.pago', $factura->id)}}" class="btn btn-success">Pagar Recibo</a>
                    @else
                        <a href="{{route('admin.facturas.solicitar_factura', $factura->id)}}" class="btn btn-success">Solicitar factura</a>
                    @endif
                </div>
            </div>
        </div>

    </div>
    <script type="application/javascript">
        $(document).ready(function() {
            $('.table-datatable').DataTable({"ordering": false, "pageLength": 25, "bLengthChange": false,
                "language": {
                    "url": "{{URL::to('/assets/lang/dataTable/Spanish.json')}}"
                }});
        } );
    </script>
@endsection
