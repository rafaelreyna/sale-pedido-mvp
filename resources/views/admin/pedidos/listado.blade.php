@extends('layouts.app')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul style="margin: 0px;">
                @foreach ($errors->all() as $error)
                    <li><i class="flaticon-alert"></i> {{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

<!-- Recent Placed Orders< -->
<div class="col-12">
    <div class="ms-panel">
        <div class="ms-panel-header">
            <h6>Pedidos por atender</h6>
        </div>
        <div class="ms-panel-body">
            <div class="table-responsive">
                @component('admin.components.tabla_pedido')
                    @slot('pedidos', $pedidos)
                @endcomponent
            </div>
        </div>
    </div>
</div>

<div class="col-12">
    <div class="ms-panel">
        <div class="ms-panel-header">
            <h6>Pedidos atendidos</h6>
        </div>
        <div class="ms-panel-body">
            <div class="table-responsive">
                @component('admin.components.tabla_pedido')
                    @slot('pedidos', $pedidos_atendidos)
                @endcomponent
            </div>
        </div>
    </div>
</div>

<script type="application/javascript">
    $(document).ready(function(){
        swal(
            "Adminsitrador de Pedidos",
            "Haz click en OK para comenzar a recibir las notificaciones",
            "success"
        )
        @if(!(isset($_GET['cancel_not']) && $_GET['cancel_not']))
            .then(() => {
                revisarpedidos();
            })
        @endif
    })
</script>
<!-- Recent Orders< -->
@endsection
