@extends('layouts.app')

@section('content')
<!-- Recent Placed Orders< -->
<div class="col-12">
    <div class="ms-panel">
        <div class="ms-panel-header">
            <h6>Historial detallado de pedidos</h6>
        </div>
        <div class="ms-panel-body">
            <div class="table-responsive">
                <div id="gridContainer"></div>
            </div>
        </div>
    </div>
</div>
<!-- Dev Extreme CSS-->
<link href="https://cdn3.devexpress.com/jslib/20.2.4/css/dx.common.css" rel="stylesheet">
<link href="https://cdn3.devexpress.com/jslib/20.2.4/css/dx.material.blue.light.compact.css" rel="stylesheet">
<!-- Dev'xpress -->
<script src="https://cdn3.devexpress.com/jslib/20.2.4/js/dx.all.js"></script>
<script src="https://cdn3.devexpress.com/jslib/20.2.4/js/localization/dx.messages.es.js"></script>
<script type="text/javascript">
    var customers = {!! json_encode($pedidos) !!}

    $(document).ready(function(){
        DevExpress.localization.locale('es-ES');

        $("#gridContainer").dxDataGrid({
            dataSource: customers,
            columns: [
                {
                    dataField: 'id',
                    width: 100,
                    caption:'No. Pedido',
                    allowFiltering: true ,
                    allowSorting: true,
                    allowHeaderFiltering: false,
                    groupIndex: 1,
                    sortOrder: 'desc',
                },
                {
                    dataField: 'status',
                    width: 130,
                    caption:'Etatus',
                    allowFiltering: false ,
                    allowSorting: true,
                    allowHeaderFiltering: true,
                    cellTemplate: function(container, options) {
                        var textStatus = '';
                        switch(options.data.status) {
                            case '{{\App\Models\Pedido::PEDIDO_STATUS_CANCELADO}}':  textStatus = '<span class="badge badge-light">Cancelado</span>'; break
                            case '{{\App\Models\Pedido::PEDIDO_STATUS_TERMINADO}}':  textStatus = '<span class="badge badge-primary">Terminado</span>'; break
                            case '{{\App\Models\Pedido::PEDIDO_STATUS_PAGADO}}':  textStatus = '<span class="badge badge-success">Pagado</span>'; break
                        }
                        $("<div />")
                            .html(textStatus)
                            .appendTo(container);
                    }
                },
                {
                    dataField: 'platillo',
                    width: 150,
                    caption:'Platillo',
                    allowFiltering: false ,
                    allowSorting: true,
                    allowHeaderFiltering: true
                },
                {
                    dataField: 'categoria',
                    width: 100,
                    caption:'Categoría',
                    allowFiltering: false ,
                    allowSorting: true,
                    allowHeaderFiltering: true
                },
                {
                    dataField: 'cantidad',
                    width: 100,
                    caption:'Cantidad',
                    allowFiltering: true ,
                    allowSorting: true,
                    allowHeaderFiltering: false,
                    dataType: "number",
                },
                {
                    dataField: 'costo_unitario',
                    width: 100,
                    caption:'Costo Platillo',
                    allowFiltering: true ,
                    allowSorting: true,
                    allowHeaderFiltering: false,
                    dataType: "number",
                    format: {
                        type: "currency",
                        currency: "MXN",
                        useGrouping: true,
                    }
                },
                {
                    dataField: 'nombre',
                    width: 150,
                    caption:'Cliente',
                    allowFiltering: true,
                    allowSorting: true,
                    allowHeaderFiltering: false
                },
                {
                    dataField: 'email',
                    width: 150,
                    caption:'Email Cliente',
                    allowFiltering: true,
                    allowSorting: true,
                    allowHeaderFiltering: false
                },
                    @if(auth::user()->tipo == 'superadmin')
                {
                    dataField: 'direccion',
                    width: 250,
                    caption:'Direccion Cliente',
                    allowFiltering: true,
                    allowSorting: true,
                    allowHeaderFiltering: false
                },
                {
                    dataField: 'telefono',
                    width: 120,
                    caption:'Teléfono Cliente',
                    allowFiltering: true,
                    allowSorting: true,
                    allowHeaderFiltering: false
                },
                @endif
                {
                    dataField: 'envio',
                    width: 150,
                    caption:'Tipo de envío',
                    allowFiltering: false ,
                    allowSorting: true,
                    allowHeaderFiltering: true
                },
                {
                    dataField: 'distancia',
                    width: 150,
                    caption:'Distancia (km)',
                    allowFiltering: true,
                    allowSorting: true,
                    allowHeaderFiltering: false,
                    dataType: "number",
                },
                {
                    dataField: 'forma_pago',
                    width: 150,
                    caption:'Forma de Pago',
                    allowFiltering: false ,
                    allowSorting: true,
                    allowHeaderFiltering: true
                },
                {
                    dataField: 'total',
                    width: 100,
                    caption: 'Total del pedido',
                    allowFiltering: true,
                    allowSorting: true,
                    allowHeaderFiltering: false,
                    dataType: "number",
                    format: {
                        type: "currency",
                        currency: "MXN",
                        useGrouping: true,
                    }
                },
                {
                    dataField: 'costo_envio',
                    width: 100,
                    caption:'Costo de Envío del pedido',
                    allowFiltering: true ,
                    allowSorting: true,
                    allowHeaderFiltering: false,
                    dataType: "number",
                    format: {
                        type: "currency",
                        currency: "MXN",
                        useGrouping: true,
                    }
                },
                {
                    dataField: 'gran_total',
                    width: 100,
                    caption:'Gran Total del pedido',
                    allowFiltering: true ,
                    allowSorting: true,
                    allowHeaderFiltering: false,
                    dataType: "number",
                    format: {
                        type: "currency",
                        currency: "MXN",
                        useGrouping: true,
                    }
                },
                {
                    dataField: 'created_at',
                    width: 240,
                    caption:'Fecha',
                    allowFiltering: true ,
                    allowSorting: true,
                    allowHeaderFiltering: false,
                    dataType: "date",
                    format: {
                        type: "longDate",
                    }
                },
                {
                    dataField: 'created_at',
                    width: 100,
                    caption:'Hora',
                    allowFiltering: true ,
                    allowSorting: true,
                    allowHeaderFiltering: false,
                    dataType: "datetime",
                    format: {
                        type: "shortTime",
                    }
                },
                ],
            paging: {
                pageSize: 20
            },
            filterRow: {
                visible: true
            },
            headerFilter: { visible: true },
            editing: {
                removeEnabled: false
            },
            scrolling: {
                mode: 'infinite',
            },
            columnWidth: 150,
            "export": {
                enabled: true,
                allowExportSelectedData: false,
                excelFilterEnabled: true,
                fileName: 'historial_detallado_{{date('Y-m-d')}}'
            },
            searchPanel: { visible: true, searchVisibleColumnsOnly:true },
            groupPanel: { visible: true },
            grouping: {
                expandMode: "rowClick"  // or "buttonClick"
            },
            allowColumnReordering: true,
            rowAlternationEnabled: true,
            showBorders: true,
            height:$( window ).height() - 100,
        });
    });
</script>
@endsection
