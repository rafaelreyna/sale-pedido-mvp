<!-- Dashboard -->
<li class="menu-item">
    <a href="{{route('admin')}}"> <span><i class="material-icons fs-16">dashboard</i>Dashboard </span>
    </a>
</li>
<!-- /Dashboard -->
<!-- product -->
<li class="menu-item" id="menu-menu">
    <a href="#" class="has-chevron" data-toggle="collapse" data-target="#menu" aria-expanded="false"
       aria-controls="menu"> <span><i class="fa fa-archive fs-16"></i>Menu </span>
    </a>
    <ul id="menu" class="collapse" aria-labelledby="product" data-parent="#side-nav-accordion">
        <li><a href="{{route('admin.platillocategorias.index')}}">Categorías de platillos</a></li>
        <li><a href="{{route('admin.platillos.index')}}">Platillos</a></li>
    </ul>
</li>
<!-- product end -->
<!-- orders -->
<li class="menu-item">
    <a href="#" class="has-chevron" data-toggle="collapse" data-target="#pedidos" aria-expanded="false"
       aria-controls="pedidos"> <span><i class="flaticon-supermarket"></i>Pedidos </span>
    </a>
    <ul id="pedidos" class="collapse" aria-labelledby="product" data-parent="#side-nav-accordion">
        <li><a href="{{route('admin.pedidos.index')}}"> Pedidos Recientes</a></li>
        <li><a href="{{route('admin.pedidos.historial')}}"> Historial de pedidos</a></li>
        <li><a href="{{route('admin.pedidos.historial_detallado')}}"> Historial detallado</a></li>
    </ul>
</li>
<!-- orders end -->

<li class="menu-item">
    <a href="{{route('admin.operadores.index')}}"> <span><i class="flaticon-user"></i>Operadores</span>
    </a>
</li>

<li class="menu-item">
    <a href="{{route('admin.facturas.index')}}"> <span><i class="flaticon-sticky-note"></i>Estado de Cuenta</span>
    </a>
</li>

<!-- restaurants -->
<li class="menu-item">
    <a href="#" class="has-chevron" data-toggle="collapse"  data-target="#configuracion"
       aria-expanded="false" aria-controls="configuracion">
        <span><i class="flaticon-gear"></i>Configuración </span>
    </a>
    <ul id="configuracion" class="collapse" aria-labelledby="configuracion"
        data-parent="#side-nav-accordion">
        @if(\Illuminate\Support\Facades\Auth::user()->negocio->verificado)
            <li><a href="{{route('admin.negocios.sucursales.index', auth::user()->negocio_id)}}">Sucursales</a></li>
        @endif
        @if(\Illuminate\Support\Facades\Auth::user()->negocio->verificado)
            <li><a href="{{route('admin.negocios.edit', auth::user()->negocio_id)}}">Datos de contacto </a></li>
        @else
            <li><a href="{{route('admin.negocios.edit', auth::user()->negocio_id)}}">Info. del negocio </a></li>
        @endif
        @if(!\Illuminate\Support\Facades\Auth::user()->negocio->verificado)
            <li><a href="{{route('admin.restaurantes.categorias.edit', auth::user()->negocio_id)}}">Categorías
                    de restaurante</a></li>
        @endif
        <li><a href="{{route('admin.horarios.index')}}">Horarios</a></li>
        <li><a href="{{route('admin.negocios.envios.edit', auth::user()->negocio_id)}}">Envíos</a></li>
        <li><a href="{{route('admin.negocios.pago.edit', auth::user()->negocio_id)}}">Opciones de Pago</a>
        <li><a href="{{route('admin.negocios.notificaciones.edit', auth::user()->negocio_id)}}">Notificaciones</a>
        </li>
    </ul>
</li>

<li class="menu-item">
    <a href="{{route('admin.tutoriales.index')}}"> <span><i class="flaticon-computer"></i>Tutoriales</span></a>
</li>
<li class="menu-item">
    <a href="mailto:hola@uki.mx"> <span><i class="flaticon-mail"></i>hola@uki.com</span></a>
</li>
