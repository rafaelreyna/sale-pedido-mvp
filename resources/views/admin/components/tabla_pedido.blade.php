<table class="table table-hover thead-primary" style="width: 100%;">
    <thead>
    <tr>
        <th scope="col"></th>
        <th scope="col">No. Orden</th>
        <th scope="col">Orden <span style="color:#190d38">-----------------------------------------------------</span></th>
        <th scope="col">Cliente</th>
        <th style="width: 30%!important;" scope="col">Dirección</th>
        <th scope="col">Teléfono</th>
        <th scope="col">Status</th>
        <th scope="col">Envío</th>
        <th scope="col">Forma de Pago</th>
        <th scope="col">Fecha</th>
        <th scope="col">Hora</th>
        <th scope="col">Subtotal</th>
        <th scope="col">Envío</th>
        <th scope="col">Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($pedidos as $pedido)
        <tr>
            <td>
                @if($pedido->status == \App\Models\Pedido::PEDIDO_STATUS_CONFIRMADO || $pedido->status == \App\Models\Pedido::PEDIDO_STATUS_PAGADO)
                    <div class="btn-group ">
                        <a href="{{route('admin.pedidos.update_status', [$pedido->id, \App\Models\Pedido::PEDIDO_STATUS_PREPARANDO])}}" type="button" class="btn btn-warning text-white" style="min-width: inherit!important;">iniciar</a>
                        <button type="button" class=" btn btn-warning dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="min-width: inherit!important;">
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu">
                            <a id="btnCancelarPedido" class="dropdown-item btn-danger" href="{{route('admin.pedidos.update_status', [$pedido->id, \App\Models\Pedido::PEDIDO_STATUS_CANCELADO])}}">Cancelar Pedido</a>
                        </div>
                    </div>

                @endif
                @if($pedido->status == \App\Models\Pedido::PEDIDO_STATUS_PREPARANDO)
                        @if($pedido->envio == \App\Models\Pedido::PEDIDO_ENVIO_PICKUP)
                            <a href="{{route('admin.pedidos.update_status', [$pedido->id, \App\Models\Pedido::PEDIDO_STATUS_EN_CAMINO])}}"
                               class="btn btn-sm btn-success text-white">Notificar</a>
                        @else
                            <a href="{{route('admin.pedidos.update_status', [$pedido->id, \App\Models\Pedido::PEDIDO_STATUS_EN_CAMINO])}}"
                               class="btn btn-sm btn-success text-white">Enviar</a>
                        @endif
                @endif
            </td>
            <th scope="row">#{{$pedido->id}}</th>
            <td style="max-width: 350px!important; width: 350px;!important;">
                <ul>
                    @foreach($pedido->items as $item)
                        @if($item->platillo_data())
                            <li>
                                <a target="_blank" href="{{route('admin.platillos.edit', $item->platillo_data()->id)}}">{{$item->platillo}}</a> x{{$item->cantidad}}
                                @if(isset($item->personalizacion->comentarios_cliente))
                                    <pre class="comentarios_clientes">{{$item->personalizacion->comentarios_cliente}}</pre>
                                    <br/>
                                @endif
                            </li>
                        @else
                            <li>
                                {{$item->platillo}} x{{$item->cantidad}}
                                @if(isset($item->personalizacion->comentarios_cliente))
                                    <pre class="comentarios_clientes">{{$item->personalizacion->comentarios_cliente}}</pre>
                                    <br/>
                                @endif
                            </li>
                        @endif

                    @endforeach
                    @if($pedido->comentarios_cliente)
                        <hr/>
                            <pre class="comentarios_clientes">{{$pedido->comentarios_cliente}}</pre>
                    @endif
                </ul>
            </td>
            <td class="nombre">{{$pedido->nombre}}</td>
            <td style="white-space:inherit; width: 200px!important; text-align: left">
                <span class="direccion">{{$pedido->direccion}}</span>
                <a target="_blank" class="btn btn-sm btn-link text-dark" href="https://www.google.com/maps/search/?api=1&query={{$pedido->lat}},{{$pedido->lon}}">
                    <i class="fa fa-map-marker"></i> Mapa
                </a>
                <a  class="btn btn-sm btn-link text-dark btnCopiarDireccion">
                    <i class="fa fa-clipboard"></i> Copiar dirección
                </a>
            </td>
            <td class="telefono"><a href="tel:{{$pedido->telefono}}">{{$pedido->telefono}}</a></td>
            <td>
                @if($pedido->status == \App\Models\Pedido::PEDIDO_STATUS_CONFIRMADO)<span class="badge badge-danger">{{\App\Models\Pedido::PEDIDO_STATUS_CONFIRMADO_TEXT}}</span>@endif
                @if($pedido->status == \App\Models\Pedido::PEDIDO_STATUS_PAGADO)<span class="badge badge-success">{{\App\Models\Pedido::PEDIDO_STATUS_PAGADO_TEXT}}</span>@endif
                @if($pedido->status == \App\Models\Pedido::PEDIDO_STATUS_PREPARANDO)<span class="badge badge-warning">{{\App\Models\Pedido::PEDIDO_STATUS_PREPARANDO_TEXT}}</span>@endif
                @if($pedido->status == \App\Models\Pedido::PEDIDO_STATUS_EN_CAMINO)<span class="badge badge-success">{{\App\Models\Pedido::PEDIDO_STATUS_EN_CAMINO_TEXT}}</span>@endif
                @if($pedido->status == \App\Models\Pedido::PEDIDO_STATUS_CANCELADO)<span class="badge badge-light">{{\App\Models\Pedido::PEDIDO_STATUS_CANCELADO_TEXT}}</span>@endif
            </td>
            <td>
                @if($pedido->envio == \App\Models\Pedido::PEDIDO_ENVIO_PICKUP)<span class="badge badge-gradient-primary">{{\App\Models\Pedido::PEDIDO_ENVIO_PICKUP_TEXT}}</span>@endif
                @if($pedido->envio == \App\Models\Pedido::PEDIDO_ENVIO_FIJO)<span class="badge badge-gradient-primary">{{\App\Models\Pedido::PEDIDO_ENVIO_FIJO_TEXT}}</span>@endif
                @if($pedido->envio == \App\Models\Pedido::PEDIDO_ENVIO_DINAMICO)<span class="badge badge-gradient-primary">{{\App\Models\Pedido::PEDIDO_ENVIO_DINAMICO_TEXT}}</span>@endif
                @if($pedido->envio == \App\Models\Pedido::PEDIDO_ENVIO_GRATIS)<span class="badge badge-gradient-primary">{{\App\Models\Pedido::PEDIDO_ENVIO_GRATIS_TEXT}}</span>@endif
            </td>
            <td>
                @if($pedido->forma_pago == \App\Models\Pedido::PEDIDO_PAGO_EFECTIVO)<span class="badge badge-gradient-success">{{\App\Models\Pedido::PEDIDO_PAGO_EFECTIVO_TEXT}}</span>@endif
                @if($pedido->forma_pago == \App\Models\Pedido::PEDIDO_PAGO_MP)<span class="badge badge-gradient-success">{{\App\Models\Pedido::PEDIDO_PAGO_MP_TEXT}}</span>@endif
                @if($pedido->forma_pago == \App\Models\Pedido::PEDIDO_PAGO_TERMINAL && $pedido->envio != \App\Models\Pedido::PEDIDO_ENVIO_PICKUP)
                        <span class="badge badge-gradient-success">{{\App\Models\Pedido::PEDIDO_PAGO_TERMINAL_TEXT_DOMICILIO}}</span>
                    @endif
                @if($pedido->forma_pago == \App\Models\Pedido::PEDIDO_PAGO_TERMINAL && $pedido->envio == \App\Models\Pedido::PEDIDO_ENVIO_PICKUP)
                        <span class="badge badge-gradient-success">{{\App\Models\Pedido::PEDIDO_PAGO_TERMINAL_TEXT_PICKUP}}</span>
                    @endif
            </td>
            <td><span class="badge badge-gradient-dark">{{date('d / m / Y',strtotime($pedido->created_at))}}</span></td>
            <td><span class="badge badge-gradient-dark">{{date('H:i',strtotime($pedido->created_at))}} Hrs</span></td>
            <td>${{$pedido->total}}</td>
            <td>${{$pedido->costo_envio ? $pedido->costo_envio : '0.00'}}</td>
            <td>${{$pedido->gran_total}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<form method="POST" id="frmCancelar">
    @method('POST')
    @csrf
    <input id="hdnRazon" name="razon" type="hidden" value=""/>
</form>
<script type="text/javascript">
    $(document).ready(function(){

        $(".btnCopiarDireccion").click(function(){
            var $temp = $("<input>");
            $("body").append($temp);
            var info_copiar = "Mapa: " + $(this).prev().attr('href') + '. Nombre: ' + $(this).parents('tr').find('.nombre').html() + '. Telefono: ' + $(this).parents('tr').find('.telefono a').html() + '. Direccion: ' + $(this).parents('tr').find('.direccion').html();
            $temp.val( info_copiar ).select();
            document.execCommand("copy");
            $temp.remove();

            toastr.remove();
            toastr.options.positionClass = "toast-bottom-right";
            toastr.success('Se ha copiado la información en el portapapeles');
            return false;
        });



        $("#btnCancelarPedido").click(function(){
            var button = $(this);
            var input = $('<label><small>¿Cuál es la razón de la cancelación de este pedido?<br>(Esta razón será compartida con el usuario)</small><br/><br/><textarea id="txtRazon"></textarea></label>');
            swal({
                title: "¡Cancelar Pedido!",
                icon: "warning",
                html: input.html(),
            }).then((value) => {
                var frmCancelar = $('#frmCancelar');
                frmCancelar.attr('action', button.attr('href'));
                $('#hdnRazon').val($('#txtRazon').val());
                frmCancelar.submit();
            });
            return false;
        });


    });
</script>
