<div class="col-xl-3 col-lg-6 col-md-6">
    <div class="ms-card card-gradient-secondary ms-widget ms-infographics-widget" style="height: 90%;">
        <div class="ms-card-img">
            <img src="../../assets/img/analytics.jpeg" alt="card_img">
        </div>
        <div class="ms-card-body media">
            <div class="media-body">
                <p class="right" style="float: right">
                    @if(isset($resumen['porcentaje']))
                        @if($resumen['porcentaje'] >= 0)
                            <span class="badge badge-success"><i class="material-icons">arrow_upward</i>
                                        {{number_format($resumen['porcentaje'], 2, '.', ',')}}%
                                    </span>
                        @else
                            <span class="badge badge-danger"><i class="material-icons">arrow_downward</i>
                                        {{number_format($resumen['porcentaje']*-1, 2, '.', ',')}}%
                                    </span>
                        @endif
                    @else

                    @endif
                </p>
                <h6>{{$titulo}}@if(isset($disclaimer)){{$disclaimer}}@endif</h6>
                @if($tipo == 'pedidos')
                    <p class="ms-card-change">
                        {{$resumen['actual']}} <small>pedidos</small><br/>
                    </p>
                    <p class="mes_pasado">
                        ({{$resumen['pasado']}} <small>pedidos</small> el mes pasado)
                    </p>
                @endif

                @if($tipo == 'dinero')
                    <p class="ms-card-change">
                        $ {{number_format($resumen['actual'], 2, '.', ',')}} <small>MXN </small>@if(isset($iva) && $iva == 'incluido') <small> (IVA incluido) </small>@endif
                    </p>
                    <p class="mes_pasado">
                        ($ {{number_format($resumen['pasado'], 2, '.', ',')}} <small>MXN</small> el mes pasado)
                    </p>
                @endif

                @if($tipo == 'porcentaje')
                    <p class="ms-card-change">
                        {{number_format($resumen['actual'], 2, '.', ',')}} <small>%</small><br/>
                    </p>
                    <p class="mes_pasado">
                        ({{number_format($resumen['pasado'], 2, '.', ',')}} <small>%</small> el mes pasado)
                    </p>
                @endif
            </div>
        </div>

    </div>
</div>
