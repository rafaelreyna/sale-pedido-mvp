@extends('layouts.app')

@section('content')
    <div _ngcontent-vbt-c44="" class="row">
        <div _ngcontent-vbt-c44="" class="col-md-12">
            <nav _ngcontent-vbt-c44="" aria-label="breadcrumb">
                <ol _ngcontent-vbt-c44="" class="breadcrumb pl-0">
                    <li _ngcontent-vbt-c44="" class="breadcrumb-item"><a _ngcontent-vbt-c44="" href="{{route('admin')}}"><i
                                _ngcontent-vbt-c44="" class="material-icons">home</i> Inicio</a></li>
                    <li _ngcontent-vbt-c44="" aria-current="page" class="breadcrumb-item active">Regiones</li>
                </ol>
            </nav>
        </div>
        <div _ngcontent-vbt-c51="" class="ms-panel col-md-12">
            <div _ngcontent-vbt-c51="" class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 _ngcontent-vbt-c51="">Regiones</h6>
                    </div>
                    <a href="{{route('superadmin.regiones.create')}}" class="btn text-white btn-info" ><i class="material-icons">add</i> Nueva Región</a>
                </div>
            </div>
            <div _ngcontent-vbt-c51="" class="ms-panel-body">
            @if($regiones->count() > 0)
                <div class="table-responsive">
                    <table id="example" class="table table-sm table-datatable table-striped thead-primary w-100 dataTable no-footer" style="width:100%">
                    <thead >
                        <tr>
                            <th>Nombre</th>
                            <th>Codigo</th>
                            <th>Tipo</th>
                            <th>Observaciones</th>
                            <th style="width: 160px;">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($regiones as $region)
                            <tr>
                                <td>{{$region->nombre}}</td>
                                <td>{{$region->codigo}}</td>
                                <td>
                                    <span class="badge badge-light">{{$region->tipo}}</span>
                                </td>
                                <td>{{$region->observaciones}}</td>
                                <td>
                                    <a href="{{route('superadmin.regiones.edit', $region->id)}}" class="btn  text-white btn-info"><i class="flaticon-pencil"></i> Editar</a>
                                    &nbsp;
                                    <a class="btn btnEliminar text-white btn-danger" data-delete-id="{{$region->id}}"><i class="flaticon-trash"></i> Borrar</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            @endif
                @if($regiones->count() <= 0)
                    <p class="alert alert-info"><i class="flaticon-information"></i> Aún no se han dado de alta ninguna región</p>
                @endif
            </div>
        </div>
    </div>
    <div class="delete_form">
        <form id="deleteForm" method="POST" action="" >
            {{ csrf_field() }}
            {{method_field('delete')}}
        </form>
    </div>
    <script type="application/javascript">
        $(document).ready(function() {
            $('.table-datatable').DataTable({"ordering": false, "pageLength": 25, "bLengthChange": false,
                "language": {
                    "url": "{{URL::to('/assets/lang/dataTable/Spanish.json')}}"
                }});

            $('.btnEliminar').click(function(){
                const borrarId = $(this).data('deleteId');
                Swal.fire({
                    title: '¿Estás seguro que deseas borrar este elemento?',
                    text: "No podrás revertir esta acción",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#666',
                    confirmButtonText: 'Sí, quiero eliminarlo'
                }).then(function (result) {
                    if (result.value) {
                        $('#deleteForm').attr('action', '{{route('superadmin.regiones.index')}}' + '/' + borrarId);
                        $('#deleteForm').submit();
                    }
                });
            });
        } );
    </script>
@endsection
