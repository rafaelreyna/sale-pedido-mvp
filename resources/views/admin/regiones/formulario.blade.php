@extends('layouts.app')

@section('content')
    <div  class="row">
        <div  class="col-md-12">
            <nav  aria-label="breadcrumb">
                <ol  class="breadcrumb pl-0">
                    <li  class="breadcrumb-item">
                        <a  href="{{route('admin')}}"><i class="material-icons">home</i> Inicio</a>
                    </li>
                    <li  class="breadcrumb-item">
                        <a  href="{{route('superadmin.regiones.index')}}">Regiones</a>
                    </li>
                    <li  aria-current="page" class="breadcrumb-item active">{{$region->id?"Editar":"Agregar"}}</li>
                </ol>
            </nav>
        </div>
        <div  class="ms-panel col-md-8">
            <div  class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 >{{$titulo}}</h6>
                    </div>
                    <a href="{{route('superadmin.regiones.index')}}"><i class="material-icons">arrow_back</i> Regresar</a>
                </div>
            </div>
            <div  class="ms-panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin: 0px;">
                            @foreach ($errors->all() as $error)
                                <li><i class="flaticon-alert"></i> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{$route}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{$region->id?method_field('put'):method_field('post')}}
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control @error('nombre') is-invalid @enderror"
                               id="nombre" name="nombre" placeholder="Nombre de la región"
                               value="{{old('nombre')?old('nombre'):$region->nombre}}">
                    </div>
                    <div class="form-group">
                        <label for="codigo">Código</label>
                        <input type="text" maxlength="50" class="form-control @error('codigo') is-invalid @enderror"
                               id="codigo" name="codigo" placeholder="Código de la región"
                               value="{{old('codigo')?old('codigo'):$region->codigo}}">
                    </div>
                    <div class="form-group">
                        <label for="tipo">Tipo</label>
                        <select class="form-control @error('tipo') is-invalid @enderror"
                               id="tipo" name="tipo">
                            <option value="">SELECCIONA UNA OPCIÓN</option>
                            <option @if($region->tipo == 'urbana' || old('tipo') == 'urbana') selected="selected" @endif
                            value="urbana">Urbana</option>
                            <option @if($region->tipo == 'pueblo_magico' || old('tipo') == 'pueblo_magico') selected="selected" @endif
                            value="pueblo_magico">Pueblo Mágico</option>
                        </select>
                    </div>

                    <!-- ubicacion y alcance -->
                    <h5>Ubicación</h5>
                    <p class="alert alert-info"><i class="fa fa-info-circle"></i> Puedes mover  el círculo para ajustar la ubicación de la región o dar click en el mapa </p>
                    <div class="form-group">
                        <label for="radio_max">Radio de la región (Km)</label>
                        <input type="number" step="0.10" class="form-control @error('radio_max') is-invalid @enderror"
                               id="radio_max" name="radio_max" placeholder="0.00"
                               min="0" value="{{old('radio_max')?old('radio_max'):$region->radio_max}}">
                    </div>
                    <br/>

                    <div id="map" style="height: 400px;" data-zoom="11"></div>
                    <div class="row form-group">
                        <div class="col-xs-12 col-md-12">
                            <input type="text" autocomplete="disabled" class="form-control" id="pac-input" placeholder="Buscar en el mapa"></input>
                        </div>
                    </div>
                    <input type="hidden" id="centro_lat" name="centro_lat" value="{{old('centro_lat')?old('centro_lat'):$region->centro_lat}}">
                    <input type="hidden" id="centro_lon" name="centro_lon" value="{{old('centro_lon')?old('centro_lon'):$region->centro_lon}}">
                    <br/>
                    <div class="form-group">
                        <label for="radio_max_negocios">Radio máximo de los restaurantes (Km)</label>
                        <input type="number" step="0.10" class="form-control @error('radio_max_negocios') is-invalid @enderror"
                               id="radio_max_negocios" name="radio_max_negocios" placeholder="0.00"
                               min="0" value="{{old('radio_max_negocios')?old('radio_max_negocios'):$region->radio_max_negocios}}">
                    </div>


                    <div class="form-group">
                        <label for="observaciones">Observaciones</label>
                        <textarea class="form-control text-counter @error('observaciones') is-invalid @enderror"
                            id="observaciones" name="observaciones" rows="3" placeholder="Observaciones" maxlength="250"
                            >{{old('observaciones')?old('observaciones'):$region->observaciones}}</textarea>
                    </div>

                    <input type="submit" class="btn btn-primary" value="{{$callToAction}}">
                </form>
            </div>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY', 'AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY')}}&libraries=places"></script>
    <script type="application/javascript">
        $(document).ready(function(){

            var lastPosition = new google.maps.LatLng(
                {{$region->centro_lat?$region->centro_lat:'20.6122138'}},
                {{$region->centro_lon?$region->centro_lon:'-100.4452366'}}
            );
            map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: {{$region->centro_lat?$region->centro_lat:'20.6122138'}},
                    lng: {{$region->centro_lon?$region->centro_lon:'-100.4452366'}},
                },
                zoom: 9,
                scrollwheel: true,
                heigth: 250
            });

            marker = new google.maps.Marker({
                position: {
                    lat: {{$region->centro_lat?$region->centro_lat:'20.6122138'}},
                    lng: {{$region->centro_lon?$region->centro_lon:'-100.4452366'}}
                },
                map: map,
                draggable:false,
                title: '{{$region->nombre}}'
            });

            const regionAreaNegocio = new google.maps.Circle({
                strokeColor: "#f9423c",
                strokeOpacity: 0.7,
                strokeWeight: 1,
                fillColor: "#f9423c",
                fillOpacity: .2,
                draggable: true,
                map,
                center: {
                    lat: {{$region->centro_lat?$region->centro_lat:'20.6122138'}},
                    lng: {{$region->centro_lon?$region->centro_lon:'-100.4452366'}},
                },
                radius: {{$region->radio_max??0}} * 1000,
            });

            const input = document.getElementById("pac-input");
            const searchBox = new google.maps.places.SearchBox(input);
            searchBox.addListener("places_changed", () => {
                const places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                places.forEach((place) => {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    marker.setPosition(place.geometry.location);
                    map.setCenter(place.geometry.location);
                    regionAreaNegocio.setCenter(place.geometry.location);
                    document.getElementById("lat").value = place.geometry.location.lat();
                    document.getElementById("lon").value = place.geometry.location.lng();
                });
            });

            google.maps.event.addListener(regionAreaNegocio, 'dragend', function (event) {
                document.getElementById("centro_lat").value = regionAreaNegocio.getCenter().lat();
                document.getElementById("centro_lon").value = regionAreaNegocio.getCenter().lng();
                marker.setPosition(new google.maps.LatLng(
                    regionAreaNegocio.getCenter().lat(),
                    regionAreaNegocio.getCenter().lng(),
                ));
            });

            google.maps.event.addListener(map, 'click', function (event) {
                document.getElementById("centro_lat").value = event.latLng.lat();
                document.getElementById("centro_lon").value = event.latLng.lng()
                marker.setPosition(event.latLng);
                regionAreaNegocio.setCenter(event.latLng);
            });

            $('#radio_max').change(function(){
                regionAreaNegocio.setRadius($(this).val() * 1000);
            });

            $('#radio_max').keyup(function(){
                regionAreaNegocio.setRadius($(this).val() * 1000);
            });

        });
    </script>
@endsection
