@extends('layouts.app')

@section('content')
<!-- Recent Placed Orders< -->
<div class="col-12">
    <div class="ms-panel">
        <div class="ms-panel-header">
            <h6>Tutoriales</h6>
        </div>
        <div class="ms-panel-body">
            <div class="row">
                <div class="col-md-6 tutorial">
                    <h4>Alta de restaurante</h4>
                    <p>En este tutorial aprenderás cómo configurar una nueva cuenta de restaurante en UKI&reg;</p>
                    <hr/>
                    <iframe width="100%" height="400" src="https://www.youtube.com/embed/-78WLaqx9Bs"
                            title="YouTube video player" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                </div>

                <div class="col-md-6 tutorial">
                    <h4>Configurar Mercado Pago</h4>
                    <p>En este tutorial aprenderás cómo configurar tus credenciales de Mercado Pago para aceptar pago en línea con tarjeta de crédito/débito</p>
                    <hr/>
                    <iframe width="100%" height="400" src="https://www.youtube.com/embed/p3_StXKqS5c"
                            title="YouTube video player" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                </div>


                <div class="col-md-6 tutorial">
                    <h4>Tutorial Fotografía de Producto</h4>
                    <p>En este tutorial aprrenderás a tomar las fotografías de tus platillos como todo un experto.</p>
                    <hr/>
                    <iframe width="100%" height="400" src="https://www.youtube.com/embed/Xs7-tAGlRxA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
