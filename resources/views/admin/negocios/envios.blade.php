@extends('layouts.app')

@section('content')
    <div  class="row">
        <div  class="col-md-12">
            <nav  aria-label="breadcrumb">
                <ol  class="breadcrumb pl-0">
                    <li  class="breadcrumb-item">
                        <a  href="{{route('admin')}}"><i class="material-icons">home</i> Inicio</a>
                    </li>
                    <li  class="breadcrumb-item"><a  href="#">Configuracion</a></li>
                    <li  aria-current="page" class="breadcrumb-item active">Completar Perfil</li>
                </ol>
            </nav>
        </div>
        <div  class="ms-panel col-md-8">
            <div  class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 >{{$titulo}}</h6>
                    </div>
                </div>
            </div>
            <div  class="ms-panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin: 0px;">
                            @foreach ($errors->all() as $error)
                                <li><i class="flaticon-alert"></i> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{$route}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{$negocio->id?method_field('put'):method_field('post')}}
                    <h5>Pick up & Go (Recoge en tienda)</h5>
                    <div class="form-group">
                        <label for="pickup">¿Deseas ofrecer opción de recoger en tienda?</label>
                        <select type="text" class="form-control" id="pickup" name="pickup">
                            <option @if (!$negocio->enviosconfig->pickup) selected @endif value="0"> No </option>
                            <option @if ($negocio->enviosconfig->pickup) selected @endif value="1"> Si </option>
                        </select>
                    </div>

                    <br/>

                    <br/>
                    <h5>Método de envío</h5>
                    <div class="form-group">
                        <label for="tipo_envio">¿Qué método de envío deseas usar?</label>
                        <select type="text" class="form-control" id="tipo_envio" name="tipo_envio">
                            <option @if (!$negocio->enviosconfig->tipo_envio) selected @endif value="null"> No deseo enviar a domicilio</option>
                            <option @if ($negocio->enviosconfig->tipo_envio == \App\Models\Pedido::PEDIDO_ENVIO_FIJO) selected @endif value="fijo"> Costo de Envío fijo </option>
                            <option @if ($negocio->enviosconfig->tipo_envio == \App\Models\Pedido::PEDIDO_ENVIO_DINAMICO) selected @endif value="dinamico"> Costo de envío  dinámico por km </option>
                        </select>
                    </div>

                    <div id="tipo_envio_options" @if (!$negocio->enviosconfig->tipo_envio) style="display:none" @endif>

                        <div id="envio_tipo_fijo_options" @if ($negocio->enviosconfig->tipo_envio != \App\Models\Pedido::PEDIDO_ENVIO_FIJO) style="display:none" @endif>
                            <label for="costo_envio_fijo">Costo de envío fijo</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">$</div>
                                </div>
                                <input type="number" min="0" step="0.01" class="form-control @error('costo_envio_fijo') is-invalid @enderror"
                                       id="costo_envio_fijo" name="costo_envio_fijo" placeholder="0.00"
                                       value="{{old('costo_envio_fijo')?old('costo_envio_fijo'):$negocio->enviosconfig->costo_envio_fijo}}">
                            </div>
                        </div>

                        <div class="row" id="envio_tipo_dinamico_options" @if ($negocio->enviosconfig->tipo_envio != \App\Models\Pedido::PEDIDO_ENVIO_DINAMICO) style="display:none" @endif>
                            <p class="alert alert-info"><i class="fa fa-info-circle"></i> El método de envío dinámico se integra por dos variables, un costo inicial "banderazo", por los primeros "X" km y un costo adicional por cada km extra. </p>
                            <div class="form-group col col-12">
                                <label for="costo_envio_km_base">Cantidad de Km del "banderazo de salida"</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Km</div>
                                    </div>
                                    <input type="number" min="0" step="1" class="form-control @error('km_base') is-invalid @enderror"
                                           id="km_base" name="km_base" placeholder="5"
                                           value="{{old('km_base')?old('km_base'):$negocio->enviosconfig->km_base}}">
                                </div>
                            </div>

                            <div class="form-group col col-6">
                                <label for="costo_envio_km_base">Costo del "banderazo de salida"</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">$</div>
                                    </div>
                                    <input type="number" min="0" step="0.01" class="form-control @error('costo_envio_km_base') is-invalid @enderror"
                                           id="costo_envio_km_base" name="costo_envio_km_base" placeholder="0.00"
                                           value="{{old('costo_envio_km_base')?old('costo_envio_km_base'):$negocio->enviosconfig->costo_envio_km_base}}">
                                </div>
                            </div>

                            <div class="form-group col col-6">
                                <label for="costo_envio_km">Costo por km extra</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">$</div>
                                    </div>
                                    <input type="number" min="0" step="0.01" class="form-control @error('costo_envio_km') is-invalid @enderror"
                                           id="costo_envio_km" name="costo_envio_km" placeholder="0.00"
                                           value="{{old('costo_envio_km')?old('costo_envio_km'):$negocio->enviosconfig->costo_envio_km}}">
                                </div>
                            </div>
                        </div>

                        <br/>
                        <h5>Distancia de envío</h5>
                        <div class="form-group">
                            <label for="radio_max">Distancia máxima de entrega a domicilio (Máximo {{$negocio->region->radio_max_negocios}} Km de radio)</label>
                            <input type="number" step="0.10" class="form-control @error('radio_max') is-invalid @enderror"
                                   id="radio_max" name="radio_max" placeholder="0.00"
                                   min="0" max="{{$negocio->region->radio_max_negocios}}"
                                   value="{{old('radio_max')?old('radio_max'):$negocio->enviosconfig->radio_max}}">
                        </div>
                        <br/>
                        <p class="alert alert-info"><i class="fa fa-info-circle"></i> Puedes mover  el círculo para ajustar el alcance de tu restaurante. </p>
                        <div id="map" style="height: 400px;" data-zoom="11"></div>

                        <br/>
                        <h5>Envío Gratis</h5>
                        <div class="form-group">
                            <label for="envio_gratis">¿Deseas ofrecer envíos gratis?</label>
                            <select type="text" class="form-control" id="envio_gratis" name="envio_gratis">
                                <option @if (!$negocio->enviosconfig->envio_gratis) selected @endif value="0"> No </option>
                                <option @if ($negocio->enviosconfig->envio_gratis) selected @endif value="1"> Si </option>
                            </select>
                        </div>

                        <div class="row" id="envio_gratis_option" @if (!$negocio->enviosconfig->envio_gratis) style="display:none;" @endif>
                            <div class="form-group col col-6">
                                <label for="envio_gratis_minimo_cantidad">Cantidad mínima para envío gratis</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">$</div>
                                    </div>
                                    <input type="number" min="0" step="0.01" class="form-control @error('envio_gratis_minimo_cantidad') is-invalid @enderror"
                                           id="envio_gratis_minimo_cantidad" name="envio_gratis_minimo_cantidad" placeholder="0.00"
                                           value="{{old('envio_gratis_minimo_cantidad')?old('envio_gratis_minimo_cantidad'):$negocio->enviosconfig->envio_gratis_minimo_cantidad}}">
                                </div>
                            </div>

                            <div class="form-group col col-6">
                                <label for="envio_gratis_maximo_km">Km Máximos para envío gratis</label>
                                <input type="number" min="0" class="form-control @error('envio_gratis_maximo_km') is-invalid @enderror"
                                       id="envio_gratis_maximo_km" name="envio_gratis_maximo_km" placeholder="10"
                                       value="{{old('envio_gratis_maximo_km')?old('envio_gratis_maximo_km'):$negocio->enviosconfig->envio_gratis_maximo_km}}">
                            </div>
                        </div>

                    </div>
                    <input type="hidden" id="centro_lat" name="centro_lat" value="{{old('centro_lat')?old('centro_lat'):$negocio->enviosconfig->centro_lat}}">
                    <input type="hidden" id="centro_lon" name="centro_lon" value="{{old('centro_lon')?old('centro_lon'):$negocio->enviosconfig->centro_lon}}">

                    <input type="submit" class="btn btn-primary" value="{{$callToAction}}">
                </form>
            </div>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY', 'AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY')}}&libraries=places"></script>
    <script type="application/javascript">
        $(document).ready(function(){

            $('#envio_gratis').change(function(){
                $('#envio_gratis_option').toggle(700);
            });

            $('#tipo_envio').change(function(){
                var val = $(this).val();
                if(val != 'null' && val != null) {
                    $('#tipo_envio_options').show(700);
                    if (val == '{{\App\Models\Pedido::PEDIDO_ENVIO_FIJO}}') {
                        $('#envio_tipo_dinamico_options').hide(0);
                        $('#envio_tipo_fijo_options').show(700);

                    }

                    if (val == '{{\App\Models\Pedido::PEDIDO_ENVIO_DINAMICO}}') {
                        $('#envio_tipo_fijo_options').hide(0);
                        $('#envio_tipo_dinamico_options').show(700);

                    }
                }
                else {
                    $('#tipo_envio_options').hide(700);
                }
            });
            var lastPosition = new google.maps.LatLng(
                {{$negocio->enviosconfig->centro_lat?$negocio->enviosconfig->centro_lat:$negocio->lat}},
                {{$negocio->enviosconfig->centro_lon?$negocio->enviosconfig->centro_lon:$negocio->lon}}
            );
            map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: {{$negocio->lat?$negocio->lat:'20.6122138'}},
                    lng: {{$negocio->lon?$negocio->lon:'-100.4452366'}},
                },
                zoom: 12,
                scrollwheel: true,
                heigth: 250
            });

            marker = new google.maps.Marker({
                position: {
                    lat: {{$negocio->lat?$negocio->lat:'20.6122138'}},
                    lng: {{$negocio->lon?$negocio->lon:'-100.4452366'}}
                },
                map: map,
                draggable:false,
                title: '{{$negocio->nombre}}'
            });

            const regionAreaNegocio = new google.maps.Circle({
                strokeColor: "#f9423c",
                strokeOpacity: 0.7,
                strokeWeight: 1,
                fillColor: "#f9423c",
                fillOpacity: .2,
                draggable: true,
                map,
                center: {
                    lat: {{$negocio->enviosconfig->centro_lat?$negocio->enviosconfig->centro_lat:$negocio->lat}},
                    lng: {{$negocio->enviosconfig->centro_lon?$negocio->enviosconfig->centro_lon:$negocio->lon}}
                },
                radius: {{$negocio->enviosconfig->radio_max}} * 1000,
            });

            google.maps.event.addListener(regionAreaNegocio, 'dragend', function (event) {
                document.getElementById("centro_lat").value = regionAreaNegocio.getCenter().lat();
                document.getElementById("centro_lon").value = regionAreaNegocio.getCenter().lng();
                if (regionAreaNegocio.getBounds().contains(marker.getPosition())) {
                    lastPosition = regionAreaNegocio.getCenter();
                } else {
                    regionAreaNegocio.setCenter(lastPosition)
                    toastr.remove();
                    toastr.options.positionClass = "toast-bottom-right";
                    toastr.error('Tu área de envío debe cubrir la posición de tu restaurante', 'Posición Incorrecta');
                }
            });

            $('#radio_max').change(function(){
                regionAreaNegocio.setRadius($(this).val() * 1000);
            });

            $('#radio_max').keyup(function(){
                regionAreaNegocio.setRadius($(this).val() * 1000);
            });

        });
    </script>
@endsection
