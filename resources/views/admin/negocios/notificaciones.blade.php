@extends('layouts.app')

@section('content')
    <div  class="row">
        <div  class="col-md-12">
            <nav  aria-label="breadcrumb">
                <ol  class="breadcrumb pl-0">
                    <li  class="breadcrumb-item">
                        <a  href="{{route('admin')}}"><i class="material-icons">home</i> Inicio</a>
                    </li>
                    <li  class="breadcrumb-item"><a  href="#">Configuracion</a></li>
                    <li  aria-current="page" class="breadcrumb-item active">Notificaciones</li>
                </ol>
            </nav>
        </div>
        <div  class="ms-panel col-md-8">
            <div  class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 >{{$titulo}}</h6>
                    </div>
                </div>
            </div>
            <div  class="ms-panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin: 0px;">
                            @foreach ($errors->all() as $error)
                                <li><i class="flaticon-alert"></i> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{$route}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{$negocio->id?method_field('put'):method_field('post')}}
                    <h5>Notificaciones de pedidos nuevos</h5>
                    <div class="form-group">
                        <label for="pickup">Notificación vía SMS</label>
                        <select type="text" class="form-control" id="pickup" name="notificacion_pedido_sms">
                            <option @if (!$negocio->config->notificacion_pedido_sms) selected @endif value="null"> Ninguno </option>
                            @foreach($usuarios as $usuario)
                                <option @if (in_array($usuario->id, $negocio->config->notificacion_pedido_sms??[])) selected @endif value="{{$usuario->id}}">({{$usuario->tipo}}) {{$usuario->name}} - {{$usuario->celular}}</option>
                            @endforeach
                        </select>
                    </div>
                    <br/>
                    <div class="form-group">
                        <label for="pickup">Notificación vía Llamada</label>
                        <select type="text" class="form-control" id="pickup" name="notificacion_pedido_call">
                            <option @if (!$negocio->config->notificacion_pedido_mail) selected @endif value="null"> Ninguno </option>
                            @foreach($usuarios as $usuario)
                                <option @if (in_array($usuario->id, $negocio->config->notificacion_pedido_call??[])) selected @endif value="{{$usuario->id}}">({{$usuario->tipo}}) {{$usuario->name}} - {{$usuario->celular}}</option>
                            @endforeach
                        </select>
                    </div>
                    <br/>
                    <div class="form-group">
                        <label>Notificación vía Email</label>
                        @foreach($usuarios as $usuario)
                            <div class="form-check">
                                <input name="notificacion_pedido_mail[]" class="form-check-input" type="checkbox" value="{{$usuario->id}}" id="flexCheckDefault" @if(in_array($usuario->id, $negocio->config->notificacion_pedido_mail??[])) checked @endif>
                                <label class="form-check-label" for="flexCheckDefault">
                                    ({{$usuario->tipo}}) {{$usuario->name}} - {{$usuario->email}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <input type="submit" class="btn btn-primary" value="{{$callToAction}}">
                </form>
            </div>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY', 'AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY')}}&libraries=places"></script>
    <script type="application/javascript">
        $(document).ready(function(){

            $('#envio_gratis').change(function(){
                $('#envio_gratis_option').toggle(700);
            });

            $('#tipo_envio').change(function(){
                var val = $(this).val();
                if(val != 'null' && val != null) {
                    $('#tipo_envio_options').show(700);
                    if (val == '{{\App\Models\Pedido::PEDIDO_ENVIO_FIJO}}') {
                        $('#envio_tipo_dinamico_options').hide(0);
                        $('#envio_tipo_fijo_options').show(700);

                    }

                    if (val == '{{\App\Models\Pedido::PEDIDO_ENVIO_DINAMICO}}') {
                        $('#envio_tipo_fijo_options').hide(0);
                        $('#envio_tipo_dinamico_options').show(700);

                    }
                }
                else {
                    $('#tipo_envio_options').hide(700);
                }
            });
            var lastPosition = new google.maps.LatLng(
                {{$negocio->enviosconfig->centro_lat?$negocio->enviosconfig->centro_lat:$negocio->lat}},
                {{$negocio->enviosconfig->centro_lon?$negocio->enviosconfig->centro_lon:$negocio->lon}}
            );
            map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: {{$negocio->lat?$negocio->lat:'20.6122138'}},
                    lng: {{$negocio->lon?$negocio->lon:'-100.4452366'}},
                },
                zoom: 12,
                scrollwheel: true,
                heigth: 250
            });

            marker = new google.maps.Marker({
                position: {
                    lat: {{$negocio->lat?$negocio->lat:'20.6122138'}},
                    lng: {{$negocio->lon?$negocio->lon:'-100.4452366'}}
                },
                map: map,
                draggable:false,
                title: '{{$negocio->nombre}}'
            });

            const regionAreaNegocio = new google.maps.Circle({
                strokeColor: "#f9423c",
                strokeOpacity: 0.7,
                strokeWeight: 1,
                fillColor: "#f9423c",
                fillOpacity: .2,
                draggable: true,
                map,
                center: {
                    lat: {{$negocio->enviosconfig->centro_lat?$negocio->enviosconfig->centro_lat:$negocio->lat}},
                    lng: {{$negocio->enviosconfig->centro_lon?$negocio->enviosconfig->centro_lon:$negocio->lon}}
                },
                radius: {{$negocio->enviosconfig->radio_max}} * 1000,
            });

            google.maps.event.addListener(regionAreaNegocio, 'dragend', function (event) {
                document.getElementById("centro_lat").value = regionAreaNegocio.getCenter().lat();
                document.getElementById("centro_lon").value = regionAreaNegocio.getCenter().lng();
                if (regionAreaNegocio.getBounds().contains(marker.getPosition())) {
                    lastPosition = regionAreaNegocio.getCenter();
                } else {
                    regionAreaNegocio.setCenter(lastPosition)
                    toastr.remove();
                    toastr.options.positionClass = "toast-bottom-right";
                    toastr.error('Tu área de envío debe cubrir la posición de tu restaurante', 'Posición Incorrecta');
                }
            });

            $('#radio_max').change(function(){
                regionAreaNegocio.setRadius($(this).val() * 1000);
            });

            $('#radio_max').keyup(function(){
                regionAreaNegocio.setRadius($(this).val() * 1000);
            });

        });
    </script>
@endsection
