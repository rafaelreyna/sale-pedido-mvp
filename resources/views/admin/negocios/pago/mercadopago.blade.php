@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb pl-0">
                    <li class="breadcrumb-item">
                        <a href="{{route('admin')}}"><i class="material-icons">home</i> Inicio</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Configuracion</a></li>
                    <li aria-current="page" class="breadcrumb-item active">Información de pago</li>
                </ol>
            </nav>
        </div>
        @if (!$negocio->tiene_metodo_pago())
            <p class="alert alert-danger">
                <i class="fa fa-info"></i> Es necesario configurar al menos un método de pago para poder vender
            </p>
        @endif
        <div class="ms-panel col-md-8">
            <div class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6>{{$titulo}}</h6>
                        <p class="alert alert-info"><i class="fa fa-info-circle"></i> Si tienes duda de cómo generar tus credenciales  de  tu cuenta Mercado Pago,  consulta nuestros “tutoriales”.</p>
                    </div>
                </div>
            </div>
            <div class="ms-panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin: 0px;">
                            @foreach ($errors->all() as $error)
                                <li><i class="flaticon-alert"></i> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{$route}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{$negocio->id?method_field('put'):method_field('post')}}
                    <h5>Información de pagos ONLINE (Mercado Pago)</h5>
                    <div class="form-group">
                        <label for="envio_gratis">¿Deseas ofrecer pago ONLINE mediante Mercado Pago?</label>
                        <select type="text" class="form-control" id="cboMP">
                            <option @if (!$negocio->config_mercadopago->public_key) selected @endif value="0"> No
                            </option>
                            <option @if ($negocio->config_mercadopago->public_key) selected @endif value="1"> Si
                            </option>
                        </select>
                    </div>
                    <br/>
                    <div id="divmercadoPagoConfig"
                         @if (!$negocio->config_mercadopago->public_key) style="display:none" @endif>
                        <div class="form-group">
                            <label for="public_key">Llave pública (public key)</label>
                            <input type="text" class="form-control @error('public_key') is-invalid @enderror"
                                   id="public_key" name="public_key" placeholder="Llave pública"
                                   value="{{old('public_key')?old('public_key'):$negocio->config_mercadopago->public_key}}">
                        </div>

                        <div class="form-group">
                            <label for="access_token">Token de acceso (access token)</label>
                            <input type="text" class="form-control @error('access_token') is-invalid @enderror"
                                   id="access_token" name="access_token" placeholder="Token de acceso"
                                   value="{{old('access_token')?old('access_token'):$negocio->config_mercadopago->access_token}}">
                        </div>
                    </div>
                    <input type="hidden" name="config_relation" value="config_mercadopago"/>
                    <input type="submit" class="btn btn-primary" value="{{$callToAction}}">
                </form>
            </div>
        </div>
        <div class="ms-panel col-md-8">
            <div class="ms-panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin: 0px;">
                            @foreach ($errors->all() as $error)
                                <li><i class="flaticon-alert"></i> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{$route}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{$negocio->id?method_field('put'):method_field('post')}}
                    <h5>Información de pagos offline</h5>
                    <input type="hidden" name="config_relation" value="config_offline"/>
                    <div class="form-group">
                        <label for="envio_gratis">¿Deseas ofrecer pago en efectivo en envíos a domicilio?</label>
                        <p class="alert alert-info"><i class="fa fa-info-circle"></i>Si el servicio de entregas lo
                            realiza un tercero, es importante que se establezca el proceso de cobro y pago entre el
                            restaurante y la empresa de reparto</p>
                        <select type="text" class="form-control" id="efectivo_domicilio" name="efectivo_domicilio">
                            <option @if (!$negocio->config_offline->efectivo_domicilio) selected @endif value="0"> No
                            </option>
                            <option @if ($negocio->config_offline->efectivo_domicilio) selected @endif value="1"> Si
                            </option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="envio_gratis">¿Deseas ofrecer pago en efectivo en pedidos Pick up & Go?</label>
                        <select type="text" class="form-control" id="efectivo" name="efectivo">
                            <option @if (!$negocio->config_offline->efectivo) selected @endif value="0"> No</option>
                            <option @if ($negocio->config_offline->efectivo) selected @endif value="1"> Si</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="envio_gratis">¿Deseas ofrecer pago con envío de terminal a domicilio?</label>
                        <select type="text" class="form-control" id="envio_terminal" name="envio_terminal">
                            <option @if (!$negocio->config_offline->envio_terminal) selected @endif value="0"> No
                            </option>
                            <option @if ($negocio->config_offline->envio_terminal) selected @endif value="1"> Si
                            </option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="envio_gratis">¿Deseas ofrecer pago con terminal en pedidos Pick up & Go?</label>
                        <select type="text" class="form-control" id="terminal" name="terminal">
                            <option @if (!$negocio->config_offline->terminal) selected @endif value="0"> No</option>
                            <option @if ($negocio->config_offline->terminal) selected @endif value="1"> Si</option>
                        </select>
                    </div>
                    <input type="submit" class="btn btn-primary" value="{{$callToAction}}">
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#cboMP').change(function () {
                $('#divmercadoPagoConfig').toggle(800);
                if ($(this).val() != 1) {
                    $('#access_token').val('');
                    $('#public_key').val('');
                }
            });
        });
    </script>
@endsection
