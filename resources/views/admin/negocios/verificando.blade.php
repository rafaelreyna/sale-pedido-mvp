@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <p style="margin: 0px;">Hola, {{\Illuminate\Support\Facades\Auth::user()->name}}</p>
            <h1 class="db-header-title">{{$negocio->nombre}}</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if(
                $negocio->constitucion_identificacion_oficial &&
                count($negocio->categorias) &&
                count($negocio->horarios) &&
                $negocio->enviosconfig &&
                $negocio->config_mercadopago &&
                count($negocio->platillo_categorias) &&
                count($negocio->platillos)
            )
                <p class="alert alert-success">
                    <i class="fa fa-info-circle"></i>
                    <big><strong>IMPORTANTE:</strong> TU NEGOCIO ESTÁ EN PROCESO DE VERIFICACIÓN</big><br/>
                    Estás a punto de comenzar a vender tus productos por medio de UKI&reg;, en este momento estamos verificando todos los datos de tu negocio y a la brevedad recibirás una notificación de parte del equipo de UKI:
                </p>
            @else
                <p class="alert alert-info">
                    <i class="fa fa-info-circle"></i>
                    <big><strong>IMPORTANTE:</strong> TU NEGOCIO AÚN NO ESTÁ EN LÍNEA</big><br/>
                    Para iniciar el proceso de verificación de tu negocio, debes completar los siguientes pasos:
                </p>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6">
            <div class="ms-card @if(!$negocio->constitucion_identificacion_oficial)card-gradient-dark @else card-success @endif ms-widget ms-infographics-widget">
                <div class="ms-card-body media">
                    <div class="media-body">
                        <h6>COMPLETAR INFORMACIÓN DEL NEGOCIO</h6>
                        <p  style="width: 75%;">Completa la información de tu negocio como el logotipo, la ubicación y la información legal</p><br/>
                        @if(!$negocio->constitucion_identificacion_oficial)
                        <p class="fs-12">
                            <a class="btn btn-gradient-light text-white"
                               href="{{route('admin.negocios.edit', $negocio->id)}}">Configurar ahora</a>
                        </p>
                        @endif
                    </div>
                </div>
                <i class="@if(!$negocio->constitucion_identificacion_oficial) flaticon-alert @else flaticon-star @endif"></i>
            </div>
        </div>

        <div class="col-xl-6 col-lg-6 col-md-6">
            <div class="ms-card @if(!count($negocio->categorias))card-gradient-dark @else card-success @endif ms-widget ms-infographics-widget">
                <div class="ms-card-body media">
                    <div class="media-body">
                        <h6>CONFIGURAR CATEGORÍAS</h6>
                        <p  style="width: 75%;">Selecciona hasta 2 categorías de tu negocio, esto permitirá a los usuarios encontrarte de una manera más sencilla</p><br/>
                        @if(!count($negocio->categorias))
                            <p class="fs-12">
                                <a class="btn btn-gradient-light text-white"
                                   href="{{route('admin.restaurantes.categorias.edit', $negocio->id)}}">Configurar ahora</a>
                            </p>
                        @endif
                    </div>
                </div>
                <i class="@if(!count($negocio->categorias)) flaticon-alert @else flaticon-star @endif"></i>
            </div>
        </div>

        <div class="col-xl-6 col-lg-6 col-md-6">
            <div class="ms-card @if(!count($negocio->horarios))card-gradient-dark @else card-success @endif ms-widget ms-infographics-widget">
                <div class="ms-card-body media">
                    <div class="media-body">
                        <h6>CONFIGURAR HORARIOS</h6>
                        <p  style="width: 75%;">Selecciona los días y horarios de apertura, también puedes configurar los días feriados del año en los que no queires recibir pedidos</p><br/>
                        @if(!count($negocio->horarios))
                            <p class="fs-12">
                                <a class="btn btn-gradient-light text-white"
                                   href="{{route('admin.horarios.index')}}">Configurar ahora</a>
                            </p>
                        @endif
                    </div>
                </div>
                <i class="@if(!count($negocio->horarios)) flaticon-alert @else flaticon-star @endif"></i>
            </div>
        </div>

        <div class="col-xl-6 col-lg-6 col-md-6">
            <div class="ms-card @if(!$negocio->enviosconfig)card-gradient-dark @else card-success @endif ms-widget ms-infographics-widget">
                <div class="ms-card-body media">
                    <div class="media-body">
                        <h6>CONFIGURAR ENVÍOS</h6>
                        <p  style="width: 75%;">Selecciona las formas de envío que quieres ofrecer a tus clientes y los costos correspondientes</p><br/>
                        @if(!$negocio->enviosconfig)
                            <p class="fs-12">
                                <a class="btn btn-gradient-light text-white"
                                   href="{{route('admin.negocios.envios.edit', $negocio->id)}}">Configurar ahora</a>
                            </p>
                        @endif
                    </div>
                </div>
                <i class="@if(!$negocio->enviosconfig) flaticon-alert @else flaticon-star @endif"></i>
            </div>
        </div>

        <div class="col-xl-6 col-lg-6 col-md-6">
            <div class="ms-card @if(!$negocio->config_mercadopago)card-gradient-dark @else card-success @endif ms-widget ms-infographics-widget">
                <div class="ms-card-body media">
                    <div class="media-body">
                        <h6>CONFIGURAR FORMAS DE PAGO</h6>
                        <p  style="width: 75%;">Configura los medios de pago que deseas aceptar</p><br/>
                        @if(!$negocio->config_mercadopago)
                            <p class="fs-12">
                                <a class="btn btn-gradient-light text-white"
                                   href="{{route('admin.negocios.pago.edit', $negocio->id)}}">Configurar ahora</a>
                            </p>
                        @endif
                    </div>
                </div>
                <i class="@if(!$negocio->config_mercadopago) flaticon-alert @else flaticon-star @endif"></i>
            </div>
        </div>


        <div class="col-xl-6 col-lg-6 col-md-6">
            <div class="ms-card @if(!count($negocio->platillo_categorias))card-gradient-dark @else card-success @endif ms-widget ms-infographics-widget">
                <div class="ms-card-body media">
                    <div class="media-body">
                        <h6>CREA UNA CATEGORÍA DE PLATILLOS</h6>
                        <p  style="width: 75%;">Crea tu primer categoría para organizar platillos: puede ser entradas, platos fuertes o postres por ejemplo</p><br/>
                        @if(!count($negocio->platillo_categorias))
                            <p class="fs-12">
                                <a class="btn btn-gradient-light text-white"
                                   href="{{route('admin.platillocategorias.create')}}">Configurar ahora</a>
                            </p>
                        @endif
                    </div>
                </div>
                <i class="@if(!count($negocio->platillo_categorias)) flaticon-alert @else flaticon-star @endif"></i>
            </div>
        </div>

        <div class="col-xl-6 col-lg-6 col-md-6">
            <div class="ms-card @if(!count($negocio->platillos))card-gradient-dark @else card-success @endif ms-widget ms-infographics-widget">
                <div class="ms-card-body media">
                    <div class="media-body">
                        <h6>SUBE TU PRIMER PLATILLO</h6>
                        <p  style="width: 75%;">Carga la información del primer platillo que deseas vender, sube una imagen, descríbelo y selecciona el precio final.</p><br/>
                        @if(!count($negocio->platillos))
                            <p class="fs-12">
                                <a class="btn btn-gradient-light text-white"
                                   href="{{route('admin.platillos.create')}}">Configurar ahora</a>
                            </p>
                        @endif
                    </div>
                </div>
                <i class="@if(!count($negocio->platillos)) flaticon-alert @else flaticon-star @endif"></i>
            </div>
        </div>
    </div>
@endsection
