@extends('layouts.app')

@section('content')
    <div  class="row">
        <div  class="col-md-12">
            <nav  aria-label="breadcrumb">
                <ol  class="breadcrumb pl-0">
                    <li  class="breadcrumb-item">
                        <a  href="{{route('admin')}}"><i class="material-icons">home</i> Inicio</a>
                    </li>
                    <li  class="breadcrumb-item"><a  href="#">Información de Restaurante</a></li>
                </ol>
            </nav>
        </div>

        <div  class="ms-panel col-md-12">
            <div  class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        @if($negocio->logo)
                            <img class="logo-negocio" src="{{route('images.public', [base64_encode('negocios/logos/'), $negocio->logo])}}"/>
                        @else
                            <img class="logo-negocio" src="{{URL::to('/assets/img')}}/sin_imagen.jpg"/>
                        @endif
                        <h2 style="display: inline">{{$negocio->nombre}}</h2>

                            <ul style="text-align: left; margin-left:120px; ">
                                <li style="display: inline"><strong>Categorías:</strong> </li>
                                @foreach($negocio->categorias as $categoria)
                                    <li style="display: inline">{{$categoria->nombre}}/ </li>
                                @endforeach
                            </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div  class="ms-panel">
                <div  class="ms-panel-header">
                    <div class="d-flex justify-content-between">
                        <div class="align-self-center align-left">
                            <h5 style="display: inline">Dirección</h5>
                        </div>
                    </div>
                </div>
                <div class="ms-panel-body">
                    <div class="form-group">
                        <label for="direccion_cp">Código Postal</label>
                        <input type="text" class="form-control @error('direccion_cp') is-invalid @enderror" disabled
                               id="direccion_cp" name="direccion_cp" placeholder="Código Postal"
                               value="{{old('direccion_cp')?old('direccion_cp'):$negocio->direccion_cp}}">
                    </div>

                    <div class="form-group">
                        <label for="direccion_calle">Calle</label>
                        <input type="text" class="form-control @error('direccion_calle') is-invalid @enderror" disabled
                               id="direccion_calle" name="direccion_calle" placeholder="Calle"
                               value="{{old('direccion_calle')?old('direccion_calle'):$negocio->direccion_calle}}">
                    </div>

                    <div class="row">
                        <div class="form-group col col-6">
                            <label for="direccion_numero_exterior">Número exterior</label>
                            <input type="text" class="form-control @error('direccion_numero_exterior') is-invalid @enderror" disabled
                                   id="direccion_numero_exterior" name="direccion_numero_exterior" placeholder="Numero Exterior"
                                   value="{{old('direccion_numero_exterior')?old('direccion_numero_exterior'):$negocio->direccion_numero_exterior}}">
                        </div>

                        <div class="form-group col col-6">
                            <label for="direccion_numero_interior">Número interior</label>
                            <input type="text" class="form-control @error('direccion_numero_interior') is-invalid @enderror" disabled
                                   id="direccion_numero_interior" name="direccion_numero_interior" placeholder="Numero Interior"
                                   value="{{old('direccion_numero_interior')?old('direccion_numero_interior'):$negocio->direccion_numero_interior}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col col-12">
                            <label for="direccion_colonia">Colonia</label>
                            <input type="text" class="form-control @error('direccion_colonia') is-invalid @enderror" disabled
                                   id="direccion_colonia" name="direccion_colonia" placeholder="Colonia"
                                   value="{{old('direccion_colonia')?old('direccion_colonia'):$negocio->direccion_colonia}}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col col-6">
                            <label for="direccion_municipio">Municipio</label>
                            <input type="text" class="form-control @error('direccion_municipio') is-invalid @enderror" disabled
                                   id="direccion_municipio" name="direccion_municipio" placeholder="Municipio"
                                   value="{{old('direccion_municipio')?old('direccion_municipio'):$negocio->direccion_municipio}}">
                        </div>

                        <div class="form-group col col-6">
                            <label for="direccion_estado">Estado</label>
                            <input type="text" class="form-control @error('direccion_estado') is-invalid @enderror" disabled
                                   id="direccion_estado" name="direccion_estado" placeholder="Estado"
                                   value="{{old('direccion_estado')?old('direccion_estado'):$negocio->direccion_estado}}">
                        </div>
                    </div>
                    <br/>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div  class="ms-panel">
                <div  class="ms-panel-header">
                    <div class="d-flex justify-content-between">
                        <div class="align-self-center align-left">
                            <h5 style="display: inline">Ubicación</h5>
                        </div>
                    </div>
                </div>
                <div class="ms-panel-body">
                    <div id="map" style="height: 400px;" data-zoom="13"></div>
                    <br/>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div  class="ms-panel">
                <div  class="ms-panel-header">
                    <div class="d-flex justify-content-between">
                        <div class="align-self-center align-left">
                            <h5 style="display: inline">Contacto</h5>
                        </div>
                    </div>
                </div>
                <div class="ms-panel-body">
                    <div class="form-group">
                        <label for="email">Correo electrónico</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" disabled
                               id="email" name="email" placeholder="Correo electrónico"
                               value="{{old('email')?old('email'):$negocio->email}}">
                    </div>

                    <div class="form-group">
                        <label for="telefono">Teléfono</label>
                        <input type="text" class="form-control @error('telefono') is-invalid @enderror" disabled
                               id="telefono" name="telefono" placeholder="Teléfono"
                               value="{{old('telefono')?old('telefono'):$negocio->telefono}}">
                    </div>

                    <div class="form-group">
                        <label for="sitio_web">Sitio Web</label>
                        <input type="text" class="form-control @error('sitio_web') is-invalid @enderror" disabled
                               id="sitio_web" name="sitio_web" placeholder="Sitio web"
                               value="{{old('sitio_web')?old('sitio_web'):$negocio->sitio_web}}">
                    </div>

                    <div class="form-group">
                        <label for="facebook">Facebook</label>
                        <input type="text" class="form-control @error('facebook') is-invalid @enderror" disabled
                               id="facebook" name="facebook" placeholder="https://facebook.com/tu_negocio"
                               value="{{old('facebook')?old('facebook'):$negocio->facebook}}">
                    </div>

                    <div class="form-group">
                        <label for="instagram">Instagram</label>
                        <input type="text" class="form-control @error('instagram') is-invalid @enderror" disabled
                               id="instagram" name="instagram" placeholder="https://instagram.com/tu_negocio"
                               value="{{old('instagram')?old('instagram'):$negocio->instagram}}">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div  class="ms-panel">
                <div  class="ms-panel-header">
                    <div class="d-flex justify-content-between">
                        <div class="align-self-center align-left">
                            <h5 style="display: inline">Información Legal</h5>
                        </div>
                    </div>
                </div>
                <div class="ms-panel-body">
                    <div class="form-group">
                        <label for="constitucion_tipo">¿Eres persona física o Moral?</label>
                        <select class="form-control @error('constitucion_tipo') is-invalid @enderror" disabled
                                id="constitucion_tipo" name="constitucion_tipo">
                            <option value="fisica">Persona física</option>
                            <option value="moral">Persona moral</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="constitucion_razon_social">Nombre / Razón Social</label>
                        <input type="text" class="form-control @error('constitucion_razon_social') is-invalid @enderror" disabled
                               id="constitucion_razon_social" name="constitucion_razon_social" placeholder="Nombre o Razón Social"
                               value="{{old('constitucion_razon_social')?old('constitucion_razon_social'):$negocio->constitucion_razon_social}}">
                    </div>

                    <div class="form-group">
                        <label for="constitucion_rfc">RFC</label>
                        <input type="text" class="form-control @error('constitucion_rfc') is-invalid @enderror" disabled
                               id="constitucion_rfc" name="constitucion_rfc" placeholder="RFC"
                               value="{{old('constitucion_rfc')?old('constitucion_rfc'):$negocio->constitucion_rfc}}">
                    </div>
                    <br/>
                    <p>Archivos</p>
                    <p>
                        @if($negocio->constitucion_comprobante_domicilio)
                            <a target="_blank"
                                  style="min-width: 0px; margin:0px; padding:0px"
                                  href="{{route('files.negocio', [base64_encode("documentos/{$negocio->id}"), $negocio->constitucion_comprobante_domicilio])}}"
                            >
                                <i class="fa fa-file-download"></i> Comprobante de domicilio
                            </a>
                        @endif
                    </p>

                    <p>
                        @if($negocio->constitucion_identificacion_oficial)
                            <a target="_blank"
                                  style="min-width: 0px; margin:0px; padding:0px"
                                  href="{{route('files.negocio', [base64_encode("documentos/{$negocio->id}"), $negocio->constitucion_identificacion_oficial])}}"
                            >
                                <i class="fa fa-file-download"></i> Identificación Oficial (Frente)
                            </a>
                        @endif
                    </p>

                    <p>
                        @if($negocio->constitucion_identificacion_oficial_reverso)
                            <a target="_blank"
                                  style="min-width: 0px; margin:0px; padding:0px"
                                  href="{{route('files.negocio', [base64_encode("documentos/{$negocio->id}"), $negocio->constitucion_identificacion_oficial_reverso])}}"
                            >
                                <i class="fa fa-file-download"></i> Identificación Oficial (Reverso)
                            </a>
                        @endif
                    </p>

                    <p>
                        @if($negocio->constitucion_acta)
                            <a target="_blank"
                                  style="min-width: 0px; margin:0px; padding:0px"
                                  href="{{route('files.negocio', [base64_encode("documentos/{$negocio->id}"), $negocio->constitucion_acta])}}"
                            >
                                <i class="fa fa-file-download"></i> Acta Constitutiva
                            </a>
                        @endif
                    </p>
                </div>
            </div>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY', 'AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY')}}&libraries=places"></script>
    <script type="application/javascript">
        $(document).ready(function(){
            var lastPosition = new google.maps.LatLng({{$negocio->lat?$negocio->lat:'20.6122138'}}, {{$negocio->lon?$negocio->lon:'-100.4452366'}});;
            map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: {{$negocio->lat?$negocio->lat:'20.6122138'}},
                    lng: {{$negocio->lon?$negocio->lon:'-100.4452366'}},
                },
                zoom: 13,
                scrollwheel: true,
                heigth: 250
            });

            marker = new google.maps.Marker({
                position: {
                    lat: {{$negocio->lat?$negocio->lat:'20.6122138'}},
                    lng: {{$negocio->lon?$negocio->lon:'-100.4452366'}}
                },
                map: map,
                title: '{{$negocio->nombre}}'
            });

            //generando el c'irculo del area permitida
            const regionArea = new google.maps.Circle({
                strokeColor: "#190d38",
                strokeOpacity: 0.7,
                strokeWeight: 1,
                fillColor: "#190d38",
                fillOpacity: 0.08,
                map,
                center: { lat: {{$negocio->region->centro_lat}}, lng: {{$negocio->region->centro_lon}} },
                radius: {{$negocio->region->radio_max}} * 1000,
            });
        });
    </script>
@endsection
