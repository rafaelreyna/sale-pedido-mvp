@extends('layouts.app')

@section('content')
    <div  class="row">
        <div  class="col-md-12">
            <nav  aria-label="breadcrumb">
                <ol  class="breadcrumb pl-0">
                    <li  class="breadcrumb-item">
                        <a  href="{{route('admin')}}"><i class="material-icons">home</i> Inicio</a>
                    </li>
                    <li  class="breadcrumb-item"><a  href="#">Configuracion</a></li>
                    <li  aria-current="page" class="breadcrumb-item active">Notificaciones</li>
                </ol>
            </nav>
        </div>
        <div  class="ms-panel col-md-8">
            <div  class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 >{{$titulo}}</h6>
                    </div>
                </div>
            </div>
            <div  class="ms-panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul style="margin: 0px;">
                            @foreach ($errors->all() as $error)
                                <li><i class="flaticon-alert"></i> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{$route}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{method_field('post')}}
                    <h5>Nueva sucursal</h5>
                    <div class="form-group">
                        <label for="direccion_cp">Nombre de la sucursal (Incluye el nomrbe del restaurante)</label>
                        <input type="text" class="form-control @error('nombre') is-invalid @enderror"
                               id="nombre" name="nombre" placeholder="Nombre  de la sucursal"
                               value="{{old('nombre')}}">
                    </div>

                    <div class="form-group">
                        <label for="region_id">Region de la sucursal</label>
                        <select class="form-control @error('region_id') is-invalid @enderror"
                                id="region_id" name="region_id">
                            @foreach(\App\Models\Region::all() as $region)
                                <option @if(old('region_id') == $region->id) selected="selected" @endif  value="{{$region->id}}">
                                    {{$region->nombre}}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="pickup">Base para la nueva sucursal</label>
                        <select type="text" class="form-control" id="pickup" name="negocio_id">
                            @if($sucursales->count())
                                @foreach($sucursales as $sucursal_base)
                                    <option value="{{$sucursal_base->negocio->id}}">{{$sucursal_base->negocio->nombre}}</option>
                                @endforeach
                            @else
                                <option value="{{$user->negocio->id}}">{{$user->negocio->nombre}}</option>
                            @endif
                        </select>
                    </div>
                    <br/><br/>
                    <h4>Incluir:</h4>
                    <div class="form-group" style="padding-left: 30px;">
                            <input name="productos_categorias" class="form-check-input" type="checkbox" value="true" id="flexCheckDefault">
                            <label class="form-check-label" for="flexCheckDefault">
                                Productos y categorías de producto
                            </label>
                    </div>
                    <br/>
                    <input type="submit" class="btn btn-primary" value="{{$callToAction}}">
                </form>
            </div>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY', 'AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY')}}&libraries=places"></script>
    <script type="application/javascript">
        $(document).ready(function(){


        });
    </script>
@endsection
