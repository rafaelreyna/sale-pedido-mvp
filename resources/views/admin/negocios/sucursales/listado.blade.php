@extends('layouts.app')

@section('content')
    <div _ngcontent-vbt-c44="" class="row">
        <div _ngcontent-vbt-c44="" class="col-md-12">
            <nav _ngcontent-vbt-c44="" aria-label="breadcrumb">
                <ol _ngcontent-vbt-c44="" class="breadcrumb pl-0">
                    <li _ngcontent-vbt-c44="" class="breadcrumb-item"><a _ngcontent-vbt-c44="" href="{{route('admin')}}"><i
                                _ngcontent-vbt-c44="" class="material-icons">home</i> Inicio</a></li>
                    <li _ngcontent-vbt-c44="" class="breadcrumb-item"><a _ngcontent-vbt-c44="" href="#">Menú</a></li>
                    <li _ngcontent-vbt-c44="" aria-current="page" class="breadcrumb-item active">Sucursales</li>
                </ol>
            </nav>
        </div>
        <div _ngcontent-vbt-c51="" class="ms-panel col-md-12">
            <div _ngcontent-vbt-c51="" class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 _ngcontent-vbt-c51="">Sucursales</h6>
                    </div>
                    <a href="{{route('admin.negocios.sucursales.create', $negocio->id)}}" class="btn text-white btn-info" ><i class="material-icons">add</i> Nueva sucursal</a>
                </div>
            </div>
            <div _ngcontent-vbt-c51="" class="ms-panel-body">
            @if($sucursales->count() > 0)
                <div class="table-responsive">
                    <table id="example" class="table table-sm table-datatable table-striped thead-primary w-100 dataTable no-footer" style="width: 100%;"style="width:100%">
                    <thead >
                        <tr>
                            <th>Nombre</th>
                            <th style="width: 160px;">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sucursales as $sucursal)
                            <tr>
                                <td>{{$sucursal->negocio->nombre}}</td>
                                <td>
                                    <a class="btn btnEliminar text-white btn-info" data-delete-id="{{$sucursal->id}}"> Seleccionar</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            @endif
                @if($sucursales->count() <= 0)
                    <p class="alert alert-info"><i class="flaticon-information"></i> No cuentas con sucursales, sólo cuentas con tu restaurante principal</p>
                @endif
            </div>
        </div>
    </div>
    <div class="delete_form">
        <form id="deleteForm" method="POST" action="" >
            {{ csrf_field() }}
            {{method_field('post')}}
        </form>
    </div>
    <script type="application/javascript">
        $(document).ready(function() {
            $('.table-datatable').DataTable({"ordering": false, "pageLength": 25, "bLengthChange": false,
                "language": {
                    "url": "{{URL::to('/assets/lang/dataTable/Spanish.json')}}"
                }});

            $('.btnEliminar').click(function(){
                const borrarId = $(this).data('deleteId');
                Swal.fire({
                    title: '¿Estás seguro que cambiar a esta sucursal?',
                    text: "Si aceptas comenzarás a visualizar toda la información de la sucursal",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#666',
                    confirmButtonText: 'Sí, seleccionar sucursal'
                }).then(function (result) {
                    if (result.value) {
                        $('#deleteForm').attr('action', '{{route('admin.negocios.sucursales.index', $negocio->id)}}' + '/' + borrarId+ '/select');
                        $('#deleteForm').submit();
                    }
                });
            });
        } );
    </script>
@endsection
