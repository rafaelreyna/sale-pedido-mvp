@extends('layouts.app')

@section('content')
    <div  class="row">
        <div  class="col-md-12">
            <nav  aria-label="breadcrumb">
                <ol  class="breadcrumb pl-0">
                    <li  class="breadcrumb-item">
                        <a  href="{{route('admin')}}"><i class="material-icons">home</i> Inicio</a>
                    </li>
                    <li  class="breadcrumb-item"><a  href="#">Configuracion</a></li>
                    @if(!$negocio->verificado || \Illuminate\Support\Facades\Auth::user()->tipo == 'superadmin')
                        <li  aria-current="page" class="breadcrumb-item active">Completar Perfil</li>
                    @else
                        <li  aria-current="page" class="breadcrumb-item active">Datos de contacto</li>
                    @endif
                </ol>
            </nav>
        </div>
        <div  class="ms-panel col-md-8">
            <div  class="ms-panel-header">
                <div class="d-flex justify-content-between">
                    <div class="align-self-center align-left">
                        <h6 >{{$titulo}}</h6>
                    </div>
                </div>
            </div>
            <div  class="ms-panel-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        * Algunos campos tienen errores, además si ya habías seleccionado algún archivo, asegurate de seleccionarlo nuevamente
                        <ul style="margin: 0px;">
                            @foreach ($errors->all() as $error)
                                <li><i class="flaticon-alert"></i> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{$route}}" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    {{$negocio->id?method_field('put'):method_field('post')}}
                    <input type="hidden" name="verificado" value="{{$negocio->verificado}}">

                    @if(!$negocio->verificado || \Illuminate\Support\Facades\Auth::user()->tipo == 'superadmin')
                    <h5>Logo del negocio</h5>
                    <p style="text-align: center">
                        @if($negocio->logo)
                            <img class="logo-negocio" src="{{route('images.public', [base64_encode('negocios/logos/'), $negocio->logo])}}"/>
                        @else
                            <img class="logo-negocio" src="{{URL::to('/assets/img')}}/sin_imagen.jpg"/>
                        @endif
                    </p>
                    <div class="form-group">
                        <label for="logo">Logo del negocio <small> (PNG, JPG | max. 1 MB | recomendado 600 x 600)</small></label>
                        <input type="file" class="form-control" id="logo" name="logo">
                    </div>
                    <br/>
                    @endif

                    @if(!$negocio->verificado || \Illuminate\Support\Facades\Auth::user()->tipo == 'superadmin')
                    <h5>Dirección del negocio</h5>
                    <div class="form-group">
                        <label for="direccion_cp">Código Postal</label>
                        <input type="text" class="form-control @error('direccion_cp') is-invalid @enderror"
                               id="direccion_cp" name="direccion_cp" placeholder="Código Postal"
                               value="{{old('direccion_cp')?old('direccion_cp'):$negocio->direccion_cp}}">
                    </div>

                    <div class="form-group">
                        <label for="direccion_calle">Calle</label>
                        <input type="text" class="form-control @error('direccion_calle') is-invalid @enderror"
                               id="direccion_calle" name="direccion_calle" placeholder="Calle"
                               value="{{old('direccion_calle')?old('direccion_calle'):$negocio->direccion_calle}}">
                    </div>

                    <div class="row">
                        <div class="form-group col col-6">
                            <label for="direccion_numero_exterior">Número exterior</label>
                            <input type="text" class="form-control @error('direccion_numero_exterior') is-invalid @enderror"
                                   id="direccion_numero_exterior" name="direccion_numero_exterior" placeholder="Numero Exterior"
                                   value="{{old('direccion_numero_exterior')?old('direccion_numero_exterior'):$negocio->direccion_numero_exterior}}">
                        </div>

                        <div class="form-group col col-6">
                            <label for="direccion_numero_interior">Número interior</label>
                            <input type="text" class="form-control @error('direccion_numero_interior') is-invalid @enderror"
                                   id="direccion_numero_interior" name="direccion_numero_interior" placeholder="Numero Interior"
                                   value="{{old('direccion_numero_interior')?old('direccion_numero_interior'):$negocio->direccion_numero_interior}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col col-12">
                            <label for="direccion_colonia">Colonia</label>
                            <input type="text" class="form-control @error('direccion_colonia') is-invalid @enderror"
                                   id="direccion_colonia" name="direccion_colonia" placeholder="Colonia"
                                   value="{{old('direccion_colonia')?old('direccion_colonia'):$negocio->direccion_colonia}}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col col-6">
                            <label for="direccion_municipio">Municipio</label>
                            <input type="text" class="form-control @error('direccion_municipio') is-invalid @enderror"
                                   id="direccion_municipio" name="direccion_municipio" placeholder="Municipio"
                                   value="{{old('direccion_municipio')?old('direccion_municipio'):$negocio->direccion_municipio}}">
                        </div>

                        <div class="form-group col col-6">
                            <label for="direccion_estado">Estado</label>
                            <input type="text" class="form-control @error('direccion_estado') is-invalid @enderror"
                                   id="direccion_estado" name="direccion_estado" placeholder="Estado"
                                   value="{{old('direccion_estado')?old('direccion_estado'):$negocio->direccion_estado}}">
                        </div>
                    </div>
                    <br/>
                    @endif

                    @if(!$negocio->verificado || \Illuminate\Support\Facades\Auth::user()->tipo == 'superadmin')
                    <h5>Ubicación del negocio</h5>
                    <input
                        autocomplete="disabled"
                        id="pac-input"
                        class="controls form-control"
                        style="width: 50%; margin-bottom: 20px;"
                        type="text"
                        placeholder="Busca una dirección o lugar"
                    />
                    <br/>
                    <p class="alert alert-info"><i class="fa fa-info-circle"></i> Puedes arrastrar el marcador para colocarlo en tu ubicación exacta. </p>
                    <div id="map" style="height: 400px;" data-zoom="13"></div>
                    <br/>
                    <div class="row">
                        <div class="form-group col col-6">
                            <label for="lat">Latitud</label>
                            <input type="text" class="form-control @error('lat') is-invalid @enderror"
                                   id="lat" name="lat" placeholder=""
                                   value="{{old('lat')?old('lat'):$negocio->lat}}">
                        </div>

                        <div class="form-group col col-6">
                            <label for="lon">Longitud</label>
                            <input type="text" class="form-control @error('lon') is-invalid @enderror"
                                   id="lon" name="lon" placeholder=""
                                   value="{{old('lon')?old('lon'):$negocio->lon}}">
                        </div>
                    </div>
                    <br/>
                    @endif


                    <h5>Contacto del negocio</h5>
                    <p class="alert alert-info"><i class="fa fa-info-circle"></i> Estos son los datos de tu negocio  que serán visibles para que tus clientes te contacten.</p>
                    <div class="form-group">
                        <label for="email">Correo electrónico</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                               id="email" name="email" placeholder="Correo electrónico"
                               value="{{old('email')?old('email'):$negocio->email}}">
                    </div>

                    <div class="form-group">
                        <label for="telefono">Teléfono</label>
                        <input type="text" class="form-control @error('telefono') is-invalid @enderror"
                               id="telefono" name="telefono" placeholder="Teléfono"
                               value="{{old('telefono')?old('telefono'):$negocio->telefono}}">
                    </div>

                    <div class="form-group">
                        <label for="sitio_web">Sitio Web</label>
                        <input type="text" class="form-control @error('sitio_web') is-invalid @enderror"
                               id="sitio_web" name="sitio_web" placeholder="Sitio web"
                               value="{{old('sitio_web')?old('sitio_web'):$negocio->sitio_web}}">
                    </div>

                    <div class="form-group">
                        <label for="facebook">Facebook</label>
                        <input type="text" class="form-control @error('facebook') is-invalid @enderror"
                               id="facebook" name="facebook" placeholder="https://facebook.com/tu_negocio"
                               value="{{old('facebook')?old('facebook'):$negocio->facebook}}">
                    </div>

                    <div class="form-group">
                        <label for="instagram">Instagram</label>
                        <input type="text" class="form-control @error('instagram') is-invalid @enderror"
                               id="instagram" name="instagram" placeholder="https://instagram.com/tu_negocio"
                               value="{{old('instagram')?old('instagram'):$negocio->instagram}}">
                    </div>

                    <br/>
                    <h5>Información Legal</h5>
                    @if(!$negocio->verificado || \Illuminate\Support\Facades\Auth::user()->tipo == 'superadmin')
                    <div class="form-group">
                        <label for="constitucion_tipo">¿Eres persona física o Moral?</label>
                        <select class="form-control @error('constitucion_tipo') is-invalid @enderror"
                               id="constitucion_tipo" name="constitucion_tipo">
                            <option value="fisica">Persona física</option>
                            <option value="moral">Persona moral</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="constitucion_razon_social">Nombre / Razón Social</label>
                        <input type="text" class="form-control @error('constitucion_razon_social') is-invalid @enderror"
                               id="constitucion_razon_social" name="constitucion_razon_social" placeholder="Nombre o Razón Social"
                               value="{{old('constitucion_razon_social')?old('constitucion_razon_social'):$negocio->constitucion_razon_social}}">
                    </div>
                    @endif


                    <div class="form-group">
                        <label for="constitucion_rfc">RFC <small>Si deseas factura deberás rellenar este campo.</small></label>
                        <input type="text" class="form-control @error('constitucion_rfc') is-invalid @enderror"
                               id="constitucion_rfc" name="constitucion_rfc" placeholder="RFC"
                               value="{{old('constitucion_rfc')?old('constitucion_rfc'):$negocio->constitucion_rfc}}">
                    </div>

                    @if(!$negocio->verificado || \Illuminate\Support\Facades\Auth::user()->tipo == 'superadmin')
                    <div class="form-group">
                        <label for="constitucion_comprobante_domicilio">
                            Subir comprobante de domicilio <small>(No mayor a 2 meses | PNG, JPG, PDF | max. 2 MB)</small>
                            @if($negocio->constitucion_comprobante_domicilio)
                                -  <a target="_blank"
                                      style="min-width: 0px; margin:0px; padding:0px"
                                      href="{{route('files.negocio', [base64_encode("documentos/{$negocio->id}"), $negocio->constitucion_comprobante_domicilio])}}"
                                >
                                    <i class="fa fa-file-download"></i> Ver archivo
                                </a>
                            @endif
                        </label>
                        <input type="file" class="form-control" id="constitucion_comprobante_domicilio" name="constitucion_comprobante_domicilio">
                    </div>


                    <div class="row">
                        <div class="form-group col col-6">
                            <label for="constitucion_identificacion_oficial">
                                Subir Identificación Oficial <small>(Frente  | PNG, JPG, PDF | max. 2 MB)</small>
                                @if($negocio->constitucion_identificacion_oficial)
                                    -  <a target="_blank"
                                          style="min-width: 0px; margin:0px; padding:0px"
                                          href="{{route('files.negocio', [base64_encode("documentos/{$negocio->id}"), $negocio->constitucion_identificacion_oficial])}}"
                                    >
                                        <i class="fa fa-file-download"></i> Ver archivo
                                    </a>
                                @endif
                            </label>
                            <input type="file" class="form-control" id="constitucion_identificacion_oficial" name="constitucion_identificacion_oficial">
                        </div>

                        <div class="form-group col col-6">
                            <label for="constitucion_identificacion_oficial_reverso">
                                Subir Identificación Oficial <small>(Reverso  | PNG, JPG, PDF | max. 2 MB)</small>
                                @if($negocio->constitucion_identificacion_oficial_reverso)
                                    -  <a target="_blank"
                                          style="min-width: 0px; margin:0px; padding:0px"
                                          href="{{route('files.negocio', [base64_encode("documentos/{$negocio->id}"), $negocio->constitucion_identificacion_oficial_reverso])}}"
                                    >
                                        <i class="fa fa-file-download"></i> Ver archivo
                                    </a>
                                @endif
                            </label>
                            <input type="file" class="form-control" id="constitucion_identificacion_oficial_reverso" name="constitucion_identificacion_oficial_reverso">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="constitucion_acta">
                            Subir Acta constitutiva <small>(Solo Personas Morales  | PNG, JPG, PDF | max. 2 MB)</small>
                            @if($negocio->constitucion_acta)
                                -  <a target="_blank"
                                      style="min-width: 0px; margin:0px; padding:0px"
                                      href="{{route('files.negocio', [base64_encode("documentos/{$negocio->id}"), $negocio->constitucion_acta])}}"
                                >
                                    <i class="fa fa-file-download"></i> Ver archivo
                                </a>
                            @endif
                        </label>
                        <input type="file" class="form-control" id="constitucion_acta" name="constitucion_acta">
                    </div>
                    @endif

                    <input type="submit" class="btn btn-primary" value="{{$callToAction}}">
                </form>
            </div>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY', 'AIzaSyBLPbEf2K9UXGKjVCul13OfKTh7D0nWWLY')}}&libraries=places"></script>
    <script type="application/javascript">
        $(document).ready(function(){
            var lastPosition = new google.maps.LatLng({{$negocio->lat?$negocio->lat:'20.6122138'}}, {{$negocio->lon?$negocio->lon:'-100.4452366'}});;
            map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: {{$negocio->lat?$negocio->lat:'20.6122138'}},
                    lng: {{$negocio->lon?$negocio->lon:'-100.4452366'}},
                },
                zoom: 13,
                scrollwheel: true,
                heigth: 250
            });

            marker = new google.maps.Marker({
                position: {
                    lat: {{$negocio->lat?$negocio->lat:'20.6122138'}},
                    lng: {{$negocio->lon?$negocio->lon:'-100.4452366'}}
                },
                map: map,
                draggable:true,
                title: '{{$negocio->nombre}}'
            });

            //generando el c'irculo del area permitida
            const regionArea = new google.maps.Circle({
                strokeColor: "#190d38",
                strokeOpacity: 0.7,
                strokeWeight: 1,
                fillColor: "#190d38",
                fillOpacity: 0.08,
                map,
                center: { lat: {{$negocio->region->centro_lat}}, lng: {{$negocio->region->centro_lon}} },
                radius: {{$negocio->region->radio_max}} * 1000,
            });

            google.maps.event.addListener(marker, 'dragend', function (event) {
                document.getElementById("lat").value = event.latLng.lat();
                document.getElementById("lon").value = event.latLng.lng();
                var position = marker.getPosition();
                if (regionArea.getBounds().contains(position)) {
                    lastPosition = position
                } else {
                    marker.setPosition(lastPosition)
                    toastr.remove();
                    toastr.options.positionClass = "toast-bottom-right";
                    toastr.error('No puedes colocar el restaurante fuera de los límites de la región', 'Posición Incorrecta');
                }
            });

            // Create the search box and link it to the UI element.
            const input = document.getElementById("pac-input");
            const searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(input);
            // Bias the SearchBox results towards current map's viewport.
            map.addListener("bounds_changed", () => {
                searchBox.setBounds(map.getBounds());
            });

            let markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener("places_changed", () => {
                const places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }
                // Clear out the old markers.
                markers.forEach((marker) => {
                    marker.setMap(null);
                });

                const bounds = new google.maps.LatLngBounds();
                places.forEach((place) => {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }

                    marker.setPosition(place.geometry.location);
                    document.getElementById("lat").value = place.geometry.location.lat();
                    document.getElementById("lon").value = place.geometry.location.lng();

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });

            //impidiendo enter
            $('body').keypress(function(e)
            {
                if (e.keyCode == '13') {
                    e.stopPropagation();
                    return false;
                }
            });


        });


    </script>
@endsection
