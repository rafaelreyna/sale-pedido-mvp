@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <p style="margin: 0px;">Hola, {{\Illuminate\Support\Facades\Auth::user()->name}}</p>
            <h1 class="db-header-title">{{$negocio->nombre}}</h1>
            @if($negocio->adeudo)
            <div class="alert alert-danger">
                <h3 style="color: #900;"><i class="fa fa-info-circle"></i> IMPORTANTE: SITIO DESACTIVADO</h3>
                <p><big>Actualmente cuentas con adeudo en uno o más de tus recibos, Accede a la sección <a href="{{route('admin.facturas.index')}}">Estado de Cuenta</a> para poder reactivar tu restaurante.</big></p>
            </div>
            @endif
        </div>

        <div class="col-md-12">
            <div class="ms-panel">
                <div class="ms-panel-header">
                    <h6>
                        Estadísticas generales de
                        {{\App\Helpers\DateHelper::mes_letra(date('m'))}}
                        {{ \Carbon\Carbon::now()->formatLocalized('%Y') }}
                    </h6>
                    <p class="alert alert-info"><i class="fa fa-info-circle"></i> La información del mes pasado y los porcentajes se calculan utilizando el acumulado del mes corriente en comparación al total del mes pasado.</p>
                </div>
                <div class="ms-panel-body">

        <div class="row row-eq-height">
                <!-- Ventas -->
            @component('admin.components.tarjeta_dashboard')
                @slot('tipo', 'dinero')
                @slot('titulo', 'Ventas')
                @slot('resumen', $negocio->resumen_ventas())
            @endcomponent

            <!-- Pedidos -->
            @component('admin.components.tarjeta_dashboard')
                @slot('tipo', 'pedidos')
                @slot('titulo', 'Pedidos')
                @slot('resumen', $negocio->resumen_pedidos())
            @endcomponent

            <!-- Ticket Promedio -->
            @component('admin.components.tarjeta_dashboard')
                @slot('tipo', 'dinero')
                @slot('titulo', 'Ticket Promedio')
                @slot('resumen', $negocio->resumen_ticket_promedio())
            @endcomponent

            <!-- Comisiones UKI -->
            @component('admin.components.tarjeta_dashboard')
                @slot('tipo', 'dinero')
                @slot('iva', 'incluido')
                @slot('titulo', 'Comisiones Uki')
                @slot('disclaimer', '*')
                @slot('resumen', $negocio->resumen_comisiones_uki())
            @endcomponent

            <!-- Ganancia -->
            @component('admin.components.tarjeta_dashboard')
                @slot('tipo', 'dinero')
                @slot('titulo', 'Ganancia')
                @slot('resumen', $negocio->resumen_ganancia())
            @endcomponent

            <!-- porcentaje de Ganancia -->
            @component('admin.components.tarjeta_dashboard')
                @slot('tipo', 'porcentaje')
                @slot('titulo', '% de Ganancia')
                @slot('resumen', $negocio->resumen_porcentaje_ganancia())
            @endcomponent

            <!-- Pedidos pickup-->
            @component('admin.components.tarjeta_dashboard')
                @slot('tipo', 'pedidos')
                @slot('titulo', 'Pedidos Pick up & GO')
                @slot('resumen', $negocio->resumen_pedidos_pickup())
            @endcomponent

            <!-- Ingresos envios -->
            @component('admin.components.tarjeta_dashboard')
                @slot('tipo', 'dinero')
                @slot('titulo', 'Ingresos por envios')
                @slot('resumen', $negocio->resumen_ingresos_envios())
            @endcomponent


                </div>
                    <p><small>* Las comisiones UKI&reg; toman en cuenta los pedidos cancelados.</small></p>
                </div>
            </div>

        </div>


        <!-- Food Orders -->
        <div class="col-md-12">
            <div class="ms-panel">
                <div class="ms-panel-header">
                    <h6>Lo más pedido en este mes</h6>
                </div>
                <div class="ms-panel-body">
                    <div class="row">
                        @foreach($negocio->mas_pedidos() as $platillo)
                            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6" style="margin-bottom: 20px;">
                                <div class="ms-card no-margin" style="height: 100%;">
                                    <div class="ms-card-img">
                                        <?php $platillo_obj = \App\Models\Restaurantes\Platillo::where('nombre',
                                            $platillo->platillo)->first() ?>
                                        @if($platillo_obj->foto)
                                            <img class="img-fluid img-platillo-dashboard"
                                                 src="{{route('images.public', [base64_encode('restaurantes/platillos/'), $platillo_obj->foto])}}"/>
                                        @else
                                            <img class="img-fluid img-platillo-dashboard"
                                                 src="{{URL::to('/assets/img')}}/sin_imagen.jpg"/>
                                        @endif
                                    </div>
                                    <div class="ms-card-body">
                                        <div class="ms-card-heading-title">
                                            <h6>{{$platillo->platillo}}</h6>
                                            <span
                                                class="green-text"><strong>$ {{number_format($platillo->costo_unitario, 2, '.', ',')}}</strong></span>
                                        </div>

                                        <div class="ms-card-heading-title">
                                            <p>Pedidos <span class="red-text">{{$platillo->count}}</span></p>
                                            <p>Ingreso <span
                                                    class="red-text">$ {{number_format($platillo->costo_unitario * $platillo->count, 2, '.', ',')}}</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <!-- END/Food Orders -->
        <div class="col-xl-6 col-lg-12">
            <div class="ms-panel">
                <div class="ms-panel-header">
                    <h6>
                        Código QR de restaurante
                    </h6>
                </div>
                <div class="ms-panel-body">
                    <br/><h5>QR de tienda en linea</h5>
                    <div class="row">
                        <div class="col-md-12">
                            <a href="https://chart.googleapis.com/chart?chs=500x500&cht=qr&chl={{urlencode(route('food.restaurante.show', $negocio->id))}}" target="_blank" download>
                                <img style="width: 100px;" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl={{urlencode(route('food.restaurante.show', $negocio->id))}}&choe=UTF-8">
                            </a>
                            <a  href="https://chart.googleapis.com/chart?chs=500x500&cht=qr&chl={{urlencode(route('food.restaurante.show', $negocio->id))}}" target="_blank" download>
                                Descargar código QR
                            </a>
                        </div>
                    </div>
                    <br/><h5>QR de menú digital</h5>
                    <div class="row">
                        <div class="col-md-12">
                            <a href="https://chart.googleapis.com/chart?chs=500x500&cht=qr&chl={{urlencode(route('food.restaurante.menu.show', $negocio->id))}}" target="_blank" download>
                                <img style="width: 100px;" src="https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl={{urlencode(route('food.restaurante.show', $negocio->id))}}&choe=UTF-8">
                            </a>
                            <a  href="https://chart.googleapis.com/chart?chs=500x500&cht=qr&chl={{urlencode(route('food.restaurante.menu.show', $negocio->id))}}" target="_blank" download>
                                Descargar código QR
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-6 col-lg-12">
            <div class="ms-panel">
                <div class="ms-panel-header">
                    <h6>
                        Enlace de restaurante
                    </h6>
                </div>
                <div class="ms-panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul style="margin: 0px;">
                                        @foreach ($errors->all() as $error)
                                            <li><i class="flaticon-alert"></i> {{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group">
                                <form method="post" action="{{route('admin.negocios.slug.update', $negocio->id)}}">
                                    {{method_field('put')}}
                                    {{ csrf_field() }}
                                    <label for="codigo">URL Amigable de tu restaurante</label>
                                    <input type="text" class="form-control @error('slug') is-invalid @enderror"
                                           id="slug" name="slug" placeholder="tu-restaurante"
                                           value="{{old('slug')?old('slug'):$negocio->config->slug}}">
                                    <p style="text-align: right"><input type="submit" class="btn btn-primary" value="Guardar"></p>
                                </form>
                            </div>
                            <br/><h5>Enlace de tienda en linea</h5>
                            <p class="alert alert-info">
                                {{route('food.restaurante.show', $negocio->slugOrId())}}
                            </p>
                            <p style="text-align: right">
                                <a href="#" class="btnCopiarEnlace" target="_blank" download>
                                    Copiar enlace
                                </a>
                            </p>
                                <br/><h5>Enlace de menú digital</h5>
                                <p class="alert alert-info">
                                    {{route('food.restaurante.menu.show', $negocio->slugOrId())}}
                                </p>
                                <p style="text-align: right">
                                    <a href="#" class="btnCopiarEnlaceMenu" target="_blank" download>
                                        Copiar enlace
                                    </a>
                                </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <script type="application/javascript">
        @if(session('loggedin'))
            $(document).ready(function(){
                swal("Bienvenido", "Acceso correcto al adminsitrador de UKI&reg;", "success").then(() => {
                    revisarpedidos();
                });
            });
        @endif

        $(document).ready(function(){
            $(".btnCopiarEnlace").click(function(){
                var $temp = $("<input>");
                $("body").append($temp);
                var info_copiar = "{{route('food.restaurante.show', $negocio->slugOrId())}}";
                $temp.val( info_copiar ).select();
                document.execCommand("copy");
                $temp.remove();

                toastr.remove();
                toastr.options.positionClass = "toast-bottom-right";
                toastr.success('Se ha copiado la información en el portapapeles');
                return false;
            });
            $(".btnCopiarEnlaceMenu").click(function(){
                var $temp = $("<input>");
                $("body").append($temp);
                var info_copiar = "{{route('food.restaurante.menu.show', $negocio->slugOrId())}}";
                $temp.val( info_copiar ).select();
                document.execCommand("copy");
                $temp.remove();

                toastr.remove();
                toastr.options.positionClass = "toast-bottom-right";
                toastr.success('Se ha copiado la información en el portapapeles');
                return false;
            });
        });


    </script>
@endsection
