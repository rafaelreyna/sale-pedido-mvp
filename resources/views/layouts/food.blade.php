<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <meta property="og:locale" content="es_MX" />
    @if(isset($custom_meta))
        @yield('custom_meta')
    @else
        <meta property="og:title" content="Uki&reg;" />
        <meta property="og:image" content="{{asset("food_assets/images/logo_og.jpg")}}" />
        <meta property="og:description" content="Somos una plataforma que conecta a los restaurantes con los consumidores de manera trasparente y justa. Sin comisiones abusivas ni pagos retrasados." />
        <meta property="og:url" content="{{route('food.home')}}" />
        <title>UKI&reg; @if(env('APP_ENV', 'local') == 'beta') - Beta @endif</title>
    @endif

    <link rel="icon" href="#">
    <!-- Bootstrap core CSS -->
    <link href="{{URL::to('/')}}/food_assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/food_assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/food_assets/css/animsition.min.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/food_assets/css/animate.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{URL::to('/')}}/food_assets/css/style.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/food_assets/css/food_style.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/assets/css/sweetalert2.min.css" rel="stylesheet">

    <script src="{{URL::to('/')}}/food_assets/js/jquery.min.js"></script>
    <script src="{{URL::to('/')}}/food_assets/js/jquery.sticky.js"></script>


    @if(env('APP_ENV', 'local') == 'production')
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-D76J32J4H1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'G-D76J32J4H1');
        </script>
    @endif
</head>

<body class="home">
<div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <!--header starts-->
    <header id="header" class="header-scroll top-header headrom">
        <!-- .navbar -->
        <nav style="padding: 6px!important;" class="navbar navbar-dark">
            <div class="container">
                <button class="navbar-toggler hidden-lg-up" type="button" data-toggle="collapse" data-target="#mainNavbarCollapse">&#9776;</button>
                <a class="navbar-brand" href="{{route('food.home')}}"> <img style="height: 64px;" class="img-rounded" src="{{URL::to('/')}}/assets/img/{{env('UKI_LOGO_NEGATIVO_FILE', 'logo_negativo.png')}}" alt=""> </a>
                @if(empty($menu_online))
                    <div class="collapse navbar-toggleable-md  float-lg-right" id="mainNavbarCollapse">
                        <ul class="nav navbar-nav">
                            <li class="nav-item"> <a target="_blank" class="nav-link active" href="https://restaurantes.uki.mx">Tengo un Restaurante</a> </li>
                            <li class="nav-item"> <a class="nav-link active" href="{{route('food.home')}}"><i class="fa fa-spoon"></i> Inicio </a> </li>
                            <li class="nav-item"> <a class="nav-link active" href="{{route('food.perfil.edit')}}"><i class="fa fa-user"></i> Mi información</a> </li>
                            <li class="nav-item"> <a class="btn theme-btn" href="{{route('food.checkout.index')}}">
                                    <i class="fa fa-shopping-cart badge-wrapper">
                                        @if(\Illuminate\Support\Facades\Session::get('last-cart-instance'))
                                                <?php
                                                \Melihovv\ShoppingCart\Facades\ShoppingCart::instance(session()->get('last-cart-instance'))->restore(session()->getId());
                                                ?>
                                            @if(\Melihovv\ShoppingCart\Facades\ShoppingCart::instance(session()->get('last-cart-instance'))->count())
                                                    <?php
                                                    $count = 0;
                                                    foreach(\Melihovv\ShoppingCart\Facades\ShoppingCart::instance(session()->get('last-cart-instance'))->content() as $item) {
                                                        $count += $item->quantity;
                                                    }
                                                    ?>
                                                <span class='badge ' style="color: #FFF!important">{{$count}}</span>
                                            @endif
                                        @endif
                                    </i> Mi Pedido
                                </a></li>
                            @if(\App\Models\Food\Perfil::getPerfil(request())->direccion)
                                <li class="nav-item"><a class="nav-link" href="{{route('food.perfil.edit')}}" style="color:#FFF!important; text-overflow: ellipsis; width: 150px;width: 250px;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;" title="{{\App\Models\Food\Perfil::getPerfil(request())->direccion}}">{{\App\Models\Food\Perfil::getPerfil(request())->direccion}}</a></li>
                            @endif
                        </ul>
                    </div>
                @else
                @endif

            </div>
        </nav>
        <!-- /.navbar -->
    </header>

    <div>
        <br/><br/><br/>
    </div>
    @if(session('mensajeFlash'))
        <div>
            <p class="alert {{session('mensajeFlash')['accion']}}">
                <strong><big>{{session('mensajeFlash')['titulo']}}</big></strong><br/>
                {!!  session('mensajeFlash')['mensaje']!!}
            </p>
        </div>
    @endif
    @if(isset($perfil))
        @if(isset($perfil->region_id))
            @if(!($perfil->lat && $perfil->lon))
                <p class="alert alert-warning"><i class="fa fa-warning"></i> Actualmente se muestran solo restaurantes con servicio de Pickup & Go, para mostrar restaurantes con envio a domicilio <a href="{{route('food.perfil.edit')}}?solo_ubicacion=true">configura tu ubicación</a></p>
            @endif
        @endif
    @endif
    @yield('content')

    <div class="menu_movil solo_movil">
        <div class="row">
            <div class="col col-xs-4">
                <p style="text-align: center">
                    <a href="{{route('food.home')}}">
                        <i style="font-size: 32px;" class="fa fa-spoon"></i><br/>
                        <small>Home</small>
                    </a>
                </p>
            </div>
            <div class="col col-xs-4">
                <p style="text-align: center">
                    <a href="{{route('food.checkout.index')}}">
                        <i style="font-size: 32px;" class="fa fa-shopping-cart badge-wrapper">
                            @if(\Illuminate\Support\Facades\Session::get('last-cart-instance'))
                                <?php
                                \Melihovv\ShoppingCart\Facades\ShoppingCart::instance(session()->get('last-cart-instance'))->restore(session()->getId());
                                ?>
                                @if(\Melihovv\ShoppingCart\Facades\ShoppingCart::instance(session()->get('last-cart-instance'))->count())
                                    <?php
                                        $count = 0;
                                        foreach(\Melihovv\ShoppingCart\Facades\ShoppingCart::instance(session()->get('last-cart-instance'))->content() as $item) {
                                            $count += $item->quantity;
                                        }
                                    ?>
                                    <span class='badge ' style="color: #FFF!important">{{$count}}</span>
                                @endif
                            @endif
                        </i>
                        <br/>
                        <small>Pedido</small>
                    </a>
                </p>
            </div>
            <div class="col col-xs-4">
                <p style="text-align: center">
                    <a href="{{route('food.perfil.edit')}}">
                        <i style="font-size: 32px;" class="fa fa-user"></i><br/>
                        <small>Mis preferencias</small>
                    </a>
                </p>
            </div>
        </div>
    </div>

    <section class="app-section solo_desktop">
        <div class="app-wrap">
            <div class="container">
                <div class="row text-img-block text-xs-left">
                    <div class="container">
                        <div class="col-xs-12 col-sm-6 hidden-xs-down right-image text-center">
                            <figure> <img style="height: 500px;" src="{{URL::to('/food_assets/images/celulara.png')}}" alt="Right Image"> </figure>
                        </div>
                        <div class="col-xs-12 col-sm-6 left-text">
                            <br/><br/>
                            <h3>Una Plataforma 100% mexicana</h3>
                            <p>Somos una plataforma que conecta a los restaurantes con los consumidores de manera trasparente y justa. Sin comisiones abusivas ni pagos retrasados, permitiendo a los negocios mexicanos ofrecer sus productos a mejores precios para el consumidor.</p>
                            <div class="social-btns" style="display: none">
                                <a href="" class="app-btn app-btn-disabled clearfix" style="cursor: initial; ">
                                    <div class="pull-left"><i class="fa fa-apple"></i> </div>
                                    <div class="pull-right"> <span class="text">Próximamente en  </span> <span class="text-2">App Store</span> </div>
                                </a>
                                <a href="https://play.google.com/store/apps/details?id=com.massimple.uki" style="cursor: initial; "  target="_blank" class="app-btn app-btn-disabled clearfix">
                                    <div class="pull-left"><i class="fa fa-android"></i> </div>
                                    <div class="pull-right"> <span class="text">Próximamente en </span> <span class="text-2">Play store</span> </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="app-section solo_movil">
        <div class="app-wrap">
            <div class="container">
                <div class="row text-img-block text-xs-left">
                    <div class="col-xs-12 col-sm-6 left-text">
                        <h3>Una Plataforma 100% mexicana</h3>
                        <p>Somos una plataforma que conecta a los restaurantes con los consumidores de manera trasparente y justa.</p>
                        <div class="social-btns" style="display: none">
                            <a href="" class="app-btn app-btn-disabled clearfix" style="cursor: initial; ">
                                <div class="pull-left"><i class="fa fa-apple"></i> </div>
                                <div class="pull-right"> <span class="text">Próximamente en  </span> <span class="text-2">App Store</span> </div>
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.massimple.uki"  target="_blank" class="app-btn android-button clearfix">
                                <div class="pull-left"><i class="fa fa-android"></i> </div>
                                <div class="pull-right"> <span class="text">Disponible en </span> <span class="text-2">Play store</span> </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if(empty($menu_online))
        <footer class="footer" style="padding: 20px;">
            <div class="container">
                <!-- bottom footer statrs -->
                <div class="bottom-footer">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 payment-options color-gray">
                            <h5>Opciones de pago</h5>
                            <ul>
                                <li>
                                    <img style="height: 90px;" src="{{URL::to('/')}}/food_assets/images/pago.png" />
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 additional-info color-gray">
                            <p >
                                <i class="fa fa-lock"></i>
                                <a target="_blank" style="color: #DDD" href="{{route('admin')}}">Acceso a adminsitradores de negocio</a>
                            </p>
                            <p >
                                <a target="_blank" style="color: #DDD" href="https://restaurantes.uki.mx">UKI&reg; para restaurantes</a>
                            </p>
                            <hr/>
                            <p class="solo_desktop">{{date('Y')}}&copy; Todos los derechos reservados</p>
                        </div>
                    </div>
                </div>
                <!-- bottom footer ends -->
            </div>
            <br/><br/><br/>
        </footer>
    @else
    @endif

    <!-- end:Footer -->
</div>
<!--/end:Site wrapper -->

<style type="text/css">
    .app-btn-disabled {
        color: #FFFFFF;
        float: left;
        margin-right: 15px;
        background: #81858b;
        margin-top: 5px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 2px;
    }

    .app-btn-disabled:hover {background-color: #81858b!important}
</style>

<script type="text/javascript">
    $(document).ready(function(){
        $('.txtConteoCaracteres').on('change keyup paste', function() {
            $(this).next('div').find('.conteoCaracteres').html($(this).val().length);
        })
    });
</script>

<!-- Bootstrap core JavaScript
================================================== -->
<script src="{{URL::to('/')}}/food_assets/js/tether.min.js"></script>
<script src="{{URL::to('/')}}/food_assets/js/bootstrap.min.js"></script>
<script src="{{URL::to('/')}}/food_assets/js/animsition.min.js"></script>
<script src="{{URL::to('/')}}/food_assets/js/bootstrap-slider.min.js"></script>
<script src="{{URL::to('/')}}/food_assets/js/jquery.isotope.min.js"></script>
<script src="{{URL::to('/')}}/food_assets/js/headroom.js"></script>
<script src="{{URL::to('/')}}/food_assets/js/foodpicky.min.js"></script>
<script src="{{URL::to('/')}}/assets/js/sweetalert2.min.js"></script>
</body>

</html>
