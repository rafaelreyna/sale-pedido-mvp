<style type="text/css">
    * { font-family: Helvetica, Arial}
    .footermail {
        font-size: 9px;
        color: #777;
    }
    .stronguki{
        color:#190d38;
    }
</style>
<div>
    <p style="text-align: left"><img style="max-width: 150px;" src="{{URL::to('/assets/img')}}/{{env('UKI_LOGO_FILE', 'logo.png')}}"/></p>
    <hr/>
    @yield('content')
    <br/>
    <hr/>
    <p class="footermail">Notificación UKI&reg; - No responder a este correo</p>
</div>
