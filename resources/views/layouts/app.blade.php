<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Uki Panel de Adminsitración</title>
    <!-- Iconic Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{URL::to('/')}}/assets/css/flaticon.css">
    <link href="{{URL::to('/')}}/assets/vendors/iconic-fonts/font-awesome/css/all.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="{{URL::to('/')}}/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery UI -->
    <link href="{{URL::to('/')}}/assets/css/jquery-ui.min.css" rel="stylesheet">
    <!-- Costic styles -->
    <link href="{{URL::to('/')}}/assets/css/style.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/assets/css/uki-style.css" rel="stylesheet">
    <link href="{{URL::to('/')}}/assets/css/toastr.min.css" rel="stylesheet">
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="32x32" href="{{URL::to('/')}}/favicon.ico">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="{{URL::to('/')}}/assets/css/sweetalert2.min.css" rel="stylesheet">

    <!-- Global Required Scripts Start -->
    <script src="{{URL::to('/')}}/assets/js/jquery-3.3.1.min.js"></script>
    <script src="{{URL::to('/')}}/assets/js/popper.min.js"></script>
    <script src="{{URL::to('/')}}/assets/js/bootstrap.min.js"></script>
    <script src="{{URL::to('/')}}/assets/js/perfect-scrollbar.js">
    </script>
    <script src="{{URL::to('/')}}/assets/js/jquery-ui.min.js"></script>
    <script src="{{URL::to('/')}}/assets/js/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.6.1/jszip.min.js"></script>
    <script type="text/javascript"
            src="https://www.encuentraescuela.com.mx/queretaro/dev-extreme/js/globalize.min.js"></script>

    <!-- Global Required Scripts End -->
</head>

<body class=" ms-body ms-primary-theme ms-logged-out">

<!-- Preloader -->
<div id="preloader-wrap">
    <div class="spinner spinner-8">
        <div class="ms-circle1 ms-child"></div>
        <div class="ms-circle2 ms-child"></div>
        <div class="ms-circle3 ms-child"></div>
        <div class="ms-circle4 ms-child"></div>
        <div class="ms-circle5 ms-child"></div>
        <div class="ms-circle6 ms-child"></div>
        <div class="ms-circle7 ms-child"></div>
        <div class="ms-circle8 ms-child"></div>
        <div class="ms-circle9 ms-child"></div>
        <div class="ms-circle10 ms-child"></div>
        <div class="ms-circle11 ms-child"></div>
        <div class="ms-circle12 ms-child"></div>
    </div>
</div>
<!-- Overlays -->
<div class="ms-aside-overlay ms-overlay-left ms-toggler" data-target="#ms-side-nav" data-toggle="slideLeft"></div>
<div class="ms-aside-overlay ms-overlay-right ms-toggler" data-target="#ms-recent-activity"
     data-toggle="slideRight"></div>
<aside id="ms-side-nav" class="side-nav fixed ms-aside-scrollable ms-aside-left ps ps--active-y">
    <!-- Logo -->
    <div class="logo-sn ms-d-block-lg">
        <a class="pl-0 ml-0 text-center" href="{{route('admin')}}">
            <img id="logoNormal" style="max-width: 150px;"
                 src="{{URL::to('/assets/img')}}/{{env('UKI_LOGO_FILE', 'logo.png')}}"/>
            <img id="logoLight" style="max-width: 150px;"
                 src="{{URL::to('/assets/img')}}/{{env('UKI_LOGO_NEGATIVO_FILE', 'logo_negativo.png')}}"/>
        </a>
    </div>
    <!-- Navigation -->
    <ul class="accordion ms-main-aside fs-14" id="side-nav-accordion">
    <!-- Menu usuario administrador -->
        @if( auth::user()->tipo != 'operador' && auth::user()->tipo != 'superadmin')
            @component('admin.components.menu.admin')
            @endcomponent
        @endif

        @if( auth::user()->tipo == 'operador')
            @component('admin.components.menu.operador')
            @endcomponent
        @endif

        @if( auth::user()->tipo == 'superadmin')
            @component('admin.components.menu.superadmin')
            @endcomponent
        @endif
    </ul>
    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
    </div>
    <div class="ps__rail-y" style="top: 0px; height: 906px; right: 0px;">
        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 700px;"></div>
    </div>
</aside>
<!-- Main Content -->
<main class="body-content">
    <nav class="navbar ms-navbar">
        <div class="ms-aside-toggler ms-toggler pl-0" data-target="#ms-side-nav" data-toggle="slideLeft"><span
                class="ms-toggler-bar bg-primary"></span>
            <span class="ms-toggler-bar bg-primary"></span>
            <span class="ms-toggler-bar bg-primary"></span>
        </div>
        <div class="logo-sn logo-sm ms-d-block-sm">
            <a class="pl-0 ml-0 text-center" href="{{route('admin')}}">
                <img id="logoNormal" style="max-width: 100px;" src="{{URL::to('/assets/img')}}/logo.png"/>
                <img id="logoLight" style="max-width: 100px;" src="{{URL::to('/assets/img')}}/logo_negativo.png"/>
            </a>
        </div>
        <ul class="ms-nav-list ms-inline mb-0" id="ms-nav-options">
            @if(\Illuminate\Support\Facades\Auth::user()->negocio)
                @if(\Illuminate\Support\Facades\Auth::user()->sucursales->count() == 0)
                    <li class="ms-nav-item dropdown">
                        <h6 style="margin: 0px; padding: 0px;">{{\Illuminate\Support\Facades\Auth::user()->negocio->nombre}}</h6>
                    </li>
                @else
                    <li class="ms-nav-item dropdown">
                        <a href="#" class="text-disabled" id="tiempoDropdown"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <h6 style="margin: 0px; padding: 0px;">{{\Illuminate\Support\Facades\Auth::user()->negocio->nombre}}</h6>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="notificationDropdown">
                            <li class="dropdown-menu-header">
                                <h6 class="dropdown-header ms-inline m-0"><span class="text-disabled">Cambiar de sucursal</span></h6>
                                <!-- <span class="badge badge-pill badge-info">4 New</span> -->
                            </li>
                            <li class="dropdown-divider"></li>
                            <li class="dropdown-item">

                                <div class="media-body">
                                    <form method="POST" action="{{route('admin.negocios.sucursales.selectsucursal', \Illuminate\Support\Facades\Auth::user()->negocio_id)}}" >
                                        @method('POST')
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label for="tiempo_estimado">Selecciona la sucursal</label>
                                            <select type="text" class="form-control" id="pickup" name="sucursal_id">
                                                @foreach(\Illuminate\Support\Facades\Auth::user()->sucursales as $sucursal_base)
                                                    <option value="{{$sucursal_base->id}}">{{$sucursal_base->negocio->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <input type="submit" class="btn btn-primary" value="Cambiar">
                                    </form>
                                </div>
                                <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                                </div>
                                <div class="ps__rail-y" style="top: 0px; right: 0px;">
                                    <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                                </div>
                            </li>
                        </ul>
                    </li>
                @endif
                <li class="ms-nav-item dropdown">
                    <div class="rating-block">
                        @component('food.components.calificacion')
                            @slot('calificacion', \Illuminate\Support\Facades\Auth::user()->negocio->calificacion)
                        @endcomponent
                        <span>{{\Illuminate\Support\Facades\Auth::user()->negocio->calificacion}}</span>
                    </div>
                </li>
                @if(\Illuminate\Support\Facades\Auth::user()->negocio->verificado)
                    @if(\Illuminate\Support\Facades\Auth::user()->negocio->activo)
                        <li class="ms-nav-item dropdown"><a href="#" class="badge badge-success" id="btnDesactivar"
                                                            aria-haspopup="true" aria-expanded="false"> Restaruante
                                activo</a></li>
                    @else
                        <li class="ms-nav-item dropdown"><a id="btnActivar" href="#" class="badge badge-danger"
                                                            aria-haspopup="true" aria-expanded="false"> Restaruante
                                inactivo</a></li>
                    @endif
                @else
                    <li class="ms-nav-item dropdown"><a href="#" class="badge badge-danger" aria-haspopup="true"
                                                        aria-expanded="false"> Restaruante inactivo</a></li>
                @endif
            @endif
            @if(\Illuminate\Support\Facades\Auth::user()->negocio)
            <li class="ms-nav-item dropdown">
                    <a href="#" class="text-disabled" id="tiempoDropdown"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(\Illuminate\Support\Facades\Auth::user()->negocio->config->tiempo_estimado)
                            <span class="badge badge-info">{{\Illuminate\Support\Facades\Auth::user()->negocio->config->tiempo_estimado}} min. aprox.</span>
                        @else
                            <span class="badge badge-info">Tiempo de preparación</span>
                        @endif
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="notificationDropdown">
                        <li class="dropdown-menu-header">
                            <h6 class="dropdown-header ms-inline m-0"><span class="text-disabled">Configurar tiempo aproximado de preparación</span></h6>
                            <!-- <span class="badge badge-pill badge-info">4 New</span> -->
                        </li>
                        <li class="dropdown-divider"></li>
                        <li class="dropdown-item">

                                <div class="media-body">
                                    <form method="POST" action="{{route('admin.negocios.tiempo.update', \Illuminate\Support\Facades\Auth::user()->negocio_id)}}" >
                                        @method('PUT')
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label for="tiempo_estimado">Tiempo aproximado en minutos</label>
                                            <input type="number" min="1" class="form-control @error('tiempo_estimado') is-invalid @enderror" id="tiempo_estimado" name="tiempo_estimado" placeholder="minutos" value="{{\Illuminate\Support\Facades\Auth::user()->negocio->config->tiempo_estimado ?? ''}}">
                                        </div>
                                        <input type="submit" class="btn btn-primary" value="Guardar">
                                        <a href="{{route('admin.negocios.tiempo.delete', \Illuminate\Support\Facades\Auth::user()->negocio_id)}}" class="btn btn-primary text-white">Borrar</a>
                                    </form>
                                </div>
                            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                            </div>
                            <div class="ps__rail-y" style="top: 0px; right: 0px;">
                                <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                            </div>
                        </li>
                    </ul>
                </li>
            @endif
            <!--
            <li class="ms-nav-item dropdown"><a href="#" class="text-disabled" id="dark-mode" aria-haspopup="true"
                                                aria-expanded="false"><i class="flaticon-control"></i></a></li>
                                                -->

            <li class="ms-nav-item dropdown">
                    <a href="#" class="text-disabled" id="notificationDropdown"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="flaticon-bell"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="notificationDropdown">
                        <li class="dropdown-menu-header">
                            <h6 class="dropdown-header ms-inline m-0"><span class="text-disabled">Notificaciones</span></h6>
                            <!-- <span class="badge badge-pill badge-info">4 New</span> -->
                        </li>
                        <li class="dropdown-divider"></li>
                        <li class="ms-scrollable ms-dropdown-list ps">
                            <a class="media p-2" href="#">
                                <div class="media-body">
                                    <p class="alert alert-info"><i class="flaticon-information"></i> No hay
                                        notificaciones...</p>
                                </div>
                            </a>
                            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                            </div>
                            <div class="ps__rail-y" style="top: 0px; right: 0px;">
                                <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                            </div>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li class="dropdown-menu-footer text-center"><a href="#">Ver todas las notificaciones</a>
                        </li>
                    </ul>
                </li>

            <li class="ms-nav-item ms-nav-user dropdown">
                <a href="#" id="userDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(\Illuminate\Support\Facades\Auth::user()->negocio && \Illuminate\Support\Facades\Auth::user()->negocio->logo)
                        <img style="height: 40px;" class="ms-user-img ms-img-round float-right"
                             src="{{route('images.public', [base64_encode('negocios/logos/'), \Illuminate\Support\Facades\Auth::user()->negocio->logo])}}"/>
                    @else
                        <img style="height: 40px;" class="ms-user-img ms-img-round float-right"
                             src="{{URL::to('/assets/img')}}/sin_imagen.jpg"/>
                    @endif
                </a>
                <ul class="dropdown-menu dropdown-menu-right user-dropdown" aria-labelledby="userDropdown">
                    <li class="dropdown-menu-footer">
                        @if( auth::user()->tipo != 'operador')
                            <a class="media fs-14 p-2" href="{{route('admin.perfil.edit')}}">
                                <span><i class="flaticon-user mr-2"></i> Editar cuenta</span>
                            </a>
                        @endif
                        @if(\Illuminate\Support\Facades\Auth::user()->negocio_id)
                            <a class="media fs-14 p-2"
                               href="{{route('admin.negocios.show', \Illuminate\Support\Facades\Auth::user()->negocio_id)}}">
                                <span><i class="flaticon-user mr-2"></i> Info del Negocio</span>
                            </a>
                        @endif
                        <a class="media fs-14 p-2" href="{{URL::to('/logout')}}">
                            <span><i class="flaticon-shut-down mr-2"></i> Cerrar sesión</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="ms-toggler ms-d-block-sm pr-0 ms-nav-toggler" data-toggle="slideDown" data-target="#ms-nav-options">
            <span class="ms-toggler-bar bg-primary"></span>
            <span class="ms-toggler-bar bg-primary"></span>
            <span class="ms-toggler-bar bg-primary"></span>
        </div>
    </nav>
    @if(\Illuminate\Support\Facades\Auth::user()->negocio)
        @if(\Illuminate\Support\Facades\Auth::user()->negocio->activo)
            <form id="frmActivarDesactivar" method="POST"
                  action="{{route('admin.negocios.update', \Illuminate\Support\Facades\Auth::user()->negocio->id)}}">
                {{ csrf_field() }}
                {{method_field('put')}}
                <input type="hidden" name="activo" value="0">
                <input type="hidden" name="return" value="back">
                <input type="hidden" name="validate" value="activo">
            </form>
        @else
            <form id="frmActivarDesactivar" method="POST"
                  action="{{route('admin.negocios.update', \Illuminate\Support\Facades\Auth::user()->negocio->id)}}">
                {{ csrf_field() }}
                {{method_field('put')}}
                <input type="hidden" name="activo" value="1">
                <input type="hidden" name="return" value="back">
                <input type="hidden" name="validate" value="activo">
            </form>
        @endif
    @endif
    <div class="ms-content-wrapper">
        @yield('content')
    </div>
</main>
<!-- SCRIPTS -->
<!-- Costic core JavaScript -->
<script src="{{URL::to('/')}}/assets/js/Chart.bundle.min.js">
</script>

<script src="{{URL::to('/')}}/assets/js/Chart.Financial.js"></script>
<script src="{{URL::to('/')}}/assets/js/d3.v3.min.js">
</script>
<script src="{{URL::to('/')}}/assets/js/topojson.v1.min.js">
</script>

<script src="{{URL::to('/')}}/assets/js/framework.js"></script>
<!-- Settings -->
<script src="{{URL::to('/')}}/assets/js/settings.js"></script>
<script src="{{URL::to('/')}}/assets/js/toastr.min.js"></script>
<script src="{{URL::to('/')}}/assets/js/sweetalert2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.2/main.min.js"></script>
<link href='https://cdn.jsdelivr.net/npm/fullcalendar@5.7.2/main.min.css' rel='stylesheet'/>
<script type="application/javascript">
    const cargarSonido = function (fuente) {
        const sonido = document.createElement("audio");
        sonido.src = fuente;
        sonido.setAttribute("preload", "auto");
        sonido.setAttribute("controls", "none");
        sonido.style.display = "none"; // <-- oculto
        document.body.appendChild(sonido);
        return sonido;
    };

    const sonido = cargarSonido('{{URL::to('/assets/audio/uki1.mp3')}}');

    //sistema de revision de pedidos
    function revisarpedidos() {
        $.get("{{URL::to('api/negocios/'. auth::user()->negocio_id . '/pedidos/entrantes')}}", function (data) {
            if (data.length > 0) { //existen pedidos entrantes
                clearInterval(revisionPedidosInterval);
                if (alertInterval) {
                    clearInterval(alertInterval);
                }
                if (posponerRevisionInterval) {
                    clearInterval(posponerRevisionInterval);
                }
                alertInterval = setInterval(revisarpedidos, 10000);
                sonido.play();
                if (data.length > 1) { //existe m'as de un pedido entrante
                    // alert('muchos pedidos');
                    Swal.fire({
                        title: '<strong>¡Pedidos pendientes!</strong>',
                        imageUrl: '{{URL::to('/')}}/food_assets/images/animaciones/loading-uki.gif',
                        html:
                            '<strong>Atención, </strong> tienes <big>' + data.length + '</big> pedidos pendientes, atiéndelos ahora para no generar calificaciones negativas' +
                            '',
                        showCloseButton: false,
                        showCancelButton: true,
                        focusConfirm: true,
                        confirmButtonText:
                            '<i class="fa fa-play"></i> Revisar pedidos',
                        confirmButtonAriaLabel: 'Iniciar Pedido!',
                        cancelButtonText:
                            '<i class="fa fa-clock"></i> Posponer',
                        cancelButtonAriaLabel: 'Thumbs down'
                    }).then((result) => {
                        console.log(result);
                        if (result.value) {
                            window.location.href = '{{route('admin.pedidos.index')}}?cancel_not=true';
                        } else {
                            clearInterval(alertInterval);
                            posponerRevisionInterval = setInterval(revisarpedidos, 600000);
                        }
                    })

                } else { //existe solo un pedido entrante
                    Swal.fire({
                        title: '<strong>¡Nuevo pedido!</strong>',
                        imageUrl: '{{URL::to('/')}}/food_assets/images/animaciones/loading-uki.gif',
                        html:
                            '<b>Felicidades, acabas de recibir un nuevo pedido</b>, ' +
                            '',
                        showCloseButton: false,
                        showCancelButton: true,
                        focusConfirm: true,
                        confirmButtonText:
                            '<i class="fa fa-play"></i> Iniciar Pedido',
                        confirmButtonAriaLabel: 'Iniciar Pedido!',
                        cancelButtonText:
                            '<i class="fa fa-clock"></i> Posponer',
                        cancelButtonAriaLabel: 'Thumbs down'
                    }).then((result) => {
                        console.log(result);
                        if (result.value) {
                            window.location.href = '{{URL::to("/")}}/admin/pedidos/' + data[0].id + '/actualizar_status/preparando';
                        } else {
                            clearInterval(alertInterval);
                            posponerRevisionInterval = setInterval(revisarpedidos, 600000);
                        }
                    })

                }
            }
        });
    }

    var alertInterval = null;
    var revisionPedidosInterval = null;
    var posponerRevisionInterval = null;


    $(document).ready(function () {
        @if(session('mensajeFlash'))
        toastr.remove();
        toastr.options.positionClass = "toast-bottom-right";
        toastr.{{session('mensajeFlash')['accion']}}('{{session('mensajeFlash')["mensaje"]}}', '{!! session('mensajeFlash')["titulo"] !!}');
        @endif

        $("#btnDesactivar").on('click', function () {
            Swal.fire({
                title: '¿Deseas desactivar el restaurante?',
                text: "No volverás a recibir pedidos hasta que lo actives",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#666',
                confirmButtonText: 'Sí, quiero desactivarlo'
            }).then(function (result) {
                if (result.value) {
                    $("#frmActivarDesactivar").submit();
                }
            });
        });

        $("#btnActivar").on('click', function () {
            Swal.fire({
                title: 'Restaurante Activado',
                text: "A partir de que hagas click en OK volverás a recibir pedidos",
                type: 'success'
            }).then(function (result) {
                $("#frmActivarDesactivar").submit();
            });
        });

        $(".text-counter").after(function(){
            return "<p style='text-align: right'>" +  ($(this).attr('maxlength') - $(this).val().length) + "</p>"
        });


        $(".text-counter").keyup(function(){
            var textarea = $(this);
            counter = textarea.next('p');
            counter.html(
                textarea.attr('maxlength') - textarea.val().length
            );
        });

        revisionPedidosInterval = setInterval(revisarpedidos, 180000);
    });
</script>
</body>

</html>
